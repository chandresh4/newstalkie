<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Memcached {
		
	private $m;
	public function __construct() {
	}
	
	public function mem_connect() {
		/*if (version_compare(PHP_VERSION, '5.4.0') < 0) {
			$this->m = new Memcache;
			$this->m->connect('cachecluster1.4wcvbl.cfg.apse1.cache.amazonaws.com', 11211);
		} else {
			$memcache = new Memcached;
			$this->m->connect('cachecluster1.4wcvbl.cfg.apse1.cache.amazonaws.com', 11211);
		}*/
		$this->m = new Memcache;
		$this->m = memcache_connect('localhost', 11211);
		
	}
	
	public function set_cache ($key, $value, $expire_time = 604800, $compress = true) {
		if (isset($key) && $key != '') {
			
			$this->mem_connect();
			$key = md5(MEMCACHE_KEY).'_'.$key;
			
			return $this->m->set($key, $value, (($compress == true) ? MEMCACHE_COMPRESSED : ''), $expire_time); 
		
		} else {
		
			return 'Key must be alpha numeric empty key is not allowed';
		
		}
	}
	
	public function get_stats() { 
	    $this->mem_connect();
		return $this->m->getExtendedStats();
	}
	
	public function get_version() { 
	    $this->mem_connect();
		return $this->m->getVersion();
	}
	
	public function _flush_cache() { 
	    $this->mem_connect();
		return $this->m->flush();
	}

	public function get_cache($key = '') {
		$this->mem_connect();
		$key = md5(MEMCACHE_KEY).'_'.$key;
			
		return (isset($key) && $key != '') ? $this->m->get($key) : false;	
	}
	
	public function del_cache($key = '') {
		
		if (isset($key) && $key != '') {
			$this->mem_connect();
			$key = md5(MEMCACHE_KEY).'_'.$key;
			
			$this->m->delete($key);
		}
	}

	public function getKeyByPrefix($prefix = '') {
				   
		if ($prefix != '') {
			
			// CONNECT TO CACHE CLUSTER
			 $this->mem_connect();
		
			// SET PREFIX TO DELETE
			$prefix = MEMCACHE_KEY.'_'.$prefix;
			
			// DECLARE EMPTY VAR
			$removable_keys = '';
			
			$list = array();
			
			// GET EXTENDED STATS FROM MEMCACHE
			$allSlabs = $this->m->getExtendedStats('slabs');
			
			$i = 1;
			
			// GET IT ONE BY ONE
			foreach($allSlabs as $server => $slabs) {
				foreach($slabs as $slabId => $slabMeta) {
				   if (!is_numeric($slabId)) {
						continue;
				   }
				   
				   $cdump = $this->m->getExtendedStats('cachedump',(int)$slabId);
				   
					foreach($cdump as $keys => $arrVal) {
						
						if (!is_array($arrVal)) continue;
						
						foreach($arrVal as $k => $v) {                   
						
							// IF $K MEANS KEY IS MATCHING WITH OUR REQUESTED PREFIX THEN KEEP IN ARRAY ELSE IGNORE
							if (strpos($k, $prefix) === 0) {
								$arr_k = explode(MEMCACHE_KEY, $k);
								$removable_keys[$arr_k[1]] = $v;
							}
						}
				   }
				   $i++;
				}
			}
			
			return (isset($removable_keys) && count($removable_keys) > 0) ? $removable_keys : false;
		}
		return false;
	}
	
}