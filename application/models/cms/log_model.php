<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class log_model extends CI_Model
{  
    public function __construct()
    {
        parent::__construct();
    }

	
	function login($email,$password){
		
        if(strlen($email) > 0 && strlen($password) > 0){
			// OFF THE CACHE FOR THIS QUERY
			$this->db->cache_off();
			
			$query = $this->db->query("select 
											u.id,
									   		u.adm_role_id as adm_role_id,
											u.username, u.admin_image,
											u.email,
											u.status as user_status,
											r.role_name,
											r.role_details,
											r.status as role_status
									   from 
									   		".ADMIN_USERS." u inner join ".ADMIN_ROLE." r on (u.adm_role_id = r.id) 
									   where 
									   		u.email    = '".$email."' and 
											u.passwd   = '".trim($password)."'");
											
				
			if($query->num_rows() > 0){
				
				$row = $query->row();
				 
				$this->session->set_userdata('username', $row->username);
				$this->session->set_userdata('uid', $row->id);			
				$this->session->set_userdata('admin_role',$row->role_name);
				$this->session->set_userdata('admin_role_id',$row->adm_role_id);
				$this->session->set_userdata('admin_role_details',$row->role_details);
				$this->session->set_userdata('admin_pic',$row->admin_image);				
							
				
				$_SESSION['login_id'] 	  = $row->id;
				$_SESSION['login_email']  = $row->email;
				
				return true;
			
						
				
			}else{
					
				return false;
			}
		}else{
			return false;
			}
    }
	
	
	function direct_login(){
		
        //if(strlen($email) > 0 && strlen($password) > 0){
			// OFF THE CACHE FOR THIS QUERY
			$this->db->cache_off();
			
			$query = $this->db->query("select 
											u.id,
									   		u.adm_role_id as adm_role_id,
											u.username, u.admin_image,
											u.email,
											u.status as user_status,
											r.role_name,
											r.role_details,
											r.status as role_status
									   from 
									   		".ADMIN_USERS." u inner join ".ADMIN_ROLE." r on (u.adm_role_id = r.id) 
									   where 
									   		u.email    = 'superadmin'");
											
				
			if($query->num_rows() > 0){
				
				$row = $query->row();
				 
				$this->session->set_userdata('username', $row->username);
				$this->session->set_userdata('uid', $row->id);			
				$this->session->set_userdata('admin_role',$row->role_name);
				$this->session->set_userdata('admin_role_id',$row->adm_role_id);
				$this->session->set_userdata('admin_role_details',$row->role_details);
				$this->session->set_userdata('admin_pic',$row->admin_image);				
							
				
				$_SESSION['login_id'] 	  = $row->id;
				$_SESSION['login_email']  = $row->email;
				
				return true;
			
						
				
			}else{
					
				return false;
			}
		/*
		}else{
			return false;
		}*/
    }
	
	function reset_password($email){
		
        if(strlen($email) > 0 ){
			// OFF THE CACHE FOR THIS QUERY
			$this->db->cache_off();
			$query = $this->db->query("select id, email from ".ADMIN_USERS." where email = '".$email."' and status = 1 LIMIT 1");											
			
			$new_pass = get_random_chracter(8, 12, true, true, false);
				
			if($query->num_rows() > 0){
				$row = $query->row();
				if($row->id > 0){
					
					$sql = "Update ".ADMIN_USERS." set passwd = ? where status = ? and id= ?";
					$this->db->query($sql, array(md5($new_pass), 1, $row->id));
					
					$this->email_templates->admin_pass_reset($row->email, $new_pass);
					
					return $new_pass;
				}
				
			}else{
				return false;
			}
		}else{
			return false;
			}
    }

}
?>