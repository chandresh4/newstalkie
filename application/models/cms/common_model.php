<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class common_model extends CI_Model
{
	protected $data;
    public function __construct()
    {
        parent::__construct();
    }
	
	public function Menu_Array(){ 
		
		$menu1  	= array(
							 array("Category", "manage_category", "manage_category",  1, " fa-bar-chart-o"),
							 array("Posts", "manage_posts", "manage_posts",  2, " fa-bar-chart-o"),
                             array("Writers Posts", "manage_posts/writer_posts", "manage_posts/writer_posts",  3, " fa-bar-chart-o"),
                             array("Tags", "manage_tags", "manage_tags",  4, " fa-bar-chart-o"),
							 array("Memes", "manage_meme", "manage_meme",  5, " fa-bar-chart-o"),
                             array("Movies", "manage_movie", "manage_movie",  6, " fa-bar-chart-o"),


							);
		 
		$user  			= array(
					    	array("Subscribe User", "manage_subscribe", "manage_subscribe",  1, "fa-code-fork"),
							array("Content Writer", "manage_user", "manage_user",  2, "fa-code-fork"),

						);	
	    $page_data  	= array(
							 array("Static Content", "manage_staticpage", "manage_staticpage",  5, " fa-bar-chart-o"),
							 

						);	
		$setting  			= array(
		                     array("Change Password", "changepassword", "changepassword",  1, " fa-bar-chart-o"),

						);				
					
		
		$cms_access    =  array(
							array("Manage user", "manage_admin", "manage_admin",  1, "fa fa-group"),
							array("Manage role", "manage_role", "manage_role", 2, "fa-magic"),
						);			
		
		$networks  		= array(
					    	array("Manage UTM Source", "manage_utm_source", "manage_utm_source",  1, "fa-code-fork"),
							array("Manage UTM Medium", "manage_utm_medium", "manage_utm_medium",  2, "fa-code-fork"),
                            array("Manage UTM Sub", "manage_utm_sub", "manage_utm_sub",  4, "fa-code-fork"),

						);						
		 /* $upload  			= array(
					    	array("Upload content", "manage_uploads", "manage_uploads",  1, "fa-code-fork"),

						);	 */			
		$menu      		= array(
								array("Menu",	"menu",	$menu1 ,1,'fa-cogs'),
								array("User", "user",$user , 2, 'fa-cogs'),	
								array("Manage Page", "Manage page",$page_data , 3, 'fa-cogs'),	
								array("Manage UTM ", "Manage UTM",$networks , 4, 'fa-cogs'),
								//array("Manage Uploads ", "Manage Uploads",$upload   , 5, 'fa-cogs'),
								array("Setting", "setting",$setting , 6, 'fa-cogs'),	
								array("Admin control","Admin",$cms_access, 7,'fa-cogs'),	
											
								);
		
		return $menu;
	}
	
	
	
	 

}

?>