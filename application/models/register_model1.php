<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class register_model extends CI_Model
{  

	protected $data;
    public function __construct(){
		///
        parent::__construct(); 
		$this->load->library('Mobile_Detect');
		//$this->load->model('cached');
		//$this->load->model('knowastro_reminder_mailer');
		$this->load->model('locallaunde_mailer');
		$this->load->helper("general","general_helper");
    }
	
	public function register($register_arr =''){
		
				 
		if(is_array($register_arr) && count($register_arr) > 0){
          $query= $this->db->query("select post_title from tbl_post where post_title= '".$register_arr['ptitle']."'");
          if($query->num_rows()>0){
                  return  "duplicate";
          }else{
		   $optin_token = get_random_chracter(8, 15, true, true, false);
		   $register    = array(
							 "name"        => trim($register_arr['name']),
							 "email"       => trim($register_arr['email']),
							 "is_optin"    => 0,
							 "email_token" => $optin_token,
							  "status"     => 1,
							 "date_created"=> date('Y-m-d H:i:s')
		   );
		  
		   $this->db->insert(CONTENTS,$register);
		    $c_id = $this->db->insert_id();
		  $post_name =$register_arr['ptitle'];
		  $parts = explode(' ', $post_name);
		  $url="";
		  
		  for($i=0;$i<count($parts);$i++){
			  $url = $parts[$i] ."-";
		  }
		 
		  $post_Arr=array();
          $post_Arr['id']        = "";
		  $post_Arr['category']   		= $register_arr['category'];
		  $post_Arr['post_title']     	= $register_arr['ptitle'];	
		  $post_Arr['post_sub_title']   = "";	
		  $post_Arr['seourl']     	    = $url;	 
		  $post_Arr['post_desciption']  = $register_arr['comment'];		 
		  $post_Arr['status']   		= 0;
		  $post_Arr['viral_status']   	= 1;
		  $post_Arr['trendy_status']   	= 0;
		  $post_Arr['writer_id']= $c_id;
		  $path = $_FILES['browse']['name'];   
		  $file_extention = pathinfo($path, PATHINFO_EXTENSION);
		  $allowed_types = array('jpg', 'gif', 'png' , 'bmp' , 'jpeg');  
		  $post_img                = $_FILES['browse']['name'];
		  $post_Arr['post_image']    = $post_img;
		  $post_Arr['date_created']  = date('Y-m-d H:i:s');
		  //if(in_array($file_extention, $allowed_types)){
		   $this->db->insert(POSTS,$post_Arr);
		   
		   $pathAndName = POSTS_FOLDER.$post_img;
		   $croppathAndName =LARGE_POSTS_FOLDER.THUMB_PREFIX.$post_img;
		   $croppathAndName1 =MEDIUM_POSTS_FOLDER.THUMB_PREFIX.$post_img;
		   $croppathAndName2 =SMALL_POSTS_FOLDER.THUMB_PREFIX.$post_img;
		   $croppathAndName3 =CROP_POSTS_FOLDER.THUMB_PREFIX.$post_img;
		   move_uploaded_file($_FILES['browse']['tmp_name'],$pathAndName);
		   make_img_thumb("./".$pathAndName, "./".$croppathAndName,756);
		   make_img_thumb("./".$pathAndName, "./".$croppathAndName1,378);
		   make_img_thumb("./".$pathAndName, "./".$croppathAndName2,303);
		   make_img_thumb("./".$pathAndName, "./".$croppathAndName3,850);
		  
		   $this->locallaunde_mailer->email_signup($register_arr['email'] , $register_arr['name'], $optin_token);

		   return true;
		   
		 }  
		}else{
		    return false;	
		}
	
	
}	
	
	
   public function update($register_arr =''){
	    
		if(is_array($register_arr) && count($register_arr) > 0){
		   
		 $query = $this->db->query("select id,email from tbl_contentwriter where email='".$register_arr['email']."' and is_optin = 1") ;
         	if ( $query->num_rows() > 0) {
			  foreach($query->result() as $p){
                 $id=$p->id;
			  }				 
			  $register    = array(
								 "name"        => trim($register_arr['name']),
								 "email"       => trim($register_arr['email']),
								 "date_created"=> date('Y-m-d H:i:s')
			   );
			  $this->db->where('id', $id);
		      $this->db->update(CONTENTS, $register);
              $post_name =$register_arr['ptitle'];
		      $parts = explode(' ', $post_name);
		       $url="";
		  
		     for($i=0;$i<count($parts);$i++){
			  $url = $parts[$i] ."-";
		  }
		      $query= $this->db->query("select post_title from tbl_post where post_title= '".$register_arr['ptitle']."'");
              if($query->num_rows()>0){
                  return  "duplicate";
               }else{		     
				  $post_Arr=array();
				  $post_Arr['id']        = "";
				  $post_Arr['category']   		= $register_arr['category'];
				  $post_Arr['post_title']     	= $register_arr['ptitle'];	
				  $post_Arr['post_sub_title']   = "";	
				  $post_Arr['seourl']     	    = $url;	 
				  $post_Arr['post_desciption']  = $register_arr['comment'];		 
				  $post_Arr['status']   		= 0;
				  $post_Arr['viral_status']   	= 1;
				  $post_Arr['trendy_status']   	= 0;
				  $post_Arr['writer_id']= $id;
				  $path = $_FILES['browse']['name'];   
				  $file_extention = pathinfo($path, PATHINFO_EXTENSION);
				  $allowed_types = array('jpg', 'gif', 'png' , 'bmp' , 'jpeg');  
				  $post_img                = $_FILES['browse']['name'];
				  $post_Arr['post_image']    = $post_img;
				  $post_Arr['date_created']  = date('Y-m-d H:i:s');
				  //if(in_array($file_extention, $allowed_types)){
				   $this->db->insert(POSTS,$post_Arr);
				   
				   $pathAndName = POSTS_FOLDER.$post_img;
				   $croppathAndName =LARGE_POSTS_FOLDER.THUMB_PREFIX.$post_img;
				   $croppathAndName1 =MEDIUM_POSTS_FOLDER.THUMB_PREFIX.$post_img;
				   $croppathAndName2 =SMALL_POSTS_FOLDER.THUMB_PREFIX.$post_img;
				   $croppathAndName3 =CROP_POSTS_FOLDER.THUMB_PREFIX.$post_img;
				   move_uploaded_file($_FILES['browse']['tmp_name'],$pathAndName);
				   make_img_thumb("./".$pathAndName, "./".$croppathAndName,756);
				   make_img_thumb("./".$pathAndName, "./".$croppathAndName1,378);
				   make_img_thumb("./".$pathAndName, "./".$croppathAndName2,303);
				   make_img_thumb("./".$pathAndName, "./".$croppathAndName3,850);
				   return true;
             }			   
			}
          else{
			  return "error";
		  }			  
		  
	}
	
		}
	
}
?>