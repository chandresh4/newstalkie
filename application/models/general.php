<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class general extends CI_Model
{
	
    public function __construct()
    {
        parent::__construct();
    } 
	
	public function checkNetworkPubId($source = "", $affid = "", $medium = "", $sub = "") {
	  
	 	if (trim($source) != "") {
			$this->session->set_userdata('utm_source', $source); 
		}
		
		if (trim($affid) != "") {
			$this->session->set_userdata('affid',$affid);  
		}
			  
		if (trim($medium) != "") {
			$this->session->set_userdata('utm_medium', $medium); 
		}
		if (trim($sub) != "") {
			$this->session->set_userdata('utm_sub', $sub); 
		}
		
		
		
	  	return false;
	}
		
	public function email_verify($email_token = ""){
	      
	   if($email_token != ""){
		  
		  $this->db->where("email_valid_token", $email_token);
		   
		  $user_verify=array();
		  $user_verify['optin'] = 1;
		   
		  $this->db->update(USER,$user_verify);   
	   }
	   return true;
	}
	
	
	public function get_if_exists ($tablename="",$column="", $field="", $value="", $cache_off=true, $echo=false) {
		
		if($cache_off == true) {
			$this->db->cache_off();
		}
			
		$query = $this->db->query("select ".$column." from ".$tablename." where ".$field." = '".$value."' LIMIT 1");
		
		if($echo == true)
			echo $this->db->last_query();
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $p){
				$data[] =$p;
			}
			return $data;
		}else{
			return true;
		}
	}
	
	public function is_duplicate_add ($tablename="", $field="", $value="", $cache_off=true, $echo=false) {
		
		if($cache_off == true) {
			$this->db->cache_off();
		}
		$s="select ".$field." from ".$tablename." where ".$field." = '".$value."'";
		
		//print_r($s);die();
		$query = $this->db->query("select ".$field." from ".$tablename." where ".$field." = '".$value."'");
		
		if($echo == true)
			echo $this->db->last_query();
		
		if ($query->num_rows() > 0) {
			return false;//duplicate
		}else{
			return true;
		}
	}
	
	
	public function checkoptin($whr=""){		 
		if(isset($whr) && $whr!=""){  
		$query= $this->db->query("SELECT id, email,is_optin FROM ".USER." ".$whr); 
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $p) { 
					if($p->is_optin == 1){ return "optin"; }else{ return "non_optin"; } 
				}
			}else{ return "new"; }
		}
	}
	
	public function get_user_mail_details($email=""){	
		$arr=array();	
		if(isset($email) && $email!=""){  
			$query= $this->db->query("SELECT id, email,is_optin,name,user_type,optin_token FROM ".USER." where email='".$email."'"); 
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $k=>$v) { 
					$arr=$v;
					return $arr;
				}
			}
		}else{return 'no_email';}
	}
	
}
?>