<?php
class locallaunde_mailer extends CI_MODEL
{
	protected $setting_phone_no;
	protected $setting_admin_name;
	protected $setting_site_name;
	protected $all_array;
	
	function __construct(){
		parent::__construct();	
		$config = array('mailtype' => "html");
		$this->load->library('smtp');
		$this->load->library('phpmailer');
		
	}
			
	function mail_send($from_email="", $from_name="", $to="", $subject="", $message="") {
		
		global $dm_name;
		
		$mail = new PHPMailer();		
		$mail->Host = "smtp1-1.oyeby.com"; // Connect to this GreenArrow server
		$mail->SMTPAuth = true; // enables SMTP authentication. Set to false for IP-based authentication
		$mail->Port = 587; // SMTP submission port to inject mail into. Usually port 587 or 25
		$mail->Username = "launde@oyeby.com"; // SMTP username
		$mail->Password = "ljh&Rv!s3bkjl"; // SMTP password
		//$mail->SMTPDebug = 2; // uncomment to print debugging info
		
		// Timezone
		date_default_timezone_set('America/Chicago');
		
		// Campaign Settings
		$mail_class = "launde"; // Mail Class to use
		
		// Create the SMTP session
		$mail->IsSMTP(); // Use SMTP
		$mail->SMTPKeepAlive = true; // prevent the SMTP session from being closed after each message
		$mail->SmtpConnect();
		
		// Set headers that are constant for every message outside of the foreach loop
		$mail->SetFrom("info@locallaunde.in", $from_name);
		$mail->Subject = $subject;
		$mail->addCustomHeader("X-GreenArrow-MailClass: $mail_class");
	

		// Generate headers that are unique for each message
		$mail->ClearAllRecipients();
		$mail->AddAddress($to);
	
		// Generate the message
		$mail->MsgHTML($message);
		
		if($mail->Send()) {
			return true;
		} else {
			return false;
		}

		// Close the SMTP session
		$mail->SmtpClose();
		
		return true;
			

		
	}
			
    
	public function email_signup($user_email = "" , $user_name="", $optin_token= "") {
   
		if($user_name != "" && $user_email != "" ) {
			$md5_optin_token = md5($optin_token);
			// GET THE DETAILS OF USER
			$from_email = SUPPORT_EMAIL;
			$from_name  = SITE_NAME;
			$to         = $user_email;			
			$subject = "Welcome to ".$from_name;
			
			$message = "<!doctype html>
    <html>
    <head>
    <meta charset='utf-8'>
    <title>Untitled Document</title>
    <style>
     / Basic Fluid Image Pattern CSS /
     @media only screen and (max-width: 599px) {
      img.pattern {
       max-width: 100%;
       height: auto !important;
      }
     }
    </style>
    </head>

    <body bgcolor='#f2f2f2'>

    <table cellspacing='0' width='100%' cellpadding='0' border='0' align='center' style='max-width:499px; font-family:arial,helvetica,sans-serif; background-color:#fff;' >
      <tbody>
      <tr bgcolor='#ffd800'>
     <td  height='10px;'></td>
      </tr>
      <tr>
     <td bgcolor='#ffd800' align='center'>
       <a href='".SITE_URL."thankyou/index/".$optin_token."' target='_blank'><img src='".S3_URL."site/images/logo.png' alt='' /></a>
       </td>
      </tr>
      <tr>
     <td  height='15px;' bgcolor='#ffd800'></td>
      </tr>
       <tr>
       <td>
       <a href='".SITE_URL."thankyou/index/".$optin_token."' target='_blank'><img class='pattern' src='".S3_URL."site/images/banner.jpg' alt='' /></a>
       </td>
     </tr>
      
     <tr>
     <td>
     <table cellspacing='0' width='100%' cellpadding='0' border='0' align='center' style='max-width:450px; padding: 10px;font-family:arial,helvetica,sans-serif; background-color:#fff; ' >
      <tbody>

     <tr>
     <td  height='30px;'></td>
      </tr>
     <tr>
     <td valign='top' style='text-align:center;'>
       <a href='".SITE_URL."thankyou/index/".$optin_token."' style=' font-size: 17px; color: #535353; text-decoration: none; font-weight:600; line-height:1.4' target='_blank' >
    Do you have a passion for writing and voicing out your opinions? </a>
     </td>
      </tr> 
     <tr>
     <td  height='30px;'></td>
      </tr>
     <tr>
     <td valign='top' style='text-align:center;'>
       <a href='".SITE_URL."thankyou/index/".$optin_token."' style=' font-size: 17px; color: #535353; text-decoration: none; font-weight:400; line-height:1.4' target='_blank' >
    Welcome! We're excited to have you in our family. First, we need you to confirm your account. Just go to your registered email and validate your account so that we can publish your content.  </a>
     </td>
      </tr> 
      <tr>
     <td  height='55px;'></td>
      </tr>
    <tr>
       <td valign='top' bgcolor='#fff' align='center'>
       <table style='background-color:#ffd800; border-radius:0px' cellspacing='0' cellpadding='0' border='0'>
     <tbody>
      <tr>
       <td style='color:#f0f1f5;font-family:Arial,Verdana,Sans-Serif;font-size:18px;font-weight:bold;line-height:150%;'>
       <div style='vertical-align:middle' align='center'><a href='".SITE_URL."thankyou/index/".$optin_token."' style='color:#000;text-transform:none;min-width:110px;text-decoration:none;vertical-align:middle;padding:10px 20px;display:block' target='_blank'>VALIDATE</a></div>
       </td>
      </tr>
     </tbody>
    </table>
    </td>
     </tr> 
      <tr>
     <td  height='50px;'></td>
      </tr>
     <tr>
     <td valign='top' style='text-align:center;'>
       <a href='".SITE_URL."category/search/science' style=' font-size: 15px; color: #535353; text-decoration: none; font-weight:400; line-height:1.4' target='_blank' >
    Science & Technology </a>| <a href='".SITE_URL."category/search/Lifestyle' style=' font-size: 15px; color: #535353; text-decoration: none; font-weight:400; line-height:1.4' target='_blank' >Lifestyle</a> | <a href='".SITE_URL."category/search/travel' style=' font-size: 15px; color: #535353; text-decoration: none; font-weight:400; line-height:1.4' target='_blank' >Travel & Adventure</a><br />
    <a href='".SITE_URL."category/search/Animals' style=' font-size: 15px; color: #535353; text-decoration: none; font-weight:400; line-height:1.4' target='_blank' >Animals</a> | <a href='".SITE_URL."category/search/history' style=' font-size: 15px; color: #535353; text-decoration: none; font-weight:400; line-height:1.4' target='_blank' >History & Culture</a> | <a href='".SITE_URL."category/search/Health' style=' font-size: 15px; color: #535353; text-decoration: none; font-weight:400; line-height:1.4' target='_blank' >Health & Fitness</a> | <a href='".SITE_URL."category/search/Inspiration' style=' font-size: 15px; color: #535353; text-decoration: none; font-weight:400; line-height:1.4' target='_blank' >Inspiration</a>
     </td>
      </tr>
    <tr>
     <td  height='25px;'></td>
      </tr>

      </tbody></table>
     </td>
      </tr>
    </tbody></table>


    </body>
    </html>

                       ";
					
			if($this->mail_send($from_email, $from_name, $to, $subject, $message)) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	
	
	
	
    public function subscribe($user_email = "" , $user_name="", $optin_token= "") {
   
		if($user_name != "" && $user_email != "" ) {
			$md5_optin_token = md5($optin_token);
			// GET THE DETAILS OF USER
			$from_email = SUPPORT_EMAIL;
			$from_name  = SITE_NAME;
			$to         = $user_email;			
			$subject    = "Verify to hear first from Locallaunde";
	   	   
		   
		$message	="<!doctype html>
    <html>
    <head>
    <meta charset='utf-8'>
    <title>Untitled Document</title>
    <style>
     / Basic Fluid Image Pattern CSS /
     @media only screen and (max-width: 599px) {
      img.pattern {
       max-width: 100%;
       height: auto !important;
      }
     }
    </style>
    </head>

    <body bgcolor='#f2f2f2'>

    <table cellspacing='0' width='100%' cellpadding='0' border='0' align='center' style='max-width:499px; font-family:arial,helvetica,sans-serif; background-color:#fff;' >
      <tbody>
      <tr bgcolor='#ffd800'>
     <td  height='10px;'></td>
      </tr>
      <tr>
     <td bgcolor='#ffd800' align='center'>
       <a href='".SITE_URL."thankyou/subscribe/".$optin_token."' target='_blank'><img src='".S3_URL."site/images/logo.png' alt='' /></a>
       </td>
      </tr>
      <tr>
     <td  height='15px;' bgcolor='#ffd800'></td>
      </tr>
       <tr>
       <td>
       <a href='".SITE_URL."thankyou/subscribe/".$optin_token."' target='_blank'><img class='pattern' src='".S3_URL."site/images/banner.jpg' alt='' /></a>
       </td>
     </tr>
      
     <tr>
     <td>
     <table cellspacing='0' width='100%' cellpadding='0' border='0' align='center' style='max-width:450px; padding: 10px;font-family:arial,helvetica,sans-serif; background-color:#fff; ' >
      <tbody>

     <tr>
     <td  height='30px;'></td>
      </tr>
     <tr>
     <td valign='top' style='text-align:center;'>
       <a href='".SITE_URL."thankyou/subscribe/".$optin_token."' style=' font-size: 17px; color: #535353; text-decoration: none; font-weight:600; line-height:1.4' target='_blank' >
    Thank You for Subscribing! </a>
     </td>
      </tr> 
     <tr>
     <td  height='30px;'></td>
      </tr>
     <tr>
     <td valign='top' style='text-align:center;'>
       <a href='".SITE_URL."thankyou/subscribe/".$optin_token."' style=' font-size: 17px; color: #535353; text-decoration: none; font-weight:400; line-height:1.4' target='_blank' >
   Stay in the loop peeps! Get you daily dosage of news, jokes, views and updates and special offers delivered directly to your inbox. Please validatye your email address so you won't miss the fun  </a>
     </td>
      </tr> 
      <tr>
     <td  height='55px;'></td>
      </tr>
    <tr>
       <td valign='top' bgcolor='#fff' align='center'>
       <table style='background-color:#ffd800; border-radius:0px' cellspacing='0' cellpadding='0' border='0'>
     <tbody>
      <tr>
       <td style='color:#f0f1f5;font-family:Arial,Verdana,Sans-Serif;font-size:18px;font-weight:bold;line-height:150%;'>
       <div style='vertical-align:middle' align='center'><a href='".SITE_URL."thankyou/subscribe/".$optin_token."' style='color:#000;text-transform:none;min-width:110px;text-decoration:none;vertical-align:middle;padding:10px 20px;display:block' target='_blank'>VALIDATE</a></div>
       </td>
      </tr>
     </tbody>
    </table>
    </td>
     </tr> 
      <tr>
     <td  height='50px;'></td>
      </tr>
     <tr>
     <td valign='top' style='text-align:center;'>
       <a href='".SITE_URL."category/search/science' style=' font-size: 15px; color: #535353; text-decoration: none; font-weight:400; line-height:1.4' target='_blank' >
    Science & Technology </a>| <a href='".SITE_URL."category/search/Lifestyle' style=' font-size: 15px; color: #535353; text-decoration: none; font-weight:400; line-height:1.4' target='_blank' >Lifestyle</a> | <a href='".SITE_URL."category/search/travel' style=' font-size: 15px; color: #535353; text-decoration: none; font-weight:400; line-height:1.4' target='_blank' >Travel & Adventure</a><br />
    <a href='".SITE_URL."category/search/Animals' style=' font-size: 15px; color: #535353; text-decoration: none; font-weight:400; line-height:1.4' target='_blank' >Animals</a> | <a href='".SITE_URL."category/search/history' style=' font-size: 15px; color: #535353; text-decoration: none; font-weight:400; line-height:1.4' target='_blank' >History & Culture</a> | <a href='".SITE_URL."category/search/Health' style=' font-size: 15px; color: #535353; text-decoration: none; font-weight:400; line-height:1.4' target='_blank' >Health & Fitness</a> | <a href='".SITE_URL."category/search/Inspiration' style=' font-size: 15px; color: #535353; text-decoration: none; font-weight:400; line-height:1.4' target='_blank' >Inspiration</a>
     </td>
      </tr>
    <tr>
     <td  height='25px;'></td>
      </tr>

      </tbody></table>
     </td>
      </tr>
    </tbody></table>


    </body>
    </html>";
	
	   
	 if($this->mail_send($from_email, $from_name, $to, $subject, $message)) {
				return true;
			} else {
				return false;
			}
		
        } else {
			return false;
		}
	
	
   }
   	
   
   
   
  
	
	
}
?>