<?php 
$post_details = $details; 
?>
            <div class="container-fluid">

                <div class="col-md-9 paddingZero">

                    <section class="detailsContentSec headerMargin">

                        <div class="row">

                            <div class="col-md-12 mobilePaddingZero">

                                <div class="col-md-9 mobilePaddingZero">

                                    <div class="newsDetailsSection">

                                        <div class="detailsImageSec">
                                            <img src="<?php echo S3_URL?>site/images/posts/postimage_crop/<?php $img ="thumb-".$post_details->post_image;  echo $img ?>" class="img-responsive" alt="<?php echo ($post_details->alt_text) != '' ? $post_details->alt_text : $post_details->post_title ?>" alt="" style="width: 100%;">
                                        </div>

                                        <br>

                                        <div class="brandImg">
                                            <img src="<?php echo S3_URL?>site/images/brand-logo.png" alt="Brand Name" height="20" class="img-responsive">
                                        </div>

                                        <h1 class="boldFont font30"><?php echo stripcslashes($post_details->post_title) ?></h1>
                                        
                                        <p><?php echo stripcslashes($post_details->post_sub_title) ?></p>

                                        <p><span class="dateTimePlace"><?php echo $newTime?> </span></p>

                                        
                                        <br>

                                        <p class="allDesc" id ="paragragh"><?php echo $post_details->post_desciption;?></p>
                                        
                                        <div class="webView">
                                            <!-- BEGIN ADVANCED JS TAG - 690x250 Newtalkie_690X250_Underarticle - DO NOT MODIFY -->
                                            <div class='ADK_BANNER' data-zoneid='130372' data-subid='' data-width='690' data-height='250' data-net='cpm.rtbwire.com' data-opts='vw doci' ></div>
                                            <SCRIPT class="ADK_SCRIPT" TYPE="text/javascript" SRC="//static.rtbwire.com/tag/display.js"></SCRIPT>
                                            <!-- END TAG -->
                                        </div>

                                        <div class="mobView">
                                            <!-- BEGIN ADVANCED JS TAG - 492x277 Newstalkie_492X277_underarticle_Mobile - DO NOT MODIFY -->
                                            <div class='ADK_BANNER' data-zoneid='130560' data-subid='' data-width='492' data-height='277' data-net='cpm.rtbwire.com' data-opts='vw doci' ></div>
                                            <SCRIPT class="ADK_SCRIPT" TYPE="text/javascript" SRC="//static.rtbwire.com/tag/display.js"></SCRIPT>
                                            <!-- END TAG -->
                                        </div>

                                        <br>
                                        
                                        <!-- Composite Start -->
                                        <div id="M639897ScriptRootC1009677">
                                        </div>
                                        <script src="https://jsc.mgid.com/n/e/newstalkie.com.1009677.js" async></script>
                                        <!-- Composite End -->


                                    </div>

                                </div>

                                <div class="col-md-3 paddingZero">

                                    <div class="descRightAdSpace marginTop15">
                                        <!-- BEGIN ADVANCED JS TAG - 240x396 Newstalkie_In article_240X396 - DO NOT MODIFY -->
                                            <div class='ADK_BANNER' data-zoneid='130370' data-subid='' data-width='240' data-height='396' data-net='cpm.rtbwire.com' data-opts='vw doci' ></div>
                                            <SCRIPT class="ADK_SCRIPT" TYPE="text/javascript" SRC="//static.rtbwire.com/tag/display.js"></SCRIPT>
                                        <!-- END TAG -->
                                    </div>

                                    <h5 class="semiBold headingH5">
                                        Top Stories
                                    </h5>
                                    <?php 
                                    $i = 0;
                                    if(is_array($result1) && count($result1) > 0){ 
                                        foreach($result1 as $r){
                                            if($i < 3 ){
                                    ?>

                                            <div class="col-md-12 paddingZero">

                                                <div class="topStoriesImgSec">
                                                    <a href="<?php echo SITE_URL ?>category/story/<?php echo $r->id ?>/<?php echo $r->seourl ?>">
                                                        <img src="<?php echo S3_URL?>site/images/posts/small_postimage_crop/<?php $img ="thumb-".$r->post_image;  echo $img ?>" alt="">
                                                    </a>
                                                </div>

                                                <div class="topStoriesContentSec">

                                                    <h5 class="semiBold headingH5">
                                                        <?php echo substr($r->post_title,0,40)?>..
                                                    </h5>

                                                </div>

                                            </div>

                                        <?php  }
                                        $i++;
                                        } 
                                    } 
                                    ?>


                                    <div class="clearfix"></div>

                                    <!--<div class="newsLetterSection">

                                        <h4 class="semiBold whiteTxt marginBottom15">YOUR DAILY NEWSLETTER</h4>

                                        <form class="form-inline">

                                            <div class="form-group">
                                            <input type="email" class="form-control" id="email" placeholder="Enter email">
                                            </div>

                                            <button type="submit" class="btn btn-default">Submit</button>

                                        <label class="error hidden">Enter Valid Email ID</label>

                                        </form>

                                    </div>-->

                                    <div class="clearfix"></div>                                            
                                    
                                                
                                    <!-- Composite Start -->
                                    <div id="M639897ScriptRootC1009686"></div>
                                    <script src="https://jsc.mgid.com/n/e/newstalkie.com.1009686.js" async></script>
                                    <!-- Composite End -->  


                                </div>

                            </div>

                        </div>

                    </section>

                    <!--<section class="partnerAdBottomSec">

                        <div class="row">

                            <h3 class="boldFont"> <span class="sponsredBadge"> Ad </span> &nbsp; <span> From Our Partners </span> </h3>

                            <div class="parentAdHolder">

                                <div class="childrenAds">
                                    <div class="smallNewsSecondImageLeftSection">
                                        <img src="<?php echo S3_URL?>site/images/dummy-ads/footerAd1.png" alt="">
                                    </div>
                                </div>
    
                                <div class="childrenAds">
                                    <div class="smallNewsSecondImageLeftSection">
                                        <img src="<?php echo S3_URL?>site/images/dummy-ads/footerAd2.png" alt="">
                                    </div>
                                </div>
    
                                <div class="childrenAds">
                                    <div class="smallNewsSecondImageLeftSection">
                                        <img src="<?php echo S3_URL?>site/images/dummy-ads/footerAd3.png" alt="">
                                    </div>
                                </div>
    
                                <div class="childrenAds">
                                    <div class="smallNewsSecondImageLeftSection">
                                        <img src="<?php echo S3_URL?>site/images/dummy-ads/footerAd1.png" alt="">
                                    </div>
                                </div>

                            </div>

                        </div>

                    </section>-->

                </div>

                <div class="col-md-3 paddingZero">

                    <div class="partnerAdSec advHeaderMargin">
                        <?php 

                        $country_code = (isset($_COOKIE["countrytest"]) != "" ? $_COOKIE["countrytest"] : "" ); 
                       
                        if($country_code == "gb"){ ?>
            
                        <div class="col-md-12 videoSec">

                            <a href="http://adcanopus.offerstrack.net/index.php?offer_id=63395&aff_id=2159" target="_blank">                            
                                <video width="300" height="170" id="myVideo" controls muted loop autoplay>
                                    <source src="<?php echo S3_URL?>site/video/local.mp4" type="video/mp4">
                                    <source src="movie.ogg" type="video/ogg">
                                    Your browser does not support the video tag.
                                </video>
                            </a>

                        </div>

                        <div class="clearfix"></div>
                        <br>
                        
                        <?php } ?>

                        <div class="col-md-12">
                            
                            <?php 
                            $i = 0;
                            if(is_array($result1) && count($result1) > 0){ 
                                foreach($result1 as $r){
                                    if($i > 2 ){
                            ?>        
                                    <div class="col-md-12 paddingZero">
                                        <div class="smallNewsSecondImageLeftSection">
                                            <a href="<?php echo SITE_URL ?>category/story/<?php echo $r->id ?>/<?php echo $r->seourl ?>">
                                                <img src="<?php echo S3_URL?>site/images/posts/small_postimage_crop/<?php $img ="thumb-".$r->post_image;  echo $img ?>" alt="">
                                            </a>
                                        </div>            
                                        <div class="smallNewsSecondContentSection marginTop10">
                                            
                                            <h5 class="semiBold headingH5"><?php echo substr($r->post_title,0,40)?>..</h5>
                                        </div>
                                    </div>
                                    
                                    <?php if($i == 5){ ?>

                           
                                    <?php } ?>

                            <?php  }
                                $i++;
                                } 
                            } 
                            ?>

                            <div class="col-md-12 paddingZero">
                                    
                                <!-- Composite Start -->
                                <div id="M639897ScriptRootC1009686"></div>
                                <script src="https://jsc.mgid.com/n/e/newstalkie.com.1009686.js" async></script>
                                <!-- Composite End -->  
                                
                                    
                            </div>

                            <div class="col-md-12 paddingZero">
                                    
                                <!-- /112279518/Native_Groping -->
                                <div id='div-gpt-ad-1611898250803-0' style='width: 850px; height: 460px;'>
                                  <script>
                                    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1611898250803-0'); });
                                  </script>
                                </div>
                                
                                    
                            </div>
                            
                        </div>
                        
                    </div>

                </div>

                <div class="clearfix"></div>

                <?php 

                $country_code = (isset($_COOKIE["countrytest"]) != "" ? $_COOKIE["countrytest"] : "" ); 
                           
                if($country_code == "gb"){ ?>

                <div class="col-md-12 videoSec stickyVideo">

                    <div class="col-md-3" style="float: right;">

                        <a href="http://adcanopus.offerstrack.net/index.php?offer_id=63395&aff_id=2159" target="_blank">                            
                            <video width="300" height="170" id="myVideo" controls muted loop autoplay>
                                <source src="<?php echo S3_URL?>site/video/local.mp4" type="video/mp4">
                                <source src="movie.ogg" type="video/ogg">
                                Your browser does not support the video tag.
                            </video>
                        </a>
                    
                    </div>

                </div>

                <?php } ?>

            </div>

            <div class="clearfix"></div>



