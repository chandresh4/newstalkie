
<div class="container-fluid">

    <div class="col-md-9 paddingZero headerMargin">

        <section class="indiaTopSecNews SportsHeaderContainSec">

            <h3 class="boldFont"> Sports 
                <span class="sportsRoutingLinks"> <a href="<?php echo SITE_URL?>sports/"> | Indian Premier League 2021</a></span>
            </h3>

            <h3 class="boldFont">Live Cricket Score </h3>

            <p class="boldFont" style="padding-left: 1em;">
                <span class="sportsRoutingLinks blueText"> <a href="<?php echo SITE_URL?>sports/index/live" > Live </a> | </span> 
                <span class="sportsRoutingLinks blueText"> <a href="<?php echo SITE_URL?>sports/index/completed" > Recent </a> | </span>  
                <span class="sportsRoutingLinks blueText"> <a href="<?php echo SITE_URL?>sports/index/upcoming"> Upcoming </a> </span>
             </p>

            <!-- <h3 class="boldFont"> Sports 
                <span class="sportsRoutingLinks"> <a href="#">IPL </a> | </span>  
                <span class="sportsRoutingLinks"> <a href="#">Tennis </a> | </span> 
                <span class="sportsRoutingLinks"> <a href="#"> Premier League </a> | </span>  
                <span class="sportsRoutingLinks"> <a href="#">Tennis </a>  </span> 
            </h3> -->

            <div class="row">

                <div class="col-md-9 scoreCardHolder">
                    <?php 
                	if(is_array($full_scorecard) && count($full_scorecard) > 0){ ?>

                        <!-- <p class="winningInfo">Western Australia won by 159 runs</p> -->

                    <?php
                        foreach ($full_scorecard as $records) {
                                                    
                    ?> 
                    
                     <div class="col-md-12 paddingZero">
                        
                        <div class="col-md-12 paddingZero tableNameScoreHolder">

                            <div class="teamTableName">
                                <span> <?php echo $records->name ?> </span>
                            </div>

                            <div class="teamTableScore">
                                <span> <?php echo $records->run ?>-<?php echo $records->wicket ?> (<?php echo $records->over ?> Over) </span>
                            </div>

                        </div>

                        <div class="col-md-12 paddingZero">

                            <table class="table table-responsive" style="margin-bottom: 0;">

                                <thead>

                                    <tr style="background: #F1F1F1;">
                                        <td>Batsmen</td>
                                        <td></td>
                                        <td>R</td>
                                        <td>B</td>
                                        <td>4s</td>
                                        <td>6s</td>
                                        <td>SR</td>
                                    </tr>

                                </thead>

                                <tbody>

                                    <?php 
                                    
                                    $fall_of_wicket_arr = array();

                                    foreach($records->batsmen as $batsmen_record) {  
                                        
                                           if(isset($batsmen_record->fallOfWicket)){ 
                                                $fall_of_wicket_arr[] = $batsmen_record->fallOfWicket ." ( <span class='blueText'> ".$batsmen_record->name ." </span>,". $batsmen_record->fallOfWicketOver.")";
                                            }
                                        ?>
                                    <tr>
                                        <td class="blueText"><?php echo $batsmen_record->name?></td>
                                        <td><?php echo $batsmen_record->howOut ?></td>
                                        <td><?php echo $batsmen_record->runs?></td>
                                        <td><?php echo $batsmen_record->balls?></td>
                                        <td><?php echo $batsmen_record->fours?></td>
                                        <td><?php echo $batsmen_record->sixes?></td>
                                        <td><?php echo $batsmen_record->strikeRate?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>

                            </table>

                            <table class="table table-responsive">

                                <tr>
                                    <td style="width: 60%;">Extra</td>
                                    <td style="width: 40%;"> <?php echo $records->extra ?> (b <?php echo $records->bye ?>, lb <?php echo $records->legBye ?>, w <?php echo $records->wide ?>, nb <?php echo $records->noBall ?>) </td>
                                </tr>

                                <tr>
                                    <td style="width: 60%;">Total</td>
                                    <td style="width: 40%;"> <?php echo $records->run ?> ( <?php echo $records->wicket ?> wkts, <?php echo $records->over ?> Ov)  </td>
                                </tr>

                            </table>

                        </div>

                        <div class="col-md-12 wicketsFallenHolder">

                        <?php 
                        if( is_array($fall_of_wicket_arr) && count($fall_of_wicket_arr) > 0) { ?>

                            <p class="semiBold">Fall of Wickets</p>
                            <div class="col-md-12 wicketsFallen">
                                <?php
                                foreach($fall_of_wicket_arr as $fall_wickets ){
                                ?>

                                <span> <?php echo $fall_wickets ?>  </span>,

                                <?php } ?>

                            </div>
                        <?php } ?>    

                        </div>

                        <div class="col-md-12 paddingZero">

                            <table class="table table-responsive" style="margin-bottom: 0;">

                                <thead>

                                    <tr style="background: #F1F1F1;">
                                        <td>Bowler</td>
                                        <td>0</td>
                                        <td>M</td>
                                        <td>R</td>
                                        <td>W</td>
                                        <td>NB</td>
                                        <td>WD</td>
                                        <td>ECO</td>
                                    </tr>

                                </thead>

                                <tbody>

                                    <?php 
                                    
                                    $fall_of_wicket_arr = array();

                                    foreach($records->bowlers as $bowler_record) {  
                                        ?>
                                    <tr>
                                        <td class="blueText"><?php echo $bowler_record->name?></td>
                                        <td><?php echo $bowler_record->overs ?></td>
                                        <td><?php echo $bowler_record->maidens?></td>
                                        <td><?php echo $bowler_record->runsConceded ?></td>
                                        <td><?php echo $bowler_record->wickets ?></td>
                                        <td><?php echo $bowler_record->noBalls ?></td>
                                        <td><?php echo $bowler_record->wides ?></td>
                                        <td><?php echo $bowler_record->economy ?></td>
                                        
                                    </tr>
                                <?php } ?>
                                </tbody>

                            </table>

                            <table class="table table-responsive">

                                <tr>
                                    <td style="width: 30%;"></td>
                                    <td style="width: 30%;"></td>
                                    <td style="width: 30%;"></td>
                                </tr>


                            </table>

                        </div>


                    </div>
                <?php } 
                }else{

                    echo '<p class="winningInfo">ScoreCard is not availale for this moment.</p>';
                }?>

                </div>

                <div class="col-md-3 rightAdSec">

                    <?php
                    if(is_array($result) && count($result) > 0){ 
                       
                       for($i=5; $i < 12 ; $i++){

                            if(isset($result[$i])){
                       ?>

                            <div class="col-md-12 paddingZero">

                                <div class="smallNewsSecondImageLeftSection">
                                    <a href="<?php echo SITE_URL ?>category/story/<?php echo $result[$i]->id ?>/<?php echo $result[$i]->seourl ?>">
                                        <img src="<?php echo S3_URL?>site/images/posts/small_postimage_crop/<?php $img ="thumb-".$result[$i]->post_image;  echo $img ?>" alt="">
                                    </a>
                                </div>

                                <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $result[$i]->id ?>/<?php echo $result[$i]->seourl ?>' ">
                                    <h5 class="semiBold"><?php echo stripcslashes($result[$i]->post_title); ?> </h5>
                                </div>

                            </div>
                    <?php 
                            }
                        }
                    }
                    ?>

                </div>

            </div>

            <div class="row">
                                        
                <div class="col-md-12 indiaTopNewsItemHolder paddingZero" style="margin-top: 2em">

                    <div class="col-md-6 paddingZero">

                        <div class="col-md-12 paddingZeroRight bigNews">


                        <?php        
                        if(isset($result) && count($result) > 0){
                           if(isset($result[0])){

                           $result_0 =  $result[0];
                           
                        ?>    

                            <div class="bigNewsImageSec">
                                <a href="<?php echo SITE_URL ?>category/story/<?php echo $result_0->id ?>/<?php echo $result_0->seourl ?>">
                                    <img src="<?php echo S3_URL?>site/images/posts/small_postimage_crop/<?php $img ="thumb-".$result_0->post_image;  echo $img ?>" alt="">
                                </a>    
                            </div>

                            <div class="bigNewsContentSec linearBackground pointer" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $result_0->id ?>/<?php echo $result_0->seourl ?>' ">

                                <h3 class="semiBold whiteTxt"><?php echo substr(stripcslashes($result_0->post_title),0,50) ?></h3>

                            </div>

                
                        <?php 
                            } 
                        }
                       
                        ?>    
                        </div>

                        <div class="col-md-12 paddingZero ">
                            <?php
                            if(is_array($result) && count($result) > 0){ 
                               
                               for($i=1; $i < 7 ; $i++){

                                    if(isset($result[$i])){
                               
                               ?>
    
                            <div class="col-md-6 paddingZeroRight marginTop15">

                                <div class="col-md-12 paddingZero">

                                    <div class="smallNewsSecondImageLeftSection">
                                        
                                        <a href="<?php echo SITE_URL ?>category/story/<?php echo $result[$i]->id ?>/<?php echo $result[$i]->seourl ?>">
                                            <img src="<?php echo S3_URL?>site/images/posts/small_postimage_crop/<?php $img ="thumb-".$result[$i]->post_image;  echo $img ?>" alt="">
                                        </a>
                                    </div>

                                    <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $result[$i]->id ?>/<?php echo $result[$i]->seourl ?>' ">
                                        <h5 class="semiBold"><?php echo substr(stripcslashes($result[$i]->post_title),0,50)?></h5>
                                    </div>

                                </div>
                            
                            </div>

                            <?php }
                                }   
                            } ?>

                        </div>
                    
                    </div>

                    <div class="col-md-6 lifestyleTopRighttNews">
                     <?php
                        if(is_array($result) && count($result) > 0){ 
                           
                           for($i=7; $i < 9 ; $i++){

                                if(isset($result[$i])){
                           
                           ?>
                    
                        <div class="col-md-6 paddingZeroRight">

                            <div class="col-md-12 paddingZero">

                                <div class="smallNewsSecondImageLeftSection maxHeight15">
                                    <a href="<?php echo SITE_URL ?>category/story/<?php echo $result[$i]->id ?>/<?php echo $result[$i]->seourl ?>">
                                        <img src="<?php echo S3_URL?>site/images/posts/small_postimage_crop/<?php $img ="thumb-".$result[$i]->post_image;  echo $img ?>" alt="">
                                    </a>
                                </div>

                                <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $result[$i]->id ?>/<?php echo $result[$i]->seourl ?>'">
                                    <h5 class="semiBold"><?php echo substr(stripcslashes($result[$i]->post_title),0,50)?></h5>
                                </div>

                                <p class="pageTag">
                                    <?php echo substr(stripcslashes($result[$i]->post_sub_title),0,50) ?>
                                <p>

                            </div>

                        </div>

                       <?php    } 
                            }

                        }?> 


                        <?php
                        if(is_array($result) && count($result) > 0){ 
                           
                           for($i=9; $i < 15 ; $i++){

                              if(isset($result[$i])){

                           ?>

                            <div class="col-md-6 paddingZeroRight marginTop15">

                                <div class="col-md-12 paddingZero">

                                    <div class="smallNewsSecondImageLeftSection">
                                        <a href="<?php echo SITE_URL ?>category/story/<?php echo $result[$i]->id ?>/<?php echo $result[$i]->seourl ?>">
                                            <img src="<?php echo S3_URL?>site/images/posts/small_postimage_crop/<?php $img ="thumb-".$result[$i]->post_image;  echo $img ?>" alt="">
                                        </a>
                                    </div>

                                    <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $result[$i]->id ?>/<?php echo $result[$i]->seourl ?>'">
                                        <h5 class="semiBold"><?php echo substr(stripcslashes($result[$i]->post_title),0,50)?> </h5>
                                    </div>

                                </div>
                            
                            </div>

                        <?php  }
                            }
                        } ?>    

                    </div>
                
                </div>

            </div>

        </section>        
          
    </div>

    <div class="col-md-3 paddingZero newHeaderMargin">

        <div class="partnerAdSec">
        
            <div class="col-md-12">


            <?php        
            if(isset($result) && count($result) > 0){
               if(isset($result[0])){

               $result_0 =  $result[0];
               
            ?> 

                <div class="col-md-12 paddingZero">

                    <div class="smallNewsSecondImageLeftSection">
                        <a href="<?php echo SITE_URL ?>category/story/<?php echo $result_0->id ?>/<?php echo $result_0->seourl ?>">
                            <img src="<?php echo S3_URL?>site/images/posts/small_postimage_crop/<?php $img ="thumb-".$result_0->post_image;  echo $img ?>" alt="">
                        </a>
                        
                    </div>

                    <div class="smallNewsSecondContentSection marginTop10">
                        <h5 class="semiBold headingH5"><?php echo stripcslashes($result[$i]->post_title); ?> </h5>
                    </div>

                </div>
            <?php 
                }
            }

            ?>

                 <!--<div class="col-md-12 quickLinks">

                    <div class="smallNewsSecondContentSection marginTop10">
                        <h5 class="semiBold headingH5"> Quick Links </h5>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Unlock 5 Guidelines</a>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Deals of the Day</a>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Bihar Elections</a>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Coronavirus Live</a>
                    </div>
                
                </div> -->


                <div class="col-md-12 paddingZero">

                    <!-- BEGIN JS EXT TAG - 300x250 Newstalkie India _300*250_below _Quick Links - DO NOT MODIFY -->
                    <SCRIPT TYPE="text/javascript">
                    window.__jscp=function(){if(window.parent==window)var a=document.URL;else if(a=document.referrer,!a)try{a=window.parent.location.href}catch(e){}if(a){var b=a.indexOf("://");0<=b&&(a=a.substring(b+3));b=a.indexOf("/");0<=b&&(a=a.substring(0,b))}b=0;for(var c=window;c!=c.parent;)++b,c=c.parent;a={pu:a,"if":b,rn:new Number(Math.floor(99999999*Math.random())+1)};b=[];for(var d in a)b.push(d+"="+encodeURIComponent(a[d]));return encodeURIComponent(b.join("&"))};
                    document.write('<S' + 'CRIPT TYPE="text/javascript" SRC="//cpm.rtbwire.com/tag?zone_id=135079&size=300x250&site_cat=IAB17&j=' + __jscp() + '"></S' + 'CRIPT>');
                    </SCRIPT>
                    <!-- END TAG -->

                </div>

                <div class="col-md-12 paddingZero">
                    <!-- BEGIN JS EXT TAG - 300x250 Newstalkie_300*250_right_ganguli article - DO NOT MODIFY -->
                    <SCRIPT TYPE="text/javascript">
                    window.__jscp=function(){if(window.parent==window)var a=document.URL;else if(a=document.referrer,!a)try{a=window.parent.location.href}catch(e){}if(a){var b=a.indexOf("://");0<=b&&(a=a.substring(b+3));b=a.indexOf("/");0<=b&&(a=a.substring(0,b))}b=0;for(var c=window;c!=c.parent;)++b,c=c.parent;a={pu:a,"if":b,rn:new Number(Math.floor(99999999*Math.random())+1)};b=[];for(var d in a)b.push(d+"="+encodeURIComponent(a[d]));return encodeURIComponent(b.join("&"))};
                    document.write('<S' + 'CRIPT TYPE="text/javascript" SRC="//cpm.rtbwire.com/tag?zone_id=135080&size=300x250&site_cat=IAB17&j=' + __jscp() + '"></S' + 'CRIPT>');
                    </SCRIPT>
                    <!-- END TAG -->
                </div>

                <div class="col-md-12 paddingZero">

                    <SCRIPT TYPE="text/javascript">
                    window.__jscp=function(){if(window.parent==window)var a=document.URL;else if(a=document.referrer,!a)try{a=window.parent.location.href}catch(e){}if(a){var b=a.indexOf("://");0<=b&&(a=a.substring(b+3));b=a.indexOf("/");0<=b&&(a=a.substring(0,b))}b=0;for(var c=window;c!=c.parent;)++b,c=c.parent;a={pu:a,"if":b,rn:new Number(Math.floor(99999999*Math.random())+1)};b=[];for(var d in a)b.push(d+"="+encodeURIComponent(a[d]));return encodeURIComponent(b.join("&"))};
                    document.write('<S' + 'CRIPT TYPE="text/javascript" SRC="//cpm.rtbwire.com/tag?zone_id=135082&size=300x250&site_cat=IAB17&j=' + __jscp() + '"></S' + 'CRIPT>');
                    </SCRIPT>
                    <!-- END TAG -->

                </div>


                
            
            </div>

        </div>

    </div>

    <div class="clearfix"></div>

    <!-- <section class="moreAds">

        <div class="container">

            <h3 class="boldFont" style="margin-bottom: 2em;"> MORE FOR YOU </h3>

            <div class="col-md-12">
            
                <div class="col-md-3 adItems">
                    <img src="<?php echo S3_URL?>site/images/videoes/ads.png" alt="Newslakie News" class="img-responsive">
                    <p class="boldFont">
                        The power of 4 devices in 1 @ ₹5699
                    </p>
                </div>

                <div class="col-md-3 adItems">
                    <img src="<?php echo S3_URL?>site/images/videoes/ads.png" alt="Newslakie News" class="img-responsive">
                    <p class="boldFont">
                        #KeepAWatch on your Vitals In Real Time with GOQii
                    </p>
                </div>

                <div class="col-md-3 adItems">
                    <img src="<?php echo S3_URL?>site/images/videoes/ads.png" alt="Newslakie News" class="img-responsive">
                    <p class="boldFont">
                        Power new possibilities
                    </p>
                </div>

                <div class="col-md-3 adItems">
                    <img src="<?php echo S3_URL?>site/images/videoes/ads.png" alt="Newslakie News" class="img-responsive">
                    <p class="boldFont">
                        Power new possibilities
                    </p>
                </div>
            
            </div>

        </div>

    </section> -->

</div>                          
                
                               