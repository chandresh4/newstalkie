scoreboard
<div class="container-fluid">

    <div class="col-md-9 paddingZero headerMargin">

        <section class="indiaTopSecNews">

            <h3 class="boldFont"> Sports 
                <span class="sportsRoutingLinks"> <a href="#">IPL </a> | </span>  
                <span class="sportsRoutingLinks"> <a href="#">Tennis </a> | </span> 
                <span class="sportsRoutingLinks"> <a href="#"> Premier League </a> | </span>  
                <span class="sportsRoutingLinks"> <a href="#">Tennis </a> | </span> 
            </h3>

            <div class="row">

                <div class="col-md-12 scoreCardHolder">

                    <p class="winningInfo">Western Australia won by 159 runs</p>

                    <div class="col-md-12 paddingZero">

                        <div class="col-md-3">

                        <div class="col-md-12 paddingZero tableNameScoreHolder">

                            <div class="teamTableName">
                                <span> MATCH INFO </span>
                            </div>

                        </div>

                            <table class="table table-responsive" style="background: #F1F1F1;">

                                <tr>
                                    <td class="blueText"> Match </td>
                                    <td> WA v TAS, Australia Domestic One-Day Cup 2021 </td>
                                </tr>

                                <tr>
                                    <td class="blueText"> Date </td>
                                    <td> Apr 8, 2021 </td>
                                </tr>

                                <tr>
                                    <td class="blueText"> Toss </td>
                                    <td> Tasmania (Bowling) </td>
                                </tr>

                                <tr>
                                    <td class="blueText"> Time </td>
                                    <td> 7:00 AM GMT </td>
                                </tr>

                                <tr>
                                    <td class="blueText"> Venue </td>
                                    <td> W.A.C.A. Ground, Perth </td>
                                </tr>

                            </table>

                        </div>

                        <div class="col-md-9">

                            <h4 class="boldFont">Teams:</h4>

                            <p>
                                <span> <b> Western Australia (Playing XI): </b> </span>
                                <span> Josh Philippe, Sam Whiteman, D Arcy Short, Mitchell Marsh(c), Josh Inglis(w), Cameron Green, Ashton Turner, Ashton Agar, Jason Behrendorff, Liam Guthrie </span>
                            </p>

                            <br>

                            <p>
                                <span> <b> Lance Morris Tasmania (Playing XI): </b> </span>
                                <span> Caleb Jewell, Ben McDermott, Matthew Wade(c), Jake Doran(w), Jordan Silk, Beau Webster, Mitchell Owen, Thomas Rogers, Tom Andrews, Jackson Bird, Sam Rainbird </span>
                           
                            </p>

                            <p> <b> Tasmania have won the toss and have opted to field </b> </p>

                            <br>

                            <h4 class="boldFont">Squad:</h4>

                            
                            <p>
                                <span> <b> Tasmania Squad: </b> </span>
                                <span> Ben McDermott, Caleb Jewell, Matthew Wade(c), Tim Paine(w), Jordan Silk, Beau Webster, Tom Andrews, Jackson Bird, Jake Doran, Jarrod Freeman, Mitchell Owen, Alex Pyecroft, Sam Rainbird, Thomas Rogers </span>
                            </p>

                            <br>

                            <p>
                                <span> <b>  Western Australia Squad: </b> </span>
                                <span> D Arcy Short, Sam Whiteman, Mitchell Marsh(c), Cameron Green, Josh Inglis(w), Hilton Cartwright, Ashton Agar, Jason Behrendorff, Liam Guthrie, David Moody, Lance Morris, Josh Philippe, Ashton Turner </span>
                           
                            </p>

                        </div>

                    </div>

                </div>

            </div>

            <div class="clearfix"></div>

            <br>

            <div class="row">
                                        
                <div class="col-md-12 indiaTopNewsItemHolder paddingZero">

                    <div class="col-md-6 paddingZero">

                        <div class="col-md-12 paddingZeroRight bigNews">

                            <div class="bigNewsImageSec">
                                <a href="http://localhost/newstalkie_new/category/story/1622/best-smart-phones-below-rs-20000-to-bring-home-this-diwali">
                                    <img src="<?php echo S3_URL?>site/images/sports/axar.png" alt="Best Smart Phones below Rs 20,000 to bring home this Diwali">
                                </a>    
                            </div>

                            <div class="bigNewsContentSec linearBackground pointer" onclick="window.location.href= 'http://localhost/newstalkie_new/category/story/1622/best-smart-phones-below-rs-20000-to-bring-home-this-diwali'">

                                <span class="newsBadge"> Breaking News</span>

                                <h2 class="semiBold whiteTxt">Just think like a spinner. No point discussing wrist vs finger debate: Axar Patel</h2>

                            </div>
            
                        </div>

                        <div class="col-md-12 paddingZero ">

                            <div class="col-md-6 paddingZeroRight marginTop15">

                                <div class="col-md-12 paddingZero">

                                    <div class="smallNewsSecondImageLeftSection">
                                        <a href="http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429">
                                            <img src="<?php echo S3_URL?>site/images/sports/field.png" alt="Newslakie News">
                                        </a>
                                    </div>

                                    <p class=" semiBold pageTag">Money</p>

                                    <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= 'http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429'">
                                        <h5 class="semiBold">With 36,469 covid cases, the Covid tally in India jumps to 79, 46,429 </h5>
                                    </div>

                                    <p class="source"> News 18 </p>

                                </div>
                            
                            </div>

                            <div class="col-md-6 paddingZeroRight marginTop15">

                                <div class="col-md-12 paddingZero">

                                    <div class="smallNewsSecondImageLeftSection">
                                        <a href="http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429">
                                            <img src="<?php echo S3_URL?>site/images/sports/sports.png" alt="Newslakie News">
                                        </a>
                                    </div>

                                    <p class=" semiBold pageTag">Money</p>

                                    <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= 'http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429'">
                                        <h5 class="semiBold">With 36,469 covid cases, the Covid tally in India jumps to 79, 46,429 </h5>
                                    </div>

                                    <p class="source"> News 18 </p>

                                </div>
                            
                            </div>
                        
                            <div class="col-md-6 paddingZeroRight marginTop15">

                                <div class="col-md-12 paddingZero">

                                    <div class="smallNewsSecondImageLeftSection">
                                        <a href="http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429">
                                            <img src="<?php echo S3_URL?>site/images/sports/tennis.png" alt="Newslakie News">
                                        </a>
                                    </div>

                                    <p class=" semiBold pageTag">Money</p>

                                    <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= 'http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429'">
                                        <h5 class="semiBold">With 36,469 covid cases, the Covid tally in India jumps to 79, 46,429 </h5>
                                    </div>

                                    <p class="source"> News 18 </p>

                                </div>
                            
                            </div>

                            <div class="col-md-6 paddingZeroRight marginTop15">

                                <div class="col-md-12 paddingZero">

                                    <div class="smallNewsSecondImageLeftSection">
                                        <a href="http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429">
                                            <img src="<?php echo S3_URL?>site/images/sports/cricket.png" alt="Newslakie News">
                                        </a>
                                    </div>

                                    <p class=" semiBold pageTag">Money</p>

                                    <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= 'http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429'">
                                        <h5 class="semiBold">With 36,469 covid cases, the Covid tally in India jumps to 79, 46,429 </h5>
                                    </div>

                                    <p class="source"> News 18 </p>

                                </div>
                            
                            </div>

                            <div class="col-md-6 paddingZeroRight marginTop15">

                                <div class="col-md-12 paddingZero">

                                    <div class="smallNewsSecondImageLeftSection">
                                        <a href="http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429">
                                            <img src="<?php echo S3_URL?>site/images/sports/football.png" alt="Newslakie News">
                                        </a>
                                    </div>

                                    <p class=" semiBold pageTag">Money</p>

                                    <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= 'http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429'">
                                        <h5 class="semiBold">With 36,469 covid cases, the Covid tally in India jumps to 79, 46,429 </h5>
                                    </div>

                                    <p class="source"> News 18 </p>

                                </div>
                            
                            </div>

                            <div class="col-md-6 paddingZeroRight marginTop15">

                                <div class="col-md-12 paddingZero">

                                    <div class="smallNewsSecondImageLeftSection">
                                        <a href="http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429">
                                            <img src="<?php echo S3_URL?>site/images/sports/conference.png" alt="Newslakie News">
                                        </a>
                                    </div>

                                    <p class=" semiBold pageTag">Money</p>

                                    <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= 'http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429'">
                                        <h5 class="semiBold">With 36,469 covid cases, the Covid tally in India jumps to 79, 46,429 </h5>
                                    </div>

                                    <p class="source"> News 18 </p>

                                </div>
                            
                            </div>

                        </div>
                    
                    </div>

                    <div class="col-md-6 lifestyleTopRighttNews">

                        <div class="col-md-6 paddingZeroRight">

                            <div class="col-md-12 paddingZero">

                                <div class="smallNewsSecondImageLeftSection">
                                    <a href="http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429">
                                        <img src="<?php echo S3_URL?>site/images/sports/rohit.png" alt="Newslakie News">
                                    </a>
                                </div>

                                <p class=" semiBold pageTag">Money</p>

                                <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= 'http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429'">
                                    <h5 class="semiBold">What India’s military commentators don’t get about drones — AI… </h5>
                                </div>

                                <p class="pageTag">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, 
                                    sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. 
                                    Ut wisi enim ad minim veniam, quis nostrud exerci
                                <p>

                                <p class="source"> News 18 </p>

                            </div>

                        </div>

                        <div class="col-md-6 paddingZeroRight">

                            <div class="col-md-12 paddingZero">

                                <div class="smallNewsSecondImageLeftSection">
                                    <a href="http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429">
                                        <img src="<?php echo S3_URL?>site/images/sports/rahul.png" alt="Newslakie News">
                                    </a>
                                </div>

                                <p class=" semiBold pageTag">Money</p>

                                <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= 'http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429'">
                                    <h5 class="semiBold">Embassy REIT is looking to buy large office spaces </h5>
                                </div>

                                <p class="pageTag">
                                    Embassy Office Parks REIT said it is looking for acquisitions of Grade A 
                                    commercial office assets to increase its portfolio across the 
                                    country at a time when the pandemic has created a 
                                    liquidity squeeze among many developers.
                                </p>

                                <p class="source"> News 18 </p>

                            </div>
                        
                        </div>



                        <div class="col-md-6 paddingZeroRight marginTop15">

                                <div class="col-md-12 paddingZero">

                                    <div class="smallNewsSecondImageLeftSection">
                                        <a href="http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429">
                                            <img src="<?php echo S3_URL?>site/images/sports/rahul.png" alt="Newslakie News">
                                        </a>
                                    </div>

                                    <p class=" semiBold pageTag">Money</p>

                                    <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= 'http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429'">
                                        <h5 class="semiBold">With 36,469 covid cases, the Covid tally in India jumps to 79, 46,429 </h5>
                                    </div>

                                    <p class="source"> News 18 </p>

                                </div>
                            
                            </div>

                            <div class="col-md-6 paddingZeroRight marginTop15">

                                <div class="col-md-12 paddingZero">

                                    <div class="smallNewsSecondImageLeftSection">
                                        <a href="http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429">
                                            <img src="<?php echo S3_URL?>site/images/sports/rohit-2.png" alt="Newslakie News">
                                        </a>
                                    </div>

                                    <p class=" semiBold pageTag">Money</p>

                                    <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= 'http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429'">
                                        <h5 class="semiBold">With 36,469 covid cases, the Covid tally in India jumps to 79, 46,429 </h5>
                                    </div>

                                    <p class="source"> News 18 </p>

                                </div>
                            
                            </div>

                            <div class="col-md-6 paddingZeroRight marginTop15">

                                <div class="col-md-12 paddingZero">

                                    <div class="smallNewsSecondImageLeftSection">
                                        <a href="http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429">
                                            <img src="<?php echo S3_URL?>site/images/sports/sports-news.png" alt="Newslakie News">
                                        </a>
                                    </div>

                                    <p class=" semiBold pageTag">Money</p>

                                    <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= 'http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429'">
                                        <h5 class="semiBold">With 36,469 covid cases, the Covid tally in India jumps to 79, 46,429 </h5>
                                    </div>

                                    <p class="source"> News 18 </p>

                                </div>
                            
                            </div>

                            <div class="col-md-6 paddingZeroRight marginTop15">

                                <div class="col-md-12 paddingZero">

                                    <div class="smallNewsSecondImageLeftSection">
                                        <a href="http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429">
                                            <img src="<?php echo S3_URL?>site/images/sports/rahul.png" alt="Newslakie News">
                                        </a>
                                    </div>

                                    <p class=" semiBold pageTag">Money</p>

                                    <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= 'http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429'">
                                        <h5 class="semiBold">With 36,469 covid cases, the Covid tally in India jumps to 79, 46,429 </h5>
                                    </div>

                                    <p class="source"> News 18 </p>

                                </div>
                            
                            </div>

                    </div>
                
                </div>

            </div>

        </section>        
          
    </div>

    <div class="col-md-3 paddingZero headerMargin">

        <div class="partnerAdSec">
        
            <div class="col-md-12">

                <div class="col-md-12 paddingZero">

                    <div class="smallNewsSecondImageLeftSection">
                        <a href="https://www.newstalkie.com/category/story/35/war-of-the-giants">
                            <img src="https://www.newstalkie.com/cdn/site/images/posts/small_postimage_crop/thumb-6.jpg" alt="">
                        </a>
                        
                    </div>

                    <div class="smallNewsSecondContentSection marginTop10">
                        <h5 class="semiBold headingH5">War of the Giants..</h5>
                    </div>

                </div>

                <div class="col-md-12 quickLinks">

                    <div class="smallNewsSecondContentSection marginTop10">
                        <h5 class="semiBold headingH5"> Quick Links </h5>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Unlock 5 Guidelines</a>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Deals of the Day</a>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Bihar Elections</a>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Coronavirus Live</a>
                    </div>
                
                </div>
            
            </div>

        </div>

    </div>

    <div class="clearfix"></div>

    <section class="moreAds">

        <div class="container">

            <h3 class="boldFont" style="margin-bottom: 2em;"> MORE FOR YOU </h3>

            <div class="col-md-12">
            
                <div class="col-md-3 adItems">
                    <img src="<?php echo S3_URL?>site/images/videoes/ads.png" alt="Newslakie News" class="img-responsive">
                    <p class="boldFont">
                        The power of 4 devices in 1 @ ₹5699
                    </p>
                </div>

                <div class="col-md-3 adItems">
                    <img src="<?php echo S3_URL?>site/images/videoes/ads.png" alt="Newslakie News" class="img-responsive">
                    <p class="boldFont">
                        #KeepAWatch on your Vitals In Real Time with GOQii
                    </p>
                </div>

                <div class="col-md-3 adItems">
                    <img src="<?php echo S3_URL?>site/images/videoes/ads.png" alt="Newslakie News" class="img-responsive">
                    <p class="boldFont">
                        Power new possibilities
                    </p>
                </div>

                <div class="col-md-3 adItems">
                    <img src="<?php echo S3_URL?>site/images/videoes/ads.png" alt="Newslakie News" class="img-responsive">
                    <p class="boldFont">
                        Power new possibilities
                    </p>
                </div>
            
            </div>

        </div>

    </section>

</div>                          
