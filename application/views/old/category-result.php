<input type="hidden" id="site_url" name="site_url" value="<?php echo SITE_URL?>"  />
<script type="text/javascript">
      var limit = 10
      var offset = 0;
      function displayRecords(lim, off,cate) {
	  var s_u = $("#site_url").val();
      $.ajax({
          type: "GET",
          async: false,
          url: s_u+"category/get_result",
          data: "limit=" + lim + "&offset=" + off+"&category="+cate,
          cache: false,
          beforeSend: function() {
          
          },
          success: function(html) {
          var data = html.split("|");
		  $("#results").append(data[0]);
            if (html == "") {
              $("#loader_message").html('').show()
            } else {
			if(data[1] =="less"){
				$("#loader_message").html('').show()
            } else {
              $("#loader_message").html('<button class="btn btn-default" type="button">View more </button>').show();
            }
		  }
          }
        });
      } 

      $(document).ready(function() {
		  var category = $('#category').val();
		  
		  // start to load the first set of data
          displayRecords(limit,offset,category);
          $('#loader_message').click(function() {
          // if it has no more records no need to fire ajax request
          var d = $('#loader_message').find("button").attr("data-atr");
          if (d != "nodata") {
            offset = limit + offset;
            displayRecords(limit, offset,category);
          }
        
        });
 
      });
</script>
<div id="about" class="container banner_container box_container">
	  <div class="row">
	  <?php if(isset($search_res) && $search_res ="trendy"){
				    echo "<div class='search_name' style='font-size: 20px;' >  TRENDY RESULTS </div>";
				  }else{
					echo "<div class='search_name' style='font-size: 20px;' >  VIRAL RESULTS </div>";
				  }
	   ?>
	    <input type="hidden" id="category" name="category" value="<?php echo $seourl; ?>">
        <div class="col-sm-9 search_box">
		<div  id="results">
		
		</div>
		<div id="loader_message"></div>
		</div> 
	    
		<div class="col-sm-3">
		 	<div class="viral_stories">	
			<?php if($check != "trendy") { ?>
			<h1>VIRAL Stories</h1>	
			<?php }else { ?>
			<h1>Trendy Stories</h1>
			<?php } ?>			
			 <div class="controls pull-right  ">
 				<a class="left fa fa-chevron-left btn btn1 btn-success" href="#carousel-example1" data-slide="prev"></a>
				<a class="right fa fa-chevron-right btn btn1 btn-success" href="#carousel-example1" data-slide="next"></a>
             </div> 
		</div>  
			<div id="carousel-example1" class="carousel slide" data-ride="carousel" data-interval="false">			   
				<div class="carousel-inner">
					<div class="item active">
						<div class="row viral_stories"> 
							<?php if(is_array($result) && count($result) > 0){ 
					           foreach($result as $r){
							   $i=0;?>
									 
									<div class="col-item">
									<a href="<?php echo SITE_URL ?>category/story/<?php echo $r->id ?>/<?php echo $r->seourl ?>">
									<div class="photo">
										 <img src="<?php echo S3_URL?>site/images/posts/small_postimage_crop/<?php $img ="thumb-".$r->post_image;  echo $img ?>" class="img-responsive" alt="a" />
									</div>
									<div class="info"> 
											<div class="price col-md-12">
											<?php echo substr($r->post_title,0,40)?>
											</div>  
										</div>
										</a>
									</div>
										
								<?php
							   $i++;
							 }
				            }  ?>
				            <div class="col-item">
								<a href="http://tracking.adcanopus.com/aff_c?offer_id=35688&aff_id=1017">
								<div class="photo">
									 <img src="<?php echo S3_URL?>site/images/akash.jpg" class="img-responsive" alt="a" />
								</div>
								</a>
								</div>
				        </div>	
					</div>
					<div class="item">
						
						<div class="row viral_stories">  
							<?php if(isset($result1) && $result1 != ""){ 
							           foreach($result1 as $v){
									   $i=0;?>
											 
													<div class="col-item">
													<a href="<?php echo SITE_URL ?>category/story/<?php echo $v->id ?>/<?php echo $v->seourl ?>">
													<div class="photo">
														 <img src="<?php echo S3_URL?>site/images/posts/small_postimage_crop/<?php $img ="thumb-".$v->post_image;  echo $img ?>" class="img-responsive" alt="a" />
													</div>
													<div class="info"> 
															<div class="price col-md-12">
															<?php echo substr($v->post_title,0,40)?>
															</div>  
														</div>
														</a>
													</div>
												
										<?php
									   $i++;
									 }
						            }  ?>

	            					

								</div>
					</div>
				</div>
			</div>
				
			 </div> 

		 <div class="clearfix"></div> 
		  	  </div>
	  </div>