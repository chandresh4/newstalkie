<div id="about" class="container banner_container box_container">
	  <div class="row">
		  <div class="col-sm-12 inner_banner_bg">
			<div class="about_banner">
				<h1>About Us</h1>
			</div>
		</div>	
		 <div class="col-sm-12 search_box" style="color:#2c3440;">
		 					
        	<h3 class="text-left"><strong> About US</strong></h3>
        	<p class="text-justify"> 
       		      <?php echo ucfirst(SITE_NAME)?> is just not an entertainment website; we are the ‘baap’ of infotainment. Our website covers a vast range of news pertaining to genres like 'News', 'Politics', 'Sports', 'Business', 'Cricket', 'Lifestyle', 'World', 'Videos'. So, whatever your interest is, we cater to your interest zone most aptly. 
We provide a plethora of information that provides ample information so you don’t get bored, we like our viewers to get engaged and relate to the stories and the content we provide on the website. We dig up stories and allow your voices to be heard among the many who feels the same way. Narrate a tale with the support of video to make you understand more, feel closer and related to the topic we unearth. You will find almost anything viral, trending, talked about topics here. 
From motivational to inspiration stories, from poems to jokes, funny videos that will tickle you, videos that will make you go ‘awww’. We have everything. So stop looking for websites with the all-in-one content, <?php echo ucfirst(SITE_NAME)?> will satisfy your thirst and hunger for crisp news and entertainment.</p>
        	 
				        
        
      </div>
    </div>
  </div>