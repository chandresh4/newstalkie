<!-- banner container -->
<script>

$(document).ready(function(){
  $('#loader_home').hide();
});

</script>
	<a href="registrationpage.html" class="submit_story_mobile"><img src="images/submit_story.png" alt=""></a>
	<div id="about" class="container banner_container box_container">
	  <div class="row">
		<div class="col-sm-7">
	
			<?php 
			       /*
		           $os = getOS();

		           if($os == "Android" ){

		           		echo "ADD SLOT"; 
		      		}
		     
		            $os = getOS();

		            if($os == "iPhone" || $os == "iPod"  || $os == "iPad"){ 

		          		echo "ADD SLOT"; 	
		       
		      		}
		      
                    $os = getOS();
                    $a  = array("1"=>"1200x628_mitron.jpg", "2"=>"1200x628-coldplay.jpg", "3"=>"1200x628-kung-fu-panda.jpg",
                                "4"=>"1200x628-man-with-a-plan.jpg", "5"=>"1200x628-pp.jpg", "6" => "zakhirkahn-1200x628.jpg");
                    $b  = array_rand($a,1);
         

                    if($os == "iPhone" || $os == "iPod"  || $os == "iPad"){ 
                    	echo "ADD SLOT"; 		
                 
	                }else if($os == "Android" ){
	                    
	                    echo "ADD SLOT"; 
	                }*/
                ?>
               
                
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
 		 	<!-- Indicators -->
 		    <ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<!--<li data-target="#myCarousel" data-slide-to="2"></li>-->
		  </ol>

		  	<!-- Wrapper for slides -->
		    <div class="carousel-inner" >
			<?php 
			$i= 0;
			if(isset($details) && $details != ""){ 
                foreach($details as $p){ ?>
				    <div class="item <?php echo ($i == 0 ? 'active' : '') ?>">
					  <a href="<?php echo SITE_URL ?>category/story/<?php echo $p->id ?>/<?php echo $p->seourl ?>">
					  <img src="<?php echo S3_URL?>site/images/posts/<?php echo $p->post_image ?>" alt="<?php echo $p->post_title ?>"></a>
					</div> 
				 
				<?php 
				$i++; 
				} 
			}?>
		  </div>
  
			</div>	
		    <div class="clearfix"></div>
				
		</div>
        <div>
		
		</div>
		<div class="col-sm-5">
		  <div class="latest_news" > 
				<a href="<?php echo SITE_URL."viral-stories"?>"><h2>VIRAL Stories</h2></a>
			    <div class="controls pull-right">
					<a class="left fa fa-chevron-left btn btn1 btn-success" href="#carousel-example" data-slide="prev"></a>
					<a class="right fa fa-chevron-right btn btn1 btn-success" href="#carousel-example" data-slide="next"></a>
				 </div>
				 
		  </div> 
		  <div id="carousel-example" class="carousel slide" data-ride="carousel" data-interval="false">			   
				<div class="carousel-inner">
                <div class="item active">
                    <div class="row">

					<?php if(is_array($res) && count($res) > 0){ 
					    foreach ($res as $n) { ?>
						
                        <div class="col-sm-6">
                            <div class="col-item">
								<a href="<?php echo SITE_URL ?>category/story/<?php echo $n->id ?>/<?php echo $n->seourl ?>" target="">
                                <div class="photo">
                                 <img src="<?php echo S3_URL?>site/images/posts/small_postimage_crop/<?php $img ="thumb-".$n->post_image;  echo $img ?>" class="img-responsive" alt="<?php echo $n->post_title ?>" />

                                </div>
                                <div class="info" style="height:58px;">
                                    <div class="row">
                                        <div class="ln_title col-md-12">
                                            <div class="ln_subject_name"><?php echo substr($n->post_title,0,40)?>...</div>  
                                        </div>
                                    </div> 
                                    <div class="clearfix">
                                    </div>
                                </div>
								</a>	
                            </div>
                        </div> 
						<?php }
					} ?>
                    </div>
                    	
                </div>
                <div class="item">
                                        
                <div class="row">
				<?php if(is_array($res1) && count($res1) > 0){ 
					    foreach ($res1 as $n1) { ?>
						
                        <div class="col-sm-6">
                            <div class="col-item">
								<a href="<?php echo SITE_URL ?>category/story/<?php echo $n1->id ?>/<?php echo $n1->seourl ?>" target="">
                                <div class="photo">
                                 <img src="<?php echo S3_URL?>site/images/posts/small_postimage_crop/<?php $img ="thumb-".$n1->post_image;  echo $img ?>" class="img-responsive" alt="<?php echo $n1->post_title ?>" />

                                </div>
                                <div class="info" style="height:58px;">
                                    <div class="row">
                                        <div class="ln_title col-md-12">
                                            <div class="ln_subject_name"><?php echo substr($n1->post_title,0,40)?>...</div>  
                                        </div>
                                    </div> 
                                    <div class="clearfix">
                                    </div>
                                </div>
								</a>	
                            </div>
                        </div> 
						<?php }
					} ?>
                    </div>
				
				</div>
				 	
            </div>
		</div>	
		</div>
	  </div>
	</div>


	<!-- viral stories container -->	
	<div class="container box_container">
	   <div class="row">	
		<div class="col-sm-12">		
		<div class="viral_stories">	
			<a href="<?php echo SITE_URL."news"?>"><h1>LATEST News</h1></a>	 
			 <div class="controls pull-right  ">
 				<a class="left fa fa-chevron-left btn btn1 btn-success" href="#carousel-example1" data-slide="prev"></a>
				<a class="right fa fa-chevron-right btn btn1 btn-success" href="#carousel-example1" data-slide="next"></a>
             </div> 
		</div> 
		</div>	 		   
	   </div> 
	  
	   <div id="carousel-example1" class="carousel slide" data-ride="carousel" data-interval="false">			   
			<div class="carousel-inner">
			<div class="item active">
			
				
					<div class="row viral_stories">
					 <?php if(is_array($news) && count($news) > 0 ){ 
		           foreach($news as $p1){
				   $i=0;?>
						<div class="col-sm-4">
						<div class="col-item">
						<a href="<?php echo SITE_URL ?>beta/category/story/<?php echo $p1->id ?>/<?php echo $p1->seourl ?>">
							<div class="photo">
								 <img src="<?php echo S3_URL?>site/images/posts/medium_postimage_crop/<?php $img ="thumb-".$p1->post_image;  echo $img ?>" class="img-responsive" alt="<?php echo $p1->post_title?>" />
							</div>
							<div class="info">
								<div class="row">
									<div class="price col-md-12">
									<?php echo substr($p1->post_title,0,100)?>...
									</div> 
								</div> 
								<div class="clearfix">
								</div>
							</div>
							</a>
						</div>
					</div>
						
						
					
					<?php
				   $i++;
				 }
	            }  ?>
				</div>
				</div>
				 
				<div class="item">
					<div class="row viral_stories">
						
						
					<?php if(is_array($news1) && count($news1) > 0){ 
		           foreach($news1 as $p2){
				   $i=0;?>
				   <a href="<?php echo SITE_URL ?>category/story/<?php echo $p2->id ?>/<?php echo $p2->seourl ?>">
						<div class="col-sm-4">
						<div class="col-item">
							<div class="photo">
								 <img src="<?php echo S3_URL?>site/images/posts/medium_postimage_crop/<?php $img ="thumb-".$p2->post_image;  echo $img ?>" class="img-responsive" alt="<?php echo $p2->post_title?>" />
							</div>
							<div class="info">
								<div class="row">
									<div class="price col-md-12">
									<?php echo substr($p2->post_title,0,100)?>...
									</div> 
								</div> 
								<div class="clearfix">
								</div>
							</div>
						</div>
					</div>
						</a>
						
					
					<?php
				   $i++;
				 }
	            }  ?>
					</div>
				</div>
				 
			</div>
		</div> 
	  
	</div>
	
	
	
    <!-- bollywood video container -->	
	<?php if(is_array($videos) && count($videos)  > 0 ) { ?>
		<div class="container bollywood_container">
	    <div class="row">
		<div class="col-sm-8">
			<a href ="<?php echo SITE_URL ?>videos"><h1>LL Tv</h1></a>
			<div class="clearfix"></div>
	<?php		
		foreach($videos as  $v){
    ?>
		
		
			<div class="video_block">
				<!--<object width="100%" height="380px" data="<?php echo $v->link;?> "></object>-->
			</div>
			<div class="video_name"><a href ="<?php echo SITE_URL ?>category/story/<?php echo $v->id ?>/<?php echo $v->seourl ?>">
			<p><?php echo $v->post_title ?></p>
			<div class="button"><button type="submit" class="btn btn-primary">Read Story</button></a></div>
			</div>
		
		<?php } ?>
		
		</div> 
		
		
		<?php } ?>
		
		<?php if(isset($video)&&$video != ""){ ?>
		<div class="col-sm-4 video_slides">
			<a href ="<?php echo SITE_URL ?>videos"><h1>MORE Videos</h1></a>
			<?php 
			foreach($video as $value){
			?>
		
			<!--
			 
			 <div class="controls pull-right" style="margin-top:30px;">
 				<a class="left fa fa-chevron-left btn btn1 btn-success" href="#carousel-example1" data-slide="prev"></a>
				<a class="right fa fa-chevron-right btn btn1 btn-success" href="#carousel-example1" data-slide="next"></a>
             </div>--> 
			<div class="clearfix"></div>
			
			<div class="video_lists">
				<a href="<?php echo SITE_URL ?>category/story/<?php echo $value->id ?>/<?php echo $value->seourl ?>">
					<div class="video_lists_image"><img src="<?php echo S3_URL?>site/images/posts/small_postimage_crop/<?php $img ="thumb-".$value->post_image;  echo $img ?>" class="img-responsive" alt="<?php echo $value->post_title ?>" />
                    </div>	
					<div class="video_lists_name"><?php echo substr($value->post_title,0,40)?>...</div>
					<!--<div class="video_lists_name1">1M VIews</div>-->
				</a>
			</div>
				<?php } ?>
			
			</div>
			</div> 
			</div>
			
			<?php }?>
			
		

	
    <!-- Misc container -->	
	<div class="container editors_pick_panel">
	   <div class="row">
	   <div class="col-sm-8">
		<?php if(is_array($movies) && count($movies) > 0)	 {?>
		<a href ="<?php echo SITE_URL ?>movies"><h1>Movies</h1></a>
		<div class="clearfix"></div>
		<div class="editors_pick">
		
		<?php
		foreach($movies as $q)	{?>
		
			
				<div class="editor_pick_videos">
				<a href="<?php echo SITE_URL ?>movie/<?php echo $q->seourl ?>">
					<div class="video_lists_image">
					<img src="<?php echo S3_URL?>site/images/movies/small_movies_images/<?php $img ="thumb-".$q->image;  echo $img ?>" class="img-responsive" alt="<?php echo $q->movie_name?>" />
					</div>
					<div class="video_lists_name"><?php echo $q->movie_name?></div>
					
					 <div class="video_lists_name1"><?php echo ucfirst($q->language) ?> </div> 
					
					
			    </a>  
				</div>
			
			<!--<div style="text-align: center;display:inline-block;width: 100%;">
		  	   <br> <br>  <button type="submit" class="btn  view-btn">View More</button>  
		   	</div>	<br><br><br>--> 
		<?php } 
		} ?>
		
		</div> 
		</div>	
				   
	   
		<div class="col-sm-4">
			
			<!-- BEGIN IFRAME EXT ASYNC TAG - 300x250 300x250 - DO NOT MODIFY -->
			<INS id='abp115141_300_250' style="display:inline-block;width:300px;height:250px;text-decoration:none;"></INS>
			<SCRIPT TYPE="text/javascript">
			(function(){
				var e = document.getElementById('abp115141_300_250');
				if (!e || e.innerHTML !== '')
					return;
				var __jscp=function(){for(var b=0,a=window;a!=a.parent;)++b,a=a.parent;if(a=window.parent==window?document.URL:document.referrer){var c=a.indexOf("://");0<=c&&(a=a.substring(c+3));c=a.indexOf("/");0<=c&&(a=a.substring(0,c))}var b={pu:a,"if":b,rn:new Number(Math.floor(99999999*Math.random())+1)},a=[],d;for(d in b)a.push(d+"="+encodeURIComponent(b[d]));return encodeURIComponent(a.join("&"))};

				e.innerHTML = '<iframe src="//cpm.rtbwire.com/tag?zone_id=115141&size=300x250&ad_type=iframe&j=' + __jscp() + '" width="300" height="250" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" allowtransparency="true"></iframe>';
			})();
			</SCRIPT>
			<!-- END TAG -->

		<!--<div class="subscribe-block" id="subscribe1">
		  <form  class="form-subscribe" id="newsletter_home" method="post" action="javascript:void(0)" onsubmit="javascript:return subscribeForm('_home');"> 
				<h2><span>Yaha Tak Aya Hain,</span><br> Subscribe toh banta hain Boss!!</h2>
				<br>
				<center><span id="subscribe_err_home" style="color:red;"></span></center>
                <center><span id="subscribe_succ_home" style="color:green;"></span></center>
			    <div class="form-group"> 
				<input type="text" class="form-control" id="name1_home" name="name1"  placeholder="Name"> 
				<input type="email" class="form-control" id="email1_home"  name="email1" placeholder="E-Mail">
				<div id="suggesstion-box1" class="suggesstion-box"></div>
			    <button type="submit" class="btn btn-default subscribe_btn" id="btn_subscribe">SUBSCRIBE</button>
			    </div> 
			</form>
			<p id="newsletter_su_home" class="thank_you hide_d">Thanks for your Subscribing
			
			</p> 
			<span class="popup_loader" id="loader_home"><img src="<?php echo S3_URL?>site/images/popup-loader.gif" alt=""></span>
		</div>-->
			
			
           
		</div> 
 		
		</div>	 
			
		
		<!-- locallaunde Ad Code-->
		
		<!--<div class="col-md-12" style="text-align: center;">
			Add Slot	
		</div>-->
		
		<br><br/>
		<!--<div class="col-md-12">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<ins class="adsbygoogle"
				 style="display:block"
				 data-ad-client="ca-pub-9625841015804508"
				 data-ad-slot="8062176989"
				 data-ad-format="auto"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		</div>-->
		
	</div>	
	