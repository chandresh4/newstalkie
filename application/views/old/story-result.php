<script>

$(document).ready(function(){
  $('#loader_s').hide();
});

</script><input type="hidden" id="site_url" name="site_url" value="<?php echo SITE_URL?>"  />

	<div id="d" hidden="hidden"><?php echo R_URL; ?></div>
	<style>
	label > input{ /* HIDE RADIO */
       visibility: hidden; /* Makes input not-clickable */
       position: absolute; /* Remove input from document flow */
    }
    label > input + img{ /* IMAGE STYLES */
       cursor:pointer;
       border:2px solid transparent;
    }
    label > input:checked + img{ /* (RADIO CHECKED) IMAGE STYLES */
       border:2px solid #f00;
    }
	</style>
	<script>
	$(document).ready(function() {
	 $('#smileys input').on('click', function() {
		var id = $("#pid").val();
		var value = $(this).val();
		var radioName = $(this).attr("name"); //Get radio name
        $(":radio[name='"+radioName+"']").attr("disabled", true);
		var s_u = $("#site_url").val();
		var t = "id="+id+"&"+"value="+value;
		$.ajax({
		url:s_u+"category/post_review",
		type:"post",
		data:t,
		beforeSend:function(){
	    },
		success:function(e){
			var res = e.split('_');
			if(res[0]=="happy"){
				$("#result1").html(res[1]);
				$("#img2").attr('src',s_u+'cdn/site/images/smiley/straight-inactive.jpg');
				$("#img3").attr('src',s_u+'cdn/site/images/smiley/sad-inactive.jpg');
				$("#img4").attr('src',s_u+'cdn/site/images/smiley/angry-inactive.jpg');
			}
			if(res[0]=="neutral"){
				$("#result2").html(res[1]);
				$("#img1").attr('src',s_u+'cdn/site/images/smiley/happy-inactive.jpg');
				$("#img3").attr('src',s_u+'cdn/site/images/smiley/sad-inactive.jpg');
				$("#img4").attr('src',s_u+'cdn/site/images/smiley/angry-inactive.jpg');
			}
			if(res[0]=="sad"){
				$("#result3").html(res[1]);
				$("#img2").attr('src',s_u+'cdn/site/images/smiley/straight-inactive.jpg');
				$("#img1").attr('src',s_u+'cdn/site/images/smiley/happy-inactive.jpg');
				$("#img4").attr('src',s_u+'cdn/site/images/smiley/angry-inactive.jpg');
			}
			if(res[0]=="angry"){
				$("#result4").html(res[1]);
				$("#img2").attr('src',s_u+'cdn/site/images/smiley/straight-inactive.jpg');
				$("#img3").attr('src',s_u+'cdn/site/images/smiley/sad-inactive.jpg');
				$("#img1").attr('src',s_u+'cdn/site/images/smiley/happy-inactive.jpg');
			}
			 //window.location.reload();
		}			
     });  
    });
  });
function createFBShareLink() {
	
	var title= $.trim($("#title").val());
	var path= $.trim($("#path").val());
	var description= $.trim($("#paragragh").val());
	var d=$("#d").val();
	var url = 'http://www.facebook.com/dialog/feed?app_id=' + '985979131559824' +
    '&link=' + encodeURIComponent(d) +
    '&picture=' +path+
	'&title=' + encodeURIComponent(title) + 
    '&caption=' + encodeURIComponent('info@locallaunde.com ') + 
    '&description=' + encodeURIComponent(description) + 
    //'&redirect_uri=' + FBVars.baseURL + 'Close.html' + 
    '&display='; 
    alert(url);
  return url; 
}

function openWindow() {
	
	var url = createFBShareLink(); 
    window.open(url, 
                'feedDialog', 
                'toolbar=0,status=0,width=626,height=436'
    ); 
}
</script>
	<div id="about" class="container banner_container box_container">
	  <div class="row">
		  <div class="breadcrumbs">
		  <?php 

		  if(isset($details) && $details!= "") {
			 foreach ($details as $p){ ?>
			<a href="<?php echo SITE_URL ?>">Home</a> / 
		 	<a href="<?php echo SITE_URL.$seourl?>"><?php echo $category ?></a> / <?php echo $p->post_title ?>
		 </div>
		
		 <div class="clearfix"></div> 
		 <div class="blog_desc">
		 
			 <h1><?php echo $p->post_title ?></h1>
			 <div class="clearfix"></div> 
			
			 <div class="clearfix"></div> 
			 <div class="blog_banner">
			 	<img src="<?php echo S3_URL?>site/images/posts/postimage_crop/<?php $img ="thumb-".$p->post_image;  echo $img ?>" class="img-responsive" alt="<?php echo $p->post_title ?>" />
			 </div>
			   <input type="hidden" name="pid" id="pid" value ="<?php echo $p->id;?>">
               <input type="hidden" name="title" id="title" value ="<?php echo $p->post_title;?>">
			   <input type="hidden" name="path" id="path" value ="<?php echo S3_URL?>site/images/posts/postimage_crop/<?php $img ="thumb-".$p->post_image;  echo $img ?>">
			  
			 <div class="row blog_content">
				  <h3><?php echo $p->post_sub_title;?></h3>
				 <p id ="paragragh"><?php echo $p->post_desciption;?></p>
				 <?php 
				  } 
				 }
				 ?>
				<?php if(isset($result) && $result!= "") {
					  $i=1;
			          foreach ($result as $q){ ?>
				 <div class="content_add">
				   <h4><?php echo $q->inner_post_title;?></h4>
				 <div class="row">
				  <div class="blog_banner">
						<img src="<?php echo S3_URL?>site/images/innerposts/large_innerpostimage_crop/<?php $img ="thumb-".$q->inner_post_image;  echo $img ?>" class="img-responsive" alt="<?php echo $q->inner_post_title;?>" />
			     </div>
					 <!--<div class="col-md-8">
						<img src="<?php echo S3_URL?>site/images/innerposts/large_innerpostimage_crop/<?php $img ="thumb-".$q->inner_post_image;  echo $img ?>" class="img-responsive" alt="a" />
					</div>--> 
					<?php if ($i % 2 != 0) { ?>
					<!-- locallaunde Ad Code-->
					<div class="col-md-10">
						<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
						<ins class="adsbygoogle"
							 style="display:block"
							 data-ad-client="ca-pub-9625841015804508"
							 data-ad-slot="8062176989"
							 data-ad-format="auto"></ins>
						<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
						</script>
					</div>
					<?php } ?>
				</div>
				</div>
				
				<p><?php echo $q->inner_post_desc;?> </p>
			
			<?php
			  $i++;
			 }
		 }?>
		 
		 <?php foreach ($details as $p){
			
           if(isset($p->link) && trim($p->link) != ''){ 	 ?>
		    <br>
			<div>
				<object width="100%" height="400" data="<?php echo $p->link;?> "></object>
			</div>
		   <?php }else{
			   
			   ?>
			   <div style="display:none">
			   </div>
			   <?php
		   }
		 } ?>
		  
			
			 
			
			<?php if(isset($tag) && $tag!=""){?>
			<br>
			<div class="tag_container"><?php
					echo "<strong>Tags:-</strong> ";
                       foreach($tag as $k){?>
					  
				 <a href="<?php echo SITE_URL ?>tags/<?php echo rtrim($k->seourl,"-") ?>" class="tags"><?php echo $k->tag_name ?></a>
					  <?php }
					  ?></div>	<?php
				}else{?>
				<div class="tag_container" style="display:none">
				</div>
				<?php }?>
				<!--<div style="margin:0; float:right" >
					
					<a href="javascript:void(0)" onclick="openWindow()" ><img style=" height: 21px; margin-top: -13px;" src="<?php echo S3_URL?>site/images/fshare.png"/></a>
				</div>-->

				<!-- <div class="writer">Source : &nbsp <?php echo ucfirst($writer_name)?>   <a  class="profile_pic" style=""><img src="<?php echo S3_URL?>site/images/profile_pic.jpg"></a><?php if($days != 0){ echo $days."days ago";}else{ echo "Today"; } ?></div> -->
				
				<div class="smiley_div">
				<span style="text-align:left">Your Reaction </span>
				<form id="smileys" >
				<label>
				<input type="radio" name="smiley" value="happy" class="happy" >
				<img src="<?php echo S3_URL?>site/images/smiley/happy.jpg" style="padding:3px" id="img1">
				<div style ="text-align :center"><span id="result1"><?php echo $happy?></span></div>
				</label>
				<label>
				<input type="radio" name="smiley" value="neutral" class="neutral">
				<img src="<?php echo S3_URL?>site/images/smiley/straight.jpg" style="padding:3px" id="img2">
				<div style ="text-align :center"><span id="result2"><?php echo $neutral?></span></div>
				</label>
				<label>
				<input type="radio" name="smiley" value="sad" class="sad">
				<img src="<?php echo S3_URL?>site/images/smiley/sad.jpg" style="padding:3px" id="img3" >
				<div style ="text-align :center"><span id="result3" ><?php echo $sad?></span></div>
				</label>
                <label>
				<input type="radio" name="smiley" value="angry" class="angry" >
				<img src="<?php echo S3_URL?>site/images/smiley/angry.jpg" style="padding:3px"id="img4" >
				<div style ="text-align :center"><span id="result4"><?php echo $angry?></span></div>
				</label>
	            </form>
				</div>
				
				<!-- locallaunde Ad Code-->
				<div class="col-md-10">
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<ins class="adsbygoogle"
						 style="display:block"
						 data-ad-client="ca-pub-9625841015804508"
						 data-ad-slot="8062176989"
						 data-ad-format="auto"></ins>
					<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
				</div>

		
		 </div>	
		</div>	
			 		 
	  </div>
	</div>
	<!--<div class="container box_container">
	   <div class="row">	
			<div class="col-md-push-3 col-sm-6">		
				<div class="subscribe-block">
					<form  class="form-subscribe" id="newsletter_s" method="post" action="javascript:void(0)" onsubmit="javascript:return subscribeForm('_s');"> 
						<h2><span>Yaha Tak Aya Hain,</span><br> Subscribe toh banta hain Boss!!!</h2>
						<br>
						<center><span id="subscribe_err_s" style="color:red;"></span></center>
						<center><span id="subscribe_succ_s" style="color:green;"></span></center>
						<div class="form-group"> 
						<div class="row">	
							<div class="col-sm-6">
								<input type="text" class="form-control" id="name1_s" name="name1"  placeholder="Name"> 
							</div>	
							<div class="col-sm-6">
								<input type="email" class="form-control" id="email1_s"  name="email1" placeholder="E-Mail">
							</div>	
						</div>			
						<div class="row">	
								
							<div class="col-sm-6">
								<button type="submit" class="btn btn-default subscribe_btn" id="btn_subscribe">SUBSCRIBE</button>
							</div>		
						</div>
						</div> 
					</form>
					<p id="newsletter_su_s" class="thank_you hide_d">Thanks for your Subscribing
			
			</p>
					<span class="popup_loader" id="loader_s"><img src="<?php echo S3_URL?>site/images/popup-loader.gif" alt=""></span>
				</div>
			</div>
		</div>
	</div>-->

	<!-- viral stories container -->	
	<div class="container box_container">
	   <div class="row">	
		<div class="col-sm-12">		
		<div class="viral_stories">	
			<h1>More Stories</h1>	 
		</div> 
		</div>	 		   
	   </div> 
	   <div id="carousel-example1" class="carousel slide" data-ride="carousel" data-interval="false">			   
			<div class="carousel-inner">
				<div class="item active">
					<div class="row viral_stories">
						<?php if(isset($result1) && $result1 != ""){
                         
		           foreach($result1 as $p2){
				  ?>
				   <a href="<?php echo SITE_URL ?>category/story/<?php echo $p2->id ?>/<?php echo $p2->seourl ?>">
						<div class="col-sm-4">
						<div class="col-item">
							<div class="photo">
								 <img src="<?php echo S3_URL?>site/images/posts/medium_postimage_crop/<?php $img ="thumb-".$p2->post_image;  echo $img ?>" class="img-responsive" alt="a" />
							</div>
							<div class="info">
								<div class="row">
									<div class="price col-md-12">
									<?php echo $p2->post_title?>
									</div> 
								</div> 
								<div class="clearfix">
								</div>
							</div>
						</div>
					</div>
						</a>
						
					
					<?php
				  
				 }
	            }  ?>
					</div>
				</div>
					
				
			</div>
		</div> 
	   <div class="row">	
		   <br> 
		  <div class="col-sm-12 text-center">
		  	<a href="<?php echo SITE_URL."category/search/Trendy"?>"><button type="submit" class="btn  view-btn">View More</button> </a>
		  </div>
		   <br><br><br>
	   </div>	   

	</div>
	