<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>Bootstrap Theme Company Page</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="cdn/site/css/bootstrap.min.css">
  <link rel="stylesheet" href="cdn/site/css/locallaunde.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
	
  <script src="cdn/site/js/jquery.min.js"></script>
  <script src="cdn/site/js/bootstrap.min.js"></script>
 
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<div class="wrapper">	
	<nav class="navbar navbar-default navbar-fixed-top" >
 	<div class="container mobile_Nav">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>   
      </button>
      <a class="navbar-brand" href="index.html"><img src="images/logo.png" alt=""></a>
	  <a href="registrationpage.html" class="submit_story"><img src="images/submit_story.png" alt=""></a>
    </div>
	
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#about">Trending</a></li>
        <li><a href="#services">News </a></li>
        <li><a href="#portfolio">Viral</a></li>
        <li><a href="#pricing">Sports</a></li>
        <li><a href="#contact">Mems</a></li>
		<li><a href="#contact">JOKES</a></li>
		<li><a href="#search" style="background:url(images/search.png) no-repeat center">&nbsp;</a></li>
      </ul>
    </div>
  </div>
	</nav> 
<div id="search">
    <button type="button" class="close">×</button>
    <form>
        <input type="search" value="" placeholder="Search Keyword" />
        <a href="searchpage.html"  class="btn btn-primary">Search</a>
    </form>
</div>
	<!-- banner container -->
	<a href="registrationpage.html" class="submit_story_mobile"><img src="images/submit_story.png" alt=""></a>
	<div id="about" class="container banner_container box_container">
	  <div class="row">
		<div class="col-sm-7">
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
 		 	<!-- Indicators -->
 		    <ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
		  </ol>

		  	<!-- Wrapper for slides -->
		    <div class="carousel-inner">
			  <div class="item active">
				  <a href="inner.html">
				  <img src="images/main_banner.jpg" alt=""></a></div> 
			  <div class="item"><a href="inner.html">
				  <img src="images/main_banner.jpg" alt=""></a></div> 
			  <div class="item"><a href="inner.html">
				  <img src="images/main_banner.jpg" alt=""></a></div> 
		  </div>
  
			</div>	
		    <div class="clearfix"></div>
			<h1 class="banner_title">This Pandya Brother's Pre-Wedding Shoot Theme Will Win Every Cricket Fan's Heart</h1>	
		</div>
		<div class="col-sm-5">
		  <div class="latest_news" > 
				<h2>LATEST News</h2> 
			    <div class="controls pull-right  ">
					<a class="left fa fa-chevron-left btn btn1 btn-success" href="#carousel-example" data-slide="prev"></a>
					<a class="right fa fa-chevron-right btn btn1 btn-success" href="#carousel-example" data-slide="next"></a>
				 </div>
				 
		  </div> 
		  <div id="carousel-example" class="carousel slide" data-ride="carousel">			   
				<div class="carousel-inner">
                <div class="item active">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="col-item">
								<a href="#" target="">
                                <div class="photo">
                                    <img src="images/img1.jpg" class="img-responsive" alt="a" />
                                </div>
                                <div class="info">
                                    <div class="row">
                                        <div class="ln_title col-md-12">
                                            <div class="ln_subject_name">Aiyaary Trailer Is Out And It Screams Of Patriotism But With A Twist.</div>  
                                        </div>
                                    </div> 
                                    <div class="clearfix">
                                    </div>
                                </div>
								</a>	
                            </div>
                        </div> 
                        <div class="col-sm-6">
                            <div class="col-item">
								<a href="#" target="">
                                <div class="photo">
                                    <img src="images/img1.jpg" class="img-responsive" alt="a" />
                                </div>
                                <div class="info">
                                    <div class="row">
                                        <div class="ln_title col-md-12">
                                            <div class="ln_subject_name">Aiyaary Trailer Is Out And It Screams Of Patriotism But With A Twist.</div> 
                                        </div>
                                    </div> 
                                    <div class="clearfix">
                                    </div>
                                </div>
								</a>	
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="col-item">
								<a href="#" target="">
                                <div class="photo">
                                    <img src="images/img1.jpg" class="img-responsive" alt="a" />
                                </div>
                                <div class="info">
                                    <div class="row">
                                        <div class="ln_title col-md-12">
                                            <div class="ln_subject_name">Aiyaary Trailer Is Out And It Screams Of Patriotism But With A Twist.</div> 
                                        </div>
                                    </div> 
                                    <div class="clearfix">
                                    </div>
                                </div>
								</a>	
                            </div>
                        </div> 
                        <div class="col-sm-6">
                            <div class="col-item">
								<a href="#" target="">
                                <div class="photo">
                                    <img src="images/img1.jpg" class="img-responsive" alt="a" />
                                </div>
                                <div class="info">
                                    <div class="row">
                                        <div class="ln_title col-md-12">
                                            <div class="ln_subject_name">Aiyaary Trailer Is Out And It Screams Of Patriotism But With A Twist.</div> 
                                        </div>
                                    </div> 
                                    <div class="clearfix">
                                    </div>
                                </div>
								</a>	
                            </div>
                        </div> 
                    </div> 	
                </div>
                <div class="item">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="col-item">
								<a href="#" target="">
                                <div class="photo">
                                    <img src="images/img1.jpg" class="img-responsive" alt="a" />
                                </div>
                                <div class="info">
                                    <div class="row">
                                        <div class="ln_title col-md-12">
                                            <div class="ln_subject_name">Aiyaary Trailer Is Out And It Screams Of Patriotism But With A Twist.</div> 
                                        </div>
                                    </div> 
                                    <div class="clearfix">
                                    </div>
                                </div>
								</a>	
                            </div>
                        </div> 
                        <div class="col-sm-6">
                            <div class="col-item">
								<a href="#" target="">
                                <div class="photo">
                                    <img src="images/img1.jpg" class="img-responsive" alt="a" />
                                </div>
                                <div class="info">
                                    <div class="row">
                                        <div class="ln_title col-md-12">
                                            <div class="ln_subject_name">Aiyaary Trailer Is Out And It Screams Of Patriotism But With A Twist.</div> 
                                        </div>
                                    </div> 
                                    <div class="clearfix">
                                    </div>
                                </div>
								</a>	
                            </div>
                        </div> 
                    </div>
					<div class="row">
                        <div class="col-sm-6">
                            <div class="col-item">
								<a href="#" target="">
                                <div class="photo">
                                    <img src="images/img1.jpg" class="img-responsive" alt="a" />
                                </div>
                                <div class="info">
                                    <div class="row">
                                        <div class="ln_title col-md-12">
                                            <div class="ln_subject_name">Aiyaary Trailer Is Out And It Screams Of Patriotism But With A Twist.</div> 
                                        </div>
                                    </div> 
                                    <div class="clearfix">
                                    </div>
                                </div>
								</a>	
                            </div>
                        </div> 
                        <div class="col-sm-6">
                            <div class="col-item">
								<a href="#" target="">
                                <div class="photo">
                                    <img src="images/img1.jpg" class="img-responsive" alt="a" />
                                </div>
                                <div class="info">
                                    <div class="row">
                                        <div class="ln_title col-md-12">
                                            <div class="ln_subject_name">Aiyaary Trailer Is Out And It Screams Of Patriotism But With A Twist.</div> 
                                        </div>
                                    </div> 
                                    <div class="clearfix">
                                    </div>
                                </div>
								</a>	
                            </div>
                        </div> 
                    </div>                      
                </div>
				 	
            </div>
		</div>	
		</div>
	  </div>
	</div>


	<!-- viral stories container -->	
	<div class="container box_container">
	   <div class="row">	
		<div class="col-sm-12">		
		<div class="viral_stories">	
			<h1>VIRAL Stories</h1>	 
			 <div class="controls pull-right  ">
 				<a class="left fa fa-chevron-left btn btn1 btn-success" href="#carousel-example1" data-slide="prev"></a>
				<a class="right fa fa-chevron-right btn btn1 btn-success" href="#carousel-example1" data-slide="next"></a>
             </div> 
		</div> 
		</div>	 		   
	   </div> 
	   <div id="carousel-example1" class="carousel slide" data-ride="carousel">			   
			<div class="carousel-inner">
				<div class="item active">
					<div class="row viral_stories">
						<div class="col-sm-4">
						<div class="col-item">
							<div class="photo">
								 <img src="images/img2.jpg" class="img-responsive" alt="a" />
							</div>
							<div class="info">
								<div class="row">
									<div class="price col-md-12">
									Top 10 Indian Entertainers Of 2017 On Google And What People Searched About Them.
									</div> 
								</div> 
								<div class="clearfix">
								</div>
							</div>
						</div>
					</div>
						<div class="col-sm-4">
						<div class="col-item">
							<div class="photo">
								 <img src="images/img22.jpg" class="img-responsive" alt="a" />
							</div>
							<div class="info">
								<div class="row">
									<div class="price col-md-12">
									Top 10 Indian Entertainers Of 2017 On Google And What People Searched About Them.
									</div> 
								</div> 
								<div class="clearfix">
								</div>
							</div>
						</div>
					</div>
						<div class="col-sm-4">
						<div class="col-item">
							<div class="photo">
								 <img src="images/img222.jpg" class="img-responsive" alt="a" />
							</div>
							<div class="info">
								<div class="row">
									<div class="price col-md-12">
									Top 10 Indian Entertainers Of 2017 On Google And What People Searched About Them.
									</div> 
								</div> 
								<div class="clearfix">
								</div>
							</div>
						</div>
					</div>
					</div>
				</div>
				<div class="item">
					<div class="row viral_stories">
						<div class="col-sm-4">
						<div class="col-item">
							<div class="photo">
								 <img src="images/img2.jpg" class="img-responsive" alt="a" />
							</div>
							<div class="info">
								<div class="row">
									<div class="price col-md-12">
									Top 10 Indian Entertainers Of 2017 On Google And What People Searched About Them.
									</div> 
								</div> 
								<div class="clearfix">
								</div>
							</div>
						</div>
					</div>
						<div class="col-sm-4">
						<div class="col-item">
							<div class="photo">
								 <img src="images/img22.jpg" class="img-responsive" alt="a" />
							</div>
							<div class="info">
								<div class="row">
									<div class="price col-md-12">
									Top 10 Indian Entertainers Of 2017 On Google And What People Searched About Them.
									</div> 
								</div> 
								<div class="clearfix">
								</div>
							</div>
						</div>
					</div>
						<div class="col-sm-4">
						<div class="col-item">
							<div class="photo">
								 <img src="images/img222.jpg" class="img-responsive" alt="a" />
							</div>
							<div class="info">
								<div class="row">
									<div class="price col-md-12">
									Top 10 Indian Entertainers Of 2017 On Google And What People Searched About Them.
									</div> 
								</div> 
								<div class="clearfix">
								</div>
							</div>
						</div>
					</div>
					</div>
				</div>
				 
			</div>
		</div> 
	</div>
	
	
	
    <!-- bollywood video container -->	
	<div class="container bollywood_container">
	   <div class="row">		 
		<div class="col-sm-8">
			<h1>ENTERTAINMENT</h1>
			<div class="clearfix"></div>
			<div class="video_block">
				<object width="100%" height="380px" data="https://www.youtube.com/embed/uGwK0mnCtpg"></object>
			</div>
			<div class="video_name">
			<p>Tiger Zinda Hai | Official Trailer | Salman Khan | Katrina Kaif
				(Release Date - 22 December 2017)</p>
			<div class="button"><button type="submit" class="btn btn-primary">BOOK TICKET</button></div>
			</div>
		</div> 		
		<div class="col-sm-4 video_slides">
			<h1>UPCOMING Movies</h1><!--
			 
			 <div class="controls pull-right" style="margin-top:30px;">
 				<a class="left fa fa-chevron-left btn btn1 btn-success" href="#carousel-example1" data-slide="prev"></a>
				<a class="right fa fa-chevron-right btn btn1 btn-success" href="#carousel-example1" data-slide="next"></a>
             </div>--> 
			<div class="clearfix"></div>
			<div class="video_lists">
				<a href="#">
					<div class="video_lists_image"><img src="images/video1.jpg" alt="" class="img-responsive"></div>	
					<div class="video_lists_name">Terminator 6: Reboot (2019) Trailer.</div>
					<div class="video_lists_name1">1M VIews</div>
				</a>
			</div>
			<div class="video_lists">
				<a href="#">
					<div class="video_lists_image"><img src="images/video1.jpg" alt="" class="img-responsive"></div>	
					<div class="video_lists_name">Terminator 6: Reboot (2019) Trailer.</div>
					<div class="video_lists_name1">1M VIews</div>
				</a>
			</div>
			<div class="video_lists">
				<a href="#">
					<div class="video_lists_image"><img src="images/video1.jpg" alt="" class="img-responsive"></div>	
					<div class="video_lists_name">Terminator 6: Reboot (2019) Trailer.</div>
					<div class="video_lists_name1">1M VIews</div>
				</a>
			</div>
			<div class="video_lists">
				<a href="#">
					<div class="video_lists_image"><img src="images/video1.jpg" alt="" class="img-responsive"></div>	
					<div class="video_lists_name">Terminator 6: Reboot (2019) Trailer.</div>
					<div class="video_lists_name1">1M VIews</div>
				</a>
			</div>
			<div class="video_lists">
				<a href="#">
					<div class="video_lists_image"><img src="images/video1.jpg" alt="" class="img-responsive"></div>	
					<div class="video_lists_name">Terminator 6: Reboot (2019) Trailer.</div>
					<div class="video_lists_name1">1M VIews</div>
				</a>
			</div>
		</div> 		   
	   </div> 
	</div>

	
    <!-- Misc container -->	
	<div class="container editors_pick_panel">
	   <div class="row">		 
		<div class="col-sm-8">
			<h1>EDITOR’s Pick</h1>
			<div class="clearfix"></div>
			<div class="editors_pick">
				<div class="editor_pick_videos">
				<a href="#">
					<div class="video_lists_image"><img src="images/video11.jpg" alt="" class="img-responsive"></div>	
					<div class="video_lists_name">Terminator 6: Reboot (2019) Trailer.</div>
					<div class="video_lists_name1">1M VIews</div>
				</a>
			</div>
			    <div class="editor_pick_videos">
				<a href="#">
					<div class="video_lists_image"><img src="images/video11.jpg" alt="" class="img-responsive"></div>	
					<div class="video_lists_name">Terminator 6: Reboot (2019) Trailer.</div>
					<div class="video_lists_name1">1M VIews</div>
				</a>
			</div> 
				<div class="editor_pick_videos">
				<a href="#">
					<div class="video_lists_image"><img src="images/video11.jpg" alt="" class="img-responsive"></div>	
					<div class="video_lists_name">Terminator 6: Reboot (2019) Trailer.</div>
					<div class="video_lists_name1">1M VIews</div>
				</a>
			</div>
			    <div class="editor_pick_videos">
				<a href="#">
					<div class="video_lists_image"><img src="images/video11.jpg" alt="" class="img-responsive"></div>	
					<div class="video_lists_name">Terminator 6: Reboot (2019) Trailer.</div>
					<div class="video_lists_name1">1M VIews</div>
				</a>
			</div> 				<div class="editor_pick_videos">
				<a href="#">
					<div class="video_lists_image"><img src="images/video11.jpg" alt="" class="img-responsive"></div>	
					<div class="video_lists_name">Terminator 6: Reboot (2019) Trailer.</div>
					<div class="video_lists_name1">1M VIews</div>
				</a>
			</div>
			    <div class="editor_pick_videos">
				<a href="#">
					<div class="video_lists_image"><img src="images/video11.jpg" alt="" class="img-responsive"></div>	
					<div class="video_lists_name">Terminator 6: Reboot (2019) Trailer.</div>
					<div class="video_lists_name1">1M VIews</div>
				</a>
			</div> 				<div class="editor_pick_videos">
				<a href="#">
					<div class="video_lists_image"><img src="images/video11.jpg" alt="" class="img-responsive"></div>	
					<div class="video_lists_name">Terminator 6: Reboot (2019) Trailer.</div>
					<div class="video_lists_name1">1M VIews</div>
				</a>
			</div>
			      				 
			    <div class="editor_pick_videos">
				<a href="#">
					<div class="video_lists_image"><img src="images/video11.jpg" alt="" class="img-responsive"></div>	
					<div class="video_lists_name">Terminator 6: Reboot (2019) Trailer.</div>
					<div class="video_lists_name1">1M VIews</div>
				</a>
			</div> 		  				
			</div>
			
			<div style="text-align: center;display:inline-block;width: 100%;">
		  	   <br> <br>  <button type="submit" class="btn  view-btn">View More</button>  
		   	</div>	<br><br><br> 
		</div> 		
		<div class="col-sm-4">
			<h1>FREElANCER Register</h1>
			<div class="clearfix"></div>
			<a href="#"><img src="images/freelancer.jpg" alt="" class="img-responsive"></a>
		</div> 		   
	   </div> 
		  
		   	
	</div>	
	
 
	<!-- Footer part -->
<footer class="page-footer">
    <!--Footer Links-->
    <div class="container-fluid">
        <div class="row">  
            <!--Second column-->
            <div class="col-md-3 col-sm-6">
                <h5 class="title">TRENDING TAGS</h5>
                <ul>
                    <li><a href="#!">Salman Khan</a></li>
                    <li><a href="#!">Bigg Boss</a></li>
                    <li><a href="#!">Start Up</a></li>
                    <li><a href="#!">Bride</a></li>
                    <li><a href="#!">Krunal Pande</a></li>
                    <li><a href="#!">Ms Dhoni</a></li>
                </ul>
            </div>
			<div class="col-md-3 col-sm-6">
                <h5 class="title">CATEGORIES</h5>
                <ul>
                    <li><a href="#!">Science & Technology</a></li>
                    <li><a href="#!">Lifestyle</a></li>
                    <li><a href="#!">Travel & Adventure</a></li>
                    <li><a href="#!">Animals</a></li>
                    <li><a href="#!">History & Culture</a></li>
                    <li><a href="#!">Health & Fitness</a></li>
                </ul>
            </div>
			<div class="col-md-3 col-sm-6">
                <h5 class="title">LOCALLAUNDE</h5>
                <ul>
                    <li><a href="#!">About Us </a></li>
                    <li><a href="#!">Terms of Use </a></li>
                    <li><a href="#!">Privacy Policy </a></li>
                    <li><a href="registrationpage.html">Submit a Story</a></li>
                    <li><a href="#!">Careers </a></li>
                    <li><a href="#!">Contact Us</a></li>
                </ul>
            </div>
			<div class="col-md-3 col-sm-6">
                <h5 class="title">TRENDING TAGS</h5>
                <div class="social-btns">
					<a class="btn facebook" href="#"><i class="fa fa-facebook"></i></a>
					<a class="btn twitter" href="#"><i class="fa fa-twitter"></i></a>
					<a class="btn dribbble" href="#"><i class="fa fa-dribbble"></i></a>
					<a class="btn skype" href="#"><i class="fa fa-skype"></i></a>
				</div>
            </div>
        </div>
    </div>
    <!--/.Footer Links-->

    <!--Copyright-->
    <div class="footer-copyright">
        <div class="container-fluid">© 2017 Locallaunde. All rights reserved.</div>
    </div>
    <!--/.Copyright-->

</footer>

</div>
<script>
$(document).ready(function(){
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
  
  $(window).scroll(function() {
    $(".slideanim").each(function(){
      var pos = $(this).offset().top;

      var winTop = $(window).scrollTop();
        if (pos < winTop + 600) {
          $(this).addClass("slide");
        }
    });
  });
})
</script>
<script>	
$(function () {
    $('a[href="#search"]').on('click', function(event) {
        event.preventDefault();
        $('#search').addClass('open');
        $('#search > form > input[type="search"]').focus();
    });
    
    $('#search, #search button.close').on('click keyup', function(event) {
        if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
            $(this).removeClass('open');
        }
    });
    
    
    //Do not include! This prevents the form from submitting for DEMO purposes only!
    $('form').submit(function(event) {
        event.preventDefault();
        return false;
    })
});	
</script>	

</body>
</html>
