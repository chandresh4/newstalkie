<input type="hidden" id="site_url" name="site_url" value="<?php echo SITE_URL?>"  />
<script type="text/javascript">
      var limit = 9
      var offset = 0;
      function displayRecords(lim, off,cate) {
	  var s_u = $("#site_url").val();
      $.ajax({
          type: "GET",
          async: false,
          url: s_u+"news/getdata",
          data: "limit=" + lim + "&offset=" + off+"&category="+cate,
          cache: false,
          beforeSend: function() {
          
          },
          success: function(html) {
		
          var data = html.split("|");
		 
          $("#results").append(data[0]);
            if (html == "") {
              $("#loader_message").html('').show()
            } else {
			if(data[1] =="less"){
				$("#loader_message").html('').show()
            } else {
              $("#loader_message").html('<button class="btn btn-default" type="button">View more </button>').show();
            }
		  }
          }
        });
      } 

      $(document).ready(function() {
		  var category = $('#category').val();
		  
		  // start to load the first set of data
          displayRecords(limit,offset,category);
          $('#loader_message').click(function() {
          // if it has no more records no need to fire ajax request
          var d = $('#loader_message').find("button").attr("data-atr");
          if (d != "nodata") {
            offset = limit + offset;
            displayRecords(limit, offset,category);
          }
        
        });
 
      });

</script>
<div id="about" class="container banner_container box_container">
	<div class="row"> 
		<div class="col-sm-12 inner_banner_bg">
			<div class="news_banner">
				<h1 style ="color:white;text-shadow: 1px 1px 5px black;font-size:15px">News</h1>
			</div>
		</div>	
		
		
		 
		<!--<div class="col-sm-12 search_box"> 
			<div class="search_name"> Search Results For <span><?php echo $res; ?></span></div> 
		</div>-->
		<input type="hidden" id="category" name="category" value="<?php echo $seourl; ?>">
        <div class="clearfix"></div>
		 <div class="viral_stories" id= "results">
			
		 </div>
        <div id="loader_message"></div> 		
		</div> 
		</div> 
		 

	<div class="clearfix"></div> 
	</div>
</div><!---->