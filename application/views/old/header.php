<!DOCTYPE html>

<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
    <?php 
	
	if(isset($metadata) && $metadata != ""){
?>
	
	<title><?php echo $metadata['title'] ?></title>
	<meta name="description" content="<?php echo $metadata['description'] ?>">
    <meta name="keywords" content="<?php echo $metadata['keywords'] ?>">
	<?php }else{ ?>
	<title>Trending News, Top Trending Topics, Viral & Funny videos & Popular news</title>
	<meta name="description" content=" Newstalkie Trending & Latest News – Find Most Trending topics,Trending videos, viral Stories, Politics & News Articles. Also Science & Technology, Lifestyle, Travel & Adventure, Popular news, Viral videos, Photos, sports, latest news and politics. Most popular trending topics & more in social media.">
    <meta name="keywords" content="Top Trending news, Viral videos, politics, fashion, health&fitness, science&technology, Travel&adventure, history&culture, Lifestyle">
	<?php } ?>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="google-site-verification" content="A-5Ge7cN2zZ6jFnObHP5l5HaR00JK4vilMRkc5TLIxw" />
	<meta name="msvalidate.01" content="B74D1BCF7AC97402CD3305E26DDE8A59" />
	
	<link rel="stylesheet" href="<?php echo S3_URL?>site/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo S3_URL?>site/css/locallaunde.css">
	<link rel="stylesheet" href="<?php echo S3_URL?>site/css/select-popover.css">	
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
	 <link href="<?php echo S3_URL?>site/images/favicon.ico" rel="icon" type="image/x-icon"/>
	<!--<script src="<?php echo S3_URL?>site/js/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
	<script src="<?php echo S3_URL?>site/js/bootstrap.min.js"></script>
	<script src="<?php echo S3_URL?>site/js/common.js"></script>
	<script src="<?php echo S3_URL?>site/js/topbar.js"></script>
	
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119354313-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-119354313-1');
	</script>
	
	<?php
	if($_SERVER['SERVER_NAME'] == "www.newstalkie.com" || $_SERVER['SERVER_NAME'] == "newstalkie.com"){
	?>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-163805997-1"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-163805997-1');
	</script>

<?php } ?>



</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
<div class="wrapper">	

<?php
if($_SERVER['SERVER_NAME'] == "www.newstalkie.com" || $_SERVER['SERVER_NAME'] == "newstalkie.com"){
?>


	<nav class="navbar navbar-default navbar-fixed-top newstalkie" >
 	<div class="container mobile_Nav">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar" style="background-color: #fff;"></span>
        <span class="icon-bar" style="background-color: #fff;"></span>
        <span class="icon-bar" style="background-color: #fff;"></span>   
      </button>

	 <a class="navbar-brand newstalkieImageSec" href="<?php echo SITE_URL?>"><img src="<?php echo S3_URL?>site/images/newstalkie-logo.svg" alt=""></a>

		

	<!-- <a href="<?php echo SITE_URL."submit-content"?>" class="submit_story"><img src="<?php echo S3_URL?>site/images/submit_story_2.png" alt=""></a> -->
    </div>
	
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
		<li><a class="whiteTxt latest" href="<?php echo SITE_URL."tags/corona-virus"?>">Corona Virus</a></li>
        <li class="active whiteTxt"><a href="<?php echo SITE_URL."trending-stories"?>">Trending</a></li>
        <li><a class="whiteTxt" href="<?php echo SITE_URL."entertainment"?>">Entertainment</a></li>
        <li><a class="whiteTxt new" href="<?php echo SITE_URL."games"?>" style="width: 100px">Games</a></li>
        <li><a class="whiteTxt" href="<?php echo SITE_URL."health-fitness"?>">Health Tips</a></li>
        <!--<li><a href="<?php echo SITE_URL."memes"?>">Memes</a></li>-->
		<li><a class="whiteTxt" href="<?php echo SITE_URL."politics"?>" >Politics</a></li>
		<li><a href="#search" id ="searching" style="background:url(<?php echo S3_URL?>site/images/white_search.png) no-repeat center">&nbsp;</a></li>
		  
      </ul>
    </div>
  </div>
</nav>

<?php }else{?>

	<nav class="navbar navbar-default navbar-fixed-top" >
 	<div class="container mobile_Nav">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>   
      </button>

	 <a class="navbar-brand" href="<?php echo SITE_URL?>"><img src="<?php echo S3_URL?>site/images/logo.png" alt=""></a>

		

	<a href="<?php echo SITE_URL."submit-content"?>" class="submit_story"><img src="<?php echo S3_URL?>site/images/submit_story.png" alt=""></a>
    </div>
	
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
		<li><a href="<?php echo SITE_URL."tags/corona-virus"?>">Corona Virus</a></li>
        <li class="active"><a href="<?php echo SITE_URL."trending-stories"?>">Trending</a></li>
        <li><a href="<?php echo SITE_URL."news"?>">News </a></li>
        <li><a href="<?php echo SITE_URL."viral-stories"?>">Viral</a></li>
        <li><a href="<?php echo SITE_URL."sports"?>">Sports</a></li>
        <!--<li><a href="<?php echo SITE_URL."memes"?>">Memes</a></li>-->
		<li><a href="<?php echo SITE_URL."politics"?>">Politics</a></li>
		<li><a href="#search" id ="searching" style="background:url(<?php echo S3_URL?>site/images/search.png) no-repeat center">&nbsp;</a></li>
		  
      </ul>
    </div>
  </div>
</nav>


<?php
} ?>

	<div id="search">
		<button type="button" class="close">×</button>
		<form action="<?php echo SITE_URL ?>search">
		  
			<input type="search" value="" placeholder="Search Keyword" id="val" name="val"/>
			
			<button type="submit" onclick="cate_search()"  class="btn btn-primary">Search</button>
		</form>
	</div>