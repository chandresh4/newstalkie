

  <?php 
    
  if($_SERVER['SERVER_NAME'] == "www.newstalkie.com" || $_SERVER['SERVER_NAME'] == "newstalkie.com"){
  
  ?>

<!-- Footer part -->
<footer class="page-footer newstalkieFooter">

    <!--Footer Links-->
    <div class="container-fluid">
        <div class="row">  
            <!--Second column-->
            <div class="col-md-3 col-sm-6">
                <h5 class="title">TRENDING TAGS</h5>
                <ul>
				   <?php 
				   $query1    = $this->db->query("select tag from tbl_post where status = 1 order by id desc limit 5");
	         $i=0;
					 if ( $query1->num_rows() > 0) {
                foreach($query1->result() as $q){
								  $tags[]=$q->tag;
							}
					 }
					 $a = $arr = "";
					 $i=0;
					 
					 foreach($tags as $k => $v){
						 $special_chars = ",";
                         $string = $v;
                         if (preg_match('/'.$special_chars.'/', $string))
                         {
                             $a = explode(',',$v);
						 }else{
							 $a= (array)$v;
						 }
						 $arr[]= $a[0]; 
						 
                     }						 
					 
					$tag=$arr1="";
					$arr1 =array_filter($arr);
					$arr=array_unique($arr1);
					foreach($arr as $k=>$v){
						$tag.=$v.",";
					}
					$tag = rtrim($tag,',');
          if($tag != ""){  
					   $query1    = $this->db->query("select t_id,tag_name,seourl from tbl_tags where status = 1 and t_id in ( ".$tag.") ");
	                  if ( $query1->num_rows() > 0) {
		                    foreach($query1->result() as $q){
			                ?> <li><a href="<?php echo SITE_URL ?>tags/<?php echo rtrim($q->seourl,"-") ?>"><?php echo $q->tag_name ?> </a>
						<?php 	   }
					         }
            }
				   ?>
                   </ul>
            </div>
			<div class="col-md-3 col-sm-6">
                <h5 class="title">CATEGORIES</h5>
                <ul>
                    <li><a href="<?php echo SITE_URL."science-technology"?>">Science & Technology</a></li>
                    <li><a href="<?php echo SITE_URL."lifestyle"?>">Lifestyle</a></li>
                    <li><a href="<?php echo SITE_URL."travel-adventure"?>">Travel & Adventure</a></li>
                    <li><a href="<?php echo SITE_URL."entertainment"?>">Entertainment</a></li>
                    <li><a href="<?php echo SITE_URL."history-culture"?>">History & Culture</a></li>
                    <li><a href="<?php echo SITE_URL."health-fitness"?>">Health & Fitness</a></li>
					<li><a href="<?php echo SITE_URL."inspiration"?>">Inspiration</a></li>
                </ul>
            </div>
			<div class="col-md-3 col-sm-6">
                <h5 class="title"><?php echo strtoupper(SITE_NAME) ?></h5>
                <ul>
                    <li><a href="<?php echo SITE_URL ?>about-us">About Us </a></li>
                    <li><a href="<?php echo SITE_URL ?>info/privacypolicy">Privacy Policy </a></li>
                    <li><a href="<?php echo SITE_URL ?>info/copyrightnotice ">Copyright Notice</a></li>
					<li><a href="<?php echo SITE_URL ?>info/terms-of-service ">Terms & Conditions</a></li>
                    <li><a href="<?php echo SITE_URL ?>info/disclaimer ">Disclaimer</a></li>
                    <li><a href="<?php echo SITE_URL ?>contact-us ">Contact Us</a></li>
                </ul>
            </div>
			<div class="col-md-3 col-sm-6">
                <h5 class="title">Follow Us</h5>
                <div class="social-btns">
					<a class="btn facebook" href="https://www.facebook.com/pg/Locallaunde-1787528351543601" target="_blank"><i class="fa fa-facebook" style="color:#fff;font-size:22px;"></i></a>
					<!--<a class="btn dribbble" href="https://plus.google.com/103965511116498203072" target="_blank"><i class="fa fa fa-google-plus" style="color:#fff;font-size: 22px;"></i></a>-->
					<a class="btn skype" href="https://www.instagram.com/locallaunde2007/" target="_blank"><i class="fa fa-instagram" style="color:#fff;font-size:22px;"></i></a>
				</div>
            </div>
        </div>
    </div>
    <!--/.Footer Links-->

    <!--Copyright-->
    <div class="footer-copyright">
        <div class="container-fluid">© 2020 <?php echo ucfirst(SITE_NAME) ?>. All rights reserved.</div>
    </div>
    <!--/.Copyright-->

</footer>


  <?php }else{ ?>

    
<!-- Footer part -->
<footer class="page-footer">

    <!--Footer Links-->
    <div class="container-fluid">
        <div class="row">  
            <!--Second column-->
            <div class="col-md-3 col-sm-6">
                <h5 class="title">TRENDING TAGS</h5>
                <ul>
           <?php 
           $query1    = $this->db->query("select tag from tbl_post where status = 1 order by id desc limit 5");
           $i=0;
           if ( $query1->num_rows() > 0) {
                foreach($query1->result() as $q){
                  $tags[]=$q->tag;
              }
           }
           $a = $arr = "";
           $i=0;
           
           foreach($tags as $k => $v){
             $special_chars = ",";
                         $string = $v;
                         if (preg_match('/'.$special_chars.'/', $string))
                         {
                             $a = explode(',',$v);
             }else{
               $a= (array)$v;
             }
             $arr[]= $a[0]; 
             
                     }             
           
          $tag=$arr1="";
          $arr1 =array_filter($arr);
          $arr=array_unique($arr1);
          foreach($arr as $k=>$v){
            $tag.=$v.",";
          }
          $tag = rtrim($tag,',');
          if($tag != ""){  
             $query1    = $this->db->query("select t_id,tag_name,seourl from tbl_tags where status = 1 and t_id in ( ".$tag.") ");
                    if ( $query1->num_rows() > 0) {
                        foreach($query1->result() as $q){
                      ?> <li><a href="<?php echo SITE_URL ?>tags/<?php echo rtrim($q->seourl,"-") ?>"><?php echo $q->tag_name ?> </a>
            <?php      }
                   }
            }
           ?>
                   </ul>
            </div>
      <div class="col-md-3 col-sm-6">
                <h5 class="title">CATEGORIES</h5>
                <ul>
                    <li><a href="<?php echo SITE_URL."science-technology"?>">Science & Technology</a></li>
                    <li><a href="<?php echo SITE_URL."lifestyle"?>">Lifestyle</a></li>
                    <li><a href="<?php echo SITE_URL."travel-adventure"?>">Travel & Adventure</a></li>
                    <li><a href="<?php echo SITE_URL."entertainment"?>">Entertainment</a></li>
                    <li><a href="<?php echo SITE_URL."history-culture"?>">History & Culture</a></li>
                    <li><a href="<?php echo SITE_URL."health-fitness"?>">Health & Fitness</a></li>
          <li><a href="<?php echo SITE_URL."inspiration"?>">Inspiration</a></li>
                </ul>
            </div>
      <div class="col-md-3 col-sm-6">
                <h5 class="title"><?php echo strtoupper(SITE_NAME) ?></h5>
                <ul>
                    <li><a href="<?php echo SITE_URL ?>about-us">About Us </a></li>
                    <li><a href="<?php echo SITE_URL ?>info/privacypolicy">Privacy Policy </a></li>
                    <li><a href="<?php echo SITE_URL ?>info/copyrightnotice ">Copyright Notice</a></li>
          <li><a href="<?php echo SITE_URL ?>info/terms-of-service ">Terms & Conditions</a></li>
                    <li><a href="<?php echo SITE_URL ?>info/disclaimer ">Disclaimer</a></li>
                    <li><a href="<?php echo SITE_URL ?>contact-us ">Contact Us</a></li>
                </ul>
            </div>
      <div class="col-md-3 col-sm-6">
                <h5 class="title">Follow Us</h5>
                <div class="social-btns">
          <a class="btn facebook" href="https://www.facebook.com/pg/Locallaunde-1787528351543601" target="_blank"><i class="fa fa-facebook" style="color:#000;font-size:22px;"></i></a>
          <a class="btn dribbble" href="https://plus.google.com/103965511116498203072" target="_blank"><i class="fa fa fa-google-plus" style="color:#000;font-size: 22px;"></i></a>
          <a class="btn skype" href="https://www.instagram.com/locallaunde2007/" target="_blank"><i class="fa fa-instagram" style="color:#000;font-size:22px;"></i></a>
        </div>
            </div>
        </div>
    </div>
    <!--/.Footer Links-->

    <!--Copyright-->
    <div class="footer-copyright">
        <div class="container-fluid">© 2018 <?php echo date('Y')." ".ucfirst(SITE_NAME) ?>. All rights reserved.</div>
    </div>
    <!--/.Copyright-->

</footer>



  <?php } ?>

</div>

<script>
 /* $(document).ready(function(){
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
  
  $(window).scroll(function() {
    $(".slideanim").each(function(){
      var pos = $(this).offset().top;

      var winTop = $(window).scrollTop();
        if (pos < winTop + 600) {
          $(this).addClass("slide");
        }
    });
  });
}) */
</script>
<script>	
/*$(function () {
     $('a[href="#search"]').on('click', function(event) {
        event.preventDefault();
        $('#search').addClass('open');
        $('#search > form > input[type="search"]').focus();
    });
    
    $('#search, #search button.close').on('click keyup', function(event) {
        if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
            $(this).removeClass('open');
        }
    });
    
    
    //Do not include! This prevents the form from submitting for DEMO purposes only!
    $('form').submit(function(event) {
        event.preventDefault();
        return false;
    })
});	 */
</script>	
<!--<script src="cdn/site/js/multiselect.js"></script>-->
</body>
</html>

