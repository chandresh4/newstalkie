<?php $country_code = (isset($_COOKIE["countrytest"]) != "" ? $_COOKIE["countrytest"] : "" ); 

$web_RightTop_300_250=$web_980_160Top=$web_980_160Bottom=$web_300_250_center=$mob_RightTop_300_250=$mob_980_160Top=$mob_980_160Bottom=$mob_300_250_center="";

$header_margin = "headermargin";

if( is_array($category_wise_ads) && count($category_wise_ads) > 0){

    foreach($category_wise_ads as $ads_list){

        if($ads_list->ads_position == 1){

            //300_250_RightTop
            $web_RightTop_300_250 = $ads_list->web_ads;
            $mob_RightTop_300_250 = $ads_list->mob_ads;

        }else if($ads_list->ads_position == 2) {

            //980_160Top
            $web_980_160Top = $ads_list->web_ads;
            $mob_980_160Top = $ads_list->mob_ads;

        }else if($ads_list->ads_position == 3){

            //980x160Bottom
            $web_980_160Bottom = $ads_list->web_ads;
            $mob_980_160Bottom = $ads_list->mob_ads;

        }else if($ads_list->ads_position == 4){

            //300_250_center
            $web_300_250_center = $ads_list->web_ads;
            $mob_300_250_center = $ads_list->mob_ads;

        }
    }

}
?>

    <?php 
                            
        if($web_980_160Top != "" ){
        $header_margin = "";
    ?>        

            <section class="categoryAdvertiseSec headerMargin webView">

                <div class="col-md-12 ">
            
                    <?php echo $web_980_160Top; ?>

                    

                </div>

            </section>

    <?php
        } 
    ?>
        
    <?php 

        if($mob_980_160Top != "" ){
        $header_margin = "";
    ?>            
            <section class="categoryAdvertiseSec headerMargin mobView">

                <div class="col-md-12">
                
                    <?php echo $mob_980_160Top; ?>

                </div>

            </section>
    <?php                 

        } 
    ?>

<div class="container-fluid">

    <div class="col-md-9 paddingZero headerMargin">

        <section class="indiaTopSecNews SportsHeaderContainSec">

            <h3 class="boldFont"> Sports 
                <span class="sportsRoutingLinks"> <a href="<?php echo SITE_URL?>sports/"> | Indian Premier League 2021</a></span>
            </h3>

            <h3 class="boldFont">Live Cricket Score </h3>

            <p class="boldFont" style="padding-left: 1em;">
                <span class="sportsRoutingLinks blueText"> <a href="<?php echo SITE_URL?>sports/index/live" class="<?php echo ($currentMatchState == 'live') ? 'blueText' : ''; ?>" > Live </a> | </span> 
                <span class="sportsRoutingLinks blueText"> <a href="<?php echo SITE_URL?>sports/index/completed" class="<?php echo ($currentMatchState == 'completed') ? 'blueText' : '' ?>"> Recent </a> | </span>  
                <span class="sportsRoutingLinks blueText"> <a href="<?php echo SITE_URL?>sports/index/upcoming" class="<?php echo ($currentMatchState == 'upcoming') ? 'blueText' : ''; ?>"> Upcoming </a> </span>
            </p>

            <div class="row">

                <div class="col-md-12 iplScoreCardHolder">

                    <?php 

                    $all_array = all_arrays();

                    if(is_array($matches_list) && count($matches_list) > 0){

                        //SORTING THE MATCH ORDER DATE WISE

                        if( $currentMatchState == "upcoming") {                       

                            $keys = array_column_($matches_list, 'id');

                            array_multisort($keys, SORT_ASC, $matches_list);
                        }else{

                            $keys = array_column_($matches_list, 'id');
                            
                            array_multisort($keys, SORT_DESC, $matches_list);
                        }

                    foreach($matches_list as $list){ 
                            
                            if($list->homeTeam->name != "TBC"){
                        
                            $match_time_string = strtotime($list->startDateTime);
                            $match_date_time =  date('M d h:i A', $match_time_string);
                            
                        ?>

                    <div class="col-md-4 iplScoreParentHolder">

                        <div class="col-md-12 matchResultHolderSec">

                            <p class="teamMatchHeading"> <?php echo $list->homeTeam->name ?> Vs <?php echo $list->awayTeam->name ?></p>

                            <p class="matchVenue"><?php echo  $match_date_time ?>  <?php echo  $list->venue->name ?> </p>

                            <div class="col-md-4 teams">
                                <img src="<?php echo S3_URL?>site/images/ipl/<?php echo $all_array['IPL_TEAM_NAME'][$list->homeTeam->name]?>.png" alt="" class="img-responsive">
                                <p class="teamName"><?php echo $all_array['IPL_TEAM_NAME'][$list->homeTeam->name];  ?></p>
                                <?php if(isset($list->scores)) { ?>
                                    <p class="teamScore"><?php echo $list->scores->homeScore ?></p>
                                    <p class="oversPlayed"><?php echo $list->scores->homeOvers ?> Overs</p>
                                <?php } ?>
                            </div>

                            <div class="col-md-4 teams pull-right">
                                <img src="<?php echo S3_URL?>site/images/ipl/<?php echo $all_array['IPL_TEAM_NAME'][$list->awayTeam->name]?>.png" alt="" class="img-responsive">
                                <p class="teamName"><?php echo $all_array['IPL_TEAM_NAME'][$list->awayTeam->name];  ?></p>
                                <?php if(isset($list->scores)) { ?>
                                <p class="teamScore"><?php echo $list->scores->awayScore ?></p>
                                <p class="oversPlayed"><?php echo $list->scores->awayOvers ?> Overs</p>
                            <?php } ?>
                            </div>

                            <?php if(isset($list->scores)) { ?>

                                <div class="col-md-12 matchSummaryContainer">
                                    <p class="matchSummary"><?php echo $list->matchSummaryText ?></p>
                                </div>

                            <?php } ?>
                            
                        </div>

                        <?php if(isset($list->scores)) { ?>

                            <div class="col-md-12 detailsLinks">

                                <a href="<?php echo SITE_URL."sports/live_cricket_scorecard/".$list->series->id."/".$list->id ?>" > Scorecard </a> 

                                <!--<a href="<?php echo SITE_URL."sports/live_cricket_score/".$list->series->id."/".$list->id ?>" > Live Score </a> |
                                <a href="<?php echo SITE_URL."sports/live_cricket_scorecard/".$list->series->id."/".$list->id ?>" > Scorecard </a> |
                                <a href="<?php echo SITE_URL."live_cricket_full_commentary/".$list->series->id."/".$list->id ?>" > Full Commentary </a>-->
                            </div>

                        <?php } ?>

                        </div>

                    <?php } ?>    
                    

                <?php } 

                }else{
                   
                    echo "<p class='noLiveFeed'>There are no Live matches at the moment. Please check back later.</p>";  

               }
                ?>

                </div>

            </div>

            <div class="row">
                                        
                <div class="col-md-12 indiaTopNewsItemHolder paddingZero" style="margin-top: 2em">

                    <div class="col-md-6 paddingZero">

                        <div class="col-md-12 paddingZeroRight bigNews">


                        <?php        
                        if(isset($result) && count($result) > 0){
                           if(isset($result[0])){

                           $result_0 =  $result[0];
                           
                        ?>    

                            <div class="bigNewsImageSec">
                                <a href="<?php echo SITE_URL ?>category/story/<?php echo $result_0->id ?>/<?php echo $result_0->seourl ?>">
                                    <img src="<?php echo S3_URL?>site/images/posts/small_postimage_crop/<?php $img ="thumb-".$result_0->post_image;  echo $img ?>" alt="">
                                </a>    
                            </div>

                            <div class="bigNewsContentSec linearBackground pointer" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $result_0->id ?>/<?php echo $result_0->seourl ?>' ">

                                <h3 class="semiBold whiteTxt"><?php echo substr(stripcslashes($result_0->post_title),0,50) ?></h3>

                            </div>

                
                        <?php 
                            } 
                        }
                       
                        ?>    
                        </div>

                        <div class="col-md-6 paddingZeroRight marginTop15 mobView">

                            <!-- BEGIN JS EXT TAG - 340x160 NewsTalkie IN_prashanthi_340x160_EntertainmentAD1 - DO NOT MODIFY -->
                            <SCRIPT TYPE="text/javascript">
                            window.__jscp=function(){if(window.parent==window)var a=document.URL;else if(a=document.referrer,!a)try{a=window.parent.location.href}catch(e){}if(a){var b=a.indexOf("://");0<=b&&(a=a.substring(b+3));b=a.indexOf("/");0<=b&&(a=a.substring(0,b))}b=0;for(var c=window;c!=c.parent;)++b,c=c.parent;a={pu:a,"if":b,rn:new Number(Math.floor(99999999*Math.random())+1)};b=[];for(var d in a)b.push(d+"="+encodeURIComponent(a[d]));return encodeURIComponent(b.join("&"))};
                            document.write('<S' + 'CRIPT TYPE="text/javascript" SRC="//cpm.rtbwire.com/tag?zone_id=135645&size=340x160&j=' + __jscp() + '"></S' + 'CRIPT>');
                            </SCRIPT>
                            <!-- END TAG -->

                        </div>

                        <div class="col-md-6 paddingZeroRight marginTop15 mobmarginbottom15 mobView">

                            <!-- BEGIN JS EXT TAG - 340x160 NewstalkieIN_prashanthi_340x160_EntertainmentAD2 - DO NOT MODIFY -->
                            <SCRIPT TYPE="text/javascript">
                            window.__jscp=function(){if(window.parent==window)var a=document.URL;else if(a=document.referrer,!a)try{a=window.parent.location.href}catch(e){}if(a){var b=a.indexOf("://");0<=b&&(a=a.substring(b+3));b=a.indexOf("/");0<=b&&(a=a.substring(0,b))}b=0;for(var c=window;c!=c.parent;)++b,c=c.parent;a={pu:a,"if":b,rn:new Number(Math.floor(99999999*Math.random())+1)};b=[];for(var d in a)b.push(d+"="+encodeURIComponent(a[d]));return encodeURIComponent(b.join("&"))};
                            document.write('<S' + 'CRIPT TYPE="text/javascript" SRC="//cpm.rtbwire.com/tag?zone_id=135647&size=340x160&j=' + __jscp() + '"></S' + 'CRIPT>');
                            </SCRIPT>
                            <!-- END TAG -->

                        </div>



                        <div class="col-md-12 paddingZero ">
                            <?php
                            if(is_array($result) && count($result) > 0){ 
                               
                               for($i=1; $i < 7 ; $i++){

                                   if(isset($result[$i])){
                               ?>
    
                            <div class="col-md-6 paddingZeroRight marginTop15">

                                <div class="col-md-12 paddingZero">

                                    <div class="smallNewsSecondImageLeftSection">
                                        
                                        <a href="<?php echo SITE_URL ?>category/story/<?php echo $result[$i]->id ?>/<?php echo $result[$i]->seourl ?>">
                                            <img src="<?php echo S3_URL?>site/images/posts/small_postimage_crop/<?php $img ="thumb-".$result[$i]->post_image;  echo $img ?>" alt="">
                                        </a>
                                    </div>

                                    <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $result[$i]->id ?>/<?php echo $result[$i]->seourl ?>' ">
                                        <h5 class="semiBold"><?php echo substr(stripcslashes($result[$i]->post_title),0,50)?></h5>
                                    </div>

                                </div>
                            
                            </div>

                            <?php }
                                }   
                            } ?>

                            

                            <!-- For Ad Space -->

                            <?php if( $i == 8 && $web_300_250_center != "" ) { ?>

                            <div class="col-md-4 paddingZeroLeft mobileBottomSpace">
                                <?php echo $web_300_250_center; ?>
                            </div>

                            <?php } ?>   

                            <?php if( $i == 8 && $mob_300_250_center != "" ) { ?>

                            <div class="col-md-4 paddingZeroLeft mobileBottomSpace">
                                <?php echo $mob_300_250_center; ?>
                            </div>

                            <?php } ?>   


                        </div>
                    
                    </div>

                    <div class="col-md-6 lifestyleTopRighttNews">
                     <?php
                        if(is_array($result) && count($result) > 0){ 
                           
                           for($i=8; $i < 10 ; $i++){

                                if(isset($result[$i])){
                           
                           ?>
                    
                        <div class="col-md-6 paddingZeroRight">

                            <div class="col-md-12 paddingZero">

                                <div class="smallNewsSecondImageLeftSection maxHeight15">
                                    <a href="<?php echo SITE_URL ?>category/story/<?php echo $result[$i]->id ?>/<?php echo $result[$i]->seourl ?>">
                                        <img src="<?php echo S3_URL?>site/images/posts/small_postimage_crop/<?php $img ="thumb-".$result[$i]->post_image;  echo $img ?>" alt="">
                                    </a>
                                </div>

                                <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $result[$i]->id ?>/<?php echo $result[$i]->seourl ?>'">
                                    <h5 class="semiBold"><?php echo substr(stripcslashes($result[$i]->post_title),0,50)?></h5>
                                </div>

                                <p class="pageTag">
                                    <?php echo substr(stripcslashes($result[$i]->post_sub_title),0,50) ?>
                                <p>

                            </div>

                        </div>

                       <?php   } 
                            }

                        }?> 


                        <div class="clearfix"></div>

                        <div class="col-md-12 webView">

                        <!-- BEGIN JS EXT TAG - 980x160 Newstalkie IN_prashanthi_980*160_Ad 1_entertainment - DO NOT MODIFY -->
                        <?php 

                        if($web_980_160Bottom != "" ){
                            
                            echo $web_980_160Bottom;

                        } 
                        ?>
                        <!-- END TAG -->
                        </div>

                        <div class="clearfix"></div>

                        <br>

                        <div class="col-md-12 mobView">
                        <!-- BEGIN JS EXT TAG - 980x160 Newstalkie IN_prashanthi_980*160_Ad 1_entertainment - DO NOT MODIFY -->
                        
                        <?php 

                            if($mob_980_160Bottom != "" ){
                                
                                echo $mob_980_160Bottom;

                            } 
                        ?>

                        <!-- END TAG -->
                        </div>

                        <!-- Mobile View Ads Starts -->

                        <div class="col-md-12 margintop15 mobView">

                            <!-- BEGIN JS EXT TAG - 300x160 Newstalkie_IN_300x160_Sports AD1_Mobile - DO NOT MODIFY -->
                            <SCRIPT TYPE="text/javascript">
                            window.__jscp=function(){if(window.parent==window)var a=document.URL;else if(a=document.referrer,!a)try{a=window.parent.location.href}catch(e){}if(a){var b=a.indexOf("://");0<=b&&(a=a.substring(b+3));b=a.indexOf("/");0<=b&&(a=a.substring(0,b))}b=0;for(var c=window;c!=c.parent;)++b,c=c.parent;a={pu:a,"if":b,rn:new Number(Math.floor(99999999*Math.random())+1)};b=[];for(var d in a)b.push(d+"="+encodeURIComponent(a[d]));return encodeURIComponent(b.join("&"))};
                            document.write('<S' + 'CRIPT TYPE="text/javascript" SRC="//cpm.rtbwire.com/tag?zone_id=135633&size=300x160&site_cat=IAB12&j=' + __jscp() + '"></S' + 'CRIPT>');
                            </SCRIPT>
                            <!-- END TAG -->

                        </div>

                        <div class="col-md-12 margintop15 mobView">

                            <!-- BEGIN JS EXT TAG - 300x160 Newstalkie_IN_300x160_Sports AD2_Mobile - DO NOT MODIFY -->
                            <SCRIPT TYPE="text/javascript">
                            window.__jscp=function(){if(window.parent==window)var a=document.URL;else if(a=document.referrer,!a)try{a=window.parent.location.href}catch(e){}if(a){var b=a.indexOf("://");0<=b&&(a=a.substring(b+3));b=a.indexOf("/");0<=b&&(a=a.substring(0,b))}b=0;for(var c=window;c!=c.parent;)++b,c=c.parent;a={pu:a,"if":b,rn:new Number(Math.floor(99999999*Math.random())+1)};b=[];for(var d in a)b.push(d+"="+encodeURIComponent(a[d]));return encodeURIComponent(b.join("&"))};
                            document.write('<S' + 'CRIPT TYPE="text/javascript" SRC="//cpm.rtbwire.com/tag?zone_id=135634&size=300x160&site_cat=IAB12&j=' + __jscp() + '"></S' + 'CRIPT>');
                            </SCRIPT>
                            <!-- END TAG -->

                        </div>


                        <!-- Mobile View Ads Ends -->

                        <?php
                        if(is_array($result) && count($result) > 0){ 
                           
                           for($i=10; $i < 20 ; $i++){

                                if(isset($result[$i])){
                           
                           ?>

                            <div class="col-md-6 paddingZeroRight marginTop15" >

                                <div class="col-md-12 paddingZero">

                                    <div class="smallNewsSecondImageLeftSection maxHeight15">
                                        <a href="<?php echo SITE_URL ?>category/story/<?php echo $result[$i]->id ?>/<?php echo $result[$i]->seourl ?>">
                                            <img src="<?php echo S3_URL?>site/images/posts/small_postimage_crop/<?php $img ="thumb-".$result[$i]->post_image;  echo $img ?>" alt="">
                                        </a>
                                    </div>

                                    <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $result[$i]->id ?>/<?php echo $result[$i]->seourl ?>'">
                                        <h5 class="semiBold">    <?php echo substr(stripcslashes($result[$i]->post_title),0,50); ?></h5>
                                    </div>

                                </div>
                            
                            </div>

                        <?php }
                            }
                        } ?>    

                    </div>
                
                </div>

            </div>

        </section>        
          
    </div>

    <div class="col-md-3 paddingZero newHeaderMargin">

        <div class="partnerAdSec marginTop15">

            <?php 
            if($web_RightTop_300_250 != "" ){
                $header_margin = "";
            ?>

                <div class="col-md-12 webView marginBottom15">
                
                <?php echo $web_RightTop_300_250; ?>

                </div>
            

            <?php
                } 
            ?>

                
            <?php 
            if($mob_RightTop_300_250 != "" ){
                $header_margin = "";
            ?>

                <div class="col-md-12 mobView marginBottom15">
                
                <?php echo $mob_RightTop_300_250; ?>

                </div>
            
            <?php
                } 
            ?>
        
            <div class="col-md-12 <?php echo $header_margin ?>">

                <?php
                if(is_array($result) && count($result) > 0){ 
                   
                   for($i=4; $i < 7 ; $i++){

                       if(isset($result[$i])){
                   ?>

                    <div class="col-md-12 paddingZero">

                        <div class="smallNewsSecondImageLeftSection">
                            <a href="<?php echo SITE_URL ?>category/story/<?php echo $result[$i]->id ?>/<?php echo $result[$i]->seourl ?>">
                                <img src="<?php echo S3_URL?>site/images/posts/small_postimage_crop/<?php $img ="thumb-".$result[$i]->post_image;  echo $img ?>" alt="">
                            </a>
                            
                        </div>

                        <div class="smallNewsSecondContentSection marginTop10">
                            <h5 class="semiBold headingH5"><?php echo stripcslashes($result[$i]->post_title); ?></h5>
                        </div>

                    </div>

                <?php 
                        }
                    }
                } 
                ?>

                <!-- <div class="col-md-12 quickLinks">

                    <div class="smallNewsSecondContentSection marginTop10">
                        <h5 class="semiBold headingH5"> Quick Links </h5>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Unlock 5 Guidelines</a>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Deals of the Day</a>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Bihar Elections</a>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Coronavirus Live</a>
                    </div>
                
                </div> -->

                <div class="clearfix"></div>

                <div class="col-md-12 paddingZero">
                    <!-- BEGIN JS EXT TAG - 300x160 Newstalkie_Sports_300*160_Down_RCB article - DO NOT MODIFY -->
                    <SCRIPT TYPE="text/javascript">
                    window.__jscp=function(){if(window.parent==window)var a=document.URL;else if(a=document.referrer,!a)try{a=window.parent.location.href}catch(e){}if(a){var b=a.indexOf("://");0<=b&&(a=a.substring(b+3));b=a.indexOf("/");0<=b&&(a=a.substring(0,b))}b=0;for(var c=window;c!=c.parent;)++b,c=c.parent;a={pu:a,"if":b,rn:new Number(Math.floor(99999999*Math.random())+1)};b=[];for(var d in a)b.push(d+"="+encodeURIComponent(a[d]));return encodeURIComponent(b.join("&"))};
                    document.write('<S' + 'CRIPT TYPE="text/javascript" SRC="//cpm.rtbwire.com/tag?zone_id=135194&size=300x160&site_cat=IAB17&j=' + __jscp() + '"></S' + 'CRIPT>');
                    </SCRIPT>
                    <!-- END TAG -->
                </div>

                <div class="clearfix"></div>

                <br>

                <div class="col-md-12 paddingZero">
                    <!-- BEGIN JS EXT TAG - 300x250 Newstalkie_300*250_right_ganguli article - DO NOT MODIFY -->
                    <SCRIPT TYPE="text/javascript">
                    window.__jscp=function(){if(window.parent==window)var a=document.URL;else if(a=document.referrer,!a)try{a=window.parent.location.href}catch(e){}if(a){var b=a.indexOf("://");0<=b&&(a=a.substring(b+3));b=a.indexOf("/");0<=b&&(a=a.substring(0,b))}b=0;for(var c=window;c!=c.parent;)++b,c=c.parent;a={pu:a,"if":b,rn:new Number(Math.floor(99999999*Math.random())+1)};b=[];for(var d in a)b.push(d+"="+encodeURIComponent(a[d]));return encodeURIComponent(b.join("&"))};
                    document.write('<S' + 'CRIPT TYPE="text/javascript" SRC="//cpm.rtbwire.com/tag?zone_id=135080&size=300x250&site_cat=IAB17&j=' + __jscp() + '"></S' + 'CRIPT>');
                    </SCRIPT>
                    <!-- END TAG -->
                </div>

                <div class="clearfix"></div>

                <br>

                <div class="col-md-12 paddingZero">
                    <SCRIPT TYPE="text/javascript">
                    window.__jscp=function(){if(window.parent==window)var a=document.URL;else if(a=document.referrer,!a)try{a=window.parent.location.href}catch(e){}if(a){var b=a.indexOf("://");0<=b&&(a=a.substring(b+3));b=a.indexOf("/");0<=b&&(a=a.substring(0,b))}b=0;for(var c=window;c!=c.parent;)++b,c=c.parent;a={pu:a,"if":b,rn:new Number(Math.floor(99999999*Math.random())+1)};b=[];for(var d in a)b.push(d+"="+encodeURIComponent(a[d]));return encodeURIComponent(b.join("&"))};
                    document.write('<S' + 'CRIPT TYPE="text/javascript" SRC="//cpm.rtbwire.com/tag?zone_id=135082&size=300x250&site_cat=IAB17&j=' + __jscp() + '"></S' + 'CRIPT>');
                    </SCRIPT>
                    <!-- END TAG -->
                </div>

                <div class="clearfix"></div>

                <br>

                <div class="col-md-12 paddingZero">
                    <!-- BEGIN JS EXT TAG - 300x160 Newstalkie_Eruditus_300*160_Right - DO NOT MODIFY -->
                    <SCRIPT TYPE="text/javascript">
                    window.__jscp=function(){if(window.parent==window)var a=document.URL;else if(a=document.referrer,!a)try{a=window.parent.location.href}catch(e){}if(a){var b=a.indexOf("://");0<=b&&(a=a.substring(b+3));b=a.indexOf("/");0<=b&&(a=a.substring(0,b))}b=0;for(var c=window;c!=c.parent;)++b,c=c.parent;a={pu:a,"if":b,rn:new Number(Math.floor(99999999*Math.random())+1)};b=[];for(var d in a)b.push(d+"="+encodeURIComponent(a[d]));return encodeURIComponent(b.join("&"))};
                    document.write('<S' + 'CRIPT TYPE="text/javascript" SRC="//cpm.rtbwire.com/tag?zone_id=135448&size=300x160&site_cat=IAB17&j=' + __jscp() + '"></S' + 'CRIPT>');
                    </SCRIPT>
                    <!-- END TAG -->
                </div>
            
            </div>

        </div>

    </div>

    <div class="clearfix"></div>

    <!-- <section class="moreAds">

        <div class="container">

            <h3 class="boldFont" style="margin-bottom: 2em;"> MORE FOR YOU </h3>

            <div class="col-md-12">
            
                <div class="col-md-3 adItems">
                    <img src="<?php echo S3_URL?>site/images/videoes/ads.png" alt="Newslakie News" class="img-responsive">
                    <p class="boldFont">
                        The power of 4 devices in 1 @ ₹5699
                    </p>
                </div>

                <div class="col-md-3 adItems">
                    <img src="<?php echo S3_URL?>site/images/videoes/ads.png" alt="Newslakie News" class="img-responsive">
                    <p class="boldFont">
                        #KeepAWatch on your Vitals In Real Time with GOQii
                    </p>
                </div>

                <div class="col-md-3 adItems">
                    <img src="<?php echo S3_URL?>site/images/videoes/ads.png" alt="Newslakie News" class="img-responsive">
                    <p class="boldFont">
                        Power new possibilities
                    </p>
                </div>

                <div class="col-md-3 adItems">
                    <img src="<?php echo S3_URL?>site/images/videoes/ads.png" alt="Newslakie News" class="img-responsive">
                    <p class="boldFont">
                        Power new possibilities
                    </p>
                </div>
            
            </div>

        </div>

    </section> -->

</div>                          
                
                               