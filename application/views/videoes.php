
<div class="container-fluid">

    <section class="iconAdWidget newHeaderMargin">

        <div class="col-md-12">

            <div class="adItemSlider">
                <div>your content 1</div>
                <div>your content 2</div>
                <div>your content 3</div>
                <div>your content 4</div>
                <div>your content 5</div>
                <div>your content 6</div>
                <div>your content 7</div>
                <div>your content 8</div>
                <div>your content 9</div>
                <div>your content 10</div>
                <div>your content 11</div>
                <div>your content 12</div>
                <div>your content 13</div>
                <div>your content 14</div>
                <div>your content 15</div>
                <div>your content 16</div>
                <div>your content 17</div>
                <div>your content 18</div>
                <div>your content 19</div>
            </div>  

        </div>

    </section>

    <div class="col-md-9 paddingZero">

        <section class="indiaTopSecNews">

            <div class="row">
                                        
                <div class="col-md-12 indiaTopNewsItemHolder paddingZero">

                    <div class="col-md-8 paddingZeroRight bigNews">

                        <video controls>
                            <source src="https://adcancdn.s3.ap-southeast-1.amazonaws.com/smak-concepts/video/Maulim%20Nagar-Smak.mp4" type="video/mp4">
                            <source src="mov_bbb.ogg" type="video/ogg">
                            Your browser does not support HTML video.
                        </video>

                        <br>

                        <p class="boldFont marginTop15">The Times of India</p>

                        <h3 class="boldFont">
                            Sonam Kapoor shares a funny video of hubby Anand Ahuja
                        </h3>

                        <p style="line-height: 22px;">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy 
                            nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. 
                            Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit 
                            lobortis nisl ut aliquip ex ea commodo consequat. 
                            
                            <br>

                            Duis autem vel eum iriure dolor 
                            in hendrerit in vulputate velit esse molestie consequat. vel illum dolore eu feugiat nulla facilisis
                            
                            <br>

                            at vero eros et accumsan et iusto odio dignissim qui blandit praesent 
                            luptatum zzril delenit augue duis dolore te feugait nulla facilisi. 
                            Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy .
                        </p>
                
                    </div>
                    

                    <div class="col-md-4 lifestyleTopRighttNews">

                        <h5 class="semiBold headingH5">Now Playing: Today</h5>

                        <div class="col-md-12 paddingZero">

                            <div class="col-md-4 smallNewsSecondImageLeftSection paddingZero">
                                <a href="">
                                    <img src="<?php echo S3_URL?>site/images/videoes/current.png" alt="Newslakie News">
                                </a>
                            </div>

                            <div class="col-md-8 smallNewsSecondContentSection pointer paddingZeroRight" onclick="">
                                <p class="semiBold"> Sonam Kapoor shares a funny video of hubby Anand Ahuja </p>
                            </div>

                        </div>

                        <div class="clearfix"></div>

                        <h5 class="semiBold headingH5">Up Next</h5>

                        <div class="col-md-12 paddingZero marginTop10">

                            <div class="col-md-4 smallNewsSecondImageLeftSection paddingZero">
                                <a href="">
                                    <img src="<?php echo S3_URL?>site/images/videoes/next-up-one.png" alt="Newslakie News">
                                </a>
                            </div>

                            <div class="col-md-8 smallNewsSecondContentSection pointer paddingZeroRight" onclick="">
                                <p class="semiBold"> Coronavirus Qs, Asked By Priyanka Chopra And Answered By WHO Chiefs </p>
                            </div>

                        </div>

                        <div class="col-md-12 paddingZero marginTop10">

                            <div class="col-md-4 smallNewsSecondImageLeftSection paddingZero">
                                <a href="">
                                    <img src="<?php echo S3_URL?>site/images/videoes/next-up-two.png" alt="Newslakie News">
                                </a>
                            </div>

                            <div class="col-md-8 smallNewsSecondContentSection pointer paddingZeroRight" onclick="">
                                <p class="semiBold"> "Little Humour Doesn't Hurt": Omar Abdullah's Meme On Detention </p>
                            </div>

                        </div>

                        <div class="col-md-12 paddingZero marginTop10">

                            <div class="col-md-4 smallNewsSecondImageLeftSection paddingZero">
                                <a href="">
                                    <img src="<?php echo S3_URL?>site/images/videoes/next-up-three.png" alt="Newslakie News">
                                </a>
                            </div>

                            <div class="col-md-8 smallNewsSecondContentSection pointer paddingZeroRight" onclick="">
                                <p class="semiBold"> Mr PM, Can We Move Beyond Announcing A Task Force? </p>
                            </div>

                        </div>

                    </div>
                
                </div>

            </div>

        </section>

        <section class="weather">

            <div class="col-md-12">

                <h3 class="boldFont"> Karnataka WEATHER </h3>

                <form class="searchForm" action="/action_page.php">
                    <input type="text" class="searchInput" placeholder="Enter city, town or region" name="search">
                    <button type="submit"><i class="fa fa-search"></i></button>
                </form>

                <div class="clearfix"></div>

                <div class="weatherMeter">

                    <div class="weathers">

                        <div class="weatherImg">
                            <img src="<?php echo S3_URL?>site/images/weather/cloudy.png" alt="" class="img-responsive">
                        </div>

                        <div class="weatherParms">
                            <p class="daySec">MON</p>
                            <p class="todayTemp"> <b> 33°C </b> </p>
                            <p class="yesterdayTemp">25°C</p>
                        </div>

                    </div>

                    <div class="weathers">

                        <div class="weatherImg">
                            <img src="<?php echo S3_URL?>site/images/weather/cloudy.png" alt="" class="img-responsive">
                        </div>

                        <div class="weatherParms">
                            <p class="daySec">TUE</p>
                            <p class="todayTemp"> <b> 33°C </b> </p>
                            <p class="yesterdayTemp">25°C</p>
                        </div>

                    </div>

                    <div class="weathers">

                        <div class="weatherImg">
                            <img src="<?php echo S3_URL?>site/images/weather/cloudy.png" alt="" class="img-responsive">
                        </div>

                        <div class="weatherParms">
                            <p class="daySec">WED</p>
                            <p class="todayTemp"> <b> 33°C </b> </p>
                            <p class="yesterdayTemp">25°C</p>
                        </div>

                    </div>

                    <div class="weathers">

                        <div class="weatherImg">
                            <img src="<?php echo S3_URL?>site/images/weather/sunny.png" alt="" class="img-responsive">
                        </div>

                        <div class="weatherParms">
                            <p class="daySec">THU</p>
                            <p class="todayTemp"> <b> 33°C </b> </p>
                            <p class="yesterdayTemp">25°C</p>
                        </div>

                    </div>

                </div>

            </div>

        </section>
        
          
    </div>

    <div class="col-md-3 paddingZero">

        <div class="partnerAdSec advHeaderMargin">
        
            <div class="col-md-12">

                <div class="col-md-12 paddingZero">

                    <div class="smallNewsSecondImageLeftSection">
                        <a href="https://www.newstalkie.com/category/story/35/war-of-the-giants">
                            <img src="https://www.newstalkie.com/cdn/site/images/posts/small_postimage_crop/thumb-6.jpg" alt="">
                        </a>
                        
                    </div>

                    <div class="smallNewsSecondContentSection marginTop10">
                        <h5 class="semiBold headingH5">War of the Giants..</h5>
                    </div>

                </div>

                <div class="col-md-12 quickLinks">

                    <div class="smallNewsSecondContentSection marginTop10">
                        <h5 class="semiBold headingH5"> Quick Links </h5>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Unlock 5 Guidelines</a>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Deals of the Day</a>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Bihar Elections</a>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Coronavirus Live</a>
                    </div>
                
                </div>
            
            </div>

        </div>

    </div>

    <div class="clearfix"></div>

    <section class="moreAds">

        <div class="container">

            <h3 class="boldFont" style="margin-bottom: 2em;"> MORE FOR YOU </h3>

            <div class="col-md-12">
            
                <div class="col-md-3 adItems">
                    <img src="<?php echo S3_URL?>site/images/videoes/ads.png" alt="Newslakie News" class="img-responsive">
                    <p class="boldFont">
                        The power of 4 devices in 1 @ ₹5699
                    </p>
                </div>

                <div class="col-md-3 adItems">
                    <img src="<?php echo S3_URL?>site/images/videoes/ads.png" alt="Newslakie News" class="img-responsive">
                    <p class="boldFont">
                        #KeepAWatch on your Vitals In Real Time with GOQii
                    </p>
                </div>

                <div class="col-md-3 adItems">
                    <img src="<?php echo S3_URL?>site/images/videoes/ads.png" alt="Newslakie News" class="img-responsive">
                    <p class="boldFont">
                        Power new possibilities
                    </p>
                </div>

                <div class="col-md-3 adItems">
                    <img src="<?php echo S3_URL?>site/images/videoes/ads.png" alt="Newslakie News" class="img-responsive">
                    <p class="boldFont">
                        Power new possibilities
                    </p>
                </div>
            
            </div>

        </div>

    </section>

</div>                          
                
                               