<?php $country_code = (isset($_COOKIE["countrytest"]) != "" ? $_COOKIE["countrytest"] : "" );  

$web_RightTop_300_250=$web_980_160Top=$web_980_160Bottom=$web_300_250_center=$mob_RightTop_300_250=$mob_980_160Top=$mob_980_160Bottom=$mob_300_250_center="";

$header_margin = "headermargin";

if( is_array($category_wise_ads) && count($category_wise_ads) > 0){

    foreach($category_wise_ads as $ads_list){

        if($ads_list->ads_position == 1){

            //300_250_RightTop
            $web_RightTop_300_250 = $ads_list->web_ads;
            $mob_RightTop_300_250 = $ads_list->mob_ads;

        }else if($ads_list->ads_position == 2) {

            //980_160Top
            $web_980_160Top = $ads_list->web_ads;
            $mob_980_160Top = $ads_list->mob_ads;

        }else if($ads_list->ads_position == 3){

            //980x160Bottom
            $web_980_160Bottom = $ads_list->web_ads;
            $mob_980_160Bottom = $ads_list->mob_ads;

        }else if($ads_list->ads_position == 4){

            //300_250_center
            $web_300_250_center = $ads_list->web_ads;
            $mob_300_250_center = $ads_list->mob_ads;

        }
    }

}

?>

            

            <?php 
                                    
                if($web_980_160Top != "" ){
                $header_margin = "";
            ?>        

                    <section class="categoryAdvertiseSec headerMargin webView">

                        <div class="col-md-12 ">
                    
                            <?php echo $web_980_160Top; ?>

                            

                        </div>

                    </section>

            <?php
                } 
            ?>
                
            <?php 
        
                if($mob_980_160Top != "" ){
                $header_margin = "";
            ?>            
                    <section class="categoryAdvertiseSec headerMargin mobView">

                        <div class="col-md-12">
                        
                            <?php echo $mob_980_160Top; ?>

                        </div>

                    </section>
            <?php                 

                } 
            ?>

            <div class="container-fluid">

                <div class="col-md-9 paddingZero <?php echo $header_margin ?> ">

                    <section class="indiaTopSecNews">

                        <div class="row">
                                                    
                            <div class="col-md-12 indiaTopNewsItemHolder">


                            <?php 
                               if(is_array($result) && count($result) > 0){ 
                               $i=0;
                               foreach($result as $r){
                               if($i < 2){
                               ?>

                                <div class="col-md-6">

                                    <div class="col-md-4 indiaTopNewsImgSec">
                                        <a href="<?php echo SITE_URL ?>category/story/<?php echo $r->id ?>/<?php echo $r->seourl ?>">
                                            <img src="<?php echo S3_URL?>site/images/posts/small_postimage_crop/<?php $img ="thumb-".$r->post_image;  echo $img ?>" alt="">
                                        </a>
                                    </div>

                                    <div class="col-md-8 indiaTopNewsContentSec" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $r->id ?>/<?php echo $r->seourl ?>' ">

                                        <h5 class="boldFont">
                                            <?php echo stripcslashes($r->post_title) ?>
                                        </h5>

                                        <span class="dateTimePlace"><?php echo date('l, jS F Y', strtotime($r->date_created)); ?></span>

                                        <p>
                                            <?php echo substr($r->post_title,0,40)?>
                                        </p>
                                    </div>
                                </div>
                            <?php 
                                        } 
                                   $i++;
                                    }
                                }
                            ?>

                            </div>

                            <div class="col-md-12 indiaTopNewsItemHolder">

                            <?php 
                               if(is_array($result) && count($result) > 0){ 
                               $i=0;
                               foreach($result as $r){
                               if($i < 4 && $i > 1){
                               ?>

                                <div class="col-md-6">

                                    <div class="col-md-4 indiaTopNewsImgSec">
                                        <a href="<?php echo SITE_URL ?>category/story/<?php echo $r->id ?>/<?php echo $r->seourl ?>">
                                            <img src="<?php echo S3_URL?>site/images/posts/small_postimage_crop/<?php $img ="thumb-".$r->post_image;  echo $img ?>" alt="">
                                        </a>
                                    </div>

                                    <div class="col-md-8 indiaTopNewsContentSec" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $r->id ?>/<?php echo $r->seourl ?>' ">

                                        <h5 class="boldFont">
                                            <?php echo stripcslashes($r->post_title) ?>
                                        </h5>

                                        <span class="dateTimePlace"><?php echo date('l, jS F Y', strtotime($r->date_created)); ?></span>

                                        <p>
                                            <?php echo substr($r->post_title,0,40)?>
                                        </p>
                                    </div>
                                </div>
                            <?php 
                                        } 
                                   $i++;
                                    }
                                }
                            ?>

                            </div>

                            <div class="col-md-12 indiaTopNewsItemHolder">

                            <?php 
                               if(is_array($result) && count($result) > 0){ 
                               $i=0;
                               foreach($result as $r){
                               if($i < 6 && $i > 3){
                               ?>

                                <div class="col-md-6" >

                                    <div class="col-md-4 indiaTopNewsImgSec">
                                        <a href="<?php echo SITE_URL ?>category/story/<?php echo $r->id ?>/<?php echo $r->seourl ?>">
                                            <img src="<?php echo S3_URL?>site/images/posts/small_postimage_crop/<?php $img ="thumb-".$r->post_image;  echo $img ?>" alt="">
                                        </a>
                                    </div>

                                    <div class="col-md-8 indiaTopNewsContentSec" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $r->id ?>/<?php echo $r->seourl ?>' ">

                                        <h5 class="boldFont">
                                            <?php echo stripcslashes($r->post_title) ?>
                                        </h5>

                                        <span class="dateTimePlace"><?php echo date('l, jS F Y', strtotime($r->date_created)); ?></span>

                                        <p>
                                            <?php echo substr($r->post_title,0,40)?>
                                        </p>

                                    </div>

                                </div>
                            <?php 
                                        } 
                                   $i++;
                                    }
                                }
                            ?>

                            </div>

                            <!--<div class="midAdvSec">
                                <img src="<?php echo S3_URL?>site/images/dummy-ads/mid_adv.png" alt="">
                            </div>-->

                            <div class="col-md-12 indiaTopNewsItemHolder">

                                <?php 
                               if(is_array($result) && count($result) > 0){ 
                               $i=0;
                               foreach($result as $r){
                               if($i < 8 && $i > 5){
                               ?>

                                <div class="col-md-6">

                                    <div class="col-md-4 indiaTopNewsImgSec">
                                        <a href="<?php echo SITE_URL ?>category/story/<?php echo $r->id ?>/<?php echo $r->seourl ?>">
                                            <img src="<?php echo S3_URL?>site/images/posts/small_postimage_crop/<?php $img ="thumb-".$r->post_image;  echo $img ?>" alt="">
                                        </a>
                                    </div>

                                    <div class="col-md-8 indiaTopNewsContentSec" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $r->id ?>/<?php echo $r->seourl ?>' ">

                                        <h5 class="boldFont">
                                            <?php echo $r->post_title ?>
                                        </h5>

                                        <span class="dateTimePlace"><?php echo date('l, jS F Y', strtotime($r->date_created)); ?></span>

                                        <p>
                                            <?php echo substr($r->post_title,0,40)?>
                                        </p>
                                    </div>
                                </div>
                            <?php 
                                        } 
                                   $i++;
                                    }
                                }
                            ?>


                            </div>


                        </div>

                    </section>

                    <div class="clearfix"></div>

                    <section class="indiaNewsMidSection">

                        <div class="row">

                            <div class="col-md-12">

                                <!--<div class="col-md-3 advSec">
                                    <img src="<?php echo S3_URL?>site/images/dummy-ads/newSecThreeAd.png" alt="">
                                </div>-->

                                <div class="col-md-12 paddingZero">

                                    <div class="col-md-12 paddingZero">

                                    <?php 
                                   if(is_array($result) && count($result) > 0){ 
                                      $i=0;
                                      foreach($result as $r){
                                        if($i < 10 && $i > 7){
                                     ?>
                            
                                        <div class="col-md-4 paddingZeroLeft mobileBottomSpace">

                                            <div class="newsFourImageSec">
                                                <a href="<?php echo SITE_URL ?>category/story/<?php echo $r->id ?>/<?php echo $r->seourl ?>">
                                                    <img src="<?php echo S3_URL?>site/images/posts/medium_postimage_crop/<?php $img ="thumb-".$r->post_image;  echo $img ?>" alt="">
                                                </a>
                                            </div>
                
                                            <div class="newsFourContentSec linearBackground" >

                                                <div class="sponsredContent">
                                                    <span class="sponsredBadge"> <?php echo strtoupper($category_name) ?> </span>
                                                </div>

                                                <h5 class="semiBold">
                                                    <?php echo substr($r->post_title,0,40)?>
                                                </h5>
                                            </div>
                
                                        </div>

                                        <!-- For Ad Space -->

                                        <?php if( $i == 8 && $web_300_250_center != "" ) { ?>

                                        <div class="col-md-4 paddingZeroLeft mobileBottomSpace">
                                            <?php echo $web_300_250_center; ?>
                                        </div>

                                        <?php } ?>   

                                        <?php if( $i == 8 && $mob_300_250_center != "" ) { ?>

                                        <div class="col-md-4 paddingZeroLeft mobileBottomSpace">
                                            <?php echo $mob_300_250_center; ?>
                                        </div>

                                        <?php } ?>   

                                            <?php 
                                                } 
                                           $i++;
                                            }
                                        }
                                    ?>    

                                    </div>

                                    <div class="col-md-12 paddingZero">
                                        
                                    <?php 
                                    if(is_array($result) && count($result) > 0){ 
                                    $i=0;
                                        foreach($result as $r){
                                            if($i < 13 && $i > 10){
                                     ?>

                                            <div class="col-md-6 paddingZeroLeft marginTop15">

                                                <div class="indiaMidNewsBottomImageSec">
                                                    <a href="<?php echo SITE_URL ?>category/story/<?php echo $r->id ?>/<?php echo $r->seourl ?>">
                                                    <img src="<?php echo S3_URL?>site/images/posts/medium_postimage_crop/<?php $img ="thumb-".$r->post_image;  echo $img ?>" alt="">
                                                    </a>
                                                </div>
                    
                                                <div class="indiaMidNewsBottomContentSec linearBackground">

                                                    <div class="sponsredContent">
                                                        <span class="sponsredBadge"> <?php echo strtoupper($category_name) ?> </span>
                                                    </div>

                                                    <h5 class="semiBold">
                                                         <?php echo substr($r->post_title,0,40)?>
                                                    </h5>
                                                    
                                                </div>
                    
                                            </div>

                                      <?php 
                                                } 
                                           $i++;
                                            }
                                        }
                                    ?>            
                                                    
                                    </div>

                                    <div class="col-md-12 marginTop15 mobView">
                                        
                                    </div> 

                                </div>

                            </div>

                        </div>

                    </section>

                    <div class="clearfix"></div>

                    <section class="indiaTopSecNews">

                        <div class="row">

                            <div class="col-md-12 indiaTopNewsItemHolder">

                                
                                <?php 
                               if(is_array($result) && count($result) > 0){ 
                               $i=0;
                               foreach($result as $r){
                               if($i < 15 && $i > 12){
                               ?>

                                <div class="col-md-6">

                                    <div class="col-md-4 indiaTopNewsImgSec">
                                        <a href="<?php echo SITE_URL ?>category/story/<?php echo $r->id ?>/<?php echo $r->seourl ?>">
                                            <img src="<?php echo S3_URL?>site/images/posts/small_postimage_crop/<?php $img ="thumb-".$r->post_image;  echo $img ?>" alt="">
                                        </a>
                                    </div>

                                    <div class="col-md-8 indiaTopNewsContentSec" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $r->id ?>/<?php echo $r->seourl ?>' ">

                                        <h5 class="boldFont">
                                            <?php echo $r->post_title ?>
                                        </h5>

                                        <!--<span class="dateTimePlace">Tuesday March 31, 2020, Lucknow</span>-->

                                        <p>
                                            <?php echo substr($r->post_title,0,40)?>
                                        </p>

                                    </div>

                                </div>

                            <?php 
                                        } 
                                   $i++;
                                    }
                                }
                            ?>

                                

                            </div>

                            <div class="col-md-12 indiaTopNewsItemHolder">

                                <?php 
                               if(is_array($result) && count($result) > 0){ 
                               $i=0;
                               foreach($result as $r){
                               if($i < 17 && $i > 14){
                               ?>

                                <div class="col-md-6">

                                    <div class="col-md-4 indiaTopNewsImgSec">
                                        <a href="<?php echo SITE_URL ?>category/story/<?php echo $r->id ?>/<?php echo $r->seourl ?>">
                                            <img src="<?php echo S3_URL?>site/images/posts/small_postimage_crop/<?php $img ="thumb-".$r->post_image;  echo $img ?>" alt="">
                                        </a>
                                    </div>

                                    <div class="col-md-8 indiaTopNewsContentSec" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $r->id ?>/<?php echo $r->seourl ?>' ">

                                        <h5 class="boldFont">
                                            <?php echo $r->post_title ?>
                                        </h5>

                                        <!--<span class="dateTimePlace">Tuesday March 31, 2020, Lucknow</span>-->

                                        <p>
                                            <?php echo substr($r->post_title,0,40)?>
                                        </p>
                                    </div>
                                </div>
                            <?php 
                                        } 
                                   $i++;
                                    }
                                }
                            ?>

                            </div>

                            <div class="col-md-12 indiaTopNewsItemHolder">

                                <?php 
                               if(is_array($result) && count($result) > 0){ 
                               $i=0;
                               foreach($result as $r){
                               if($i < 19 && $i > 16){
                               ?>

                                <div class="col-md-6">

                                    <div class="col-md-4 indiaTopNewsImgSec">
                                        <a href="<?php echo SITE_URL ?>category/story/<?php echo $r->id ?>/<?php echo $r->seourl ?>">
                                            <img src="<?php echo S3_URL?>site/images/posts/small_postimage_crop/<?php $img ="thumb-".$r->post_image;  echo $img ?>" alt="">
                                        </a>
                                    </div>

                                    <div class="col-md-8 indiaTopNewsContentSec" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $r->id ?>/<?php echo $r->seourl ?>' ">

                                        <h5 class="boldFont">
                                            <?php echo $r->post_title ?>
                                        </h5>

                                        <!--<span class="dateTimePlace">Tuesday March 31, 2020, Lucknow</span>-->

                                        <p>
                                            <?php echo substr($r->post_title,0,40)?>
                                        </p>

                                    </div>

                                </div>

                            <?php 
                                        } 
                                   $i++;
                                    }
                                }
                            ?>   

                            </div>

                            <div class="clearfix"></div>

                            <div class="col-md-12 webView">

                             <!-- BEGIN JS EXT TAG - 980x160 Newstalkie IN_prashanthi_980*160_Ad 1_entertainment - DO NOT MODIFY -->
                            <?php 
                            
                                if($web_980_160Bottom != "" ){
                                    
                                    echo $web_980_160Bottom;

                                } 
                            ?>
                            <!-- END TAG -->
                            </div>

                            <div class="clearfix"></div>
                            
                            <br>

                            <div class="col-md-12 mobView">
                                <!-- BEGIN JS EXT TAG - 980x160 Newstalkie IN_prashanthi_980*160_Ad 1_entertainment - DO NOT MODIFY -->
                                
                                <?php 
                            
                                    if($mob_980_160Bottom != "" ){
                                        
                                        echo $mob_980_160Bottom;

                                    } 
                                ?>

                                <!-- END TAG -->
                            </div>

                        </div>

                    </section>

                    <div class="clearfix"></div>

                    <section class="midAdvSec">

                        <!--<div class="container-fluid">
                            <img src="<?php echo S3_URL?>site/images/dummy-ads/mid_adv.png" alt="" class="img-responsive">
                        </div>-->

                    </section>

                    <!--<section class="partnerAdBottomSec">

                        <div class="row">

                            <h3 class="boldFont"> <span class="sponsredBadge"> Ad </span> &nbsp; <span> From Our Partners </span> </h3>

                            <div class="parentAdHolder">

                                <div class="childrenAds">
                                    <div class="smallNewsSecondImageLeftSection">
                                        <img src="<?php echo S3_URL?>site/images/dummy-ads/footerAd1.png" alt="">
                                    </div>
                                </div>
    
                                <div class="childrenAds">
                                    <div class="smallNewsSecondImageLeftSection">
                                        <img src="<?php echo S3_URL?>site/images/dummy-ads/footerAd2.png" alt="">
                                    </div>
                                </div>
    
                                <div class="childrenAds">
                                    <div class="smallNewsSecondImageLeftSection">
                                        <img src="<?php echo S3_URL?>site/images/dummy-ads/footerAd3.png" alt="">
                                    </div>
                                </div>
    
                                <div class="childrenAds">
                                    <div class="smallNewsSecondImageLeftSection">
                                        <img src="<?php echo S3_URL?>site/images/dummy-ads/footerAd1.png" alt="">
                                    </div>
                                </div>

                            </div>

                        </div>

                    </section>-->

                </div>

                <div class="col-md-3 paddingZero">

                    <div class="partnerAdSec marginTop15">

                        <?php 
                        if($web_RightTop_300_250 != "" ){
                            $header_margin = "";
                        ?>

                            <div class="col-md-12 webView marginBottom15">
                            
                            <?php echo $web_RightTop_300_250; ?>

                            </div>
                        

                        <?php
                            } 
                        ?>

                            
                        <?php 
                        if($mob_RightTop_300_250 != "" ){
                            $header_margin = "";
                        ?>

                            <div class="col-md-12 mobView marginBottom15">
                            
                            <?php echo $mob_RightTop_300_250; ?>

                            </div>
                        
                        <?php
                            } 
                        ?>

                        
                    
                        <div class="col-md-12 <?php echo $header_margin ?>">
    
                        
                        <?php 
                       if(is_array($result) && count($result) > 0){ 
                       $i=0;
                           foreach($result as $r){
                                if($i < 26 && $i > 18){
                        ?>    

                                <div class="col-md-12 paddingZero">
        
                                    <div class="smallNewsSecondImageLeftSection">
                                        <a href="<?php echo SITE_URL ?>category/story/<?php echo $r->id ?>/<?php echo $r->seourl ?>">
                                            <img src="<?php echo S3_URL?>site/images/posts/small_postimage_crop/<?php $img ="thumb-".$r->post_image;  echo $img ?>" alt="">
                                        </a>
                                        
                                    </div>
        
                                    <div class="smallNewsSecondContentSection marginTop10">
                                        <h5 class="semiBold headingH5"><?php echo substr($r->post_title,0,40)?>..</h5>
                                    </div>
        
                                </div>
                            
                        <?php 
                                } 
                           $i++;
                            }
                        }
                        ?>        

                            <div class="col-md-12 paddingZero">
                
                                <!-- Composite Start -->
                                    <div id="M639897ScriptRootC1009686"></div>
                                    <script src="https://jsc.mgid.com/n/e/newstalkie.com.1009686.js" async></script>
                                    <!-- Composite End -->  
                                    
    
                            </div>

                             <?php

                            if($country_code == "us"){ ?>

                                <div class="col-md-12 paddingZero">

                                    <SCRIPT TYPE="text/javascript">
                                    window.__jscp=function(){if(window.parent==window)var a=document.URL;else if(a=document.referrer,!a)try{a=window.parent.location.href}catch(e){}if(a){var b=a.indexOf("://");0<=b&&(a=a.substring(b+3));b=a.indexOf("/");0<=b&&(a=a.substring(0,b))}b=0;for(var c=window;c!=c.parent;)++b,c=c.parent;a={pu:a,"if":b,rn:new Number(Math.floor(99999999*Math.random())+1)};b=[];for(var d in a)b.push(d+"="+encodeURIComponent(a[d]));return encodeURIComponent(b.join("&"))};
                                    document.write('<S' + 'CRIPT TYPE="text/javascript" SRC="//cpm.rtbwire.com/tag?zone_id=133942&size=300x250&site_cat=IAB12&j=' + __jscp() + '"></S' + 'CRIPT>');
                                    </SCRIPT>

                                </div>

                            <?php } ?>

    
                        </div>
                        
                    </div>

                </div>

            </div>

            <div class="clearfix"></div>
