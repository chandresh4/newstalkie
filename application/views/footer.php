            <footer class="webView">

                <div class="container-fluid">

                    <div class="col-md-12 paddingLeft6em marginTop15 webView">
                        <img src="<?php echo S3_URL?>site/images/footerlogo.svg" alt="" height="92">
                    </div>

                    <div class="col-md-12 footerUl">
                        
                        <?php $country_code = (isset($_COOKIE["countrytest"]) != "" ? $_COOKIE["countrytest"] : "" ); 

                            if($country_code == "in"){ 
                        ?>
                        
                        <ul>
                            <li> <a href="<?php echo SITE_URL ?>history"><?php echo get_country_name($country_code); ?> History </a> </li>
                            <li> <a href="<?php echo SITE_URL ?>culture"><?php echo get_country_name($country_code); ?> Culture </a> </li>
                        </ul>

                        <ul>
                            <li> <a href="<?php echo SITE_URL ?>lifestyle">Lifestyle</a> </li>
                            <li> <a href="<?php echo SITE_URL ?>inspiration">Inspiration </a> </li>
                        </ul>

                        <ul>
                            <li> <a href="<?php echo SITE_URL ?>environment">Environment</a> </li>
                            <!--<li> <a href="<?php echo SITE_URL ?>food-recipes">Food Recipes </a> </li>-->
                        </ul>

                        <ul>
                            <li> <a href="<?php echo SITE_URL ?>celebrity">Celebrity</a> </li>
                            <li> <a href="<?php echo SITE_URL ?>animals">Animals </a> </li>
                        </ul>

                        <ul>
                            <li> <a href="<?php echo SITE_URL ?>politics"> Politics</a> </li>
                            <li> <a href="<?php echo SITE_URL ?>science">Science </a> </li>
                        </ul>

                        <ul>
                            <li> <a href="<?php echo SITE_URL ?>tech-updates" class="mobileMargin">Tech Update</a> </li>
                            <li> <a href="<?php echo SITE_URL ?>sports" class="mobileMargin">Sports</a> </li>
                        </ul>

                    <?php } ?>
                    
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-12 footerSeconUl paddingZero">

                        <ul>

                            <li><a href="<?php echo SITE_URL ?>about-us">About Us </a></li>
                            <li><a href="<?php echo SITE_URL ?>info/privacypolicy">Privacy Policy </a></li>
                            <li><a href="<?php echo SITE_URL ?>info/copyrightnotice">Copyright Notice</a></li>
                            <li><a href="<?php echo SITE_URL ?>info/terms-conditions">Terms & Conditions</a></li>
                            <li><a href="<?php echo SITE_URL ?>info/disclaimer">Disclaimer</a></li>
                            <li><a href="<?php echo SITE_URL ?>contact-us">Contact Us</a></li>
                            
                        </ul>

                    </div>

                    <div class="clearfix"></div>

                    <p class="copyrightPara">
                        
                        <span> © 2021 Newstalkie .All Rights Reserved. </span>

                        <span class="socialIcons"> 

                            <!-- <a href="https://www.facebook.com/newstalkieindia" target="_blank"> 
                                <img src="<?php echo S3_URL?>site/images/social/twitter.svg" alt="">
                            </a>

                            <a href="https://www.facebook.com/newstalkieindia" target="_blank"> 
                                <img src="<?php echo S3_URL?>site/images/social/linkedin.svg" alt="">
                            </a> -->

                            <?php  if($country_code == "gb"){ ?> 

                                <a href="https://www.facebook.com/Newstalkie-UK-103825058316274" target="_blank"> 
                                    <img src="<?php echo S3_URL?>site/images/social/facbook.svg" alt="">
                                </a>
                            <?php }else{ ?>            
                                    <a href="https://www.facebook.com/newstalkieindia" target="_blank"> 
                                    <img src="<?php echo S3_URL?>site/images/social/facbook.svg" alt="">
                                </a>

                            <?php } ?>

                        </span>

                    </p>

                </div>

            </footer>

            <div class="clearfix"></div>

            <footer class="mobView">

                <div class="container-fluid">

                    <div class="col-md-12 footerUl">
                        
                        <?php $country_code = (isset($_COOKIE["countrytest"]) != "" ? $_COOKIE["countrytest"] : "" ); 

                            if($country_code == "in"){ 
                        ?>
                        
                        <ul>

                            <li> <a href="<?php echo SITE_URL ?>history"><?php echo get_country_name($country_code); ?> History </a> </li>
                            <li> <a href="<?php echo SITE_URL ?>culture"><?php echo get_country_name($country_code); ?> Culture </a> </li>
                        
                            <li> <a href="<?php echo SITE_URL ?>lifestyle">Lifestyle</a> </li>
                            <li> <a href="<?php echo SITE_URL ?>inspiration">Inspiration </a> </li>

                            <li> <a href="<?php echo SITE_URL ?>environment">Environment</a> </li>
                            <!--<li> <a href="<?php echo SITE_URL ?>food-recipes">Food Recipes </a> </li>-->

                            <li> <a href="<?php echo SITE_URL ?>celebrity">Celebrity</a> </li>
                            <li> <a href="<?php echo SITE_URL ?>animals">Animals </a> </li>

                            <li> <a href="<?php echo SITE_URL ?>politics"> Politics</a> </li>
                            <li> <a href="<?php echo SITE_URL ?>science"> Science </a> </li>

                            <li> <a href="<?php echo SITE_URL ?>tech-updates" class="">Tech Update</a> </li>
                            <li> <a href="<?php echo SITE_URL ?>sports" class="">Sports</a> </li>
                        

                        </ul>


                    <?php } ?>
                    
                    </div>

                    <div class="clearfix"></div>

                    

                    <div class="col-md-12 footerSeconUl paddingZero">

                        <ul>
                            <li><a href="<?php echo SITE_URL ?>about-us">About Us </a></li>
                            <li><a href="<?php echo SITE_URL ?>info/privacypolicy">Privacy Policy </a></li>
                            <li><a href="<?php echo SITE_URL ?>info/copyrightnotice">Copyright Notice</a></li>
                            <li><a href="<?php echo SITE_URL ?>info/terms-conditions">Terms & Conditions</a></li>
                            <li><a href="<?php echo SITE_URL ?>info/disclaimer">Disclaimer</a></li>
                            <li><a href="<?php echo SITE_URL ?>contact-us">Contact Us</a></li>
                            
                        </ul>

                    </div>

                    <div class="clearfix"></div>

                    <p class="copyrightPara">
                        
                        <span> © 2021 Newstalkie .All Rights Reserved. </span>

                        <span class="socialIcons"> 

                            <!-- <a href="https://www.facebook.com/newstalkieindia" target="_blank"> 
                                <img src="<?php echo S3_URL?>site/images/social/twitter.svg" alt="">
                            </a>

                            <a href="https://www.facebook.com/newstalkieindia" target="_blank"> 
                                <img src="<?php echo S3_URL?>site/images/social/linkedin.svg" alt="">
                            </a> -->

                            <?php  if($country_code == "gb"){ ?> 

                                <a href="https://www.facebook.com/Newstalkie-UK-103825058316274" target="_blank"> 
                                    <img src="<?php echo S3_URL?>site/images/social/facbook.svg" alt="">
                                </a>
                            <?php }else{ ?>            
                                    <a href="https://www.facebook.com/newstalkieindia" target="_blank"> 
                                    <img src="<?php echo S3_URL?>site/images/social/facbook.svg" alt="">
                                </a>

                            <?php } ?>

                        </span>

                    </p>

                </div>

            </footer>

        </div>

        <!-- Footer Scripts Section -->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="<?php echo S3_URL?>site/scripts/script.js"></script>


        <script type="application/javascript" src="https://sdki.truepush.com/sdk/v2.0.2/app.js" async></script>
        <script>
        var truepush = window.truepush || [];
        truepush.push(function(){
            truepush.Init({
                id: "601153d69e28dbc9bf0b4918"
                }, function(error){
                  if(error) console.error(error);
                })
            })
        </script>


    </body>

</html>