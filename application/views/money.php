
<div class="container-fluid">

    <section class="iconAdWidget newHeaderMargin">

        <div class="col-md-12">

            <div class="adItemSlider">
                <div class="stocksName"> ICICIBANK <span class="stockup"> ▲ +3.43% </span> </div>
                <div class="stocksName"> HINDALCO <span class="stockup"> ▲ +3.43% </span>  </div>
                <div class="stocksName"> HDFC <span class="stockup"> ▲ +3.43% </span>  </div>
                <div class="stocksName"> INDUSINDBK <span class="stockup"> ▲ +3.43% </span>  </div>
                <div class="stocksName"> BPCL <span class="stockup"> ▲ +3.43% </span>  </div>

                <div class="stocksName"> ICICIBANK <span class="stockup"> ▲ +3.43% </span> </div>
                <div class="stocksName"> HINDALCO <span class="stockup"> ▲ +3.43% </span>  </div>
                <div class="stocksName"> HDFC <span class="stockup"> ▲ +3.43% </span>  </div>
                <div class="stocksName"> INDUSINDBK <span class="stockup"> ▲ +3.43% </span>  </div>
                <div class="stocksName"> BPCL <span class="stockup"> ▲ +3.43% </span>  </div>

                <div class="stocksName"> ICICIBANK <span class="stockup"> ▲ +3.43% </span> </div>
                <div class="stocksName"> HINDALCO <span class="stockup"> ▲ +3.43% </span>  </div>
                <div class="stocksName"> HDFC <span class="stockup"> ▲ +3.43% </span>  </div>
                <div class="stocksName"> INDUSINDBK <span class="stockup"> ▲ +3.43% </span>  </div>
                <div class="stocksName"> BPCL <span class="stockup"> ▲ +3.43% </span>  </div>

                <div class="stocksName"> ICICIBANK <span class="stockup"> ▲ +3.43% </span> </div>
                <div class="stocksName"> HINDALCO <span class="stockup"> ▲ +3.43% </span>  </div>
                <div class="stocksName"> HDFC <span class="stockup"> ▲ +3.43% </span>  </div>
                <div class="stocksName"> INDUSINDBK <span class="stockup"> ▲ +3.43% </span>  </div>
                <div class="stocksName"> BPCL <span class="stockup"> ▲ +3.43% </span>  </div>


            </div>  

        </div>

    </section>

    <div class="col-md-9 paddingZero">

        <section class="moneyGraph">

           <div class="row">

                <h3 class="boldFont"> Money </h3>

                <div class="col-md-12 moneyGraphConatiner">

                    <div class="col-md-4">

                        <div class="col-md-12 openMarketItemsHeading text-left">
                            <span class="stockCategory">India Markets Open</span>
                        </div>  
                        
                        <div class="col-md-12 openMarketItems">

                            <table class="table">

                                <tr>
                                    <td> <span class="stocksName"> ICICIBANK </span> </td>
                                    <td> <span class="stockup" > ▲ </span> </td>
                                    <td> <span> 11,806.75 </span> </td>
                                    <td> <span class="stockup"> +3.43% </span> </td>
                                </tr>

                                <tr>
                                    <td> <span class="stocksName"> NASDAQ </span> </td>
                                    <td> <span class="stockup" > ▲ </span> </td>
                                    <td> <span> 11,806.75 </span> </td>
                                    <td> <span class="stockup"> +3.43% </span> </td>
                                </tr>

                                <tr>
                                    <td> <span class="stocksName"> FTSE 100 </span> </td>
                                    <td> <span class="stockup" > ▲ </span> </td>
                                    <td> <span> 11,806.75 </span> </td>
                                    <td> <span class="stockup"> +3.43% </span> </td>
                                </tr>

                            </table>

                        </div>

                    </div>

                    <div class="col-md-4">

                        <div class="col-md-12 openMarketItemsHeading">
                            <span class="stockCategory">Recent Quotes</span>
                        </div>

                        <div class="col-md-12 openMarketItems">

                            <table class="table">
                                <tr>
                                    <td> <span class="stocksName"> SENSEX </span> </td>
                                    <td> <span class="stockup" > ▲ </span> </td>
                                    <td> <span> 11,806.75 </span> </td>
                                    <td> <span class="stockup"> -0.28% </span> </td>
                                </tr>
                            </table>

                        </div>

                    </div>

                    <div class="col-md-4">

                        <div class="col-md-12 openMarketItemsHeading">
                            <span class="stockCategory">Recent Quotes</span>
                        </div>

                        <div class="col-md-12 openMarketItems">

                            <table class="table">

                                <tr>
                                    <td> <span class="stocksName"> USD/INR </span> </td>
                                    <td> <span class="stockDown" > ▼ </span> </td>
                                    <td> <span> 74.2870 </span> </td>
                                    <td> <span class="stockDown"> -0.28% </span> </td>
                                </tr>

                                <tr>
                                    <td> <span class="stocksName"> Gold </span> </td>
                                    <td> <span class="stockDown" > ▼ </span> </td>
                                    <td> <span> 1,891.70 </span> </td>
                                    <td> <span class="stockDown"> -0.28% </span> </td>
                                </tr>

                                <tr>
                                    <td> <span class="stocksName"> Silver </span> </td>
                                    <td> <span class="stockup" > ▲ </span> </td>
                                    <td> <span> 24.02 </span> </td>
                                    <td> <span class="stockup"> +0.05%</span> </td>
                                </tr>

                                <tr>
                                    <td> <span class="stocksName"> Crude Oil </span> </td>
                                    <td> <span class="stockDown" > ▼ </span> </td>
                                    <td> <span> 36.74 </span> </td>
                                    <td> <span class="stockDown"> -0.14% </span> </td>
                                </tr>

                            </table>

                        </div>

                    </div>
                    
                </div>

           </div> 

        </section>

        <section class="indiaTopSecNews">

            <div class="row">
                                        
                <div class="col-md-12 indiaTopNewsItemHolder paddingZero">

                    <div class="col-md-6 paddingZero">

                        <div class="col-md-12 paddingZeroRight bigNews">

                            <div class="bigNewsImageSec">
                                <a href="http://localhost/newstalkie_new/category/story/1622/best-smart-phones-below-rs-20000-to-bring-home-this-diwali">
                                    <img src="<?php echo S3_URL?>site/images/money/moneyTopImg.png" alt="Best Smart Phones below Rs 20,000 to bring home this Diwali">
                                </a>    
                            </div>

                            <div class="bigNewsContentSec linearBackground pointer" onclick="window.location.href= 'http://localhost/newstalkie_new/category/story/1622/best-smart-phones-below-rs-20000-to-bring-home-this-diwali'">

                                <span class="newsBadge"> Breaking News</span>

                                <h2 class="semiBold whiteTxt">Cadila Health shares surge 10% as profit jumps over four-fold in Sept quarter</h2>

                            </div>
            
                        </div>

                        <div class="col-md-12 paddingZero ">

                            <div class="col-md-6 paddingZeroRight marginTop15">

                                <div class="col-md-12 paddingZero">

                                    <div class="smallNewsSecondImageLeftSection">
                                        <a href="http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429">
                                            <img src="<?php echo S3_URL?>site/images/money/whatsApp.png" alt="Newslakie News">
                                        </a>
                                    </div>

                                    <p class=" semiBold pageTag">Money</p>

                                    <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= 'http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429'">
                                        <h5 class="semiBold">With 36,469 covid cases, the Covid tally in India jumps to 79, 46,429 </h5>
                                    </div>

                                    <p class="source"> News 18 </p>

                                </div>
                            
                            </div>

                            <div class="col-md-6 paddingZeroRight marginTop15">

                                <div class="col-md-12 paddingZero">

                                    <div class="smallNewsSecondImageLeftSection">
                                        <a href="http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429">
                                            <img src="<?php echo S3_URL?>site/images/money/fortnite.png" alt="Newslakie News">
                                        </a>
                                    </div>

                                    <p class=" semiBold pageTag">Money</p>

                                    <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= 'http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429'">
                                        <h5 class="semiBold">With 36,469 covid cases, the Covid tally in India jumps to 79, 46,429 </h5>
                                    </div>

                                    <p class="source"> News 18 </p>

                                </div>
                            
                            </div>
                        
                            <div class="col-md-6 paddingZeroRight marginTop15">

                                <div class="col-md-12 paddingZero">

                                    <div class="smallNewsSecondImageLeftSection">
                                        <a href="http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429">
                                            <img src="<?php echo S3_URL?>site/images/money/wp_two.png" alt="Newslakie News">
                                        </a>
                                    </div>

                                    <p class=" semiBold pageTag">Money</p>

                                    <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= 'http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429'">
                                        <h5 class="semiBold">With 36,469 covid cases, the Covid tally in India jumps to 79, 46,429 </h5>
                                    </div>

                                    <p class="source"> News 18 </p>

                                </div>
                            
                            </div>

                            <div class="col-md-6 paddingZeroRight marginTop15">

                                <div class="col-md-12 paddingZero">

                                    <div class="smallNewsSecondImageLeftSection">
                                        <a href="http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429">
                                            <img src="<?php echo S3_URL?>site/images/money/whatsApp.png" alt="Newslakie News">
                                        </a>
                                    </div>

                                    <p class=" semiBold pageTag">Money</p>

                                    <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= 'http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429'">
                                        <h5 class="semiBold">With 36,469 covid cases, the Covid tally in India jumps to 79, 46,429 </h5>
                                    </div>

                                    <p class="source"> News 18 </p>

                                </div>
                            
                            </div>

                            <div class="col-md-6 paddingZeroRight marginTop15">

                                <div class="col-md-12 paddingZero">

                                    <div class="smallNewsSecondImageLeftSection">
                                        <a href="http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429">
                                            <img src="<?php echo S3_URL?>site/images/money/whatsApp.png" alt="Newslakie News">
                                        </a>
                                    </div>

                                    <p class=" semiBold pageTag">Money</p>

                                    <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= 'http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429'">
                                        <h5 class="semiBold">With 36,469 covid cases, the Covid tally in India jumps to 79, 46,429 </h5>
                                    </div>

                                    <p class="source"> News 18 </p>

                                </div>
                            
                            </div>

                            <div class="col-md-6 paddingZeroRight marginTop15">

                                <div class="col-md-12 paddingZero">

                                    <div class="smallNewsSecondImageLeftSection">
                                        <a href="http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429">
                                            <img src="http://localhost/newstalkie_new/cdn/site/images/posts/medium_postimage_crop/thumb-covid ind.jpg" alt="Newslakie News">
                                        </a>
                                    </div>

                                    <p class=" semiBold pageTag">Money</p>

                                    <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= 'http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429'">
                                        <h5 class="semiBold">With 36,469 covid cases, the Covid tally in India jumps to 79, 46,429 </h5>
                                    </div>

                                    <p class="source"> News 18 </p>

                                </div>
                            
                            </div>

                        </div>
                    
                    </div>

                    
                    

                    <div class="col-md-6 lifestyleTopRighttNews">

                        <div class="col-md-6 paddingZeroRight">

                            <div class="col-md-12 paddingZero">

                                <div class="smallNewsSecondImageLeftSection">
                                    <a href="http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429">
                                        <img src="<?php echo S3_URL?>site/images/money/whatsApp.png" alt="Newslakie News">
                                    </a>
                                </div>

                                <p class=" semiBold pageTag">Money</p>

                                <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= 'http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429'">
                                    <h5 class="semiBold">What India’s military commentators don’t get about drones — AI… </h5>
                                </div>

                                <p class="pageTag">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, 
                                    sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. 
                                    Ut wisi enim ad minim veniam, quis nostrud exerci
                                <p>

                                <p class="source"> News 18 </p>

                            </div>

                        </div>

                        <div class="col-md-6 paddingZeroRight">

                            <div class="col-md-12 paddingZero">

                                <div class="smallNewsSecondImageLeftSection">
                                    <a href="http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429">
                                        <img src="http://localhost/newstalkie_new/cdn/site/images/posts/medium_postimage_crop/thumb-covid ind.jpg" alt="Newslakie News">
                                    </a>
                                </div>

                                <p class=" semiBold pageTag">Money</p>

                                <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= 'http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429'">
                                    <h5 class="semiBold">Embassy REIT is looking to buy large office spaces </h5>
                                </div>

                                <p class="pageTag">
                                    Embassy Office Parks REIT said it is looking for acquisitions of Grade A 
                                    commercial office assets to increase its portfolio across the 
                                    country at a time when the pandemic has created a 
                                    liquidity squeeze among many developers.
                                </p>

                                <p class="source"> News 18 </p>

                            </div>
                        
                        </div>



                        <div class="col-md-6 paddingZeroRight marginTop15">

                                <div class="col-md-12 paddingZero">

                                    <div class="smallNewsSecondImageLeftSection">
                                        <a href="http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429">
                                            <img src="<?php echo S3_URL?>site/images/money/wp_two.png" alt="Newslakie News">
                                        </a>
                                    </div>

                                    <p class=" semiBold pageTag">Money</p>

                                    <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= 'http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429'">
                                        <h5 class="semiBold">With 36,469 covid cases, the Covid tally in India jumps to 79, 46,429 </h5>
                                    </div>

                                    <p class="source"> News 18 </p>

                                </div>
                            
                            </div>

                            <div class="col-md-6 paddingZeroRight marginTop15">

                                <div class="col-md-12 paddingZero">

                                    <div class="smallNewsSecondImageLeftSection">
                                        <a href="http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429">
                                            <img src="<?php echo S3_URL?>site/images/money/whatsApp.png" alt="Newslakie News">
                                        </a>
                                    </div>

                                    <p class=" semiBold pageTag">Money</p>

                                    <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= 'http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429'">
                                        <h5 class="semiBold">With 36,469 covid cases, the Covid tally in India jumps to 79, 46,429 </h5>
                                    </div>

                                    <p class="source"> News 18 </p>

                                </div>
                            
                            </div>

                            <div class="col-md-6 paddingZeroRight marginTop15">

                                <div class="col-md-12 paddingZero">

                                    <div class="smallNewsSecondImageLeftSection">
                                        <a href="http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429">
                                            <img src="<?php echo S3_URL?>site/images/money/whatsApp.png" alt="Newslakie News">
                                        </a>
                                    </div>

                                    <p class=" semiBold pageTag">Money</p>

                                    <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= 'http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429'">
                                        <h5 class="semiBold">With 36,469 covid cases, the Covid tally in India jumps to 79, 46,429 </h5>
                                    </div>

                                    <p class="source"> News 18 </p>

                                </div>
                            
                            </div>

                            <div class="col-md-6 paddingZeroRight marginTop15">

                                <div class="col-md-12 paddingZero">

                                    <div class="smallNewsSecondImageLeftSection">
                                        <a href="http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429">
                                            <img src="http://localhost/newstalkie_new/cdn/site/images/posts/medium_postimage_crop/thumb-covid ind.jpg" alt="Newslakie News">
                                        </a>
                                    </div>

                                    <p class=" semiBold pageTag">Money</p>

                                    <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= 'http://localhost/newstalkie_new/category/story/1612/with-36469-covid-cases-the-covid-tally-in-india-jumps-to-79-46429'">
                                        <h5 class="semiBold">With 36,469 covid cases, the Covid tally in India jumps to 79, 46,429 </h5>
                                    </div>

                                    <p class="source"> News 18 </p>

                                </div>
                            
                            </div>

                    </div>
                
                </div>

            </div>

        </section>        
          
    </div>

    <div class="col-md-3 paddingZero">

        <div class="partnerAdSec advHeaderMargin">
        
            <div class="col-md-12">

                <div class="col-md-12 paddingZero stockMarketNews">
                    
                    <div class="smallNewsSecondContentSection marginTop10">
                        <h5 class="semiBold headingH5"> Stock Market News </h5>
                    </div>

                    <div class="stockNewsItem">

                        <p class="stockNewsItemHeading">
                            Share price of Marico Ltd. falls as Nifty weakens
                        </p>

                        <p class="stockPrimaryPara">
                            Marico <span class="stockDown"> ▼ </span> 360.00 <span class="stockDown"> - 2.04% </span>
                        </p>

                    </div>


                </div>


                <!-- Gainers -->

                <div class="col-md-12 paddingZero stockMarketNews">
                    
                    <div class="smallNewsSecondContentSection marginTop10">
                        <h5 class="semiBold headingH5"> Gainers </h5>
                    </div>

                    <div class="stockNewsItem">

                        <p class="stockNewsItemHeading">
                            Welspun India
                        </p>

                        <p class="stockPrimaryPara">
                            <span class="stockup"> ▼ </span> 71.9<span class="stockup"> 3.4 4.96%</span>
                        </p>

                        <p class="stockSecondaryPara">
                            <span class="stockup"> 201 </span> Companies added  <span class="stockup"> 107366Cr. </span> at BSE
                        </p>

                    </div>


                </div>


                <!-- Losers -->


                <div class="col-md-12 paddingZero stockMarketNews">
                    
                    <div class="smallNewsSecondContentSection marginTop10">
                        <h5 class="semiBold headingH5"> Losers </h5>
                    </div>

                    <div class="stockNewsItem">

                        <p class="stockNewsItemHeading">
                            MindTree
                        </p>

                        <p class="stockPrimaryPara">
                            <span class="stockDown"> ▼ </span> 3393.05 <span class="stockDown"> -71.85 -5.03%</span>
                        </p>

                        <p class="stockSecondaryPara">
                            <span class="stockDown"> 201 </span> Companies added  <span class="stockDown"> 107366Cr. </span> at BSE
                        </p>

                    </div>


                </div>

                <!-- Commodity Gainers -->

                <div class="col-md-12 paddingZero stockMarketNews">
                    
                    <div class="smallNewsSecondContentSection marginTop10">
                        <h5 class="semiBold headingH5"> Commodity Gainers </h5>
                    </div>

                    <div class="stockNewsItem">

                        <p class="stockNewsItemHeading">
                            Welspun India
                        </p>

                        <p class="stockPrimaryPara">
                            <span class="stockup"> ▼ </span> 71.9<span class="stockup"> 3.4 4.96%</span>
                        </p>

                        <p class="stockSecondaryPara">
                            <span class="stockup"> 201 </span> Companies added  <span class="stockup"> 107366Cr. </span> at BSE
                        </p>

                    </div>


                </div>

                <!-- Commoditiy Losers -->

                <div class="col-md-12 paddingZero stockMarketNews">
                    
                    <div class="smallNewsSecondContentSection marginTop10">
                        <h5 class="semiBold headingH5"> Commoditiy Losers </h5>
                    </div>

                    <div class="stockNewsItem">

                        <p class="stockNewsItemHeading">
                            MindTree
                        </p>

                        <p class="stockPrimaryPara">
                            <span class="stockDown"> ▼ </span> 3393.05 <span class="stockDown"> -71.85 -5.03%</span>
                        </p>

                        <p class="stockSecondaryPara">
                            <span class="stockDown"> 201 </span> Companies added  <span class="stockDown"> 107366Cr. </span> at BSE
                        </p>

                    </div>


                </div>


                <div class="clearfix"></div>

                <br>


                <div class="col-md-12 paddingZero">

                    <div class="smallNewsSecondImageLeftSection">
                        <a href="https://www.newstalkie.com/category/story/35/war-of-the-giants">
                            <img src="https://www.newstalkie.com/cdn/site/images/posts/small_postimage_crop/thumb-6.jpg" alt="">
                        </a>
                        
                    </div>

                    <div class="smallNewsSecondContentSection marginTop10">
                        <h5 class="semiBold headingH5">War of the Giants..</h5>
                    </div>

                </div>

                <div class="col-md-12 quickLinks">

                    <div class="smallNewsSecondContentSection marginTop10">
                        <h5 class="semiBold headingH5"> Quick Links </h5>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Unlock 5 Guidelines</a>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Deals of the Day</a>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Bihar Elections</a>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Coronavirus Live</a>
                    </div>
                
                </div>
            
            </div>

        </div>

    </div>

    <div class="clearfix"></div>

    <section class="moreAds">

        <div class="container">

            <h3 class="boldFont" style="margin-bottom: 2em;"> MORE FOR YOU </h3>

            <div class="col-md-12">
            
                <div class="col-md-3 adItems">
                    <img src="<?php echo S3_URL?>site/images/videoes/ads.png" alt="Newslakie News" class="img-responsive">
                    <p class="boldFont">
                        The power of 4 devices in 1 @ ₹5699
                    </p>
                </div>

                <div class="col-md-3 adItems">
                    <img src="<?php echo S3_URL?>site/images/videoes/ads.png" alt="Newslakie News" class="img-responsive">
                    <p class="boldFont">
                        #KeepAWatch on your Vitals In Real Time with GOQii
                    </p>
                </div>

                <div class="col-md-3 adItems">
                    <img src="<?php echo S3_URL?>site/images/videoes/ads.png" alt="Newslakie News" class="img-responsive">
                    <p class="boldFont">
                        Power new possibilities
                    </p>
                </div>

                <div class="col-md-3 adItems">
                    <img src="<?php echo S3_URL?>site/images/videoes/ads.png" alt="Newslakie News" class="img-responsive">
                    <p class="boldFont">
                        Power new possibilities
                    </p>
                </div>
            
            </div>

        </div>

    </section>

</div>                          
                
                               