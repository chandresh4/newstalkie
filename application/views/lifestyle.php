
<div class="container-fluid">

    <div class="col-md-9 paddingZero">

        <section class="indiaTopSecNews headerMargin">

            <div class="row">
                                        
                <div class="col-md-12 indiaTopNewsItemHolder paddingZero">

                    <div class="col-md-8 paddingZeroRight bigNews">

                        <div class="bigNewsImageSec">
                            <a href="">
                                <img src="<?php echo S3_URL?>site/images/lifestyle/lifestyle_big.png" alt="Kangana Ranaut gives an interview on completion of 12 years of the Fashion movie">
                            </a>    
                        </div>

                        <div class="bigNewsContentSec linearBackground pointer whiteTxt" onclick="">
                            <h2 class="semiBold">Mouni Roy Glams Up Her Sneakers In Sequins And A Skater Style</h2>
                        </div>
                
                    </div>
                    

                    <div class="col-md-4 lifestyleTopRighttNews">

                        <div class="col-md-12 paddingZero">

                            <div class="smallNewsSecondImageLeftSection">
                                <a href="">
                                    <img src="<?php echo S3_URL?>site/images/lifestyle/rightTopNewsImg.png" alt="Newslakie News">
                                </a>
                            </div>

                            <div class="smallNewsSecondContentSection pointer" onclick="">
                                <h5 class="semiBold headingH5">Tanishaa Mukerji Looks Like A Ray Of Sunshine In A Colour Pop Navratri Special </h5>
                            </div>

                        </div>

                        <div class="col-md-12 paddingZero">

                            <div class="smallNewsSecondImageLeftSection">
                                <a href="">
                                    <img src="<?php echo S3_URL?>site/images/lifestyle/rightTopNewsImg.png" alt="Newslakie News">
                                </a>
                            </div>

                            <div class="smallNewsSecondContentSection pointer" onclick="">
                                <h5 class="semiBold headingH5"> We Spy Karisma Kapoor’s Clear Skin And Wavy Hair That Is No Short Of Perfect </h5>
                            </div>

                        </div>

                    </div>

                </div>

                <div class="col-md-12 indiaTopNewsItemHolder">

                    <div class="col-md-4 paddingZeroLeft">

                        <h3 class="boldFont text-center font18"> FASHION </h3>

                        <div class="newsFourImageSec categoryLifestyleSec">
                            <a href="">
                                <img src="<?php echo S3_URL?>site/images/lifestyle/fashion.png" alt="Newslakie News">
                            </a>
                        </div>

                        <div class="newsFourContentSec linearBackground pointer" onclick="">
                            <h5 class="semiBold">
                                Remember when Audrey Hepburn defined elegance in a little black dress?                           
                            </h5>
                        </div>

                    </div>

                    <div class="col-md-4 paddingZeroLeft">

                        <h3 class="boldFont text-center font18"> BEAUTY </h3>

                        <div class="newsFourImageSec categoryLifestyleSec">
                            <a href="">
                                <img src="<?php echo S3_URL?>site/images/lifestyle/beauty.png" alt="Newslakie News">
                            </a>
                        </div>

                        <div class="newsFourContentSec linearBackground pointer" onclick="">
                            <h5 class="semiBold">
                                Remember when Audrey Hepburn defined elegance in a little black dress?                           
                            </h5>
                        </div>

                    </div>

                    <div class="col-md-4 paddingZeroLeft">

                        <h3 class="boldFont text-center font18"> SEX & RELATIONSHIPS </h3>

                        <div class="newsFourImageSec categoryLifestyleSec">
                            <a href="">
                                <img src="<?php echo S3_URL?>site/images/lifestyle/relationship.png" alt="Newslakie News">
                            </a>
                        </div>

                        <div class="newsFourContentSec linearBackground pointer" onclick="">
                            <h5 class="semiBold">
                                Remember when Audrey Hepburn defined elegance in a little black dress?                           
                            </h5>
                        </div>

                    </div>
                
                </div>

                <div class="midAdvSec">
                    <img src="https://www.newstalkie.com/cdn/site/images/dummy-ads/mid_adv.png" alt="" class="img-responsive">
                </div>

                <div class="col-md-12 indiaTopNewsItemHolder">

                    <div class="col-md-4 paddingZeroLeft">

                        <h3 class="boldFont text-center font18"> WORK & MONEY </h3>

                        <div class="newsFourImageSec categoryLifestyleSec">
                            <a href="">
                                <img src="<?php echo S3_URL?>site/images/lifestyle/work-money.png" alt="Newslakie News">
                            </a>
                        </div>

                        <div class="newsFourContentSec linearBackground pointer" onclick="">
                            <h5 class="semiBold">
                                Remember when Audrey Hepburn defined elegance in a little black dress?                           
                            </h5>
                        </div>

                    </div>

                    <div class="col-md-4 paddingZeroLeft">

                        <h3 class="boldFont text-center font18"> MOTHERHOOD & BABY CARE </h3>

                        <div class="newsFourImageSec categoryLifestyleSec">
                            <a href="">
                                <img src="<?php echo S3_URL?>site/images/lifestyle/motherhood.png" alt="Newslakie News">
                            </a>
                        </div>

                        <div class="newsFourContentSec linearBackground pointer" onclick="">
                            <h5 class="semiBold">
                                Remember when Audrey Hepburn defined elegance in a little black dress?                           
                            </h5>
                        </div>

                    </div>

                    <div class="col-md-4 paddingZeroLeft">

                        <h3 class="boldFont text-center font18"> TRAVEL </h3>

                        <div class="newsFourImageSec categoryLifestyleSec">
                            <a href="">
                                <img src="<?php echo S3_URL?>site/images/lifestyle/travel.png" alt="Newslakie News">
                            </a>
                        </div>

                        <div class="newsFourContentSec linearBackground pointer" onclick="">
                            <h5 class="semiBold">
                                Remember when Audrey Hepburn defined elegance in a little black dress?                           
                            </h5>
                        </div>

                    </div>
                
                </div>

                <div class="clearfix"></div>

                <div class="col-md-12 indiaTopNewsItemHolder">

                    <div class="col-md-4 paddingZeroLeft">

                        <h3 class="boldFont text-center font18"> KITCHEN & DINING </h3>

                        <div class="newsFourImageSec categoryLifestyleSec">
                            <a href="">
                                <img src="<?php echo S3_URL?>site/images/lifestyle/kitchen.png" alt="Newslakie News">
                            </a>
                        </div>

                        <div class="newsFourContentSec linearBackground pointer" onclick="">
                            <h5 class="semiBold">
                                Remember when Audrey Hepburn defined elegance in a little black dress?                           
                            </h5>
                        </div>

                    </div>

                    <div class="col-md-4 paddingZeroLeft">

                        <h3 class="boldFont text-center font18"> WELLNESS </h3>

                        <div class="newsFourImageSec categoryLifestyleSec">
                            <a href="">
                                <img src="<?php echo S3_URL?>site/images/lifestyle/wellness.png" alt="Newslakie News">
                            </a>
                        </div>

                        <div class="newsFourContentSec linearBackground pointer" onclick="">
                            <h5 class="semiBold">
                                Remember when Audrey Hepburn defined elegance in a little black dress?                           
                            </h5>
                        </div>

                    </div>

                    <div class="col-md-4 paddingZeroLeft">

                        <h3 class="boldFont text-center font18"> HOME & DECOR </h3>

                        <div class="newsFourImageSec categoryLifestyleSec">
                            <a href="">
                                <img src="<?php echo S3_URL?>site/images/lifestyle/decor.png" alt="Newslakie News">
                            </a>
                        </div>

                        <div class="newsFourContentSec linearBackground pointer" onclick="">
                            <h5 class="semiBold">
                                Remember when Audrey Hepburn defined elegance in a little black dress?                           
                            </h5>
                        </div>

                    </div>
                
                </div>

                <div class="clearfix"></div>

                <div class="col-md-12 indiaTopNewsItemHolder paddingZero">

                    <div class="col-md-8 paddingZeroRight bigNews">

                        <div class="bigNewsImageSec">
                            <a href="">
                                <img src="<?php echo S3_URL?>site/images/lifestyle/lifestyle_big.png" alt="Kangana Ranaut gives an interview on completion of 12 years of the Fashion movie">
                            </a>    
                        </div>

                        <div class="bigNewsContentSec linearBackground pointer whiteTxt" onclick="">
                            <h2 class="semiBold">Mouni Roy Glams Up Her Sneakers In Sequins And A Skater Style</h2>
                        </div>
                
                    </div>
                    

                    <div class="col-md-4 lifestyleTopRighttNews">

                        <div class="col-md-12 paddingZero">

                            <div class="smallNewsSecondImageLeftSection">
                                <a href="">
                                    <img src="<?php echo S3_URL?>site/images/lifestyle/rightTopNewsImg.png" alt="Newslakie News">
                                </a>
                            </div>

                            <div class="smallNewsSecondContentSection pointer" onclick="">
                                <h5 class="semiBold headingH5">Tanishaa Mukerji Looks Like A Ray Of Sunshine In A Colour Pop Navratri Special </h5>
                            </div>

                        </div>

                        <div class="col-md-12 paddingZero">

                            <div class="smallNewsSecondImageLeftSection">
                                <a href="">
                                    <img src="<?php echo S3_URL?>site/images/lifestyle/rightTopNewsImg.png" alt="Newslakie News">
                                </a>
                            </div>

                            <div class="smallNewsSecondContentSection pointer" onclick="">
                                <h5 class="semiBold headingH5"> We Spy Karisma Kapoor’s Clear Skin And Wavy Hair That Is No Short Of Perfect </h5>
                            </div>

                        </div>

                    </div>

                </div>


            </div>

        </section>

    </div>

    <div class="col-md-3 paddingZero">

        <div class="partnerAdSec advHeaderMargin">
        
            <div class="col-md-12">

                <div class="col-md-12 paddingZero">

                    <div class="smallNewsSecondImageLeftSection">
                        <a href="https://www.newstalkie.com/category/story/35/war-of-the-giants">
                            <img src="https://www.newstalkie.com/cdn/site/images/posts/small_postimage_crop/thumb-6.jpg" alt="">
                        </a>
                        
                    </div>

                    <div class="smallNewsSecondContentSection marginTop10">
                        <h5 class="semiBold headingH5">War of the Giants..</h5>
                    </div>

                </div>

                <div class="col-md-12 quickLinks">

                    <div class="smallNewsSecondContentSection marginTop10">
                        <h5 class="semiBold headingH5"> Quick Links </h5>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Unlock 5 Guidelines</a>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Deals of the Day</a>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Bihar Elections</a>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Coronavirus Live</a>
                    </div>
                
                </div>
            
            </div>

        </div>

    </div>

</div>                          
                
                               