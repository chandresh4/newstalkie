<script src="//cdn.ckeditor.com/4.5.6/standard/ckeditor.js"></script>
<script type="application/javascript">
 function checkInputs(){ 
    var regex     = /^[0-9]*$/;
	var category  = document.getElementById("category").value;
	var tag       = document.getElementById("tag").value;
	var title  	  = document.getElementById("title").value;
	var url  	  = document.getElementById("url").value;
	var status    = document.getElementById("status").value;
    var sub_title = document.getElementById("sub_title").value;
	
    if(category == "" || category.trim() ==""){
		alert("category  is required");
		document.getElementById("category").focus();
		return false;
	}

	if(title == "" || title.trim() ==""){
		alert("Title  is required");
		document.getElementById("title").focus();
		return false;
	}
	if(!$("#title").val().match(/^[a-zA-Z0-9?.!-_,:\s]+$/)){
	       alert("Title  is required");
		  document.getElementById("title").focus();
	       return false;	
	}    
	if(url == "" || url.trim() ==""){
		alert("URL  is required");
		document.getElementById("url").focus();
		return false;
	}
	if(sub_title == "" || sub_title.trim() ==""){
		alert("Sub Title is required");
		document.getElementById("sub_title").focus();
		return false;
	}
	if(!$("#sub_title").val().match(/^[a-zA-Z0-9?.!-_,@*:\s]+$/)){
	       alert("sub title  is required");
		  document.getElementById("sub_title").focus();
	       return false;	
	}   
	
	
	
	if(status == "" || status.trim() ==""){
	   alert("Status field is required");
	   document.getElementById("status").focus();
	   return false;
	}
	if(vstatus == "" || vstatus.trim() ==""){
	   alert("Viral Status field is required");
	   document.getElementById("vstatus").focus();
	   return false;
	}
	if(tstatus == "" || tstatus.trim() ==""){
	   alert("Trendy Status field is required");
	   document.getElementById("tstatus").focus();
	   return false;
	}
	
	

		
		
}

</script>

<script>
$(document).ready(function(){
	$('#title').on('keyup', function() {
		var title_url = $("#title").val();
		var string = title_url.replace(/[^a-zA-Z 0-9]+/g,'');
		var post_url  = string.replace(/\s+/g, '-').toLowerCase();
		$("#url").val(post_url);
		
	});
 });
</script>
 
<div class="span9">
<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Update Post
              <a style ="float:right;" href="<?php echo FULL_CMS_URL."/manage_posts/"?> ">Go back</a>
          </h3>
        </div>
        <div class="module-body">
               <?php 
				if( $this->session->flashdata('error') ) { 
				   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
						<strong>'.$this->session->flashdata('error').'</strong></div>';
			
				}else if( $this->session->flashdata('success') ) { 
				
				   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
					  <strong>'.$this->session->flashdata('success').'</strong></div>';
				}
				?>
                
                <br />                
                 
                  <?php   foreach($val as $record){ ?>
                  <form class="form-horizontal row-fluid" method="post" action="<?php echo FULL_CMS_URL.'/manage_posts/edit_action' ?>" enctype="multipart/form-data" />
                    <input type="hidden" name="id" id="id" value="<?php echo $record->id;?>" />
                     <input type="hidden" name="writer_id" id="writer_id" value="<?php echo $record->writer_id;?>" />

                     <div class="control-group">
                            <label class="control-label" for="basicinput">Country*</label>
                            <div class="controls">
                               <select tabindex="8" id="country" name="country" data-placeholder="Select Country.." class="js-example-basic-single span8" required>
                               <option value="" <?php echo ($record->country == NULL)   ? 'selected' : '';?>>Select Country</option>
                               <option value="in" <?php echo ($record->country == 'in') ? 'selected' : '';?>>India</option>
                               <option value="gb" <?php echo ($record->country == 'gb') ? 'selected' : '';?>>UK</option>
                               <option value="us" <?php echo ($record->country == 'us') ? 'selected' : '';?>>US</option>
                                </select>
                            </div>
                    </div>

                    <div class="control-group">
                            <label class="control-label" for="basicinput">category*</label>
                            <div class="controls">
                               <select tabindex="8" id="category" name="category" data-placeholder="Select Category.." class="js-example-basic-single span8">
                               <option value="" <?php echo ($record->category == NULL) ? 'selected="selectec"' : '';?>>Select Category</option>
                               
                               <?php foreach($res as $p){ ?>
              							   <option value="<?php echo $p->cat_id?>" <?php echo ($record->category ==$p->cat_id ) ? 'selected="selectec"' : '';?>><?php echo $p->name?></option>
              							   <?php } ?>
                                </select>
                            </div>
                        </div>
                        
                        
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Tag Category *</label>
                            <div class="controls">
                              <select multiple="multiple" tabindex="6" name="tag[]" id="tag" data-placeholder="Select tag category.." class="js-example-basic-multiple span8">
                               
                              <?php
                                 $tags ="";
                                 $tags = explode(",",$record->tag);
                                
                                 foreach($res1 as $p1){
                                 ?>
                                 <option value="<?php echo $p1->t_id?>" <?php echo (isset($tags) && in_array($p1->t_id, $tags) ) ? "selected" : "" ?>><?php echo $p1->tag_name?></option>
                                 <?php 
                                 } 
                              ?>
                                
                                </select>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="basicinput">Title *</label>
                            <div class="controls">
                                <textarea id="title" name="title" placeholder="title" tabindex="5" class="span8"><?php echo $record->post_title?></textarea>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">URL *</label>
                            <div class="controls">
                                <textarea id="url" name="url" placeholder="url" tabindex="5" class="span8"><?php echo $record->seourl?></textarea>
                            </div>
                        </div>
                       
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Sub Title *</label>
                            <div class="controls">
                                <input type="text" id="sub_title" name="sub_title" placeholder="Enter Page Title" class="span8" tabindex="2" value="<?php if(isset($record->post_sub_title)){echo $record->post_sub_title ;}?>">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Post Image *</label>
                            <div class="controls">
                                <input type="file" name="post_image" id="post_image" placeholder="Post Image" tabindex="3" class="span8">
                               
                            </div>
                        </div>
                                                
                    
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Post Image</label>
                            <div class="controls">
                                <img alt="blog image<?php echo $record->post_title?>" src="<?php echo SITE_URL.POSTS_FOLDER.$record->post_image;  ?>" width="150">                                
                            </div>
                        </div>
                        
                         <div class="control-group">
                            <label class="control-label" for="basicinput">Post Content *</label>
                            <div class="controls">
                                <textarea id="content" name="content" placeholder="content" rows="16" tabindex="7" class="span8"><?php if(isset($record->post_desciption)){echo $record->post_desciption;}?></textarea>
                                <script> CKEDITOR.replace( 'content' ); </script>
                            </div>
                        </div>
                         <div class="control-group">
                            <label class="control-label" for="basicinput">Status*</label>
                            <div class="controls">
                               <select tabindex="8" id="status" name="status" data-placeholder="Select Status.." class="span8">
                               <option value="" <?php echo ($record->status == NULL) ? 'selected="selectec"' : '';?>>Select Status</option>
                               <option value="1" <?php echo ($record->status == '1') ? 'selected="selectec"' : '';?>>Active</option>
                               <option value="0" <?php echo ($record->status == '0') ? 'selected="selectec"' : '';?>>Inactive</option>
                                </select>
                            </div>
                        </div>
                          <div class="control-group">
                            <label class="control-label" for="basicinput">Want to make Viral? *</label>
                            <div class="controls">
                               <select tabindex="8" id="vstatus" name="vstatus" data-placeholder="Select Status.." class="span8">
                               <option value="" <?php echo ($record->viral_status == NULL) ? 'selected="selectec"' : '';?>>Select Viral Status</option>
                               <option value="1" <?php echo ($record->viral_status == '1') ? 'selected="selectec"' : '';?>>Yes</option>
                               <option value="0" <?php echo ($record->viral_status == '0') ? 'selected="selectec"' : '';?>>No</option>
                                </select>
                            </div>
                        </div>
                           <div class="control-group">
                            <label class="control-label" for="basicinput">Want to make Trendy? *</label>
                            <div class="controls">
                               <select tabindex="8" id="tstatus" name="tstatus" data-placeholder="Select Status.." class="span8">
                               <option value="" <?php echo ($record->trendy_status == NULL) ? 'selected="selectec"' : '';?>>Select trendy Status</option>
                               <option value="1" <?php echo ($record->trendy_status == '1') ? 'selected="selectec"' : '';?>>Yes</option>
                               <option value="0" <?php echo ($record->trendy_status == '0') ? 'selected="selectec"' : '';?>>No</option>
                                </select>
                            </div>
                        </div>     
                           <div class="control-group">
                            <label class="control-label" for="basicinput">Video Url</label>
                            <div class="controls">
                                <input type="text" id="link" name="link" placeholder="Enter Video Url" class="span8" tabindex="2" value="<?php if(isset($record->link)){echo $record->link ;}?>">
                            </div>
                        </div>
                        
                        
                        

                        <div class="control-group">
                            <div class="controls">
                                <input type="submit" name="addform" value="Save" onclick="return checkInputs()">
                            </div>
                        </div>
                    </form>
                    <?php  }?>
                </div>
                </div>
                                
            </div><!--/.content-->
        </div> 
 
         <script>
            $(document).ready(function() {
                $('.js-example-basic-multiple').select2();
                $('.js-example-basic-single').select2();

            });
        </script>
