<script type="application/javascript">
  function checkInputs(){ 
   var name=document.getElementById("name").value;
   var status = document.getElementById("status").value;
   if(name == "" || name.trim() =="")
	{
		    alert("Tag field is required");
			document.getElementById("name").value="";
			document.getElementById("name").focus();
			return false;
	}
	if(status == "" || status.trim() =="")
	{
		    alert("status is required");
			document.getElementById("name").value="";
			document.getElementById("name").focus();
			return false;
	}	
}

</script>
 <script>
$(document).ready(function(){
	$('#name').on('keyup', function() {
		var name = $("#name").val();
		var string = name.replace(/[^a-zA-Z 0-9]+/g,'');
		var name_url  = string.replace(/\s+/g, '-').toLowerCase()+'-';
		$("#seourl").val(name_url);
		
	});
 });
</script>
<div class="span9">
<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Update Tags</h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo FULL_CMS_URL."/manage_tags"?> ">Manage Tags</a>
            </h3>
        </div>
        <div class="module-body">
               <?php 
				if( $this->session->flashdata('error') ) { 
				   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
						<strong>'.$this->session->flashdata('error').'</strong></div>';
			
				}else if( $this->session->flashdata('success') ) { 
				
				   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
					  <strong>'.$this->session->flashdata('success').'</strong></div>';
				}
				?>
                
                <br />                
                  
                  <?php foreach($result as $record){ ?>
                  <form class="form-horizontal row-fluid" method="post" action="<?php echo FULL_CMS_URL.'/manage_tags/edit_action' ?>" enctype="multipart/form-data" />
                    <input type="hidden" name="id" id="id" value="<?php echo $record->t_id;?>" />
                     
						
						
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Tags Name*</label>
                            <div class="controls">
                                <input type="text" id="name" name="name" placeholder="Tags Name" class="span8" tabindex="5" value="<?php echo $record->tag_name?>">
                            </div>
                        </div> 
                        
                        <div class="control-group">
                        <label class="control-label" for="basicinput">Tag Seourl* </label>
                        <div class="controls">
                            <input type="text" name="seourl" id="seourl" autofocus="autofocus" tabindex="1" class="span8" value="<?php echo $record->seourl?>"/>
						</div>
                       </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Status*</label>
                            <div class="controls">
                               <select tabindex="7" id="status" name="status" data-placeholder="Select Status.." class="span8">
                               <option value="" <?php echo ($record->status == NULL) ? 'selected="selectec"' : '';?>>Select Status</option>
                               <option value="1" <?php echo ($record->status == '1') ? 'selected="selectec"' : '';?>>Active</option>
                               <option value="0" <?php echo ($record->status == '0') ? 'selected="selectec"' : '';?>>Inactive</option>
                                </select>
                            </div>
                        </div>

                        <div class="control-group">
                            <div class="controls">
                                <input type="submit" name="addform" value="Save" onclick="return checkInputs()">
                            </div>
                        </div>
                    </form>
                    <?php } ?>
                </div>
                </div>
                                
            </div><!--/.content-->
        </div> 
 
 