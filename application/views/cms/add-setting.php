<?php $arr_all = all_arrays(); ?>
<script type="application/javascript">

 function checkInputs(){
	  
    var regex = /^[0-9]*$/;
	var setting_name =document.getElementById("setting_name").value;
	var setting_value=document.getElementById("setting_value").value;
	
	
	if( setting_name== "" || setting_name.trim() ==""){
		alert("Name is required");
		document.getElementById("setting_name").focus();
		return false;
	}
		
	if(setting_value == "" || setting_value.trim() ==""){
		alert("Value is required");
		document.getElementById("setting_value").focus();
		return false;
	}
		
		
}

 </script>


<div class="span9">
<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Add Setting</h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo SITE_URL?>index.php/cms/manage_setting">Manage Setting</a></h3>	
        </div>
        <div class="module-body">
                <?php 
					if( $this->session->flashdata('error') ) { 
					   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
							<strong>'.$this->session->flashdata('error').'</strong></div>';
				
					}else if( $this->session->flashdata('success') ) { 
					
					   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
						  <strong>'.$this->session->flashdata('success').'</strong></div>';
					}
				  ?>
                
                <br />                

                  <form class="form-horizontal row-fluid" method="post" action="<?php echo SITE_URL.'index.php/cms/manage_setting/add' ?>">
                    
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Setting name</label>
                            <div class="controls">
                                <input type="text" id="setting_name" name="setting_name" tabindex="1" placeholder="setting name" class="span8">
                            </div>
                        </div>
                        
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Setting Value</label>
                            <div class="controls">
                                <textarea name="setting_value" id="setting_value" placeholder="setting value" tabindex="2" class="span8" rows="8"/></textarea>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <div class="controls">
                                <input type="submit" name="addform" value="Save" onclick="return checkInputs()">
                            </div>
                        </div>
                    </form>
                </div>
                </div>
                                
            </div><!--/.content-->
        </div>
