<?php $arr_all = all_arrays(); ?>
<script type="application/javascript">

 function checkInputs(){ 
    var regex = /^[0-9]*$/;
    var net_name=document.getElementById("net_name").value;
	var net_pixel=document.getElementById("net_pixel").value;
	var handle_type=document.getElementById("handle_type").value;
	var cpl=document.getElementById("cpl").value;
	var goal=document.getElementById("goal").value;
	var fire_type=document.getElementById("fire_type").value;

		if( net_name== "" || net_name.trim() ==""){
			alert("Net Name is required");
			document.getElementById("net_name").focus();
			return false;
		}
		
		if(net_pixel == "" || net_pixel.trim() ==""){
		    alert("Net pixel is required");
			document.getElementById("net_pixel").focus();
			return false;
		}
		
		if(handle_type == "" || handle_type.trim() ==""){
		    alert("Handle type is required");
			document.getElementById("handle_type").focus();
			return false;
		}
		
		if(cpl == "" || cpl.trim() ==""){
		    alert("cpl is required");
			document.getElementById("cpl").focus();
			return false;
		}
		
		if(fire_type == "" || fire_type.trim() ==""){
		    alert("fire type is required");
			document.getElementById("fire_type").focus();
			return false;
		}
		
		if(goal == "" || goal.trim() ==""){
		    alert("goal is required");
			document.getElementById("goal").focus();
			return false;
		}
		
 }

 </script>


<div class="span9">
<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Add UTM Source</h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo SITE_URL?>index.php/cms/manage_utm_source">Manage UTM Source</a></h3>	
        </div>
        <div class="module-body">
                <?php 
					if( $this->session->flashdata('error') ) { 
					   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
							<strong>'.$this->session->flashdata('error').'</strong></div>';
				
					}else if( $this->session->flashdata('success') ) { 
					
					   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
						  <strong>'.$this->session->flashdata('success').'</strong></div>';
					}
				  ?>
                
                <br />                

                  <form class="form-horizontal row-fluid" method="post" action="<?php echo SITE_URL.'cms/manage_utm_source/add' ?>">
                    
                        <div class="control-group">
                            <label class="control-label" for="basicinput">UTM Source name*</label>
                            <div class="controls">
                                <input type="text" id="net_name" name="net_name" tabindex="1" placeholder="UTM Source name" class="span8">
                            </div>
                        </div>
                        
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">UTM Source*</label>
                            <div class="controls">
                                <textarea name="net_pixel" id="net_pixel" placeholder="net_pixel" tabindex="2" class="span8"/></textarea>
                                <span class="help-inline">Replace dynamic user id with "{USERID}" & payout with "{PAYOUT}"</span>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Handle Type*</label>
                            <div class="controls">
                                <select name="handle_type" id="handle_type"  tabindex="3" class="span8">
                                    <option value="NULL">Select Type</option>
                                    <option value="1">Manual</option>
                                    <option value="2">Auto</option>
                                </select>
                            </div>
                        </div>
                        
                        
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Cpl*</label>
                            <div class="controls">
                                <input type="text" id="cpl" name="cpl" placeholder="Cpl" tabindex="4" class="span8">
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Fire Type*</label>
                            <div class="controls">
                                <select name="fire_type" id="fire_type"  tabindex="5" class="span8">
                                    <option value="">Select Type</option>
                                <?php foreach ($arr_all['FIRE_LIMIT'] as $fire_limit){
                                     echo "<option value='".$fire_limit."'>".$fire_limit."</option>";
                                } ?>
                                </select>
                            </div>
                        </div>
                        
                         
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Goal*</label>
                            <div class="controls">
                                <input type="text" id="goal" name="goal" placeholder="goal" tabindex="6" class="span8">
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <div class="controls">
                                <input type="submit" name="addform" value="Save" onclick="return checkInputs()">
                            </div>
                        </div>
                    </form>
                </div>
                </div>
                                
            </div><!--/.content-->
        </div>
