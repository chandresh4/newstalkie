<script>
function valid_form(){
	
	var r_= new Array("role_name", "role_details", "status");
	
	for(var i=0; i < r_.length; i++   ){
		if($.trim($("#"+r_[i]).val()) == "" || $("#"+r_[i]).val() == 0 || $("#"+r_[i]).val() == "undefined"){
		   $("#"+r_[i]+"_err").html($("#"+r_[i]).attr("data-attr"));
		   $("#"+r_[i]).focus();
		   return false;	
		}else{
		   $("#"+r_[i]+"_err").html("");
		}
	}
}

</script>

<?php //$arr_all = all_arrays();
if (isset($result_data['role_details'])) {
	$unserialize_role = unserialize($result_data['role_details']);
	if (is_array($unserialize_role)) {
		$role_details = unserialize($result_data['role_details']);
	}
}
?>

<div class="span9">                
  <div class="content">        
    <form  class="form-horizontal row-fluid" action="<?php echo $form_submit?>" method="post" enctype="multipart/form-data">
    	<div class="module">
        	<div class="module-head">
                <h3 ><?php echo $page_title?></h3>
                <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo FULL_CMS_URL."/".$cur_controller?>">Manage Setting</a></h3>	
            </div>
       		<div class="module-body">
			<?php 
              if( $this->session->flashdata('error') ) { 
                  echo '<div class="alert alert-danger" role="alert" style="height:39px; line-height:8px">
                          <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true" >&times;</span></button>
                          '.$this->session->flashdata('error').'</div>';
          
              }else if( $this->session->flashdata('success') ) { 
                 echo '<div class="alert alert-success" role="alert" style="height:39px; line-height:8px">
                          <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true" >&times;</span></button>
                          '.$this->session->flashdata('success').'</div>';
          
              }
            ?>        
        	</div>
        	<div class="module-body">
            	<div class="control-group">                                        
                    <label class="col-md-2 col-xs-12 control-label">Role Name*</label>
                    <div class="col-md-4 col-xs-12">
                        <div class="controls">
                          <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                          <input type="text" class="form-control " name="role_name" id="role_name" tabindex="1" value="<?php echo $result_data['role_name']?>" data-attr="Please enter role ">
                        </div>            
                        <span class="help-block" id="role_name_err"></span>
                    </div>
                </div>
                <div class="control-group">                                        
                    <label class="col-md-2 col-xs-12 control-label ">Rolls*</label>
                    <div class="col-md-4 col-xs-12 controls">
                    <?php
                        $menu_list = $menu;
                        $total_menu = count($menu_list) - 1;
                        $i = 1;    
                         foreach($menu_list as $k => $v){
                              echo "<label style='color:blue'><strong>".$v[0]."</strong></label><br/>" ; 
                                foreach($v[2] as $k1 => $v1) {                                      
                                    if(strtolower($v1[1]) == strtolower($v1[2])) {
                                        ?>
                                        <label class="check">
                                        <input id="role_details" name="role_details[]" type="checkbox" class="icheckbox" value="<?php echo $v1[1] ?>"
                                        <?php
                                            if(isset($role_details)) {
                                                echo (in_array($v1[1], $role_details)) ? "checked='checked'" : "";
                                            }
                                        ?> /> <?php
                                    } else {
                                        ?>
                                        <input id="role_details" name="role_details[]" type="checkbox" value="<?php echo $v1[1].",".$v1[2]; ?>"
                                        <?php
                                            if(isset($role_details)) {
                                                echo (in_array($v1[1].",".$v1[2], $role_details)) ? "checked='checked'" : "";
                                            }
                                        ?> /> 
                                        <?php
                                    }
                                
                                
                                  echo "&nbsp;".$v1[0]."<br/>";
                                  echo "</label><br/>";
                                 }
                                echo "<br/>";
                              }	
                    ?>            
                        <span class="help-block" id="role_details_err"></span>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="basicinput">Status*</label>
                    <div class="controls">
                        <select class="form-control select " name="status" id="status" tabindex="3" data-attr="Please select status">
                        <option value=""  <?php echo ($result_data['status'] == NULL) ? 'selected="selectec"' : '';?>>Select Status</option>
                        <option value="1" <?php echo ($result_data['status'] == '1') ? 'selected="selectec"' : '';?>>Active</option>
                        <option value="2" <?php echo ($result_data['status'] == '2') ? 'selected="selectec"' : '';?>>Inactive</option>
                        </select>
                     <span class="help-block" id="status_err"></span>
                    </div>
                </div>
                <input type="hidden" name="id" id="id" value="<?php echo $result_data[$primary_field]?>" />
                <input type="hidden" name="add_page" id="add_page" value="<?php echo $add_page?>" />
                <input type="hidden" name="mode" id="mode" value="<?php echo ((strtolower ($mode) == 'edit')  ? 'edit' : 'add');?>" />            
            	<div class="control-group">
                    <div class="controls">
                        <button class="btn btn-primary " type="submit" name="submit" value="Go" tabindex="4" onclick="return valid_form()">Submit</button>
                        <button class="btn btn-default">Clear Form</button> 
                    </div>
                </div>
            </div>
    	</div>
    </form>        
  </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->  

