
<script src="//cdn.ckeditor.com/4.5.6/standard/ckeditor.js"></script>
<script type="application/javascript">
 function checkInputs(){ 
    
	var language = document.getElementById("language").value;
	var title  	 = document.getElementById("title").value;
	var url  	 = document.getElementById("url").value;
	var image    = document.getElementById("post_image").value;
	var status   = document.getElementById("status").value;
    var link  = document.getElementById("link").value;
	
	
    if(language == "" || language.trim() ==""){
		alert("language  is required");
		document.getElementById("language").focus();
		return false;
	}

	if(title == "" || title.trim() ==""){
		alert("Title  is required");
		document.getElementById("title").focus();
		return false;
	}
	    
	if(url == "" || url.trim() ==""){
		alert("URL  is required");
		document.getElementById("url").focus();
		return false;
	}
	
	  
	if(image == "" ||image.trim() ==""){
		alert("Post image  is required");
		document.getElementById("_image").focus();
		return false;
	}
		
	if(status == "" || status.trim() ==""){
	   alert("Status field is required");
	   document.getElementById("status").focus();
	   return false;
	}
	
	if(link == "" || link.trim() ==""){
	   alert("video is required");
	   document.getElementById("link").focus();
	   return false;
	}
	

		
}

 </script>
<script>
$(document).ready(function(){
	$('#title').on('keyup', function() {
		var title_url = $("#title").val();
		var string = title_url.replace(/[^a-zA-Z 0-9]+/g,'');
		var post_url  = string.replace(/\s+/g, '-').toLowerCase();
		$("#url").val(post_url);
		
	});
 });
</script>
 
<div class="span9">
<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Add Posts</h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo FULL_CMS_URL?>/manage_movie">Manage Movie</a></h3>	
        </div>
        <div class="module-body">
              <?php 
				if( $this->session->flashdata('error') ) { 
				   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
						<strong>'.$this->session->flashdata('error').'</strong></div>';
			
				}else if( $this->session->flashdata('success') ) { 
				
				   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
					  <strong>'.$this->session->flashdata('success').'</strong></div>';
				}
			  ?>
                            
             <br />                

                  <form class="form-horizontal row-fluid" method="post" action="<?php echo FULL_CMS_URL.'/manage_movie/add_action' ?>" enctype="multipart/form-data" />
                    
                         <div class="control-group">
                            <label class="control-label" for="basicinput">Language *</label>
                            <div class="controls">
                                <select tabindex="6" name="language" id="language" data-placeholder="Select category.." class="span8">
                                <option value="" >Select Language</option>
                                 <option value="hindi" >Hindi</option>
                                <option value="kannada" >Kannada</option>
                                <option value="english" >English</option>
                                <option value="telugu" >Telugu</option>
                                <option value="tamil" >Tamil</option>
                               
                                </select>
                            </div>
                        </div>
                       
						<div class="control-group">
                            <label class="control-label" for="basicinput"> Movie Title*</label>
                            <div class="controls">
                                <textarea id="title" name="title" placeholder="title" tabindex="2" class="span8" ></textarea>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">URL*</label>
                            <div class="controls">
                                <textarea id="url" name="url" placeholder="url" tabindex="2" class="span8"></textarea>
                            </div>
                        </div>
                        
						
						<div class="control-group">
                            <label class="control-label" for="basicinput">Movie Image *</label>
                            <div class="controls">
                                <input type="file" name="post_image" id="post_image" placeholder="Post Image" tabindex="3" class="span8">
                               <!--<label>Image size should not be less than 700*245 </label> -->
                               
                        </div>
						</div>
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Movie Discription *</label>
                            <div class="controls">
                                <textarea id="content" name="content" placeholder="content" rows="16" tabindex="7" class="span8"></textarea>
                                <script> CKEDITOR.replace( 'content' ); </script>
                            </div>
                        </div>
						
                        
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Status *</label>
                            <div class="controls">
                                <select tabindex="6" name="status" id="status" data-placeholder="Select Status.." class="span8">
                                <option value="" >Select Status</option>
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                                </select>
                            </div>
                        </div>

						
                         <div class="control-group">
                            <label class="control-label" for="basicinput">Video Url</label>
                            <div class="controls">
                                <input type="text" id="link" name="link" placeholder="Enter Video Url" class="span8" tabindex="2" value="">
                            </div>
                        </div>

                        <div class="control-group">
                            <div class="controls">
                                <input type="submit" name="addblog" value="Save" onclick="return checkInputs()" tabindex="8">
                            </div>
                        </div>
                    </form>
                </div>
                </div>
                                
            </div><!--/.content-->
        </div>
