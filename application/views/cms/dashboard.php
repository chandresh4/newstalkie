	
	<style>
  #li_p p{
	 display:inline;padding:10px; margin:10px;line-height:3.5; 
	 background-color:rgb(255,255,255);border:rgb(102,51,51);
  }
  #li_p p input[type="submit"]{
	background-color: rgba(40, 164, 164, 0.09);
	border: white;
	border-style: double;
	
  }
  .tot{ padding:0px}
  .tot #total,#total2, #total1, #optin1,#optin2, #sub1, #sub2{ float:left; width:20%;margin-top: 0px; line-height: 40px; background: #efefef; color: #000;}
  .tot .text-muted{ float:left;width: 75%; margin-bottom: 0px; line-height: 40px; text-align: left; margin-left: 10px;}    
  
  
  .countbox { height:100px; width: 23%!important; margin-top: 20px;}  
	.sechbtn {  margin-top: 20px; margin-left: 20px;}    
  
</style> 


<script src="<?php echo SITE_URL.SITE_JS;?>highcharts.js"></script>
<link rel="stylesheet" href="<?php echo S3_URL;?>site/css/jquery-ui.css">
<!--<script src="<?php echo SITE_URL.SITE_JS;?>exporting.js"></script>

//<script src="<?php echo S3_URL;?>site/js/jquery-ui.js"></script>-->

<script>
	$(function() {
		var dt = new Date();
		var startDt = (dt.getFullYear() - 100) + '-' + ("0" + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getDate() ;
		var endDt = (dt.getFullYear()  - 0)+ '-' + ("0" + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getDate();
		$( ".datepicker" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			minDate: startDt,
    		maxDate: endDt,
			yearRange: (dt.getFullYear() - 100) + ":" + (dt.getFullYear() ),
			inline: true
		});
		$('.content-box ul.content-box-tabs li a, .change_criteria').click(
			function() { 
				var _this = ".content-box ul.content-box-tabs li a";
				$(_this).parent().siblings().find("a").removeClass('current');
				$(_this).removeClass("default-tab");
				$(_this).removeClass("current");
				
				var currentTab = $(_this).attr('href');
				$(_this).addClass('current default-tab');
				$(currentTab).siblings().hide();
				$(currentTab).show();
				return false; 
			}
		);		
	});	
</script>  
<script>
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}
function getMonday(d) {
  d = new Date(d);
  var day = d.getDay(),
  diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
  return new Date(d.setDate(diff));
}
</script>

<script>
var flag_changed="";

function disable_date()
{
	var date_filter= $("#date_filter").val();
	var c = new Date();var from="";var to="";b = new Date();c = new Date();c1 = new Date();d = new Date();d1 = new Date();
	
	if(date_filter == 1){//today
	
		from 	= to	=	c.toISOString().slice(0,10);
	}else if(date_filter == 2){//yesterday
		c1.setDate(c1.getDate() - 1);
		from 	= to	=	formatDate(c1);		
	}else if(date_filter == 3){//Present Week
		b		= getMonday(b);
		b1		= c.toISOString().slice(0,10);
		from 	= formatDate(b);
		to		= formatDate(b1);
	}else if(date_filter == 4){//Previous Week
		b		= getMonday(b);
		d1		= d1.setDate(b.getDate() - 1);
		d		= getMonday(d1);
		from 	= formatDate(d);
		to		= formatDate(d1);
	}else if(date_filter == 5){//present month
		e		=new Date(c.getFullYear(), c.getMonth(), 1); 
		f		=new Date().toISOString().slice(0,10);
		//f		=new Date(c.getFullYear(), c.getMonth() + 1, 0);
		from 	= formatDate(e);
		to 		= formatDate(f);
	}else if(date_filter == 6){//previous month
		g 		= new Date(c.getFullYear(), c.getMonth() - 1, 1);
		h 		= new Date(c.getFullYear(), c.getMonth() , 0);
		from 	= formatDate(g);
		to 		= formatDate(h);
	}else if(date_filter == 7){//customized data		
		from 	= $("#from_date").val();
		to 		= $("#to_date").val();
	}
	document.getElementById("from_date").value 	= from;
	document.getElementById("to_date").value 	= to;
}
$(function () { 
  
 var cms_site_url  = $("#cms_site_url").val();
	$.ajax({
			url:cms_site_url+"/dashboard/get_graph",
			type: "post",
			beforeSend: function() {
				},
			success: function(e) {
		      JSON.stringify(e);
			  var obj = JSON.parse(e);
				$('#all_div').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 1,//null,
            plotShadow: false
        },
        title: {
            text: 'Category Status'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
				showInLegend: true
            }
        },
        series: [{
            type: 'pie',
            name: 'share',
            data: obj
        }]
    });
				
			}
	});
	
});
</script>
<script>
function getcount(){

	var n = "";
	var from = $("#from_date").val();
	var to = $("#to_date").val();
	var category = $("#category").val();
	var optin = $("#optin").val();
	var status = $("#status").val();
	var source = $("#source").val();
	var medium = $("#medium").val();
	var sub = $("#sub").val();
	var cms_site_url  = $("#cms_site_url").val();
	$.ajax({
			url:cms_site_url+"/dashboard/get_val_count/"+from+"/"+to+"/"+category+"/"+status+"/"+optin+"/"+source+"/"+medium+"/"+sub,
			type: "post",
			data: n,
			beforeSend: function() {
				},
			success: function(e) {
				
				JSON.stringify(e);
					if(e != ""){
						var obj = JSON.parse(e);
						var writers  =  parseFloat(obj['writers']);
						var writers_posts  =  parseFloat(obj['writers_posts']);
						var subscribers  =  parseFloat(obj['subscribers']);
						var total_posts  =  parseFloat(obj['total_posts']);
						document.getElementById("writers").innerHTML = writers;
						document.getElementById("writers_posts").innerHTML = writers_posts;
						document.getElementById("subscribers").innerHTML = subscribers;
						document.getElementById("posts").innerHTML = total_posts;
				}
			}
		});
}
  function show_graph(smiley){
   var cms_site_url  = $("#cms_site_url").val();
	$.ajax({
			url:cms_site_url+"/dashboard/get_graphcount/"+smiley,
			type: "post",
			beforeSend: function() {
			},
			success: function(html) {
			var html = $.trim(html); 
			var data  = html;
			JSON.stringify(data);
			var obj = JSON.parse(data);
				
			if(obj != "nodata"){
				
			  $('#graph_div').highcharts({
             chart: {
				plotBackgroundColor: null,
				plotBorderWidth: 1,//null,
				plotShadow: false
             },
            title: {
                text: 'Rating count'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
               pie: {
                  allowPointSelect: true,
                  cursor: 'pointer',
                  dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                 },
				 showInLegend: true
              }
           },
          series: [{
            type: 'pie',
            name: 'share',
            data: obj
          }]
     }); 
				
			}else{
			
	 	        $('#graph_div').html("<span>No data Available</span>");
		  } 
			}
	});
	
}
</script>

<div class="span9">
        <div class="content">
		<div class="module-body">
			<div class="btn-controls">
				
                <div class="btn-box-row row-fluid " style="background-color:#fff;">
			
                      <div  class=" big span">
                       <!-- <form  class="navbar-search pull-left input-append" action="javascript:void(0)" onsubmit="return getcount()" method="post">
                       --> <form name="dashborad_form" action="javascript:void(0)" onsubmit="return getcount()" >     
						<div class="btn-box-row row-fluid sechbtn">
                         <select  name="date_filter" id="date_filter" onChange="disable_date()" >     
                                <option value="1" >Today</option>
                                <option value="2" >Yesterday</option>
                                <option value="3" >Present week</option>
                                <option value="4" >Previous week</option>
                                <option value="5" >Present month - <?php echo date("F");?></option>
                                <option value="6" >Previous month - <?php echo date("F", strtotime("last month"));?></option>
                                <option value="7" >Custom filter</option>
                            </select>
							
							<input class="input-block-level datepicker" type="text" style="width:250px;"
                            name="from_date" id="from_date" placeholder="Click Custom filter to choose date"  value= "<?php echo  date("Y-m-d");?>" />
							
							<input class="input-block-level datepicker" style="width:250px;" type="text"  name="to_date" id="to_date" placeholder="Click Custom filter to choose date"  value= "<?php echo  date("Y-m-d");?>" />
							</div>
							<div class="btn-box-row row-fluid sechbtn">
							
							<select name="category" id="category"  >     
                                <option value="all" >ALL Category</option>
                                <?php if(isset($category) && $category !=""){
									foreach($category as $p){ ?>
									<option value="<?php echo $p->cat_id?>" ><?php echo $p->name?></option>
									<?php }
								}?>
                            </select>
							
							<select  name="optin" id="optin"  >     
                                <option value="1" >Optin</option>
                                <option value="2" >Non-Optin</option>
                                <option value="3" >Both Optin and Non-Optin</option>
                            </select>
							
							<select  name="status" id="status"  >     
                                <option value="1" >Active</option>
                                <option value="2" >Inactive</option>
                                <option value="3" >Both Active and Inactive</option>
                            </select>
							</div>
							
							<div class="btn-box-row row-fluid sechbtn">
							
							<select name="source" id="source"  >     
                                <option value="sel" >Select Source</option>
                                <?php if(isset($source) && $source !=""){
									foreach($source as $p){ ?>
									<option value="<?php echo $p->net?>" ><?php echo $p->net?></option>
									<?php }
								}?>
                            </select>
							
							<select name="medium" id="medium"  >     
                                <option value="sel" >Select UTM medium</option>
                                <?php if(isset($medium) && $medium !=""){
									foreach($medium as $p){ ?>
									<option value="<?php echo $p->medium?>" ><?php echo $p->medium?></option>
									<?php }
								}?>
                            </select>
					
							<select  name="sub" id="sub">     
                                <option value="sel" >Select UTM Sub </option>
                                <?php if(isset($sub) && $sub !=""){
									foreach($sub as $p){ ?>
									<option value="<?php echo $p->sub?>" ><?php echo $p->sub?></option>
									<?php }
								}?>
                            </select>
							</div>
							
                     <input type="submit" name="dashboard_sbmt" id="dashboard_sbmt" value= "Submit"/>
                       
                        </form>
                    </div>
                </div>
              
                <div class="btn-box-row row-fluid ">
					<a href="#" class="btn-box big span4 total1 tot countbox" id='writer' style="border-color: transparent; border-style: solid; border-width: thick;"><b id="writers">0</b>
                        <p class="text-muted">
                            Writers Count</p>
                    </a>
					<a href="#" class="btn-box big span4 optin1 tot countbox" id='writer_post' style="border-color: transparent; border-style: solid; border-width: thick;"><b id="writers_posts">0</b>
                        <p class="text-muted">
                             Writer Post Count</p>
                    </a>
					<a href="#" class="btn-box big span4 sub1 tot countbox" id='sub' style="border-color: transparent; border-style: solid; border-width: thick;"><b id="subscribers">0</b>
                        <p class="text-muted">
                            Subscriber Count</p>
                    </a>
                    <a href="#" class="btn-box big span4 total1 tot countbox" id='sub' style="border-color: transparent; border-style: solid; border-width: thick;"><b id="posts">0</b>
                        <p class="text-muted">
                            Posts Count</p>
                    </a>					
                </div>
                
            </div>
			
            
			<!--GRAPHS START getchart-->                               
                    <ul  style=" list-style:none;margin-left:0px; "> 
                       <li style="display:inline-block;">
                        <div id="all_div" style="height:350px; width:860px;  margin: 0 auto;"></div>
                       </li>                 
                    </ul>
                 <!--GRAPHS END getchart-->
				  <ul class="widget widget-usage unstyled span20" style=" margin-left: 0px;">
                        <li id="li_p"  align="left">
                            <p >
                            	<input type="submit" name="happy" id="happy" onclick="show_graph('happy')" value= "Happy Count Chart"/>
                            </p>
                            <p >
                            	<input type="submit" name="sad" id="sad" onclick="show_graph('sad')" value= "Sad Count Chart"/>
                            </p>
							<p >
                            	<input type="submit" name="angry" id="angry" onclick="show_graph('angry')" value= "Angry Count Chart"/>
                            </p>
							<p >
                            	<input type="submit" name="neutral" id="neutral" onclick="show_graph('neutral')" value= "Neutral Count Chart"/>
                            </p>
                        </li>
						
                        
                     </ul>
					  <!--GRAPHS START getchart-->                               
                    <ul  style=" list-style:none;margin-left:0px; "> 
					
                       <li style="display:inline-block;">
                        <div id="graph_div" style="height:350px; width:860px;  margin: 0 auto;"></div>
                       </li>                 
                    </ul>
                 <!--GRAPHS END getchart-->
			
			
        </div>
		</div>
   <!--/.content-->
   </div>







           
        </div>
   </div>