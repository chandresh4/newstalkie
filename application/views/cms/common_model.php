<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class common_model extends CI_Model
{
	protected $data;
    public function __construct()
    {
        parent::__construct();
    }

	public function Menu_Array(){ 
		
		$horoscope  	= array(
							array("Manage Zodiac", "zodiac_horoscope", "zodiac_horoscope",  1, " fa-bar-chart-o"),
							array("Daily Horoscope", "manage_daily_horos", "manage_daily_horos",  2, " fa-bar-chart-o"),
							array("Numerology Content ", "manage_numerology", "manage_numerology",  3, " fa-bar-chart-o"),
							array("Palm Reading", "mmanage_palm", "manage_palm",  4, " fa-bar-chart-o"),
							array("Latest News", "news", "news",  5, " fa-bar-chart-o"),
							array("Astrological Stones", "birthstones", "birthstones",  6, " fa-bar-chart-o"),
							array("Love Calculator Content", "love_content", "love_content",  7, " fa-bar-chart-o"),
							array("Love Book Content", "manage_love_book", "manage_love_book",  8, " fa-bar-chart-o"),
							array("Notifications", "manage_notifications", "manage_notifications",  9, " fa-bar-chart-o"),
							array("All Kalas", "manage_kala", "manage_kala",  10, " fa-bar-chart-o"),
							array("Static pages", "static_pages", "static_pages",  11, " fa-bar-chart-o"),
							array("Celebrity", "manage_celebrity", "manage_celebrity",  12, " fa-bar-chart-o"),
							array("Banner Settings", "manage_setting", "manage_setting",  13, " fa-bar-chart-o"),
							array("Website Blogs", "manage_blog", "manage_blog",  14, " fa-bar-chart-o"),
							array("2017 Yearly Horoscope", "zodiac_horoscope_2017", "zodiac_horoscope_2017",  15, " fa-bar-chart-o"),
							);
							
		$horoscopereport  = array(
			                array("Upload 2017 Horoscope", "horoscope_uploads", "horoscope_uploads",  1, " fa-bar-chart-o"),
			                array("2017 Horoscope-Subscribers", "horoscope_subscribe", "horoscope_subscribe",  2, "fa-code-fork"),
							array("Generate Horoscope Report", "horoscope_report", "horoscope_report",  4, " fa-bar-chart-o"),
		                    );						
		
		$numerology  	= array(
							array("Upload Numerology", "manage_uploads", "manage_uploads",  1, " fa-bar-chart-o"),
							array("Numerology-Subscribers", "numerology_subscribe", "numerology_subscribe",  2, "fa-code-fork"),
			                array("Upload Files", "file_uploads", "file_uploads",  3, " fa-bar-chart-o"),
            				);
		
		$kundalimatch 	= array(
							array("Upload Kundali Match", "kundali_uploads", "kundali_uploads",  1, " fa-bar-chart-o"),
							array("Kundalimatch-Subscribers", "kundalimatch_subscribe", "kundalimatch_subscribe",  2, "fa-code-fork")
						);						
						
		$networks  		= array(
					    	array("Manage UTM Source", "manage_utm_source", "manage_utm_source",  1, "fa-code-fork"),
							array("Manage UTM Affiliate", "manage_utm_aff", "manage_utm_aff",  2, "fa-code-fork")
						);	
						
		$report  		= array(
							array("Report", "manage_netwisereport", "manage_netwisereport",  1, " fa-bar-chart-o")
						);	
		
		$user  			= array(
					    	array("Registered User", "user_details", "user_details",  1, "fa-code-fork"),
							array("Subscribed User", "user_subscribe", "user_subscribe",  2, "fa-code-fork"),
							array("User Signrequest", "user_signrequest", "user_signrequest",  2, "fa-code-fork")
						);	
						
		$caching  		= array(
			  				array("Clear Cache", "cache", "cache",  1, "fa-code-fork")
		  				);				
		
		$cms_access    =  array(
							array("Manage user", "manage_admin", "manage_admin",  1, "fa fa-group"),
							array("Manage role", "manage_role", "manage_role", 2, "fa-magic"),
						);			
		
		$pagedata  	= array(
							array("Manage Page Data", "manage_meta", "manage_meta",  1, " fa-bar-chart-o")
		);							
		
		$compa_res	= array(
			                array("Name dob compatibility", "manage_namedob_com", "manage_namedob_com",  1, " fa-bar-chart-o"),
							array("Zodiac sign compatibility", "manage_zodiac_com", "manage_zodiac_com",  2, " fa-bar-chart-o"),

		               );	
		
		$menu      		= array(
							array("Horoscope",	"horoscope",	$horoscope ,	  1,     'fa-cogs'),	
					        array("Numerology",	"numerology",	$numerology,	  2,     'fa-sitemap'),
							array("Kundali Match",	"kundali match",	$kundalimatch ,	  3,     'fa-sitemap'),
							array("Horoscope 2017",	"horoscope 2017",	$horoscopereport ,	  4,     'fa-sitemap'),
							array("Compatibility Reports",	"name_num",	$compa_res ,	  6,     'fa-sitemap'),
							array("Networks",	"networks",		$networks ,		  4,     'fa-cogs'),	
					        array("Report",		"report",		$report ,		  5,     'fa-sitemap'),
							array("User",		"user",			$user ,			  6,     'fa-cogs'),	
					        array("Admin control","Admin",		$cms_access,	  7,     'fa-cogs'),	
					        array("Caching",	"caching",		$caching ,		  8,     'fa-sitemap'),
							array("Page Data",	"pagedata",		$pagedata ,	  	  9,     'fa-sitemap')
							
						);
		
		return $menu;
	}

	public function get_today_count()
	{
		//$sql="select count(id) as cnt from survey_data where DATE_FORMAT(participated_on,'%Y-%m-%d')=CURDATE()";
		$result=array();
		$sql="SELECT name, email, net, pub, bnr, fire_status, participated_on FROM survey_data WHERE DATE_FORMAT(participated_on,'%Y-%m-%d')=CURDATE() GROUP BY email ORDER BY id DESC";
		$query=$this->db->query($sql);
		$count=0;
		$details=array();
		if($query->num_rows > 0)
		{
			foreach($query->result() as $row)
			{
				//return $row->cnt;
				$details[]=$row;
				$count++;
			}
		}
		$result['details']=$details;
		$result['count']=$count;
		return $result;
    }
	public function status_dropdown($sel_status='')
	{
		$status_dropdown="<option value=''>select status</option>";
		$a_status="";
		$i_status="";
		if($sel_status)
		{
			$a_status="selected=selected";
		}
		else
		{
			$i_status="selected=selected";
		}
		$status_dropdown.="<option value='1' ".$a_status.">active</option>";
		$status_dropdown.="<option value='0' ".$i_status.">inactive</option>";
		return $status_dropdown;
    }
	public function filter_dropdown($filter='', $checking)
	 {
		 $sql='select DISTINCT('.$filter.') from '.USER.' where status=1 && '.$filter.' != "" ';
		 $result=array();
		 $query=$this->db->query($sql);
		 $dropdown='<select class="style-select" name="'.$filter.'" id="'.$filter.'" ><option value="sel" > Select '.ucfirst($filter).'</option>';
		 if($query->num_rows() > 0)
		 {  
			 $result['result']=true;
			 foreach($query->result() as $row)
			 { $x = $row->$filter;
			   $y='<option value="'. $x.'" '.($x == $checking ? "selected" : "").' >'.$x.'</option>';
        		  $dropdown.=$y;
			 }
			 $dropdown.="</select>";
			 
			 $result['filter_dropdown']=$dropdown;
		 }
		 else
		 {
			 $result['filter_dropdown']=false;
		 }
		 return $result;
	 }
	public function utm_sources()
	 {
		 $result = $this->db->query('SELECT DISTINCT(net) FROM '.USER.'  where status=1 && net != "" order by net asc');
		   if ($result->num_rows() < 1 ){
			   return "empty";
		   }
		   else{
			   return $result->result();
		   }
	 }
	 public function utm_mediums()
	 {
		 $result = $this->db->query('SELECT DISTINCT(utm_medium) FROM '.USER.'  where status=1 order by utm_medium asc');
		   if ($result->num_rows() < 1 ){
			   return "empty";
		   }
		   else{
			   return $result->result();
		   }
	 }
	 public function utm_pubs($net="")
	 {
		 $result = $this->db->query('SELECT DISTINCT(pub) FROM '.USER.'  where net="'.$net.'" && status=1 && pub != "" order by pub asc');
		   if ($result->num_rows() < 1 ){
			   return "empty";
		   }
		   else{
			   return $result->result();
		   }
	 }
	function send_mail($email_elements='')
    {
	 
      $from =  $email_elements['from_email']; // change it to yours
      $to = $email_elements['to_email'];// change it to yours
      $subject = $email_elements['subject'];
	  $message = $email_elements['message'];
	  
	  $headers = "MIME-Version: 1.0" . "\r\n";
	  $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	  $headers .= 'From: '.$from.' <'.$from.'>' . "\r\n";
		
		@mail($to, $subject, $message, $headers);
         
     }
	 public function get_country_dropdown($country_id='')
	 {
		 $sql="select id, state from datamanager_state";
		 $result=array();
		 if($country_id)
		 {
			 $sql.=" where country_id = ".$country_id;
		 }
		 $query=$this->db->query($sql);
		 $dropdown='<select class="style-select" name="state" id="state" ><option value="" >state</option>';
		 if($query->num_rows() > 0)
		 {  
			 $result['result']=true;
			 foreach($query->result() as $row)
			 {
				 $dropdown.="<option value='".$row->id."'>".$row->state."</option>";
			 }
			 $dropdown.="</select>";
			 
			 $result['country_dropdown']=$dropdown;
		 }
		 else
		 {
			 $result['result']=false;
		 }
		 return $result;
	 }
	 public function add_nothanks_url($inputs='')
	 {
		 $sql="select id from no_thanks where url='".$inputs['url']."'";
		 $query=$this->db->query($sql);
		 if($query->num_rows() < 1)
		 {
			 $this->db->insert('no_thanks',$inputs);
			 return true;
		 }
		 else
		 {
			 return false;
		 }
	 }
	 public function get_add_url()
	 {
		 $sql="select id, campaign_id, location, url, url_fire_limit, status, created_on from no_thanks";
		 $query=$this->db->query($sql);
		 $result=array();
		 foreach($query->result() as $row)
		 {
			 $result[]=$row;
		 }
		 return $result;
	 }

	 public function get_rand_url()
	 {
		 $country = $this->session->userdata('country') != "" ? $this->session->userdata('country') : 'in';
		 
		 $sql="select id, campaign_id, url from no_thanks where status =1 and location = '".trim($country)."' order by rand() limit 1";
		 
		 $query=$this->db->query($sql);
		 $result="";
		 foreach($query->result() as $row)
		 {
			 $result['url']=$row->url;
			 $result['id'] = $row->id;
			 $create_logs = $this->create_logs($row->id, $row->campaign_id);
		 }

		 return $result;
	 }
	 

     public function create_logs($close_id ="", $campaign_id=""){
		 
		 if($close_id != ""){
			$query= $this->db->query("select close_link_id from no_thanks_logs where close_link_id =".$close_id." and DATE_FORMAT(created_on,'%Y-%m-%d') = CURDATE()");
		 
			if($query->num_rows() < 1){
		
				 $this->db->query("insert into no_thanks_logs (close_link_id, campaign_id, close_link_fired, status) values ($close_id,$campaign_id,0,1)");
			 }
		 }
		 return true;
	 }
	 
	 public function closebtn_fire($close_id =""){
		
		$sql="update no_thanks_logs set 
		                                close_link_fired = (close_link_fired +1) 
								   where 
								       close_link_id = ".$close_id." and 
		                               DATE_FORMAT(created_on,'%Y-%m-%d') = CURDATE()";
		$query=$this->db->query($sql);
		return true;
	 }


      public function change_net_pub($table_name, $fire_per, $goal, $id, $fire_type, $net_pub_name, $header_name){
		 
		 if($id > 0 ) {
			 if($fire_per > $goal){
				$new_fire_check = $fire_type - 1 ;
			    $new_fire_type = ($new_fire_check > 1) ? $new_fire_check : 1 ;
				$status = "Fire Type Decrease";
				$this->db->where('id', $id);
				$this->db->update($table_name, array('fire_type' => $new_fire_type));
				
			 }else if($fire_per < $goal){
			    $new_fire_check = $fire_type + 1 ;
				$new_fire_type = ($new_fire_check < 9) ? $new_fire_check : 9 ;
			    $status = "Fire Type Increase";
				$this->db->where('id', $id);
				$this->db->update($table_name, array('fire_type' => $new_fire_type));
			}else{
				$status = "Not Chnaged";
				$new_fire_type = "Not Chnaged";
			}
		 }
		 
                $net_report = "<tr>
								<td>".$net_pub_name."</td>
								<td>".$goal."</td>
								<td>".$fire_per."</td>
								<td>".$fire_type."</td>								
								<td>".$new_fire_type."</td>
								<td>".$status."</td>
								</tr>";
								
				return $net_report; 			
		 
	 }
	 



public function get_banner($id )
	 {   
	     $this->db->cache_on();
		 //$country = $this->session->userdata('country') != "" ? $this->session->userdata('country') : 'in';
		 if( $id != ""){
			 $sql="select id, setting_name, setting_value, status from ".SETTING." where status = 1 and id = '".trim($id)."'";
			 
			 $query=$this->db->query($sql);
			 $ads_banner="";
			 foreach($query->result() as $row)
			 {
				 $ads_banner =$row->setting_value;
			 }
	
			 return $ads_banner;
		 }else{
			 return false;
		 }
			 
	}
	

   public function homepage_popunder($id = "" ){   
		$this->db->cache_on();
		//$country = $this->session->userdata('country') != "" ? $this->session->userdata('country') : 'in';
		if( $id != ""){
		    $sql="select id, campaign, campaign_url, popunder_type, status from ".HOMEPAGE_CAMPAIGN." where status = 1 and id = '".trim($id)."'";
		 
			$query=$this->db->query($sql);
			$home_banner="";
			
			foreach($query->result() as $row){
				$home_banner =$row->campaign_url;
			}
		
		    return $home_banner;
		}else{
		    return false;
		}
			 
	}
	
	 

}

?>