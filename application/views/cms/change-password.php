<script>
 function myFunction()
 {
   
	var password=document.getElementById("password").value;
	var newpassword=document.getElementById("newpassword").value;
    var cpassword=document.getElementById("cpassword").value;


       

		if(password == "" || password.trim() =="")
	    {
		    alert("password field is required");
			document.getElementById("password").value="";
			document.getElementById("password").focus();
			return false;
		}

		if(newpassword == "" || newpassword.trim() =="")
	    {
		    alert("new password field is required");
			document.getElementById("newpassword").value="";
			document.getElementById("newpassword").focus();
			return false;
		}
		if(cpassword == "" || cpassword.trim() =="")
	    {
		    alert("confirmed password field is required");
			document.getElementById("cpassword").value="";
			document.getElementById("cpassword").focus();
			return false;
		}
		
    
 }

</script>



 

<div class="span9">
<div class="content">
<div class="module">
    <div class="module-head">
        <h3>Change Password</h3>
        
    </div>
    <div class="module-body">
            <?php 
            if( $this->session->flashdata('error') ) { 
               echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>'.$this->session->flashdata('error').'</strong></div>';
        
            }else if( $this->session->flashdata('success') ) { 
            
               echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>'.$this->session->flashdata('success').'</strong></div>';
            }
          ?>
            <br />                

              <form class="form-horizontal row-fluid" method="post" action="<?php echo FULL_CMS_URL.'/changepassword/update' ?>">
               
               
                
                    
                    
                    
                    <div class="control-group">
                        <label class="control-label" for="basicinput"> Current Password</label>
                        <div class="controls">
                           <input type="password" name="password" id="password" tabindex="3" class="span8"/>
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="basicinput"> New Password</label>
                        <div class="controls">
                           <input type="password" name="newpassword" id="newpassword" tabindex="3" class="span8"/>
                        </div>
                    </div>
					<div class="control-group">
                        <label class="control-label" for="basicinput">Confirm Password</label>
                        <div class="controls">
                           <input type="password" name="cpassword" id="cpassword" tabindex="3" class="span8"/>
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <div class="controls">
                            
                            <input type="submit" name="register" id="register" value="Update" onclick ="return myFunction()"/>
                        </div>
                    </div>
                   
                </form>
            </div>
            </div>
                            
        </div><!--/.content-->
    </div>

