
<script src="//cdn.ckeditor.com/4.5.6/standard/ckeditor.js"></script>
<script type="application/javascript">
 function checkInputs(){ 
    
	var category = document.getElementById("category").value;
	var title  	 = document.getElementById("title").value;
	var url  	 = document.getElementById("url").value;
	var sub_title= document.getElementById("sub_title").value;
	var image    = document.getElementById("post_image").value;
	var status   = document.getElementById("status").value;
    var vstatus  = document.getElementById("vstatus").value;
	var tstatus  = document.getElementById("tstatus").value;
	
    if(category == "" || category.trim() ==""){
		alert("category  is required");
		document.getElementById("category").focus();
		return false;
	}

	if(title == "" || title.trim() ==""){
		alert("Title  is required");
		document.getElementById("title").focus();
		return false;
	}
	if(!$("#title").val().match(/^[a-zA-Z0-9?.!-&_,:\s]+$/)){
	       alert("Title  is required");
		  document.getElementById("title").focus();
	       return false;	
	}    
	if(url == "" || url.trim() ==""){
		alert("URL  is required");
		document.getElementById("url").focus();
		return false;
	}
	if(sub_title == "" || sub_title.trim() ==""){
		alert("Sub Title is required");
		document.getElementById("sub_title").focus();
		return false;
	}
	if(!$("#sub_title").val().match(/^[a-zA-Z0-9?.!-_@*,:\s]+$/)){
	       alert("sub title  is required");
		  document.getElementById("sub_title").focus();
	       return false;	
	}   
	if(image == "" ||image.trim() ==""){
		alert("Post image  is required");
		document.getElementById("_image").focus();
		return false;
	}
		
	if(status == "" || status.trim() ==""){
	   alert("Status field is required");
	   document.getElementById("status").focus();
	   return false;
	}
	if(vstatus == "" || vstatus.trim() ==""){
	   alert("Viral Status field is required");
	   document.getElementById("vstatus").focus();
	   return false;
	}
	if(tstatus == "" || tstatus.trim() ==""){
	   alert("Trendy Status field is required");
	   document.getElementById("vstatus").focus();
	   return false;
	}
	

		
}

 </script>
<script>
$(document).ready(function(){
	$('#title').on('keyup', function() {
		var title_url = $("#title").val();
		var string = title_url.replace(/[^a-zA-Z 0-9]+/g,'');
		var post_url  = string.replace(/\s+/g, '-').toLowerCase();
		$("#url").val(post_url);
		
	});
 });
</script>
 
<div class="span9">
<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Add Posts</h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo FULL_CMS_URL?>/manage_posts">Manage Posts</a></h3>	
        </div>
        <div class="module-body">
              <?php 
				if( $this->session->flashdata('error') ) { 
				   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
						<strong>'.$this->session->flashdata('error').'</strong></div>';
			
				}else if( $this->session->flashdata('success') ) { 
				
				   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
					  <strong>'.$this->session->flashdata('success').'</strong></div>';
				}
			  ?>
                            
             <br />                

                  <form class="form-horizontal row-fluid" method="post" action="<?php echo FULL_CMS_URL.'/manage_posts/add_action' ?>" enctype="multipart/form-data" />
                    
                         <div class="control-group">                                        
                                <label class="control-label" for="basicinput">Country *</label>
                                <div class="controls">
                                   <select class="js-example-basic-single span8" name="country" id="country" tabindex="1" data-attr="Please select status" required>
                                    <option value="">Select Country</option>
                                    <option value="in">India</option>
                                    <option value="gb">UK</option>
                                    <option value="us">US</option>
                                </select>
                                <span class="help-block" id="country_err"></span>
                              </div>
                          </div>
                         <div class="control-group">
                            <label class="control-label" for="basicinput">Category *</label>
                            <div class="controls">
                                <select tabindex="6" name="category" id="category" data-placeholder="Select category.." class="js-example-basic-single span8">
                                <option value="" >Select category</option>
                               <?php foreach ($result as $res) { ?>
							   <option value =" <?php echo $res->cat_id ?>"><?php echo $res->name ?></option>
							   <?php } ?>
                                </select>
                            </div>
                        </div>

                       <div class="control-group">
                            <label class="control-label" for="basicinput">Tag Category *</label>
                            <div class="controls">
                                <select multiple="multiple" tabindex="6" name="tag[]" id="tag" data-placeholder="Select tag category.." class=" js-example-basic-multiple span8">
                               
                               <?php foreach ($result1 as $res1) { ?>
							   <option value =" <?php echo $res1->t_id ?>"><?php echo $res1->tag_name ?></option>
							   <?php } ?>
                                </select>
                            </div>
                        </div>

						<div class="control-group">
                            <label class="control-label" for="basicinput"> Post Title*</label>
                            <div class="controls">
                                <textarea id="title" name="title" placeholder="title" tabindex="2" class="span8" ></textarea>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">URL*</label>
                            <div class="controls">
                                <textarea id="url" name="url" placeholder="url" tabindex="2" class="span8"></textarea>
                            </div>
                        </div>
                        
						<div class="control-group">
                            <label class="control-label" for="basicinput"> Post Sub Title*</label>
                            <div class="controls">
                                <textarea id="sub_title" name="sub_title" placeholder="sub title" tabindex="2" class="span8" ></textarea>
                            </div>
                        </div>
						<div class="control-group">
                            <label class="control-label" for="basicinput">Post Image *</label>
                            <div class="controls">
                                <input type="file" name="post_image" id="post_image" placeholder="Post Image" tabindex="3" class="span8">
                               <!--<label>Image size should not be less than 700*245 </label> -->
                               
                        </div>
						</div>
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Post Discription *</label>
                            <div class="controls">
                                <textarea id="content" name="content" placeholder="content" rows="16" tabindex="7" class="span8"></textarea>
                                <script> CKEDITOR.replace( 'content' ); </script>
                            </div>
                        </div>
						
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Status *</label>
                            <div class="controls">
                                <select tabindex="6" name="status" id="status" data-placeholder="Select Status.." class="span8">
                                <option value="" >Select Status</option>
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                                </select>
                            </div>
                        </div>

						<div class="control-group">
                            <label class="control-label" for="basicinput">Want to make Viral? *</label>
                            <div class="controls">
                                <select tabindex="6" name="vstatus" id="vstatus" data-placeholder="Select Viral Status.." class="span8">
                                <option value="" >Select Viral Status</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                                </select>
                            </div>
                        </div>
						<div class="control-group">
                            <label class="control-label" for="basicinput">Want to make Trendy? *</label>
                            <div class="controls">
                                <select tabindex="6" name="tstatus" id="tstatus" data-placeholder="Select Trendy Status.." class="span8">
                                <option value="" >Select Trendy Status</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                         <div class="control-group">
                            <label class="control-label" for="basicinput">Video Url</label>
                            <div class="controls">
                                <textarea id="link" name="link" placeholder="Video Url" tabindex="2" class="span8" ></textarea>
                            </div>
                        </div>

                        <div class="control-group">
                            <div class="controls">
                                <input type="submit" name="addblog" value="Save" onclick="return checkInputs()" tabindex="8">
                            </div>
                        </div>
                    </form>
                </div>
                </div>
                                
            </div><!--/.content-->
        </div>


         <script>
            $(document).ready(function() {
                $('.js-example-basic-multiple').select2();
                $('.js-example-basic-single').select2();
            });
        </script>
