<?php //$arr_all = all_arrays(); ?>

<!-- PAGE CONTENT WRAPPER -->
<div class="span9">                
    <div class="content">
        <div class="module">
            
            <!-- START DATATABLE EXPORT -->
                <div class="module-head">
                    <h3>User Posts</h3>
                        <h3 align="right"><a  style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;"href="<?php echo FULL_CMS_URL."/manage_user"?>">Manage User</a></h3>  
                </div>
                <div class="module-body">
				  <?php 
				if( $this->session->flashdata('error') ) { 
				   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
						<strong>'.$this->session->flashdata('error').'</strong></div>';
			
				}else if( $this->session->flashdata('success') ) { 
				
				   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
					  <strong>'.$this->session->flashdata('success').'</strong></div>';
				}
				?>
				<br />
				<!-- <hr /> -->
				<br />
                    <table id="customers2" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>S No.</th>
                                <th >Category Name</th>
                                 <th>Post Name</th>
                                <th >Status</th>
								<th >Action</th>
                                <th >Update</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            if(is_array($details) && count($details) > 0){	
                              $sl_no=1;
                              foreach($details as $p){
                                  
                                    echo "<tr>";
                                    echo "<td>". $sl_no."</td>";
									foreach($res as $q){
									if($p->category == $q->cat_id){echo "<td>". ucfirst($q->name)."</td>";}
									}
									echo "<td>". ucfirst($p->post_title)."</td>";
                                    echo "<td><span class='label label-success'>".($p->status ==1 ? 'Active' : 'Inactive')."</span></td>";
									//echo "<td><a href='".FULL_CMS_URL."/manage_innerposts?id=".$p->id."'> <img alt='Edit' src='".S3_URL."cms/images/icons/plus.jpg'  width='20' height='20'></img></a></td>";
									echo "<td><a href='".FULL_CMS_URL."/manage_posts/edit?id=".$p->id."'> <img alt='Edit' src='".S3_URL."cms/images/icons/pencil.png'  width='20' height='20'></img></a></td>";
									if($p->live_status != 1){
									echo "<td><a href='".FULL_CMS_URL."/manage_user/update?id=".$p->writer_id."&pid=".$p->id."'> Make Live</a></td>";
                                    }else{
										echo "<td><span>Already In Live</span></td>";
									}
							
                                    echo "</tr>";
                                    $sl_no++;
                              }
                            }else{
								 echo "<tr>";
                                    echo "<td columnspan='4'>No Data Found</td>";
									echo "</tr>";
							}
                        ?>
                        </tbody>
                    </table>                                    
                    
                </div>
            <!-- END DATATABLE EXPORT -->                            
            
        </div>
    </div>

</div>         
<!-- END PAGE CONTENT WRAPPER -->




