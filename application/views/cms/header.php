<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Newstalkie</title>
        <!--<link rel="stylesheet" type="text/css" href="<?php echo S3_URL;?>/css/pagename.css" />-->
        
        <link type="text/css" href="<?php echo S3_URL;?>cms/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css" href="<?php echo S3_URL;?>cms/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link type="text/css" href="<?php echo S3_URL;?>cms/css/theme.css" rel="stylesheet">
        <link type="text/css" href="<?php echo S3_URL;?>cms/images/icons/css/font-awesome.css" rel="stylesheet">
        <script src="<?php echo S3_URL;?>cms/scripts/jquery-1.9.1.min.js" type="text/javascript"></script> 
        <script src="<?php echo S3_URL;?>cms/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script> 
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'rel='stylesheet'>
        
		<!--
        
        <style>	.no-js #loader { display: none;  }	.js #loader { display: block; position: absolute; left: 100px; top: 0; }</style>	
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
		<script> $(window).load(function() {	$("#loader").animate({	top: -2000	}, 1500);	});</script>
        
        -->
        
    </head>
    <?php //flush(); ?>
    <body>
    <!--<img src="<?php //echo S3_URL;?>cms/images/user.png" id="loader">-->
	
	
	<!--<img src="//farm6.static.flickr.com/5299/5400751421_55d49b2786_o.jpg">-->
		
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner" style="background: #0C3851;">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <i class="icon-reorder shaded"></i></a><a class="brand" href="<?php echo FULL_CMS_URL;?>/dashboard"><img style="height: 75px" alt="logo" src="<?php echo S3_URL ?>/site/images/newstalkie-logo.svg" class="img-responsive" ></a>
                    <div class="nav-collapse collapse navbar-inverse-collapse">
                        
                        <ul class="nav pull-right">
                            <li class="nav-user dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img alt="user" src="<?php echo S3_URL;?>cms/images/user.png" class="nav-avatar" />
                                <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Your Profile</a></li>
                                    <li><a href="#">Edit Profile</a></li>
                                    <li class="divider"></li>
                                    <li><a href="<?php echo FULL_CMS_URL;?>/logout_user/logout">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- /.nav-collapse -->
                </div>
            </div>
            <!-- /navbar-inner -->
        </div>
       
       
       
    <input type="hidden" id="cms_site_url" value="<?php echo FULL_SITE_URL;?>/cms">
    <input type="hidden" id="cms_url" value="<?php echo SITE_URL;?>cms">
    
