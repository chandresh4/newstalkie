<script src="//cdn.ckeditor.com/4.5.6/standard/ckeditor.js"></script>
<script type="application/javascript">
 function checkInputs(){ 
    
	var language = document.getElementById("language").value;
	var title  	 = document.getElementById("title").value;
	var url  	 = document.getElementById("url").value;
	var image    = document.getElementById("post_image").value;
	var status   = document.getElementById("status").value;
    var video  = document.getElementById("video").value;
	
	
    if(language == "" || language.trim() ==""){
		alert("language  is required");
		document.getElementById("language").focus();
		return false;
	}

	if(title == "" || title.trim() ==""){
		alert("Title  is required");
		document.getElementById("title").focus();
		return false;
	}
	    
	if(url == "" || url.trim() ==""){
		alert("URL  is required");
		document.getElementById("url").focus();
		return false;
	}
	
	  
	if(image == "" ||image.trim() ==""){
		alert("Post image  is required");
		document.getElementById("_image").focus();
		return false;
	}
		
	if(status == "" || status.trim() ==""){
	   alert("Status field is required");
	   document.getElementById("status").focus();
	   return false;
	}
	
	if(video == "" || video.trim() ==""){
	   alert("video is required");
	   document.getElementById("video").focus();
	   return false;
	}
	

		
}

 </script>
<script>
$(document).ready(function(){
	$('#title').on('keyup', function() {
		var title_url = $("#title").val();
		var string = title_url.replace(/[^a-zA-Z 0-9]+/g,'');
		var post_url  = string.replace(/\s+/g, '-').toLowerCase();
		$("#url").val(post_url);
		
	});
 });
</script>
 
<div class="span9">
<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Update Post</h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo FULL_CMS_URL."/manage_posts/"?> ">Manage Posts</a>
            <h3><a style ="margin-top:-22px;float:left;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo FULL_CMS_URL."/manage_user/"?> ">Manage User</a>

			</h3>
        </div>
        <div class="module-body">
               <?php 
				if( $this->session->flashdata('error') ) { 
				   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
						<strong>'.$this->session->flashdata('error').'</strong></div>';
			
				}else if( $this->session->flashdata('success') ) { 
				
				   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
					  <strong>'.$this->session->flashdata('success').'</strong></div>';
				}
				?>
                
                <br />                
                 
                  <?php   foreach($val as $record){ ?>
                  <form class="form-horizontal row-fluid" method="post" action="<?php echo FULL_CMS_URL.'/manage_movie/edit_action' ?>" enctype="multipart/form-data" />
                    <input type="hidden" name="id" id="id" value="<?php echo $record->mid;?>" />

                   <div class="control-group">
                            <label class="control-label" for="basicinput">Language*</label>
                            <div class="controls">
                               <select tabindex="8" id="language" name="language" data-placeholder="Select language.." class="span8">
                               <option value="" <?php echo ($record->language == NULL) ? 'selected="selectec"' : '';?>>Select Viral Status</option>
                               <option value="hindi" <?php echo ($record->language == 'hindi') ? 'selected="selectec"' : '';?>>Hindi</option>
                               <option value="kannada" <?php echo ($record->language == 'kannada') ? 'selected="selectec"' : '';?>>Kannada</option>
                               <option value="english" <?php echo ($record->language == 'english') ? 'selected="selectec"' : '';?>>English</option>
                               <option value="telugu" <?php echo ($record->language == 'telugu') ? 'selected="selectec"' : '';?>>Telugu</option>
                               <option value="tamil" <?php echo ($record->language == 'tamil') ? 'selected="selectec"' : '';?>>Tamil</option>
                                
								</select>
                            </div>
                        </div>
                       
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Title *</label>
                            <div class="controls">
                                <textarea id="title" name="title" placeholder="title" tabindex="5" class="span8"><?php echo $record->movie_name?></textarea>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">URL *</label>
                            <div class="controls">
                                <textarea id="url" name="url" placeholder="url" tabindex="5" class="span8"><?php echo $record->seourl?></textarea>
                            </div>
                        </div>
                       
                       
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Movie Image *</label>
                            <div class="controls">
                                <input type="file" name="post_image" id="post_image" placeholder="Post Image" tabindex="3" class="span8">
                               
                            </div>
                        </div>
                                                
                    
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Movie Image</label>
                            <div class="controls">
                                <img alt="blog image<?php echo $record->movie_name?>" src="<?php echo SITE_URL.MOVIE_FOLDER.$record->image;  ?>" width="150">                                
                            </div>
                        </div>
                        
                         <div class="control-group">
                            <label class="control-label" for="basicinput">Movie Content *</label>
                            <div class="controls">
                                <textarea id="content" name="content" placeholder="content" rows="16" tabindex="7" class="span8"><?php if(isset($record->details)){echo $record->details;}?></textarea>
                                <script> CKEDITOR.replace( 'content' ); </script>
                            </div>
                        </div>
                         <div class="control-group">
                            <label class="control-label" for="basicinput">Status*</label>
                            <div class="controls">
                               <select tabindex="8" id="status" name="status" data-placeholder="Select Status.." class="span8">
                               <option value="" <?php echo ($record->status == NULL) ? 'selected="selectec"' : '';?>>Select Status</option>
                               <option value="1" <?php echo ($record->status == '1') ? 'selected="selectec"' : '';?>>Active</option>
                               <option value="0" <?php echo ($record->status == '0') ? 'selected="selectec"' : '';?>>Inactive</option>
                                </select>
                            </div>
                        </div>
                              
                           <div class="control-group">
                            <label class="control-label" for="basicinput">Video Url</label>
                            <div class="controls">
                                <input type="text" id="link" name="link" placeholder="Enter Video Url" class="span8" tabindex="2" value="<?php if(isset($record->link)){echo $record->link ;}?>">
                            </div>
                        </div>
                        
                        
                        

                        <div class="control-group">
                            <div class="controls">
                                <input type="submit" name="addform" value="Save" onclick="return checkInputs()">
                            </div>
                        </div>
                    </form>
                    <?php  }?>
                </div>
                </div>
                                
            </div><!--/.content-->
        </div> 
 
 