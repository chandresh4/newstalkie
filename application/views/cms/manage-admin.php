<?php //$arr_all = all_arrays(); ?>

<!-- PAGE CONTENT WRAPPER -->
<div class="span9">                
    <div class="content">
        <div class="module">
            
            <!-- START DATATABLE EXPORT -->
                <div class="module-head">
                    <h3><?php echo $page_title?></h3>
                        <h3 align="right"><a  style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo FULL_CMS_URL."/".$add_edit_page?>"><?php echo $add_page_title?></a></h3>  
                </div>
                <div class="module-body">
                    <table id="customers2" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>S No.</th>
                                <th >Username</th>
                                <th >Role</th>
                                <th >Email</th>
                                <th >Status</th>
                                <th >Update</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            if(is_array($details) && count($details) > 0){	
                              $sl_no=1;
                              foreach($details as $p){
                                  
                                    echo "<tr>";
                                    echo "<td>". $sl_no."</td>";
									echo "<td>". ucfirst($p->username)."</td>";
                                    echo "<td>". ucfirst((isset($role_data[$p->adm_role_id]) ? $role_data[$p->adm_role_id] : null)). "</td>";
                                    echo "<td>". $p->email."</td>";
                                    echo "<td><span class='label label-success'>".($p->status ==1 ? 'Active' : 'Inactive')."</span></td>";
									echo "<td><a href='".FULL_CMS_URL."/".$add_edit_page."/edit/".$p->id."'> <img alt='Edit' src='".S3_URL."cms/images/icons/pencil.png'  width='20' height='20'></img></a></td>";
									
                                    echo "</tr>";
                                    $sl_no++;
                              }
                            }
                        ?>
                        </tbody>
                    </table>                                    
                    
                </div>
            <!-- END DATATABLE EXPORT -->                            
            
        </div>
    </div>

</div>         
<!-- END PAGE CONTENT WRAPPER -->




