<script type="application/javascript">
  function checkInputs(){ 
	
	var name=document.getElementById("name").value;
	var title=document.getElementById("meta_title").value;
	var keywords=document.getElementById("meta_keyword").value;
	var description=document.getElementById("meta_description").value;
    var status = document.getElementById("status").value;
	var image    = document.getElementById("post_image").value;
	if(!$("#name").val().match(/^[a-zA-Z0-9&\s]+$/))
	{
            alert("invalid category name");
			document.getElementById("name").value="";
			document.getElementById("name").focus();
			return false;
    } 
    if(name == "" || name.trim() =="")
	{
		    alert("name field is required");
			document.getElementById("name").value="";
			document.getElementById("name").focus();
			return false;
	}
	
    if(status == "" || name.trim() =="")
	{
		    alert("Status field is required");
			document.getElementById("status").value="";
			document.getElementById("status").focus();
			return false;
	}
    
	if(title == "" || title.trim() =="")
	{
		    alert("meta title  is required");
			document.getElementById("meta_title").value="";
			document.getElementById("meta_title").focus();
			return false;
	}
	if(description == "" || description.trim() =="")
	{
		    alert("meta descriptionis required");
			document.getElementById("meta_description").value="";
			document.getElementById("meta_description").focus();
			return false;
	}
	if(keywords == "" || keywords.trim() =="")
	{
		    alert("meta keywords are required");
			document.getElementById("meta_keyword").value="";
			document.getElementById("meta_keyword").focus();
			return false;
	}	
    
 }
</script>
 
<div class="span9">
<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Update Category</h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo FULL_CMS_URL."/manage_category"?> ">Manage Category</a>
            </h3>
        </div>
        <div class="module-body">
               <?php 
				if( $this->session->flashdata('error') ) { 
				   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
						<strong>'.$this->session->flashdata('error').'</strong></div>';
			
				}else if( $this->session->flashdata('success') ) { 
				
				   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
					  <strong>'.$this->session->flashdata('success').'</strong></div>';
				}
				?>
                
                <br />                
                  
                  <?php foreach($result as $record){ ?>
                  <form class="form-horizontal row-fluid" method="post" action="<?php echo FULL_CMS_URL.'/manage_category/edit_action' ?>" enctype="multipart/form-data" />
                    <input type="hidden" name="id" id="id" value="<?php echo $record->cat_id;?>" />
                     
						
						
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Category Name*</label>
                            <div class="controls">
                                <input type="text" id="name" name="name" placeholder="Category Name" class="span8" tabindex="5" value="<?php echo $record->name?>">
                            </div>
                        </div> 
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Banner Image *</label>
                            <div class="controls">
                                <input type="file" name="post_image" id="post_image" placeholder="Banner Image" tabindex="3" class="span8">
                               
                            </div>
                        </div>
                                                
                    
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Banner Image</label>
                            <div class="controls">
                                <img alt="blog image" src="<?php echo SITE_URL.CATEGORYBANNER_FOLDER.$record->image;  ?>" width="150">                                
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Meta Description *</label>
                            <div class="controls">
                                <textarea id="meta_description" name="meta_description" style="height:80px;" placeholder="Enter MetaDescription" tabindex="4" class="span8"><?php echo $record->meta_description ?></textarea>
							</div>
                        </div>
                          <div class="control-group">
                            <label class="control-label" for="basicinput">Meta Title *</label>
                            <div class="controls">
                                <input type="text" id="meta_title" name="meta_title" placeholder="Enter Meta Title" class="span8" tabindex="2" value="<?php echo $record->meta_title?>">
                            </div>
                        </div>
                        
                         <div class="control-group">
                            <label class="control-label" for="basicinput">Meta Keyword *</label>
                            <div class="controls">
                               <input type="text" id="meta_keyword" name="meta_keyword" placeholder="Enter Meta Keyword" class="span8" tabindex="2" value="<?php echo $record->meta_keywords?>">
							</div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Status*</label>
                            <div class="controls">
                               <select tabindex="7" id="status" name="status" data-placeholder="Select Status.." class="span8">
                               <option value="" <?php echo ($record->status == NULL) ? 'selected="selectec"' : '';?>>Select Status</option>
                               <option value="1" <?php echo ($record->status == '1') ? 'selected="selectec"' : '';?>>Active</option>
                               <option value="0" <?php echo ($record->status == '0') ? 'selected="selectec"' : '';?>>Inactive</option>
                                </select>
                            </div>
                        </div>

                        <div class="control-group">
                            <div class="controls">
                                <input type="submit" name="addform" value="Save" onclick="return checkInputs()">
                            </div>
                        </div>
                    </form>
                    <?php } ?>
                </div>
                </div>
                                
            </div><!--/.content-->
        </div> 
 
 