<script>
 function myFunction()
 {
	
    
	var name=document.getElementById("name").value;
	var title=document.getElementById("meta_title").value;
	var keywords=document.getElementById("meta_keyword").value;
	var description=document.getElementById("meta_description").value;
    var status = document.getElementById("status").value;
	var image    = document.getElementById("post_image").value;
	if(!$("#name").val().match(/^[a-zA-Z0-9&\s]+$/))
	{
            alert("invalid category name");
			document.getElementById("name").value="";
			document.getElementById("name").focus();
			return false;
    } 
    if(name == "" || name.trim() =="")
	{
		    alert("name field is required");
			document.getElementById("name").value="";
			document.getElementById("name").focus();
			return false;
	}
	
    if(status == "" || name.trim() =="")
	{
		    alert("Status field is required");
			document.getElementById("status").value="";
			document.getElementById("status").focus();
			return false;
	}
    if(image == "" ||image.trim() ==""){
		alert("Banner image  is required");
		document.getElementById("post_image").focus();
		return false;
	}
	if(title == "" || title.trim() =="")
	{
		    alert("meta title  is required");
			document.getElementById("meta_title").value="";
			document.getElementById("meta_title").focus();
			return false;
	}
	if(description == "" || description.trim() =="")
	{
		    alert("meta descriptionis required");
			document.getElementById("meta_description").value="";
			document.getElementById("meta_description").focus();
			return false;
	}
	if(keywords == "" || keywords.trim() =="")
	{
		    alert("meta keywords are required");
			document.getElementById("meta_keyword").value="";
			document.getElementById("meta_keyword").focus();
			return false;
	}	
    
 }

</script>



 

<div class="span9">
<div class="content">
<div class="module">
    <div class="module-head">
        <h3>Add Category</h3>
        <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo SITE_URL?>index.php/cms/manage_category">Manage Category</a></h3>	
    </div>
    <div class="module-body">
            <?php 
            if( $this->session->flashdata('error') ) { 
               echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>'.$this->session->flashdata('error').'</strong></div>';
        
            }else if( $this->session->flashdata('success') ) { 
            
               echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>'.$this->session->flashdata('success').'</strong></div>';
            }
          ?>
            <br />                

              <form class="form-horizontal row-fluid" method="post" action="<?php echo FULL_CMS_URL.'/manage_category/add_action' ?>" enctype="multipart/form-data">
                
                <div class="control-group">
                        <label class="control-label" for="basicinput">Category Name* </label>
                        <div class="controls">
                            <input type="text" name="name" id="name" autofocus="autofocus" tabindex="1" class="span8"/>
                        </div>
                    </div>
                    <div class="control-group">
                            <label class="control-label" for="basicinput">Banner*</label>
                            <div class="controls">
                                <input type="file" name="post_image" id="post_image" placeholder="Banner Image" tabindex="3" class="span8">
                               <!--<label>Image size should not be less than 700*245 </label> -->
                               
                        </div>
					</div>
					 <div class="control-group">
                            <label class="control-label" for="basicinput">Meta Title *</label>
                            <div class="controls">
                                <input type="text" id="meta_title" name="meta_title" placeholder="Enter Page Title" class="span8" tabindex="2">
                            </div>
                        </div>
					<div class="control-group">
                            <label class="control-label" for="basicinput">Meta Description *</label>
                            <div class="controls">
                                <textarea id="meta_description" name="meta_description" style="height:80px;" placeholder="Enter Description" tabindex="4" class="span8"></textarea>
							</div>
                        </div>
                         
                        
                          <div class="control-group">
                            <label class="control-label" for="basicinput">Meta Keyword *</label>
                            <div class="controls">
                               <input type="text" id="meta_keyword" name="meta_keyword" placeholder="Enter Meta Keyword" class="span8" tabindex="2" value="">
							</div>
                        </div>
                    <div class="control-group">                                        
                        <label class="control-label" for="basicinput">Status*</label>
                        <div class="controls">
                       <select class="span8" name="status" id="status" tabindex="6" data-attr="Please select status">
                        <option value="">Select Status</option>
                        <option value="1">Active</option>
                        <option value="2">Inactive</option>
                        </select>
                        <span class="help-block" id="status_err"></span>
              </div>
          </div>
                    
                    <div class="control-group">
                        <div class="controls">
                            <input type="submit" name="submit" id="submit" value="Submit" onclick="return myFunction()"/>
                        </div>
                    </div>
                </form>
            </div>
            </div>
                            
        </div><!--/.content-->
    </div>

