<div class="span9">
<div class="content">

    <div class="module">
        <div class="module-head">
            <h3><?php echo $page_title?></h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo SITE_URL?>index.php/cms/manage_staticpage/add_form"><?php echo $add_title?></a></h3>	
        </div>
        <div class="module-body">
            <p>
                <strong> <?php echo $page_title?> List </strong>
            </p>
             <?php 
				if( $this->session->flashdata('error') ) { 
				   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
						<strong>'.$this->session->flashdata('error').'</strong></div>';
			
				}else if( $this->session->flashdata('success') ) { 
				
				   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
					  <strong>'.$this->session->flashdata('success').'</strong></div>';
				}
			?>
            

            <form class="navbar-search pull-left input-append" action="<?php echo FULL_CMS_URL?>/manage_staticpage" method="post">
            <select name="search_field" id="search_field" >                                
                <option value="page_name" <?php echo ($search_field == "page_name") ? "selected='selected'" : ""; ?>>Page name</option>
            </select>
            
            &nbsp;&nbsp;
            <input type="text" name="search_txt" value="<?php echo $search_txt;?>" class="text-input small-input" />
            <select name="status" id="status">   
                <option>Status</option>                             
                <option value="1" <?php echo ($status == "1") ? "selected='selected'" : ""; ?> />Active</option>
                <option value="0" <?php echo ($status == "0") ? "selected='selected'" : ""; ?> />Inactive</option>
            </select>
            <button class="btn" id="btn_search" name="btn_search"  value="GO" type="submit">
            <i class="icon-search"></i>
            </button>
            </form>
            <br />
            <!-- <hr /> -->
            <br />
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>No.</th>
                  <th >ID</th>
                  <th >PAGE</th>
                  <th >STATUS</th>
                  <th >UPDATE</th>
                </tr>
              </thead>
              <tbody>
              <?php
                if(is_array($details) && count($details) > 0){	
                  $sl_no=1;
                  foreach($details as $p)
                  {
                        echo "<tr>";
                        echo "<td>".$sl_no. "</td>";
						echo "<td>".$p->page_id. "</td>";
                        echo "<td>".$p->page_name."</td>";
                        echo "<td>".($p->status == 1 ? 'Active' : 'Inactive')."</td>";
                        echo "<td><a href='".FULL_CMS_URL."/manage_staticpage/edit_form/".$p->page_id."'><img alt='Edit' src='".S3_URL."cms/images/icons/pencil.png'  width='20' height='20'></img></td>";
                        echo "</tr>";
                $sl_no++;
                  }
                }
                ?>
               <tr>
                  <td colspan="7" style="line-height: 1.5em; !important">
                    <?php
                      if (count($details) > 0) {
                         echo $links;
                      }
                    ?>
                  </td>
                </tr>
              </tbody>
            </table>
            <br>
        </div>
    </div>

    <!--/.module-->
<br />    
</div><!--/.content-->
</div>
