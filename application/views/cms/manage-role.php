<?php //$arr_all = all_arrays(); ?>

<!-- PAGE CONTENT WRAPPER -->
<div class="span9">
                
    <div class="content">
        <div class="module">
            
            <!-- START DATATABLE EXPORT -->
           <!-- <div class="panel panel-default">-->
                <div class="module-head">
                    <h3 ><?php echo $page_title?></h3>
                    
                   <!-- <div class="btn-group pull-right">-->
                       <h3 align="right"> <a  style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo FULL_CMS_URL."/".$add_edit_page?>"><?php echo $add_page_title?></a>
                        <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button></h3>
                        <ul class="dropdown-menu">
                            <!--//<li><a href="#" onClick ="$('#customers2').tableExport({type:'sql'});">
                            <img src='img/icons/sql.png' width="24"/> SQL</a></li>-->
                            <li><a href="#" onClick ="$('#customers2').tableExport({type:'excel',escape:'false'});">
                            <img src='<?php echo S3_URL?>cms/images/icons/doc_type/xls.png' width="24"/> Excel</a></li>
                            <li><a href="#" onClick ="$('#customers2').tableExport({type:'png',escape:'false'});">
                            <img src='<?php echo S3_URL?>cms/images/icons/doc_type/png.png' width="24"/> PNG</a></li>
                            <li><a href="#" onClick ="$('#customers2').tableExport({type:'pdf',escape:'false'});">
                            <img src='<?php echo S3_URL?>cms/images/icons/doc_type/pdf.png' width="24"/> PDF</a></li>
                        </ul>
                 <!--   </div> -->                                   
                    
                </div>
                <div class="module-body">
                    <table id="customers2" class="table table-bordered">
                        <thead>
                            <tr>
                                <th >S no</th>
                                <th >Role</th>
                                <th >Status</th>
                                <th >Update</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            if(is_array($details) && count($details) > 0){	
                              $sl_no=1;
                              foreach($details as $p){
                                  
                                    echo "<tr>";
                                    echo "<td>". $sl_no. "</td>";
                                    echo "<td>". $p->role_name."</td>";
                                    echo "<td><span class='label label-success'>".($p->status ==1 ? 'Active' : 'Inactive')."</span></td>";
									echo "<td><a href='".FULL_CMS_URL."/".$add_edit_page."/edit/".$p->id."'> <img alt='Edit' src='".S3_URL."cms/images/icons/pencil.png'  width='20' height='20'></img></a></td>";
                                    echo "</tr>";
                                    $sl_no++;
                              }
                            }
                        ?>
                            
                        </tbody>
                    </table>                                    
                    
                </div>
            <!--</div>-->
            <!-- END DATATABLE EXPORT -->                            
            
        </div>
    </div>

</div>         
<!-- END PAGE CONTENT WRAPPER -->








