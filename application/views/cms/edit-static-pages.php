<script src="//cdn.ckeditor.com/4.5.6/standard/ckeditor.js"></script>
<script type="application/javascript">
 function checkInputs(){ 
        
    var regex = /^[0-9]*$/;
	var page_name       = document.getElementById("page_name").value;
	var status          = document.getElementById("status").value;
	 
	if(page_name == "" || page_name.trim() ==""){
		alert("Page Name is required");
		document.getElementById("page_name").focus();
		return false;
	}
	
	if(status == "" || status.trim() ==""){
		alert("status is required");
		document.getElementById("status").focus();
		return false;
	}
		
}

 </script>
 <script>
$(document).ready(function(){
	$('#page_name').on('keyup', function() {
		var page_name = $("#page_name").val();
		var page_url  = page_name.replace(/\s+/g, '-').toLowerCase();
		$("#page_url").val(page_url);
		
	});});
 
</script>
<div class="span9">
<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Update Static pages</h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo SITE_URL?>index.php/cms/manage_staticpage">Static Pages List</a>
            </h3>
        </div>
        <div class="module-body">
               <?php 
				if( $this->session->flashdata('error') ) { 
				   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
						<strong>'.$this->session->flashdata('error').'</strong></div>';
			
				}else if( $this->session->flashdata('success') ) { 
				
				   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
					  <strong>'.$this->session->flashdata('success').'</strong></div>';
				}
				?>
                
                <br />                
                  
                  <?php foreach($val as $record){ ?>
                  <form class="form-horizontal row-fluid" method="post" action="<?php echo SITE_URL.'cms/manage_staticpage/edit' ?>" enctype="multipart/form-data" />
                    <input type="hidden" name="id" id="id" value="<?php echo $record->page_id;?>" />
                    
                    <div class="control-group">
                            <label class="control-label" for="basicinput">Page name *</label>
                            <div class="controls">
                                <input type="text" id="page_name" name="page_name" tabindex="1" placeholder="Page name" class="span8" value="<?php echo $record->page_name?>">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Seo url *</label>
                            <div class="controls">
                                <input type="text" id="page_url" name="page_url" tabindex="1" placeholder="Page name" class="span8" value="<?php echo $record->seo_url?>">
                            </div>
                        </div>
                        
                         <div class="control-group">
                            <label class="control-label" for="basicinput">Description *</label>
                            <div class="controls">
                                <textarea id="editor1" name="editor1" placeholder="Page Description" tabindex="2" class="span8"><?php echo $record->page_description ?></textarea>
								<script> CKEDITOR.replace( 'editor1' ); </script>
                            </div>
                        </div>
                         <div class="control-group">
                            <label class="control-label" for="basicinput">Page Title *</label>
                            <div class="controls">
                                <input type="text" id="page_title" name="page_title" placeholder="Enter Page Title" class="span8" tabindex="2" value="<?php echo $record->page_title?>">
                            </div>
                        </div>
                        
                         <div class="control-group">
                            <label class="control-label" for="basicinput">Meta Description *</label>
                            <div class="controls">
                                <textarea id="meta_description" name="meta_description" style="height:80px;" placeholder="Enter MetaDescription" tabindex="4" class="span8"><?php echo $record->meta_description ?></textarea>
							</div>
                        </div>
                          <div class="control-group">
                            <label class="control-label" for="basicinput">Meta Title *</label>
                            <div class="controls">
                                <input type="text" id="meta_title" name="meta_title" placeholder="Enter Meta Title" class="span8" tabindex="2" value="<?php echo $record->meta_title?>">
                            </div>
                        </div>
                        
                         <div class="control-group">
                            <label class="control-label" for="basicinput">Meta Keyword *</label>
                            <div class="controls">
                               <input type="text" id="meta_keyword" name="meta_keyword" placeholder="Enter Meta Keyword" class="span8" tabindex="2" value="<?php echo $record->meta_keyword?>">
							</div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Status</label>
                            <div class="controls">
                               <select tabindex="2" id="status" name="status" data-placeholder="Select Status.." class="span8">
                               <option value="" <?php echo ($record->status == NULL) ? 'selected="selectec"' : '';?>>Select Status</option>
                               <option value="1" <?php echo ($record->status == '1') ? 'selected="selectec"' : '';?>>Active</option>
                               <option value="0" <?php echo ($record->status == '0') ? 'selected="selectec"' : '';?>>Inactive</option>
                                </select>
                            </div>
                        </div>

                        <div class="control-group">
                            <div class="controls">
                                <input type="submit" name="addform" value="Save" onclick="return checkInputs()" tabindex="4">
                            </div>
                        </div>
                    </form>
                    <?php } ?>
                </div>
                </div>
                                
            </div><!--/.content-->
        </div> 
 
 