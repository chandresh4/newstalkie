
<div class="span9">
<div class="content">

    <div class="module">
         <div class="module-head">
              <h3>Category Lists <a  style ="float:right;" href="<?php echo FULL_CMS_URL."/manage_category/add"?>">Add category</a></h3>  
          </div>
            
            <?php 
    				if( $this->session->flashdata('error') ) { 
    				   echo '<div class="module-body"><div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
    						<strong>'.$this->session->flashdata('error').'</strong></div></div>';

    				}else if( $this->session->flashdata('success') ) { 
    				
    				   echo '<div class="module-body"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
    					  <strong>'.$this->session->flashdata('success').'</strong></div></div>';
    				}else{
              echo "<br>";
            }
    			?>
            
                  
            
            <form class="navbar-search pull-left input-append fullwidth" action="<?php echo FULL_CMS_URL?>/manage_category" method="post">            
                <label style="width:40%; float:left"><span style="width:75px">From </span> <input type="date" name="from_date" value="<?php echo ($from_date !="" ? $from_date : date("Y-m-d"));?>" class="text-input small-input" /></label>
                <label style="width:40%; float:left"><span style="width:75px">To </span><input type="date" name="to_date" value="<?php echo ($to_date !="" ? $to_date : date("Y-m-d"));?>" class="text-input small-input"></label>
                <button class="btn" id="btn_search" name="btn_search" type="submit" value="GO">
                <i class="icon-search"></i>
                </button>
            </form>
            
            <br />
            
            <table class="table table-bordered">
              <thead>
                <tr>
                 <th>S No.</th>
        			   <th >Category Name</th>
        			   <th>Count of posts </th>
        			   <th >Status</th>
        			   <th >Update</th>
                <!--<th>Status</th>-->
                 </tr>
              </thead>
              <tbody>
              <?php
                if(is_array($details) && count($details) > 0){	
                              $sl_no=1;
                              foreach($details as $p){
                                $query = $this->db->query("SELECT COUNT( id ) as count ,category FROM tbl_post where category = ".$p->cat_id);
								if ( $query->num_rows() > 0) {
								foreach($query->result() as $p1){
								$count = $p1->count;
								}
								}else{
								$count = 0;
								}
								echo "<tr>";
								echo "<td>". $sl_no."</td>";
								echo "<td>". ucfirst($p->name)."</td>";

								echo "<td>". $count."</td>";
								echo "<td><span class='label label-success'>".($p->status ==1 ? 'Active' : 'Inactive')."</span></td>";
								echo "<td><a href='".FULL_CMS_URL."/manage_category/edit/?id=".$p->cat_id."'> <img alt='Edit' src='".S3_URL."cms/images/icons/pencil.png'  width='20' height='20'></img></a></td>";
								echo "</tr>";
								$sl_no++;
                              }
                            }
                ?>
               
                 <tr>
                  <td colspan="9" style="line-height: 1.5em; !important">
                    <?php
                      if (count($details) > 0) {
                         echo $links;
                      }
                    ?>
                  </td>
                </tr>
              </tbody>
            </table>
            <br>
        </div>
    </div>

    <!--/.module-->
<br />    
</div><!--/.content-->
</div>

