<script>
 function myFunction()
 {
    var regex = /^[a-zA-Z ]*$/;
	var emailregex =/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/;
	var name=document.getElementById("user_name").value;
	var email=document.getElementById("email_address").value;
	var pwd=document.getElementById("password").value;


        if (! regex.test(name)) 
		{
            alert("invalid user name");
			document.getElementById("user_name").value="";
			document.getElementById("user_name").focus();
			return false;
        } 

		if(name == "" || name.trim() =="")
	    {
		    alert("name field is required");
			document.getElementById("user_name").value="";
			document.getElementById("user_name").focus();
			return false;
		}

		if(email == "" || email.trim() =="")
	    {
		    alert("email field is required");
			document.getElementById("email_address").value="";
			document.getElementById("email_address").focus();
			return false;
		}
		if(password == "" || pwd.trim() =="")
	    {
		    alert("password field is required");
			document.getElementById("password").value="";
			document.getElementById("password").focus();
			return false;
		}
		if (! emailregex.test(email)) 
		{
            alert("invalid email");
			document.getElementById("email_address").value="";
			document.getElementById("email_address").focus();
			return false;
        }
    
 }

</script>



 

<div class="span9">
<div class="content">
<div class="module">
    <div class="module-head">
        <h3>Add Users</h3>
        <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo SITE_URL?>index.php/cms/manage_user">Manage User</a></h3>	
    </div>
    <div class="module-body">
            <?php 
            if( $this->session->flashdata('error') ) { 
               echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>'.$this->session->flashdata('error').'</strong></div>';
        
            }else if( $this->session->flashdata('success') ) { 
            
               echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>'.$this->session->flashdata('success').'</strong></div>';
            }
          ?>
            <br />                

              <form class="form-horizontal row-fluid" method="post" action="<?php echo FULL_CMS_URL.'/manage_user/add' ?>">
                
                <div class="control-group">
                        <label class="control-label" for="basicinput">Username</label>
                        <div class="controls">
                            <input type="text" name="user_name" id="user_name" autofocus="autofocus" tabindex="1" class="span8"/>
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="basicinput">Email</label>
                        <div class="controls">                    
                          <input type="text" name="email_address" id="email_address" tabindex="2" class="span8"/>
                       </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="basicinput">password</label>
                        <div class="controls">
                           <input type="password" name="password" id="password" tabindex="3" class="span8"/>
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <div class="controls">
                            <input type="submit" name="register" id="register" value="Register" onclick="return myFunction()"/>
                        </div>
                    </div>
                </form>
            </div>
            </div>
                            
        </div><!--/.content-->
    </div>

