<script type="application/javascript">

 function checkInputs(){ 
    var regex   = /^[0-9]*$/;
	var from_date  = document.getElementById("from_date").value;
	var to_date    = document.getElementById("to_date").value;
	
	if(from_date.trim() == ""){
		alert("Select From date");
		document.getElementById("from_date").focus();
		return false;
	}

	if(to_date.trim() == ""){
		alert("Select To date");
		document.getElementById("to_date").focus();
		return false;
	}

}

 </script>
 



<div class="span9">
<div class="content">

    <div class="module">
        <div class="module-head">
            <h3>Registered Users</h3>	
        </div>
        <div class="module-body">
            <!--<p>
                <strong><?php echo "";//($quest_id != '0' ? $this->db_function->get_single_value(CAMPAIGN_QUEST, "question" ,"quest_id =".$quest_id) : '0')?> </strong>
            </p>-->
            
            <div class="module-body">
            <?php 
				if( $this->session->flashdata('error') ) { 
				   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
						<strong>'.$this->session->flashdata('error').'</strong></div>';

				}else if( $this->session->flashdata('success') ) { 
				
				   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
					  <strong>'.$this->session->flashdata('success').'</strong></div>';
				}
			?>
            
                               
            </div>
            
            <form class="navbar-search pull-left input-append fullwidth" action="<?php echo FULL_CMS_URL?>/manage_user" method="post">            
                <label style="width:40%; float:left"><span style="width:75px">From </span> <input type="date" name="from_date" value="<?php echo ($from_date !="" ? $from_date : date("Y-m-d"));?>" class="text-input small-input" /></label>
                <label style="width:40%; float:left"><span style="width:75px">To </span><input type="date" name="to_date" value="<?php echo ($to_date !="" ? $to_date : date("Y-m-d"));?>" class="text-input small-input"></label>
                <button class="btn" id="btn_search" name="btn_search" type="submit" value="GO">
                <i class="icon-search"></i>
                </button>
            </form>
            
            <br />
            
            <table class="table table-bordered">
              <thead>
                <tr>
                <th >S.No</th>
                <th >Email</th>
                <th>Story Count</th>
                <th >Optin Status</th>
                <th >Status</th>
				<th>Action</th>
				
                <!--<th>Status</th>-->
                 </tr>
              </thead>
              <tbody>
              <?php
                if(is_array($details) && count($details) > 0){	
				  $sl_no=1;
				   
				   foreach($details as $p){
					    
				        echo "<tr>";
						echo "<td>" .$sl_no. "</td>";
						echo "<td>" .$p->email. "</td>";
					    $query = $this->db->query("SELECT COUNT( id ) as count  FROM tbl_post where writer_id = ".$p->id);
						if ( $query->num_rows() > 0) {
						  foreach($query->result() as $p1){
						   $count = $p1->count;
						  }
						}else{
						  $count = 0;
						}
						echo "<td>" .$count. "</td>";
						echo "<td>" .($p->is_optin== 1 ? 'Optin' : 'Non Optin'). "</td>";
						echo "<td>" .($p->status== 1 ? 'Active' : 'Inactive'). "</td>";
						echo "<td><a href='".FULL_CMS_URL."/manage_user/view_post/?id=".$p->id."'> View </a></td>";
                        echo "</tr>";
						$sl_no++;
											
                   }					
                }
                ?>
               
                 <tr>
                  <td colspan="9" style="line-height: 1.5em; !important">
                    <?php
                      if (count($details) > 0) {
                         echo $links;
                      }
                    ?>
                  </td>
                </tr>
              </tbody>
            </table>
            <br>
        </div>
    </div>

    <!--/.module-->
<br />    
</div><!--/.content-->
</div>

