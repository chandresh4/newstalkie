<script>
function valid_form(){
	
	var r_= new Array("role", "username", "password", "status");
	for(var i=0; i < r_.length; i++   ){
		if($.trim($("#"+r_[i]).val()) == "" || $("#"+r_[i]).val() == 0 || $("#"+r_[i]).val() == "undefined"){
		   alert($("#"+r_[i]).val())
		   $("#"+r_[i]+"_err").html($("#"+r_[i]).attr("data-attr"));
		   $("#"+r_[i]).focus();
		   return false;	
		}else{
		   $("#"+r_[i]+"_err").html("");
		}
	}
}

</script>

<?php// $arr_all = all_arrays();?>
                
<div class="span9">
                
<div class="content"> 
        
      <form  class="form-horizontal" action="<?php echo $form_submit?>" method="post" enctype="multipart/form-data">
        <div class="module">
            <div class="module-head">
                <h3 ><?php echo $page_title?></h3>
                <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo FULL_CMS_URL."/".$cur_controller?>">Manage Admin</a></h3>
            </div>
            <div class="module-body">
			  <?php 
                if( $this->session->flashdata('error') ) { 
                    echo '<div class="alert alert-danger" role="alert" style="height:39px; line-height:8px">
                			<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true" >&times;</span></button>
                			'.$this->session->flashdata('error').'</div>';
            
                }else if( $this->session->flashdata('success') ) { 
                   echo '<div class="alert alert-success" role="alert" style="height:39px; line-height:8px">
                			<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true" >&times;</span></button>
                			'.$this->session->flashdata('success').'</div>';
            
                }
			  ?>
            
            </div>
            <div class="module-body">                                                                                   
                
                <div class="control-group">
                    <label class="col-md-2 col-xs-12 control-label">Role*</label>
                    <div class="controls" >                                            
                        <select class="form-control select " name="role" id="role" tabindex="1" data-attr="Please select role">
                        <option value="">Select Role</option>
                         <?php 
						  foreach($role as $val){
							echo "<option value='".$val->id."' ".($val->id == $result_data['adm_role_id'] ? 'selected' : '' ).">".$val->role_name."</option>";
						  }
					  	  ?>
                        </select>                                                  
                       <span class="help-block" id="role_err"></span>
                    </div>
                </div>
               	
                <div class="control-group">                                        
                    <label class="col-md-2 col-xs-12 control-label">Username*</label>
                    <div class="controls">
                        <div class="input-group">
                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                            <input type="text" class="form-control " name="username" id="username" tabindex="5" value="<?php echo $result_data['username']?>" data-attr="Please enter username"/>
                        </div>            
                        <span class="help-block" id="username_err"></span>
                    </div>
                </div>
                
                <div class="control-group">                                        
                    <label class="col-md-2 col-xs-12 control-label">Email*</label>
                    <div class="controls">
                        <div class="input-group">
                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                            <input type="text" class="form-control " name="email" id="email" tabindex="6" value="<?php echo $result_data['email']?>" data-attr="Please enter email"/>
                        </div>            
                        <span class="help-block" id="email_err"></span>
                    </div>
                </div>
                
               <div class="control-group">                                        
                    <label class="col-md-2 col-xs-12 control-label">Password*</label>
                    <div class="controls">
                        <div class="input-group">
                            <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
                            <input type="password" class="form-control" name="password" id="password" tabindex="7" data-attr="Please enter password"/>
                        </div>            
                        <span class="help-block" id="password_err"></span>
                    </div>
                </div>
 				
                
                <div class="control-group" >
                    <label class="col-md-2 col-xs-12 control-label">Profile Image</label>
                    <div class="controls" >
                        <?php if( $result_data[$primary_field] != ""){?>                        
                                <img src="<?php echo S3_URL?>/cms/images/admin_img/<?php echo $result_data['admin_image']?>" alt="admin image" height="100px" width="100px">    
						<?php } ?>
                        <input type="file" class="fileinput btn" name="admin_image" id="admin_image" title="Browse file" tabindex="8"/>
                        <!--<span class="help-block">Input type file</span>-->
                    </div>
                </div>
               
               
                <div class="control-group">
                    <label class="col-md-2 col-xs-12 control-label">Status*</label>
                    <div class="controls">                                                                                            
                        <select class="form-control select " name="status" id="status" tabindex="9" data-attr="Please select status">
                        <option value=""  <?php echo ($result_data['status'] == NULL) ? 'selected="selectec"' : '';?>>Select Status</option>
                        <option value="1" <?php echo ($result_data['status'] == '1') ? 'selected="selectec"' : '';?>>Active</option>
                        <option value="2" <?php echo ($result_data['status'] == '2') ? 'selected="selectec"' : '';?>>Inactive</option>
                        </select>
                        <span class="help-block" id="status_err"></span>
                    </div>
                </div>
                
            </div>
            
            <input type="hidden" name="id" id="id" value="<?php echo $result_data[$primary_field]?>" />
            <input type="hidden" name="add_page" id="add_page" value="<?php echo $add_page?>" />
            <input type="hidden" name="mode" id="mode" value="<?php echo ((strtolower ($mode) == 'edit')  ? 'edit' : 'add');?>" />
            
           <div class="control-group">
                <div class="controls">
                <button class="btn btn-primary " type="submit" name="submit" value="Go" tabindex="10">Submit</button>
                <button class="btn btn-default">Clear Form</button>                                    
            </div></div>
        </div>
        </form>
        
    </div>
</div>                    
                    
</div>
<!-- END PAGE CONTENT WRAPPER -->  

