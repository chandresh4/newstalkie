<script type="application/javascript">

 function checkInputs()
 { 
    var regex = /^[0-9]*$/;
	var medium_name=document.getElementById("medium_name").value;
	
	var status=document.getElementById("status").value;

    	if( medium_name== "" || medium_name.trim() ==""){
			alert("Medium Name is required");
			document.getElementById("medium_name").focus();
			return false;
		}
		
		
		if(status == "" || status.trim() ==""){
		    alert("status field is required");
			return false;
		}
		
 }

 </script>


<div class="span9">
<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Update UTM Medium</h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo SITE_URL?>index.php/cms/manage_utm_medium">Manage UTM Medium</a></h3>	
        </div>
        <div class="module-body">
               <?php 
				if( $this->session->flashdata('error') ) { 
				   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
						<strong>'.$this->session->flashdata('error').'</strong></div>';
			
				}else if( $this->session->flashdata('success') ) { 
				
				   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
					  <strong>'.$this->session->flashdata('success').'</strong></div>';
				}
			  ?>
                
                <br />                
                  <?php foreach($medium_data as $n){ ?>
                  <form class="form-horizontal row-fluid" method="post" action="<?php echo SITE_URL.'cms/manage_utm_medium/edit' ?>">
                    <input type="hidden" name="id" id="id" value="<?php echo $n->id;?>"/>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">UTM Medium name*</label>
                            <div class="controls">
                                <input type="text" id="medium_name" name="medium_name" tabindex="2" placeholder="Medium name" class="span8" value="<?php echo $n->medium; ?>">
                            </div>
                        </div>
                        
                        
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Status*</label>
                            <div class="controls">
                               <select name="status" id="status"  tabindex="9" class="span8">
                                    <option value=""  <?php echo ($n->status == NULL) ? 'selected="selectec"' : '';?>>Select Status</option>
                                    <option value="1" <?php echo ($n->status == '1') ? 'selected="selectec"' : '';?>>Active</option>
                                    <option value="0" <?php echo ($n->status == '0') ? 'selected="selectec"' : '';?>>Inactive</option>
                                </select>
                           </div>
                        </div>
                        
                        <div class="control-group">
                            <div class="controls">
                                <input type="submit" name="addform" value="Save" onclick="return checkInputs()">
                            </div>
                        </div>
                    </form>
                 <?php } ?>   
                </div>
                </div>
                                
            </div><!--/.content-->
        </div>

