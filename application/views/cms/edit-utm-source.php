<script type="application/javascript">

 function checkInputs()
 { 
    var regex = /^[0-9]*$/;
	var net_name=document.getElementById("net_name").value;
	var net_pixel=document.getElementById("net_pixel").value;
	var handle_type=document.getElementById("handle_type").value;
	var cpl=document.getElementById("cpl").value;
	var goal=document.getElementById("goal").value;
	var fire_type=document.getElementById("fire_type").value;
	var status=document.getElementById("status").value;

    	if( net_name== "" || net_name.trim() ==""){
			alert("Net Name is required");
			document.getElementById("net_name").focus();
			return false;
		}
		
		if(net_pixel == "" || net_pixel.trim() ==""){
		    alert("Net pixel is required");
			document.getElementById("net_pixel").focus();
			return false;
		}
		
		if(handle_type == "" || handle_type.trim() ==""){
		    alert("Handle type is required");
			document.getElementById("handle_type").focus();
			return false;
		}
		
		if(cpl == "" || cpl.trim() ==""){
		    alert("cpl is required");
			document.getElementById("cpl").focus();
			return false;
		}
		
		if(fire_type == "" || fire_type.trim() ==""){
		    alert("fire type is required");
			document.getElementById("fire_type").focus();
			return false;
		}
		
		if(goal == "" || goal.trim() ==""){
		    alert("goal is required");
			document.getElementById("goal").focus();
			return false;
		}
		
		if(status == "" || status.trim() ==""){
		    alert("status field is required");
			return false;
		}
		
 }

 </script>


<div class="span9">
<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Update UTM Source</h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo SITE_URL?>index.php/cms/manage_utm_source">Manage UTM Source</a></h3>	
        </div>
        <div class="module-body">
               <?php 
				if( $this->session->flashdata('error') ) { 
				   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
						<strong>'.$this->session->flashdata('error').'</strong></div>';
			
				}else if( $this->session->flashdata('success') ) { 
				
				   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
					  <strong>'.$this->session->flashdata('success').'</strong></div>';
				}
			  ?>
                
                <br />                
                  <?php foreach($net_pixel as $n){ ?>
                  <form class="form-horizontal row-fluid" method="post" action="<?php echo SITE_URL.'cms/manage_utm_source/edit' ?>">
                    <input type="hidden" name="id" id="id" value="<?php echo $n->id;?>"/>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">UTM Source name*</label>
                            <div class="controls">
                                <input type="text" id="net_name" name="net_name" tabindex="2" placeholder="Network name" class="span8" value="<?php echo $n->net; ?>">
                            </div>
                        </div>
                        
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">UTM Source Pixel*</label>
                            <div class="controls">
                                <textarea name="net_pixel" id="net_pixel" placeholder="net_pixel" tabindex="3" class="span8"/><?php echo $n->pixel; ?>
                                </textarea>
                                <span class="help-inline">Replace dynamic user id with "{USERID}"</span>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Handle Type*</label>
                            <div class="controls">
                                <select name="handle_type" id="handle_type"  tabindex="4" class="span8">
                                    <option value="NULL" <?php echo ($n->handle_type) == NULL ? 'selected="selectec"' : '' ;?>>Select Type</option>
                                    <option value="1" <?php echo ($n->handle_type) == '1' ? 'selected="selectec"' : '' ;?>>Manual</option>
                                    <option value="2" <?php echo ($n->handle_type) == '2' ? 'selected="selectec"' : '' ;?>>Auto</option>
                                </select>
                            </div>
                        </div>
                        
                        
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Cpl*</label>
                            <div class="controls">
                                <input type="text" id="cpl" name="cpl" placeholder="Cpl" tabindex="5" class="span8" value="<?php echo $n->cpl; ?>"/>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Fire Type*</label>
                            <div class="controls">
                               <input type="text" name="fire_type" id="fire_type" class="span8" tabindex="6" value="<?php echo $n->fire_type; ?>">
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Goal*</label>
                            <div class="controls">
                                <input type="text" id="goal" name="goal" placeholder="goal" tabindex="8" class="span8" value="<?php echo $n->goal; ?>"/>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Status*</label>
                            <div class="controls">
                               <select name="status" id="status"  tabindex="9" class="span8">
                                    <option value=""  <?php echo ($n->status == NULL) ? 'selected="selectec"' : '';?>>Select Status</option>
                                    <option value="1" <?php echo ($n->status == '1') ? 'selected="selectec"' : '';?>>Active</option>
                                    <option value="0" <?php echo ($n->status == '0') ? 'selected="selectec"' : '';?>>Inactive</option>
                                </select>
                           </div>
                        </div>
                        
                        <div class="control-group">
                            <div class="controls">
                                <input type="submit" name="addform" value="Save" onclick="return checkInputs()">
                            </div>
                        </div>
                    </form>
                 <?php } ?>   
                </div>
                </div>
                                
            </div><!--/.content-->
        </div>

