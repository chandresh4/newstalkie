<?php //$arr_all = all_arrays(); ?>

<!-- PAGE CONTENT WRAPPER -->
<div class="span9">                
    <div class="content">
        <div class="module">
            
            <!-- START DATATABLE EXPORT -->
                <div class="module-head">
				<?php
				if(isset($res) && $res!=""){
				foreach($res as $p){ ?>
				<h2><?php echo $p->post_title?></h2>
				<?php }
				}				?>
                    <h3 style ="margin-top:-2px;padding: 10px 10px 10px 11px;"><a href="<?php echo FULL_CMS_URL."/manage_posts"?>">Retun To Main Post</a></h3>
                        <h3 align="right"><a  style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo FULL_CMS_URL."/manage_innerposts/add?id=".$pid.""?>">Add Post</a></h3>  
                </div>
                <div class="module-body">
				 <?php 
				if( $this->session->flashdata('error') ) { 
				   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
						<strong>'.$this->session->flashdata('error').'</strong></div>';
			
				}else if( $this->session->flashdata('success') ) { 
				
				   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
					  <strong>'.$this->session->flashdata('success').'</strong></div>';
				}
			  ?>
			   <form class="navbar-search pull-left input-append" action="<?php echo FULL_CMS_URL?>/manage_innerposts?id=<?php echo $pid?>" method="post">
               <label style="width:35%; float:left"><span style="width:45px">From </span> 
				<input type="date" name="from_date" value="<?php echo ($from_date !="" ? $from_date : date("Y-m-d"));?>" class="text-input small-input" />
				</label>
				<label style="width:35%; float:left"><span style="width:40px">To </span>
				<input type="date" name="to_date" value="<?php echo ($to_date !="" ? $to_date : date("Y-m-d"));?>" class="text-input small-input">
				</label>

				<select name="status" id="status">   
					<option>Status</option>                             
					<option value="1" <?php echo ($status == "1") ? "selected='selected'" : ""; ?> />Active</option>
					<option value="0" <?php echo ($status == "0") ? "selected='selected'" : ""; ?> />Inactive</option>
				</select>
				<button class="btn" id="btn_search" name="btn_search"  value="GO" type="submit">
				<i class="icon-search"></i>
				</button>
			   </form>
                    <table id="customers2" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>S No.</th>
                                
                                 <th>Post Name</th>
                                <th >Status</th>
                                <th >Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                              if(is_array($details) && count($details) > 0){	
                              $sl_no=1;  
                              foreach($details as $p){
                                  
                                    echo "<tr>";
                                    echo "<td>". $sl_no."</td>";
									
									echo "<td>". ucfirst($p->inner_post_title)."</td>";
                                    echo "<td><span class='label label-success'>".($p->status ==1 ? 'Active' : 'Inactive')."</span></td>";
									echo "<td><a href='".FULL_CMS_URL."/manage_innerposts/edit?id=".$p->id."_".$p->parent_post_id."'> <img alt='Edit' src='".S3_URL."cms/images/icons/pencil.png'  width='20' height='20'></img></a></td>";
									
                                    echo "</tr>";
                                    $sl_no++;
                              }
                            }
                        ?>
						 <tr>
                  <td colspan="7" style="line-height: 1.5em; !important">
                    <?php
                      if (count($details) > 0) {
                         echo $links;
                      }
                    ?>
                  </td>
                </tr>
                        </tbody>
                    </table>                                    
                    
                </div>
            <!-- END DATATABLE EXPORT -->                            
            
        </div>
    </div>

</div>         
<!-- END PAGE CONTENT WRAPPER -->




