<script>
 function myFunction()
 {
	var name=document.getElementById("name").value;
	var status = document.getElementById("status").value;
    if(name == "" || name.trim() =="")
	{
		    alert("Tag field is required");
			document.getElementById("name").value="";
			document.getElementById("name").focus();
			return false;
	}
	if(status == "" || status.trim() =="")
	{
		    alert("status is required");
			document.getElementById("name").value="";
			document.getElementById("name").focus();
			return false;
	}	

 }

</script>
<script>
$(document).ready(function(){
	$('#name').on('keyup', function() {
		var name = $("#name").val();
		var string = name.replace(/[^a-zA-Z 0-9]+/g,'');
		var name_url  = string.replace(/\s+/g, '-').toLowerCase()+'-';
		$("#seourl").val(name_url);
		
	});
 });
</script>
<div class="span9">
<div class="content">
<div class="module">
    <div class="module-head">
        <h3>Add Tag</h3>
        <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo SITE_URL?>index.php/cms/manage_tags">Manage Tags</a></h3>	
    </div>
    <div class="module-body">
            <?php 
            if( $this->session->flashdata('error') ) { 
               echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>'.$this->session->flashdata('error').'</strong></div>';
        
            }else if( $this->session->flashdata('success') ) { 
            
               echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>'.$this->session->flashdata('success').'</strong></div>';
            }
          ?>
            <br />                

              <form class="form-horizontal row-fluid" method="post" action="<?php echo FULL_CMS_URL.'/manage_tags/add_action' ?>">
                
                    <div class="control-group">
                        <label class="control-label" for="basicinput">Tag Name* </label>
                        <div class="controls">
                            <input type="text" name="name" id="name" autofocus="autofocus" tabindex="1" class="span8"/>
						</div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="basicinput">Tag Seourl* </label>
                        <div class="controls">
                            <input type="text" name="seourl" id="seourl" autofocus="autofocus" tabindex="1" class="span8"/>
						</div>
                    </div>
                    <div class="control-group">                                        
                        <label class="control-label" for="basicinput">Status*</label>
                        <div class="controls">
                       <select class="span8" name="status" id="status" tabindex="6" data-attr="Please select status">
                        <option value="">Select Status</option>
                        <option value="1">Active</option>
                        <option value="2">Inactive</option>
                        </select>
                        <span class="help-block" id="status_err"></span>
              </div>
          </div>
                    
                    <div class="control-group">
                        <div class="controls">
                            <input type="submit" name="submit" id="submit" value="Submit" onclick="return myFunction()"/>
                        </div>
                    </div>
                </form>
            </div>
            </div>
                            
        </div><!--/.content-->
    </div>

