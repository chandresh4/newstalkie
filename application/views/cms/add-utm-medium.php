<?php //$arr_all = all_arrays(); ?>
<script type="application/javascript">

 function checkInputs(){ 
    var regex = /^[0-9]*$/;
    var medium_name=document.getElementById("medium_name").value;
	var status           =document.getElementById("status").value;

		if( medium_name== "" || medium_name.trim() ==""){
			alert("Medium  Name is required");
			document.getElementById("medium_name").focus();
			return false;
		}
		
		if(status == "" || status.trim() ==""){
	     alert("status field is required");
	     document.getElementById("status").focus();
	      return false;
	    }
		
 }

 </script>


<div class="span9">
<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Add UTM Medium</h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo SITE_URL?>index.php/cms/manage_utm_medium">Manage UTM Medium</a></h3>	
        </div>
        <div class="module-body">
                <?php 
					if( $this->session->flashdata('error') ) { 
					   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
							<strong>'.$this->session->flashdata('error').'</strong></div>';
				
					}else if( $this->session->flashdata('success') ) { 
					
					   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
						  <strong>'.$this->session->flashdata('success').'</strong></div>';
					}
				  ?>
                
                <br />                

                  <form class="form-horizontal row-fluid" method="post" action="<?php echo SITE_URL.'cms/manage_utm_medium/add' ?>">
                        
					
					
                        <div class="control-group">
                            <label class="control-label" for="basicinput">UTM Medium name*</label>
                            <div class="controls">
                                <input type="text" id="medium_name" name="medium_name" tabindex="1" placeholder="UTM Medium name" class="span8">
                            </div>
                        </div>
                        
                        
                       
                        
						<div class="control-group">
                            <label class="control-label" for="basicinput">Status *</label>
                            <div class="controls">
                                <select tabindex="4" name="status" id="status" data-placeholder="Select Status.." class="span8">
                                <option value="" >Select Status</option>
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                        
                        
                        <div class="control-group">
                            <div class="controls">
                                <input type="submit" name="addform" value="Save" onclick="return checkInputs()">
                            </div>
                        </div>
                    </form>
                </div>
                </div>
                                
            </div><!--/.content-->
        </div>
