<html lang = "en">

<head>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta property="fb:pages" content="113521663750198" />
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="manifest" href="<?php echo SITE_URL?>pwa/manifest.json" />


<meta name="title" content="Newstalkie.com, India, Business, Bollywood, Cricket, Video and Breaking News"> 

<?php 
if(isset($meta_keywords) && $meta_keywords != ""){  ?>
    <meta name="keywords" content="<?php echo $meta_keywords?>">

<?php }else{ ?>
    <meta name="keywords" content="news, English news, India news, breaking news, business news, Bollywood news, cricket news, world news, Latest News, Breaking and Latest news, online news, English News channel, News channels">
<?php } 

if(isset($meta_description) && $meta_description != ""){  ?>
    <meta name="description" content="<?php echo $meta_description?>">

<?php }else{ ?>
    
    <meta name="description" content="Newstalkie.com, India, Business, Bollywood, Cricket, Video and Breaking News">

<?php } 

if(isset($meta_og_image) && $meta_og_image != "") { ?>

    <meta property="og:image" content="<?php echo S3_URL?>site/images/posts/postimage_crop/<?php echo "thumb-".$meta_og_image ?>">

<?php } ?>

<title>Newstalkie.com, India, Business, Bollywood, Cricket, Video and Breaking News</title>
<link rel="shortcut icon" href="<?php echo S3_URL?>site/images/favicon.jpg" type="image/x-icon">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo S3_URL?>site/css/main.css">


    
    <script data-ad-client="ca-pub-9625841015804508" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    
    <script data-ad-client="ca-pub-9625841015804508" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    
    <!-- Search Console Code -->

    <meta name="google-site-verification" content="Rry1xgkvWH1XOfU8owZ-7I0EEWRgDmeB6VdG9x88uyc" />

    <!-- Global site tag (gtag.js) - Google Analytics -->

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-163805997-1"></script>
   
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-163805997-1');
    </script>


    <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
<script>
  window.googletag = window.googletag || {cmd: []};
  googletag.cmd.push(function() {
    googletag.defineSlot('/112279518/Native_Groping', [850, 460], 'div-gpt-ad-1611898250803-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();
  });
</script>

<script type="text/javascript">
    if ("serviceWorker" in navigator) {
      // register service worker
      navigator.serviceWorker.register("<?php echo SITE_URL?>pwa/service-worker.js");
    }
</script>


</head>

    <body>

        <div class="container-fluid" style="padding: 0;">

            <header>

                <div class="container-fluid webView">

                    <div class="col-md-12 paddingZero">

                        <div class="col-md-1">
                            <div class="logoSec goToHome">
                                <a href="<?php echo SITE_URL?>"><img src="<?php echo S3_URL?>site/images/footerlogo.svg" alt=""></a>
                            </div>
                        </div>
    
                        <div class="col-md-11 paddingZeroRight">
    
                            <div class="headerItemSec">
    
                                <div class="headerTopItemSec">

                                    <div class="userSec">
                                        <span> &nbsp;</span>
                                        <!--<span>Sign In</span>-->
                                    </div>
            
                                    <!--<div class="weatherSec">
                                        <span> <img src="<?php echo S3_URL?>site/images/weather.svg" alt=""> </span>
                                        <span>Bengaluru / 30°C</span>
                                    </div>-->
            
                                </div>
            
                                <div class="clearfix"></div>

                                <?php $country_code = (isset($_COOKIE["countrytest"]) != "" ? $_COOKIE["countrytest"] : "" ); ?>
            
                                <div class="headerBottomSec">
                                    <ul>
                                        <li><a href="<?php echo SITE_URL?>">Home</a></li>
                                        <li><a href="<?php echo SITE_URL."corona-virus"?>">Corona Virus</a></li>
                                        <li><a href="<?php echo SITE_URL."sports"?>">Sports</a></li>
                                        <li><a href="<?php echo SITE_URL."entertainment"?>">Entertainment</a></li>
                                        <li><a href="<?php echo SITE_URL."health-fitness"?>">Health</a></li>
                                        <li><a href="<?php echo SITE_URL?>environment">Environment</a> </li>

                                        <?php if($country_code == "in"){ ?>
                                        
                                            <li><a href="<?php echo SITE_URL."politics"?>"> Politics</a></li>
                                            <li><a href="<?php echo SITE_URL."tech-updates"?>">Tech Updates</a></li>
                                            <!-- <li><a href="<?php echo SITE_URL."education"?>">Education</a></li> -->
                                            <li><a href="<?php echo SITE_URL."travel-adventure"?>">Travel-Adventure</a> </li>

                                        <?php } ?>
                                        
                                        <li><a href="<?php echo SITE_URL."viral-news"?>">Viral</a></li>
                                        

                                    </ul>
                                </div>
            
                            </div>
    
                        </div>
    
                    </div>

                </div>

                <div class="topnav mobView">

                    <a href="<?php echo SITE_URL?>"><img src="<?php echo S3_URL?>site/images/newstalkie-logo.svg" class="mobLogoSec" alt=""></a>

                    <div id="myLinks">
                        
                        <a href="<?php echo SITE_URL?>">Home</a>
                        <a href="<?php echo SITE_URL."corona-virus"?>">Corona Virus</a>
                        <a href="<?php echo SITE_URL."sports"?>">Sports</a>
                        <a href="<?php echo SITE_URL."entertainment"?>">Entertainment</a>
                        <a href="<?php echo SITE_URL."health-fitness"?>">Health</a>
                        <li> <a href="<?php echo SITE_URL ?>environment">Environment</a> </li>

                        <?php if($country_code == "in"){ ?>
                        
                            <a href="<?php echo SITE_URL."politics"?>"> Politics</a>
                            <a href="<?php echo SITE_URL."tech-updates"?>">Tech Updates</a>
                            <!-- <a href="<?php echo SITE_URL."education"?>">Education</a> -->
                            <a href="<?php echo SITE_URL."travel-adventure"?>">Travel-Adventure</a>
                        
                        <?php } ?>
                        
                        <a href="<?php echo SITE_URL."viral-news"?>">Viral</a>
                        

                    </div>
                    
                    <a href="javascript:void(0);" class="icon" onclick="myFunction()">
                        <i class="fa fa-bars"></i>
                    </a>

                </div>

            </header>

            <div class="clearfix"></div>
