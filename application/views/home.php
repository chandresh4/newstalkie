
<section class="advertiseSec headerMargin">


<?php

$country_code = (isset($_COOKIE["countrytest"]) != "" ? $_COOKIE["countrytest"] : "" ); 

if($country_code == "gb"){ ?>


    <!-- BEGIN ADVANCED JS TAG - 1600x168 Newstalkie_1600x168_HP Top Ads - DO NOT MODIFY -->
        <div class='ADK_BANNER' data-zoneid='127607' data-subid='' data-width='1600' data-height='168' data-net='cpm.rtbwire.com' data-opts='vw doci' ></div>
        <SCRIPT class="ADK_SCRIPT" TYPE="text/javascript" SRC="//static.rtbwire.com/tag/display.js"></SCRIPT>
    <!-- END TAG -->

<!--Ads place-->

<?php } ?>



<?php

if($country_code == "us"){ ?>


<!-- BEGIN JS EXT TAG - 728x90 NewstalkieUSA_728x90_leaderboard_HP - DO NOT MODIFY -->
<SCRIPT TYPE="text/javascript">
window.__jscp=function(){if(window.parent==window)var a=document.URL;else if(a=document.referrer,!a)try{a=window.parent.location.href}catch(e){}if(a){var b=a.indexOf("://");0<=b&&(a=a.substring(b+3));b=a.indexOf("/");0<=b&&(a=a.substring(0,b))}b=0;for(var c=window;c!=c.parent;)++b,c=c.parent;a={pu:a,"if":b,rn:new Number(Math.floor(99999999*Math.random())+1)};b=[];for(var d in a)b.push(d+"="+encodeURIComponent(a[d]));return encodeURIComponent(b.join("&"))};
document.write('<S' + 'CRIPT TYPE="text/javascript" SRC="//cpm.rtbwire.com/tag?zone_id=133999&size=728x90&site_cat=IAB12&j=' + __jscp() + '"></S' + 'CRIPT>');
</SCRIPT>
<!-- END TAG -->


<?php } ?>


<?php

if($country_code == "in"){ ?>

    <div class="webView" id="top_div_ad">

        <!-- BEGIN JS EXT TAG - 728x90 Newstalkie_IN_725x90_Leaderboard_HP - DO NOT MODIFY -->
        <SCRIPT TYPE="text/javascript">
        window.__jscp=function(){if(window.parent==window)var a=document.URL;else if(a=document.referrer,!a)try{a=window.parent.location.href}catch(e){}if(a){var b=a.indexOf("://");0<=b&&(a=a.substring(b+3));b=a.indexOf("/");0<=b&&(a=a.substring(0,b))}b=0;for(var c=window;c!=c.parent;)++b,c=c.parent;a={pu:a,"if":b,rn:new Number(Math.floor(99999999*Math.random())+1)};b=[];for(var d in a)b.push(d+"="+encodeURIComponent(a[d]));return encodeURIComponent(b.join("&"))};
        document.write('<S' + 'CRIPT TYPE="text/javascript" SRC="//cpm.rtbwire.com/tag?zone_id=135447&size=728x90&site_cat=IAB12&j=' + __jscp() + '"></S' + 'CRIPT>');
        </SCRIPT>
        <!-- END TAG -->

    </div>

    <div class="mobView">

        <!-- BEGIN JS EXT TAG - 340x160 Newstalkie_IN_340x160_HP_mobile - DO NOT MODIFY -->
        <SCRIPT TYPE="text/javascript">
        window.__jscp=function(){if(window.parent==window)var a=document.URL;else if(a=document.referrer,!a)try{a=window.parent.location.href}catch(e){}if(a){var b=a.indexOf("://");0<=b&&(a=a.substring(b+3));b=a.indexOf("/");0<=b&&(a=a.substring(0,b))}b=0;for(var c=window;c!=c.parent;)++b,c=c.parent;a={pu:a,"if":b,rn:new Number(Math.floor(99999999*Math.random())+1)};b=[];for(var d in a)b.push(d+"="+encodeURIComponent(a[d]));return encodeURIComponent(b.join("&"))};
        document.write('<S' + 'CRIPT TYPE="text/javascript" SRC="//cpm.rtbwire.com/tag?zone_id=135527&size=340x160&site_cat=IAB12&j=' + __jscp() + '"></S' + 'CRIPT>');
        </SCRIPT>
        <!-- END TAG -->

    </div>

<?php } ?>


</section>



<div class="clearfix"></div>

<div class="container-fluid">

    <div class="col-md-9 paddingZero">

        <section class="newsSecOne">

            <div class="row">

                <div class="col-md-12 paddingZero">

                    <div class="col-md-6 paddingZeroRight bigNews">

                        <?php        
                        if(isset($news) && count($news) > 0){
                           if(isset($news[0])){

                           $viral_news =  $news[0];
                           
                        ?>    

                        <div class="bigNewsImageSec">
                            <a href="<?php echo SITE_URL ?>category/story/<?php echo $viral_news->id ?>/<?php echo $viral_news->seourl ?>">
                                <img src="<?php echo S3_URL?>site/images/posts/<?php echo $viral_news->post_image ?>" alt="<?php echo $viral_news->post_title ?>" alt="">
                            </a>    
                        </div>

                        <div class="bigNewsContentSec linearBackground pointer" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $viral_news->id ?>/<?php echo $viral_news->seourl ?>'">

                            <span class="newsBadge"> Breaking News</span>

                            <h2 class="semiBold"><?php echo stripcslashes($viral_news->post_title); ?></h2>

                        </div>

                        <?php } 
                        }?>
                    
                    </div>

                    <div class="col-md-6 paddingZeroRight mobilePaddingZeroLeft">

                        <div class="smallImageTopSec">


                       <?php 
                       $i = 0 ;
                       if(is_array($news) && count($news) > 0 ){ 
                            foreach($news as $p1){ 
                                if($i > 0 && $i < 3){
                                ?>

                            <div class="col-md-6 paddingZeroRight">

                                <div class="smallNewsImageSection">
                                    <a href="<?php echo SITE_URL ?>category/story/<?php echo $p1->id ?>/<?php echo $p1->seourl ?>">
                                        <img src="<?php echo S3_URL?>site/images/posts/medium_postimage_crop/<?php $img ="thumb-".$p1->post_image;  echo $img ?>" alt="Newslakie News">
                                    </a>    
                                </div>

                                <div class="smallNewsContentSection linearBackground pointer" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $p1->id ?>/<?php echo $p1->seourl ?>'">
                                    <h5 class><?php echo substr(stripcslashes($p1->post_title),0,100)?></h5>
                                </div>

                            </div>
                        <?php } 
                           $i++;
                            }
                        }?>    

                        </div>

                        <div class="clearfix"></div>

                        <div class="smallImageBottomSec">

                        <?php 
                        $i = 0 ;
                        if(is_array($news) && count($news) > 0 ){ 
                            foreach($news as $p1){ 
                                if($i > 2 && $i < 5){
                                ?>
                        
                            <div class="col-md-6 paddingZeroRight">

                                <div class="smallNewsImageSection">
                                    <a href="<?php echo SITE_URL ?>category/story/<?php echo $p1->id ?>/<?php echo $p1->seourl ?>">
                                        <img src="<?php echo S3_URL?>site/images/posts/medium_postimage_crop/<?php $img ="thumb-".$p1->post_image;  echo $img ?>" alt="Newslakie News">
                                    </a>
                                </div>

                                <div class="smallNewsContentSection linearBackground pointer" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $p1->id ?>/<?php echo $p1->seourl ?>'">
                                    <h5 class><?php echo substr(stripcslashes($p1->post_title),0,62 )?>... </h5>
                                </div>

                            </div>

                        <?php } 
                           $i++;
                            }
                        }?>     

                        </div>

                    </div>

                </div>

            </div>

        </section>

        <div class="clearfix"></div>

        <section class="newsSecTwo">

            <div class="row">

                <hr>

                <div class="col-md-12 secondNewsSec">

                    <div class="col-md-9 paddingZero">

                        <div class="col-md-5 paddingZero">

                            <h5 class="boldFont headingH5">Top Stories</h5>

                                <?php 

                                if($country_code == "us"){ ?>

                                <div class="col-md-12 paddingZero">

                                    <!-- BEGIN JS EXT TAG - 300x250 Newstalkie USA_prashanthi_300*250_Lefttop - DO NOT MODIFY -->
                                    <SCRIPT TYPE="text/javascript">
                                    window.__jscp=function(){if(window.parent==window)var a=document.URL;else if(a=document.referrer,!a)try{a=window.parent.location.href}catch(e){}if(a){var b=a.indexOf("://");0<=b&&(a=a.substring(b+3));b=a.indexOf("/");0<=b&&(a=a.substring(0,b))}b=0;for(var c=window;c!=c.parent;)++b,c=c.parent;a={pu:a,"if":b,rn:new Number(Math.floor(99999999*Math.random())+1)};b=[];for(var d in a)b.push(d+"="+encodeURIComponent(a[d]));return encodeURIComponent(b.join("&"))};
                                    document.write('<S' + 'CRIPT TYPE="text/javascript" SRC="//cpm.rtbwire.com/tag?zone_id=133990&size=300x250&j=' + __jscp() + '"></S' + 'CRIPT>');
                                    </SCRIPT>
                                    <!-- END TAG -->  

                                </div>

                                <div class="col-md-12 paddingZero">

                                    <!-- BEGIN JS EXT TAG - 300x250 Newstakie USA_300*250_Topleft_Topstories - DO NOT MODIFY -->
                                    <SCRIPT TYPE="text/javascript">
                                    window.__jscp=function(){if(window.parent==window)var a=document.URL;else if(a=document.referrer,!a)try{a=window.parent.location.href}catch(e){}if(a){var b=a.indexOf("://");0<=b&&(a=a.substring(b+3));b=a.indexOf("/");0<=b&&(a=a.substring(0,b))}b=0;for(var c=window;c!=c.parent;)++b,c=c.parent;a={pu:a,"if":b,rn:new Number(Math.floor(99999999*Math.random())+1)};b=[];for(var d in a)b.push(d+"="+encodeURIComponent(a[d]));return encodeURIComponent(b.join("&"))};
                                    document.write('<S' + 'CRIPT TYPE="text/javascript" SRC="//cpm.rtbwire.com/tag?zone_id=134003&size=300x250&site_cat=IAB12&j=' + __jscp() + '"></S' + 'CRIPT>');
                                    </SCRIPT>
                                    <!-- END TAG -->

                                </div>

                                <?php } ?>

                            <?php 
                           $i = 0 ;
                            if(is_array($news) && count($news) > 0 ){ 
                            foreach($news as $p1){ 
                                if($i >  4 && $i < 8){
                            ?>

                            <div class="col-md-12 paddingZero">

                                <div class="smallNewsSecondImageLeftSection">
                                    <a href="<?php echo SITE_URL ?>category/story/<?php echo $p1->id ?>/<?php echo $p1->seourl ?>">
                                        <img src="<?php echo S3_URL?>site/images/posts/medium_postimage_crop/<?php $img ="thumb-".$p1->post_image;  echo $img ?>" alt="Newslakie News">
                                    </a>
                                </div>

                                <div class="smallNewsSecondContentSection pointer" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $p1->id ?>/<?php echo $p1->seourl ?>'">
                                    <h5 class="semiBold headingH5"><?php echo substr(stripcslashes($p1->post_title),0,100)?> </h5>
                                </div>

                            </div>

                           <?php
                                     }
                             $i++;
                                }
                            }

                            ?>

                            <div class="clearfix"></div>

                            <!--<div class="newsLetterSection">

                                <h4 class="semiBold whiteTxt marginBottom15">YOUR DAILY NEWSLETTER</h4>

                                <form class="form-inline">

                                    <div class="form-group">
                                    <input type="email" class="form-control" id="email" placeholder="Enter email">
                                    </div>

                                    <button type="submit" class="btn btn-default">Submit</button>

                                <label class="error hidden">Enter Valid Email ID</label>

                                </form>

                            </div>-->

                        </div>

                        <div class="col-md-7">
                            <h5 class="boldFont headingH5">Around the world</h5>

                            <div class="col-md-12 paddingZero">


                                <?php 
                                $i = 0 ;
                                if(is_array($news) && count($news) > 0 ){ 
                                    foreach($news as $p1){ 
                                        if($i >  7 && $i < 11){
                                ?>

                                        <div class="col-md-12 paddingZero secondNewMiddleTopSec">

                                            <div class="col-md-5 paddingZero smallNewsSecondImageMiddleSection">
                                              <a href="<?php echo SITE_URL ?>category/story/<?php echo $p1->id ?>/<?php echo $p1->seourl ?>">
                                                <img src="<?php echo S3_URL?>site/images/posts/medium_postimage_crop/<?php $img ="thumb-".$p1->post_image;  echo $img ?>" alt="Newslakie News">
                                            </a>
                                            </div>

                                            <div class="col-md-7 smallNewsSecondContentMiddleSection pointer" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $p1->id ?>/<?php echo $p1->seourl ?>'">
                                                <h5 class="semiBold"><?php echo substr(stripcslashes($p1->post_title),0,100)?> </h5>
                                            </div>

                                        </div>

                                <?php
                                         }
                                 $i++;
                                    }
                                }

                                ?>


                            </div>

                            <div class="col-md-12 paddingZero">

                                <?php 
                                $i = 0 ;
                                if(is_array($news) && count($news) > 0 ){ 
                                    foreach($news as $p1){ 
                                        if($i >  12 && $i < 17){
                                ?>

                                        <div class="col-md-6 paddingZeroLeft marginBottom15">

                                            <div class="smallNewsSecondImageBottomSection">
                                                <a href="<?php echo SITE_URL ?>category/story/<?php echo $p1->id ?>/<?php echo $p1->seourl ?>">
                                                    <img src="<?php echo S3_URL?>site/images/posts/medium_postimage_crop/<?php $img ="thumb-".$p1->post_image;  echo $img ?>" alt="Newslakie News">
                                                </a>
                                            </div>

                                            <div class="smallNewsSecondBottomContentSection linearBackground pointer" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $p1->id ?>/<?php echo $p1->seourl ?>'">
                                                <h5 class=""><?php echo substr(stripcslashes($p1->post_title),0,50)?>...</h5>
                                            </div>

                                        </div>

                                <?php
                                        }
                                 $i++;
                                    }
                                }

                                ?>          

                            </div>                                

                        </div>

                    </div>

                    <div class="col-md-3 paddingZero">

                        <h5 class="boldFont headingH5">Must See</h5>

                        <div class="col-md-12">

                            <?php 
                            $i = 0 ;
                            if(is_array($news) && count($news) > 0 ){ 
                                foreach($news as $p1){ 
                                    if($i >  10 && $i < 14){
                            ?>
                                    <div class="col-md-12 paddingZero marginBottom15">

                                        <div class="smallNewsSecondImageRightSection">
                                            <a href="<?php echo SITE_URL ?>category/story/<?php echo $p1->id ?>/<?php echo $p1->seourl ?>">
                                                <img src="<?php echo S3_URL?>site/images/posts/medium_postimage_crop/<?php $img ="thumb-".$p1->post_image;  echo $img ?>" alt="Newslakie News">
                                            </a>
                                        </div>

                                        <div class="smallNewsSecondRIghtContentSection linearBackground pointer" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $p1->id ?>/<?php echo $p1->seourl ?>'">
                                            <h5 class=""><?php echo substr(stripcslashes($p1->post_title),0,100)?> </h5>
                                            
                                        </div>
                                        
                                    </div>

                             <?php
                                    }
                             $i++;
                                }
                            }

                            ?>       

                        </div>
                        

                    </div>

                </div>

            </div>

        </section>

        <section class="newsSecThree">

            <div class="row">

                <div class="col-md-12">

                        <!-- Composite Start -->
                        <div id="M639897ScriptRootC1009677">
                        </div>
                        <script src="https://jsc.mgid.com/n/e/newstalkie.com.1009677.js" async></script>
                        <!-- Composite End -->

                        <br><br>

                        <div class="clearfix"></div>

                    <div class="col-md-12 paddingZero">

                        <?php 
                        if(isset($news['23']) != ""){
                        $news_23 = $news['23'];
                        ?>

                        <div class="col-md-12 paddingZero bottomBorder marginBottom15">

                            <div class="newsThreeImageSec pointer" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $news_23->id ?>/<?php echo $news_23->seourl ?>'">
                                <a href="<?php echo SITE_URL ?>category/story/<?php echo $news_23->id ?>/<?php echo $news_23->seourl ?>">
                                    <img src="<?php echo S3_URL?>site/images/posts/<?php echo $news_23->post_image ?>" alt="<?php echo $news_23->post_title ?>" alt="">
                                </a>
                            </div>

                            <div class="newsThreeImageSecInner linearBackground">
                                <h2 class="semiBold"><?php echo stripcslashes($news_23->post_title) ?> </h2>
                            </div>

                            <div class="newsThreeImageSecOuter">
                                <h2 class="semiBold" ><?php echo substr($news_23->post_sub_title, 0, 76);  ?></h2>
                            </div>

                        </div>

                        <?php 
                        }
                        ?>

                        <div class="col-md-12 paddingZero">

                            <?php 
                            $i = 0 ;
                            if(is_array($news) && count($news) > 0 ){ 
                                foreach($news as $p1){ 
                                    if($i >  24 && $i < 27){
                            ?> 
                            <div class="col-md-6 paddingZero">

                                <div class="col-md-6 paddingZero newsThreeBottomImageSection">
                                    <a href="<?php echo SITE_URL ?>category/story/<?php echo $p1->id ?>/<?php echo $p1->seourl ?>" >
                                        <img src="<?php echo S3_URL?>site/images/posts/medium_postimage_crop/<?php $img ="thumb-".$p1->post_image;  echo $img ?>" alt="Newslakie News">
                                    </a>
                                </div>

                                <div class="col-md-6 newsThreeBottomContentSection pointer" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $p1->id ?>/<?php echo $p1->seourl ?>'">
                                    <p><?php echo substr($p1->post_title,0,100) ?></p>
                                </div>

                            </div>

                           <?php
                                    }
                             $i++;
                                }
                            }

                            ?>        

                        </div>

                    </div>

                </div>

            </div>

        </section>

        <section class="newsSecFour">

            <div class="row">

                <div class="col-md-12 greyBg paddingBottom15">

                    <h3 class="boldFont"> Featured Sections </h3>
                    
                    <?php 
                    $i = 0 ;
                    if(is_array($news) && count($news) > 0 ){ 
                        foreach($news as $p1){ 
                            if($i >  26 && $i < 31){
                    ?>

                    <div class="col-md-3 paddingZeroLeft mobPaddingZeroRight">

                        <div class="newsFourImageSec">
                            <a href="<?php echo SITE_URL ?>category/story/<?php echo $p1->id ?>/<?php echo $p1->seourl ?>">
                                <img src="<?php echo S3_URL."site/images/posts/".$p1->post_image?>" alt="Newslakie News">
                            </a>
                        </div>

                        <div class="newsFourContentSec linearBackground pointer" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $p1->id ?>/<?php echo $p1->seourl ?>'">
                            <h5 class="semiBold">
                                <?php echo substr($p1->post_title,0,100)?>
                            </h5>
                        </div>

                    </div>

                    <?php
                            }
                     $i++;
                        }
                    }

                    ?> 
                    

                </div>

            </div>

        </section>

        <!--<section class="midAdvSec">

            <div class="container-fluid text-center">

                
                <scriptasync src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>

                <script>

                    window.googletag = window.googletag || {cmd: []};

                    googletag.cmd.push(function() {

                    googletag.defineSlot('/360613911/Newstalkie.com', [988, 168], 'div-gpt-ad-1600694827318-0').addService(googletag.pubads());

                    googletag.pubads().enableSingleRequest();

                    googletag.enableServices();

                    });

                </script>

                
                <div id='div-gpt-ad-1600694827318-0' style='width: 988px; height: 168px;'>

                    <script>

                    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1600694827318-0'); });

                    </script>

                </div>
             
            </div>

        </section>-->

        <section class="newsSecFive">

            <div class="row">

                <div class="col-md-12 greyBg entertainmentSec">

                    <h3 class="boldFont"> Entertainment </h3>

                    <div class="col-md-12 paddingZero">

                                    
                            <?php 
                                $i = 0;
                                if(is_array($entertainment) && count($entertainment) > 0 ){ 
                                    $p1 = $entertainment[0];
                                ?>
                                    <div class="col-md-3 paddingZero">
                                        <div class="newsFourImageSec">
                                            <a href="<?php echo SITE_URL ?>category/story/<?php echo $p1->id ?>/<?php echo $p1->seourl ?>">
                                                <img src="<?php echo S3_URL."site/images/posts/".$p1->post_image?>" alt="Newslakie News">
                                            </a>
                                        </div>

                                        <div class="newsFiveContentSec linearBackground pointer" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $p1->id ?>/<?php echo $p1->seourl ?>'">
                                            <h5 class="semiBold">
                                                <?php echo substr(stripcslashes($p1->post_title),0,100)?>
                                            </h5>
                                        </div>
                                    </div>  

                                
                                <div class="col-md-9">
                                 
                                <?php    
                                foreach($entertainment as $p1){ 
                                    if($i >  0){
                                ?>                        
                                    
                                    <div class="col-md-6 col-sm-12 newsFiveRightSection">

                                        <div class="col-md-4 paddingZero newsFiveRightImageSection">
                                            <a href="<?php echo SITE_URL ?>category/story/<?php echo $p1->id ?>/<?php echo $p1->seourl ?>">
                                                <img src="<?php echo S3_URL."site/images/posts/".$p1->post_image?>" alt="Newslakie News">
                                            </a>
                                        </div>

                                        <div class="col-md-8 col-sm-12 newsFiveBottomContentSection pointer" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $p1->id ?>/<?php echo $p1->seourl ?>'">
                                            <p><?php echo substr(stripcslashes($p1->post_title),0,100)?></p>
                                        </div>

                                    </div>

                            <?php
                               
                                    }
                                 
                                $i++;
                             }
                            ?>    

                        </div>

                        <?php
                        }?>

                    </div>
                    

                </div>

            </div>

        </section>

        <section class="partnerAdBottomSec">

            <div class="row">

                <h3 class="boldFont"> <span class="sponsredBadge"> Ad </span> &nbsp; <span> From Our Partners </span> </h3>

                <div class="parentAdHolder">

                    <!-- Composite Start -->
					<div id="M639897ScriptRootC1009677">
					</div>
					<script src="https://jsc.mgid.com/n/e/newstalkie.com.1009677.js" async></script>
					<!-- Composite End -->
                    

                </div>

            </div>

        </section>

    </div>

    <div class="col-md-3 paddingZero">

        <div class="partnerAdSec marginTopMinus3">
        
            <div class="col-md-12">
                
                <?php 

                if($country_code == "us"){ ?>

                <div class="col-md-12 paddingZero">

                    <div class="smallNewsSecondImageLeftSection">

                        <!-- BEGIN JS EXT TAG - 300x250 Newstalkie USA_prashanthi_300*250_ Right top - DO NOT MODIFY -->
                        <SCRIPT TYPE="text/javascript">
                        window.__jscp=function(){if(window.parent==window)var a=document.URL;else if(a=document.referrer,!a)try{a=window.parent.location.href}catch(e){}if(a){var b=a.indexOf("://");0<=b&&(a=a.substring(b+3));b=a.indexOf("/");0<=b&&(a=a.substring(0,b))}b=0;for(var c=window;c!=c.parent;)++b,c=c.parent;a={pu:a,"if":b,rn:new Number(Math.floor(99999999*Math.random())+1)};b=[];for(var d in a)b.push(d+"="+encodeURIComponent(a[d]));return encodeURIComponent(b.join("&"))};
                        document.write('<S' + 'CRIPT TYPE="text/javascript" SRC="//cpm.rtbwire.com/tag?zone_id=133469&size=300x250&j=' + __jscp() + '"></S' + 'CRIPT>');
                        </SCRIPT>
                        <!-- END TAG -->
                        
                    </div> 

                </div>

                <div class="col-md-12 paddingZero">
                    <!-- BEGIN JS EXT TAG - 300x250 Newstalkie USA_prashanthi_300*250_right top - DO NOT MODIFY -->
                    <SCRIPT TYPE="text/javascript">
                    window.__jscp=function(){if(window.parent==window)var a=document.URL;else if(a=document.referrer,!a)try{a=window.parent.location.href}catch(e){}if(a){var b=a.indexOf("://");0<=b&&(a=a.substring(b+3));b=a.indexOf("/");0<=b&&(a=a.substring(0,b))}b=0;for(var c=window;c!=c.parent;)++b,c=c.parent;a={pu:a,"if":b,rn:new Number(Math.floor(99999999*Math.random())+1)};b=[];for(var d in a)b.push(d+"="+encodeURIComponent(a[d]));return encodeURIComponent(b.join("&"))};
                    document.write('<S' + 'CRIPT TYPE="text/javascript" SRC="//cpm.rtbwire.com/tag?zone_id=133989&size=300x250&j=' + __jscp() + '"></S' + 'CRIPT>');
                    </SCRIPT>
                    <!-- END TAG -->
                </div>

                <?php } ?>

                <?php 
                $i = 0 ;
                if(is_array($news) && count($news) > 0 ){ 
                    foreach($news as $r){ 
                        if($i >  16 && $i < 24){
                ?>        

                        <div class="col-md-12 paddingZero">
                            <div class="smallNewsSecondImageLeftSection">

                                <a href="<?php echo SITE_URL ?>category/story/<?php echo $r->id ?>/<?php echo $r->seourl ?>">
                                    <img src="<?php echo S3_URL?>site/images/posts/small_postimage_crop/<?php $img ="thumb-".$r->post_image;  echo $img ?>" alt="">
                                </a>
                            </div>

                            <div class="smallNewsSecondContentSection marginTop10 pointer" onclick="window.location.href= '<?php echo SITE_URL ?>category/story/<?php echo $r->id ?>/<?php echo $r->seourl ?>'">
                                <!-- <span class="sponsredBadge"> Sponsored </span> -->
                                <h5 class="semiBold headingH5"><?php echo substr($r->post_title,0,40)?>..</h5>
                            </div>

                        </div>

                <?php   }
                        
                    $i++;} 

                 } ?>

                <div class="col-md-12 paddingZero">

                    <div class="smallNewsSecondImageLeftSection">

                        <!-- Composite Start -->
                        <div id="M639897ScriptRootC1009686"></div>
                        <script src="https://jsc.mgid.com/n/e/newstalkie.com.1009686.js" async></script>
                        <!-- Composite End -->  
                        
                    </div> 

                </div>

                <?php

                if($country_code == "us"){ ?>

                <div class="col-md-12 paddingZero">

                    <div class="smallNewsSecondImageLeftSection">

                        <!-- BEGIN JS EXT TAG - 300x250 NewstalkieUSA_Madhu_300*250_right_adustMGID - DO NOT MODIFY -->
                        <SCRIPT TYPE="text/javascript">
                        window.__jscp=function(){if(window.parent==window)var a=document.URL;else if(a=document.referrer,!a)try{a=window.parent.location.href}catch(e){}if(a){var b=a.indexOf("://");0<=b&&(a=a.substring(b+3));b=a.indexOf("/");0<=b&&(a=a.substring(0,b))}b=0;for(var c=window;c!=c.parent;)++b,c=c.parent;a={pu:a,"if":b,rn:new Number(Math.floor(99999999*Math.random())+1)};b=[];for(var d in a)b.push(d+"="+encodeURIComponent(a[d]));return encodeURIComponent(b.join("&"))};
                        document.write('<S' + 'CRIPT TYPE="text/javascript" SRC="//cpm.rtbwire.com/tag?zone_id=133467&size=300x250&j=' + __jscp() + '"></S' + 'CRIPT>');
                        </SCRIPT>
                        <!-- END TAG -->
                        
                    </div> 

                </div>

                <?php } ?>


                <?php

                if($country_code == "us"){ ?>

                <div class="col-md-12 paddingZero">

                    <div class="smallNewsSecondImageLeftSection">

                        <!-- BEGIN JS EXT TAG - 300x250 NewstalkieUSA_Shivaraj_300*250_right_adustENTERTAINMENT - DO NOT MODIFY -->
                        <SCRIPT TYPE="text/javascript">
                        var __jscp=function(){for(var b=0,a=window;a!=a.parent;)++b,a=a.parent;if(a=window.parent==window?document.URL:document.referrer){var c=a.indexOf("://");0<=c&&(a=a.substring(c+3));c=a.indexOf("/");0<=c&&(a=a.substring(0,c))}var b={pu:a,"if":b,rn:new Number(Math.floor(99999999*Math.random())+1)},a=[],d;for(d in b)a.push(d+"="+encodeURIComponent(b[d]));return encodeURIComponent(a.join("&"))};

                        document.write('<S' + 'CRIPT TYPE="text/javascript" SRC="//cpm.rtbwire.com/tag?zone_id=133468&size=300x250&site_cat=IAB12&j=' + __jscp() + '"></S' + 'CRIPT>');
                        </SCRIPT>
                        <!-- END TAG -->
                        
                    </div> 

                </div>

                <div class="clearfix"></div>

                <?php } ?>

                <?php

                if($country_code == "us"){ ?>

                <div class="col-md-12 paddingZero margintop15">

                    <div class="smallNewsSecondImageLeftSection">

                        <!-- BEGIN JS EXT TAG - 300x250 NewstalkieUSA_300*250_bottomright_adjusmgid - DO NOT MODIFY -->
                        <SCRIPT TYPE="text/javascript">
                        window.__jscp=function(){if(window.parent==window)var a=document.URL;else if(a=document.referrer,!a)try{a=window.parent.location.href}catch(e){}if(a){var b=a.indexOf("://");0<=b&&(a=a.substring(b+3));b=a.indexOf("/");0<=b&&(a=a.substring(0,b))}b=0;for(var c=window;c!=c.parent;)++b,c=c.parent;a={pu:a,"if":b,rn:new Number(Math.floor(99999999*Math.random())+1)};b=[];for(var d in a)b.push(d+"="+encodeURIComponent(a[d]));return encodeURIComponent(b.join("&"))};
                        document.write('<S' + 'CRIPT TYPE="text/javascript" SRC="//cpm.rtbwire.com/tag?zone_id=133927&size=300x250&site_cat=IAB12&j=' + __jscp() + '"></S' + 'CRIPT>');
                        </SCRIPT>
                        <!-- END TAG -->
                        
                    </div> 

                </div>

                <?php } ?>


                <?php

                if($country_code == "in"){ ?>

                <div class="col-md-12 paddingZero margintop15">

                    <div class="smallNewsSecondImageLeftSection">

                        <!-- BEGIN JS EXT TAG - 300x250 NewsTalkie IN_prashanthi_300*250_Right_adjmgid - DO NOT MODIFY -->
                        <SCRIPT TYPE="text/javascript">
                        window.__jscp=function(){if(window.parent==window)var a=document.URL;else if(a=document.referrer,!a)try{a=window.parent.location.href}catch(e){}if(a){var b=a.indexOf("://");0<=b&&(a=a.substring(b+3));b=a.indexOf("/");0<=b&&(a=a.substring(0,b))}b=0;for(var c=window;c!=c.parent;)++b,c=c.parent;a={pu:a,"if":b,rn:new Number(Math.floor(99999999*Math.random())+1)};b=[];for(var d in a)b.push(d+"="+encodeURIComponent(a[d]));return encodeURIComponent(b.join("&"))};
                        document.write('<S' + 'CRIPT TYPE="text/javascript" SRC="//cpm.rtbwire.com/tag?zone_id=134657&size=300x250&j=' + __jscp() + '"></S' + 'CRIPT>');
                        </SCRIPT>
                        <!-- END TAG -->
                        
                    </div> 

                </div>

                <?php } ?>


                <?php

                if($country_code == "in"){ ?>

                <div class="col-md-12 paddingZero margintop15">

                    <div class="smallNewsSecondImageLeftSection">

                        <!-- BEGIN JS EXT TAG - 300x250 Newstalkie IN_prashanthi_300*250_Rightbelow_mgd - DO NOT MODIFY -->
                        <SCRIPT TYPE="text/javascript">
                        window.__jscp=function(){if(window.parent==window)var a=document.URL;else if(a=document.referrer,!a)try{a=window.parent.location.href}catch(e){}if(a){var b=a.indexOf("://");0<=b&&(a=a.substring(b+3));b=a.indexOf("/");0<=b&&(a=a.substring(0,b))}b=0;for(var c=window;c!=c.parent;)++b,c=c.parent;a={pu:a,"if":b,rn:new Number(Math.floor(99999999*Math.random())+1)};b=[];for(var d in a)b.push(d+"="+encodeURIComponent(a[d]));return encodeURIComponent(b.join("&"))};
                        document.write('<S' + 'CRIPT TYPE="text/javascript" SRC="//cpm.rtbwire.com/tag?zone_id=134672&size=300x250&site_cat=IAB12&j=' + __jscp() + '"></S' + 'CRIPT>');
                        </SCRIPT>
                        <!-- END TAG -->

                        
                    </div> 

                </div>

                <?php } ?>
                

                <?php

                if($country_code == "in"){ ?>

                <div class="col-md-12 paddingZero margintop15">

                    <div class="smallNewsSecondImageLeftSection">

                        <!-- BEGIN JS EXT TAG - 300x250 Newstalkie_MPL_Cricket_300*250_Right_mgid - DO NOT MODIFY -->
                        <SCRIPT TYPE="text/javascript">
                        window.__jscp=function(){if(window.parent==window)var a=document.URL;else if(a=document.referrer,!a)try{a=window.parent.location.href}catch(e){}if(a){var b=a.indexOf("://");0<=b&&(a=a.substring(b+3));b=a.indexOf("/");0<=b&&(a=a.substring(0,b))}b=0;for(var c=window;c!=c.parent;)++b,c=c.parent;a={pu:a,"if":b,rn:new Number(Math.floor(99999999*Math.random())+1)};b=[];for(var d in a)b.push(d+"="+encodeURIComponent(a[d]));return encodeURIComponent(b.join("&"))};
                        document.write('<S' + 'CRIPT TYPE="text/javascript" SRC="//cpm.rtbwire.com/tag?zone_id=135353&size=300x250&site_cat=IAB12&j=' + __jscp() + '"></S' + 'CRIPT>');
                        </SCRIPT>
                        <!-- END TAG -->
                        
                    </div> 

                </div>

                <?php } ?>



            </div>
            
        </div>

    </div>

</div>

<script type="text/javascript">
    
        
//$('#top_div_ad img').width('340px').height('160px');

    

</script>