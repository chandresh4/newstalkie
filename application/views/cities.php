
<div class="container-fluid">

    <div class="col-md-9 paddingZero">

        <section class="indiaTopSecNews headerMargin">

            <div class="row">

                <h1 class="boldFont mobileCenter">Cities News</h1>
                                        
                <div class="col-md-12 indiaTopNewsItemHolder">

                    <div class="col-md-6">

                        <div class="col-md-4 indiaTopNewsImgSec">
                            <a href="">
                                <img src="<?php echo S3_URL?>site/images/cities/cities-left-img.png" alt="">
                            </a>
                        </div>

                        <div class="col-md-8 indiaTopNewsContentSec" onclick="">

                            <h5 class="boldFont">
                                Coronavirus Cases In Uttar Pradesh Cross 100: State Health Ministry
                            </h5>

                            <span class="dateTimePlace">Tuesday March 31, 2020, Lucknow</span>

                            <p>
                                With five more people testing positive for coronavirus in Bareilly today, the total count of COVID-19 or Coronavirus cases in Uttar Pradesh has crossed the 100 mark, said officials.                       
                            </p>

                        </div>

                    </div>
                
                    <div class="col-md-6">

                        <div class="col-md-4 indiaTopNewsImgSec">
                            <a href="">
                                <img src="<?php echo S3_URL?>site/images/cities/cities-right-img.png" alt="">
                            </a>
                        </div>

                        <div class="col-md-8 indiaTopNewsContentSec" onclick="">

                            <h5 class="boldFont">
                                Curb Fake News, Set Up COVID-19 Portal In 24 Hours For Real Time Info: Supreme Court To Centre                                       
                            </h5>

                            <span class="dateTimePlace">Tuesday March 31, 2020, Lucknow</span>

                            <p>
                                The Supreme Court today asked the Centre to set up a portal within 24 hours for dissemination of real time information on the coronavirus pandemic 

                        </div>

                    </div>
                
                </div>

                <div class="col-md-12 indiaTopNewsItemHolder">

                    <div class="col-md-6">

                        <div class="col-md-4 indiaTopNewsImgSec">
                            <a href="">
                                <img src="<?php echo S3_URL?>site/images/cities/city-imge-second.png" alt="">
                            </a>
                        </div>

                        <div class="col-md-8 indiaTopNewsContentSec" onclick="">

                            <h5 class="boldFont">
                                Coronavirus Cases In Uttar Pradesh Cross 100: State Health Ministry
                            </h5>

                            <span class="dateTimePlace">Tuesday March 31, 2020, Lucknow</span>

                            <p>
                                With five more people testing positive for coronavirus in Bareilly today, the total count of COVID-19 or Coronavirus cases in Uttar Pradesh has crossed the 100 mark, said officials.                       
                            </p>

                        </div>

                    </div>
                
                    <div class="col-md-6">

                        <div class="col-md-4 indiaTopNewsImgSec">
                            <a href="">
                                <img src="<?php echo S3_URL?>site/images/cities/citi-img-third.png" alt="">
                            </a>
                        </div>

                        <div class="col-md-8 indiaTopNewsContentSec">

                            
                            <div class="sponsredContent">
                                <span class="sponsredBadge"> Sponsored </span>
                            </div>
                            

                            <h4 class="boldFont">
                                American Lottery Known for ₹10,000 Crores Jackpot Coming to India
                            </h4>

                        </div>

                    </div>
                
                </div>

                <div class="col-md-12 indiaTopNewsItemHolder">

                    <div class="col-md-6">

                        <div class="col-md-4 indiaTopNewsImgSec">
                            <a href="">
                                <img src="<?php echo S3_URL?>site/images/cities/cities-left-img.png" alt="">
                            </a>
                        </div>

                        <div class="col-md-8 indiaTopNewsContentSec" onclick="">

                            <h5 class="boldFont">
                                Coronavirus Cases In Uttar Pradesh Cross 100: State Health Ministry
                            </h5>

                            <span class="dateTimePlace">Tuesday March 31, 2020, Lucknow</span>

                            <p>
                                With five more people testing positive for coronavirus in Bareilly today, the total count of COVID-19 or Coronavirus cases in Uttar Pradesh has crossed the 100 mark, said officials.                       
                            </p>

                        </div>

                    </div>
                
                    <div class="col-md-6">

                        <div class="col-md-4 indiaTopNewsImgSec">
                            <a href="">
                                <img src="<?php echo S3_URL?>site/images/cities/cities-right-img.png" alt="">
                            </a>
                        </div>

                        <div class="col-md-8 indiaTopNewsContentSec" onclick="">

                            <h5 class="boldFont">
                                Curb Fake News, Set Up COVID-19 Portal In 24 Hours For Real Time Info: Supreme Court To Centre                                       
                            </h5>

                            <span class="dateTimePlace">Tuesday March 31, 2020, Lucknow</span>

                            <p>
                                The Supreme Court today asked the Centre to set up a portal within 24 hours for dissemination of real time information on the coronavirus pandemic 
                            </p>

                        </div>

                    </div>
                
                </div>

                <div class="midAdvSec">
                    <img src="https://www.newstalkie.com/cdn/site/images/dummy-ads/mid_adv.png" alt="" class="img-responsive">
                </div>

                <div class="col-md-12 indiaTopNewsItemHolder">

                    <div class="col-md-6">

                        <div class="col-md-4 indiaTopNewsImgSec">
                            <a href="">
                                <img src="<?php echo S3_URL?>site/images/cities/citi-img-third.png" alt="">
                            </a>
                        </div>

                        <div class="col-md-8 indiaTopNewsContentSec">

                            
                            <div class="sponsredContent">
                                <span class="sponsredBadge"> Sponsored </span>
                            </div>
                            

                            <h4 class="boldFont">
                                American Lottery Known for ₹10,000 Crores Jackpot Coming to India
                            </h4>

                        </div>

                    </div>
                
                    <div class="col-md-6">

                        <div class="col-md-4 indiaTopNewsImgSec">
                            <a href="">
                                <img src="<?php echo S3_URL?>site/images/cities/city-imge-second.png" alt="">
                            </a>
                        </div>

                        <div class="col-md-8 indiaTopNewsContentSec" onclick="">

                            <h5 class="boldFont">
                                Curb Fake News, Set Up COVID-19 Portal In 24 Hours For Real Time Info: Supreme Court To Centre                                       
                            </h5>

                            <span class="dateTimePlace">Tuesday March 31, 2020, Lucknow</span>

                            <p>
                                The Supreme Court today asked the Centre to set up a portal within 24 hours for dissemination of real time information on the coronavirus pandemic 

                        </div>

                    </div>
                
                </div>

                <div class="clearfix"></div>

                <section class="iplNewsSection">

                    <div class="row">

                        <div class="col-md-12 iplSectionContainer">

                            <h2 class="boldFont iplHeading">IPL</h2>

                            <div class="col-md-4 upcomingMatch">

                                <div class="col-md-12 upcomingMatchHolder">

                                    <div class="col-md-3 dateSec">

                                        <span>Wednesday</span>

                                        <div class="matchDate">
                                            <b>21</b>
                                            <p>OCT</p>
                                        </div>

                                        <span>
                                            19.30 IST
                                        </span>

                                    </div>

                                    <div class="col-md-9 teamDetails">

                                        <label class="upcoming">Upcoming</label>

                                        <div class="clearfix"></div>

                                        <div class="col-md-12 teamDiv">

                                            <div class="col-md-4 teamName">
                                                <h5>KOL</h5>
                                            </div>

                                            <div class="col-md-4 mobDisplayInlineBlock">
                                                <h5>vs</h5>
                                            </div>

                                            <div class="col-md-4 teamName mobilePullRight">
                                                <h5 class="text-right">KOL</h5>
                                            </div>

                                        </div>

                                        <p class="location">Shikh Zayed stadium, abu dhabi, uae</p>

                                    </div>

                                </div>

                            </div>

                            <div class="col-md-4 upcomingMatch">

                                <div class="col-md-12 upcomingMatchHolder">

                                    <div class="col-md-3 dateSec">

                                        <span>Wednesday</span>

                                        <div class="matchDate">
                                            <b>21</b>
                                            <p>OCT</p>
                                        </div>

                                        <span>
                                            19.30 IST
                                        </span>

                                    </div>

                                    <div class="col-md-9 teamDetails">

                                        <label class="upcoming">Upcoming</label>

                                        <div class="clearfix"></div>

                                        <div class="col-md-12 teamDiv">

                                            <div class="col-md-4 teamName">
                                                <h5>KOL</h5>
                                            </div>

                                            <div class="col-md-4 mobDisplayInlineBlock">
                                                <h5>vs</h5>
                                            </div>

                                            <div class="col-md-4 teamName mobilePullRight">
                                                <h5 class="text-right">KOL</h5>
                                            </div>

                                        </div>

                                        <p class="location">Shikh Zayed stadium, abu dhabi, uae</p>

                                    </div>

                                </div>

                            </div>

                            <div class="col-md-4 upcomingMatch">

                                <div class="col-md-12 upcomingMatchHolder finishedMatch">

                                    <div class="col-md-12 matchResultContainer">

                                        <div class="col-md-4 teamSec">
                                            <h5 class="teamNames">KOL</h5>
                                            <h5 class="teamScore">164/5</h5>
                                            <p>20.0 Overs</p>
                                        </div>

                                        <div class="col-md-4 resultLabel">
                                            <label for="" >RESULT</label>    
                                        </div>

                                        <div class="col-md-4 teamSec">
                                            <h5 class="teamNames">KOL</h5>
                                            <h5 class="teamScore">164/5</h5>
                                            <p>20.0 Overs</p>
                                        </div>

                                    </div>

                                    <p class="winningDetails"> Punjab beat delhi by 5 wickets </p>

                                </div>

                            </div>

                        </div>

                    </div>

                </section>

                <div class="clearfix"></div>

                <section class="indiaNewsMidSection">

                    <div class="row">

                        <div class="col-md-12">

                            <div class="col-md-3 advSec">
                                <img src="<?php echo S3_URL?>site/images/cities/left-ad.png" alt="">
                            </div>

                            <div class="col-md-9 paddingZero">

                                <div class="col-md-12 paddingZero">

                                    <div class="col-md-4 paddingZeroLeft mobileTopMargin">

                                        <div class="newsFourImageSec">
                                            <img src="<?php echo S3_URL?>site/images/cities/shaktimaan.png" alt="">
                                        </div>
            
                                        <div class="newsFourContentSec linearBackground">

                                            <div class="sponsredContent">
                                                <span class="sponsredBadge"> Sponsored </span>
                                            </div>

                                            <h5 class="semiBold">
                                                Shaktimaan Sequel On The Cards? Here's What Mukesh Khanna Said
                                            </h5>
                                        </div>
            
                                    </div>

                                    <div class="col-md-4 paddingZeroLeft mobileTopMargin">

                                        <div class="newsFourImageSec">
                                            <img src="<?php echo S3_URL?>site/images/cities/intel.png" alt="">
                                        </div>
            
                                        <div class="newsFourContentSec linearBackground">

                                            <div class="sponsredContent">
                                                <span class="sponsredBadge"> Sponsored </span>
                                            </div>

                                            <h5 class="semiBold">
                                                Build for the Data-Centric Era
                                            </h5>

                                        </div>
            
                                    </div>

                                    <div class="col-md-4 paddingZeroLeft mobileTopMargin">

                                        <div class="newsFourImageSec">
                                            <img src="<?php echo S3_URL?>site/images/cities/family.png" alt="">
                                        </div>
            
                                        <div class="newsFourContentSec linearBackground">

                                            <div class="sponsredContent">
                                                <span class="sponsredBadge"> Sponsored </span>
                                            </div>

                                            <h5 class="semiBold">
                                                Enter Phone Number &amp; Age To Check Eligibility up to Rs.1 Crore Life Insurance Plan.
                                            </h5>
                                            
                                        </div>
            
                                    </div>

                                </div>

                                <div class="col-md-12 paddingZero">
                                    
                                    <div class="col-md-6 paddingZeroLeft marginTop15">

                                        <div class="indiaMidNewsBottomImageSec">
                                            <img src="<?php echo S3_URL?>site/images/cities/baba.png" alt="">
                                        </div>
            
                                        <div class="indiaMidNewsBottomContentSec linearBackground">

                                            <div class="sponsredContent">
                                                <span class="sponsredBadge"> Sponsored </span>
                                            </div>

                                            <h5 class="semiBold">
                                                Best Places in India where you can Settle Down after Retirement
                                            </h5>
                                            
                                        </div>
            
                                    </div>

                                    <div class="col-md-6 paddingZeroLeft marginTop15">

                                        <div class="indiaMidNewsBottomImageSec">
                                            <img src="<?php echo S3_URL?>site/images/cities/appartment.png" alt="">
                                        </div>
            
                                        <div class="indiaMidNewsBottomContentSec linearBackground">

                                            <div class="sponsredContent">
                                                <span class="sponsredBadge"> Sponsored </span>
                                            </div>

                                            <h5 class="semiBold">
                                                The Cost of Apartments for Sale in Dubai Might Totally Surprise You
                                            </h5>
                                            
                                        </div>
            
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </section>

            </div>

        </section>

    </div>

    <div class="col-md-3 paddingZero">

        <div class="partnerAdSec advHeaderMargin">
        
            <div class="col-md-12">

                <div class="col-md-12 paddingZero">

                    <div class="smallNewsSecondImageLeftSection">
                        <a href="https://www.newstalkie.com/category/story/35/war-of-the-giants">
                            <img src="https://www.newstalkie.com/cdn/site/images/posts/small_postimage_crop/thumb-6.jpg" alt="">
                        </a>
                    </div>

                    <div class="smallNewsSecondContentSection marginTop10">
                        <h5 class="semiBold headingH5">War of the Giants..</h5>
                    </div>

                </div>

                <div class="col-md-12 quickLinks">

                    <div class="smallNewsSecondContentSection marginTop10">
                        <h5 class="semiBold headingH5"> Quick Links </h5>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Unlock 5 Guidelines</a>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Deals of the Day</a>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Bihar Elections</a>
                    </div>

                    <div class="quickLinksSec">
                        <a href="">Coronavirus Live</a>
                    </div>
                
                </div>
            
            </div>

        </div>

    </div>

</div>                          
                
                               