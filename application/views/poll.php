
<div class="container-fluid">

<div class="col-md-9 paddingZero">

    <section class="indiaTopSecNews headerMargin">

        <div class="row">
                                    
            <div class="col-md-12 indiaTopNewsItemHolder paddingZero">

                <h3 class="boldFont font18"> Polls </h3>

                <div class="col-md-4 pollItemHolder">

                    <div class="col-md-12 pollItem">
                    
                        <div class="imgSec">
                        
                            <img src="<?php echo S3_URL?>site/images/polls/poll-one.png" alt="" class="img-responsive">

                        </div>

                        <div class="pollContent">
                        
                            <p class="pollheading">
                                <b>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy</b>
                            </p>

                            <p class="greyFont pollPara">
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, 
                                sed diam nonummy nibh euismod tincidunt ut laoreet dolore 
                                magna aliquam adipiscing elit.
                            </p>

                        </div>

                        <div class="pollVoteSecContainer text-center">

                            <span class="pollVoteHeader pollVoteTopHeader">Newstalkie Poll</span>

                            <div class="pollVoteSec">

                                <p>Lorem ipsum dolor sit amet ?</p>

                                <div class="voteButton">
                                    <button class="btn btn-primary voteButton yesBtn">Yes</button>
                                    <button class="btn btn-default voteButton noBtn">No</button>
                                </div>
                                
                            </div>

                            <div class="col-md-12 paddingZero pollGraphSec" style="display: none;">


                                <div class="col-md-12 paddingZero">

                                    <p class="text-center pollGraphHeading">Lorem ipsum dolor sit amet ?</p>

                                    <div class="col-md-6 paddingZero paddingbottom15">
                                        <div id="chart"></div>
                                        <span class="voteForName">Yes</span>
                                    </div>

                                    <div class="col-md-6 paddingZero paddingbottom15">
                                        <div id="chartTwo"></div>
                                        <span class="voteForName">No</span>
                                    </div>

                                    <br>

                                </div>

                                
                                
                            </div>

                        </div>

                        

                    </div>
                
                </div>

                <div class="col-md-4 pollItemHolder">
                
                </div>

                <div class="col-md-4 pollItemHolder">
                
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="midAdvSec">
                <img src="https://www.newstalkie.com/cdn/site/images/dummy-ads/mid_adv.png" alt="" class="img-responsive">
            </div>

        </div>

    </section>

</div>

<div class="col-md-3 paddingZero">

    <div class="partnerAdSec advHeaderMargin">
    
        <div class="col-md-12 paddingZero">

            <div class="col-md-12 paddingZero">

                <div class="smallNewsSecondImageLeftSection">
                    <a href="https://www.newstalkie.com/category/story/35/war-of-the-giants">
                        <img src="https://www.newstalkie.com/cdn/site/images/posts/small_postimage_crop/thumb-6.jpg" alt="">
                    </a>
                    
                </div>

                <div class="smallNewsSecondContentSection marginTop10">
                    <h5 class="semiBold headingH5">War of the Giants..</h5>
                </div>

            </div>

            <div class="col-md-12 quickLinks">

                <div class="smallNewsSecondContentSection marginTop10">
                    <h5 class="semiBold headingH5"> Quick Links </h5>
                </div>

                <div class="quickLinksSec">
                    <a href="">Unlock 5 Guidelines</a>
                </div>

                <div class="quickLinksSec">
                    <a href="">Deals of the Day</a>
                </div>

                <div class="quickLinksSec">
                    <a href="">Bihar Elections</a>
                </div>

                <div class="quickLinksSec">
                    <a href="">Coronavirus Live</a>
                </div>
            
            </div>
        
        </div>

    </div>

</div>

</div>                          
            
<script src="<?php echo S3_URL?>site/scripts/apexcharts.js"></script>

<script>

</script>

