<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Calcutta');
/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


// for local

if($_SERVER['SERVER_NAME'] == "www.newstalkie.com" || $_SERVER['SERVER_NAME'] == "newstalkie.com"){

   define("SITE_NAME", "newstalkie");   
}else{
   define("SITE_NAME", "Locallaunde");
}




define("ALLOWED_IP", "localhost, 10.0.0.1, 10.0.0.2, 10.0.0.13, 10.0.0.4, 10.0.0.5, 10.0.0.6, 10.0.0.7, 10.0.0.8, 10.0.0.19, 172.16.0.250, 10.0.0.24, panelss.in, 10.0.0.23, 10.0.0.33, 10.0.0.250, 192.168.1.13, 192.168.0.100, ::1");

$arr_ip_range = explode(", ", ALLOWED_IP);

//if($_SERVER['HTTP_HOST'] == "10.0.0.33"){
if(in_array($_SERVER['HTTP_HOST'], $arr_ip_range) ||  $_SERVER['HTTP_HOST'] == "panelss.in"){
	
   define("BASE_URL",          "http://".$_SERVER['HTTP_HOST'] );	
   define("SITE_URL",          "http://".$_SERVER['HTTP_HOST'] . "/newstalkie/");	
   define("FULL_SITE_URL",     "http://".$_SERVER['HTTP_HOST'] . "/newstalkie/index.php");
   define("S3_URL",            SITE_URL."cdn/");
}else{
	
   $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
   
   define("BASE_URL",          $protocol.$_SERVER['HTTP_HOST'] );
   define("SITE_URL",          $protocol.$_SERVER['HTTP_HOST'] . "/");
   define("FULL_SITE_URL",     $protocol.$_SERVER['HTTP_HOST'] . "/index.php");
   define("S3_URL",            SITE_URL."cdn/");
   define("Beta_Path",         "beta/");

   //define("S3_URL", 'https://playeasygame.s3-ap-southeast-1.amazonaws.com/');	
}

define('IMAGE_UPLOAD_MAX_SIZE', 2*1024*1024);
define("ADMIN_IMAGE_FOLDER", "cdn/cms/images/admin_img/");

define('R_URL', $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);

define("POSTS_FOLDER", "cdn/site/images/posts/");
define("CROP_POSTS_FOLDER", "cdn/site/images/posts/postimage_crop/");
define("LARGE_POSTS_FOLDER", "cdn/site/images/posts/large_postimage_crop/");
define("MEDIUM_POSTS_FOLDER", "cdn/site/images/posts/medium_postimage_crop/");
define("SMALL_POSTS_FOLDER", "cdn/site/images/posts/small_postimage_crop/");
define("INNERPOSTS_FOLDER", "cdn/site/images/innerposts/");
define("CROP_INNERPOSTS_FOLDER", "cdn/site/images/innerposts/innerpostimage_crop/");
define("LARGE_INNERPOSTS_FOLDER", "cdn/site/images/innerposts/large_innerpostimage_crop/");
define("MEDIUM_INNERPOSTS_FOLDER", "cdn/site/images/innerposts/medium_innerpostimage_crop/");
define("SMALL_INNERPOSTS_FOLDER", "cdn/site/images/innerposts/small_innerpostimage_crop/");
define("MEMES_FOLDER", "cdn/site/images/memes/");
define("CROP_MEMES_FOLDER", "cdn/site/images/memes/memes_crop/");
define("CATEGORYBANNER_FOLDER", "cdn/site/images/category_banner/");
define("MOVIE_FOLDER", "cdn/site/images/movies/");
define("CROP_MOVIE_FOLDER", "cdn/site/images/movies/crop_movies_images/");
define("LARGE_MOVIE_FOLDER", "cdn/site/images/movies/large_movies_images/");
define("MEDIUM_MOVIE_FOLDER", "cdn/site/images/movies/medium_movies_images/");
define("SMALL_MOVIE_FOLDER", "cdn/site/images/movies/small_movies_images/");




define("CMS_FOLDER", "cms/");
define("FULL_CMS_URL", FULL_SITE_URL."/cms");
define("FROM_EMAIL_ID", 'locallaunde');
define("SUPPORT_EMAIL", "info@locallaunde.com");
define("THUMB_PREFIX", "thumb-");
define("SITE_JS", "cdn/site/js/");





//TABLES

define('ADMIN_USERS',         'tbl_admin_user');
define('ADMIN_ROLE',          'tbl_admin_role');
define('CATEGORY',            'tbl_category');
define('POSTS',               'tbl_post');
define('INNERPOSTS',          'tbl_inner_post');
define('CONTENTS',            'tbl_contentwriter');
define('SUBSCRIBE',           'tbl_subscribers');
define('TAGS',                'tbl_tags');
define('MEMES',               'tbl_memes');
define('STATICPAGES',         'tbl_static_page');
define('REVIEW',              'tbl_postreview');
define('TRACKER',             'tbl_utm_tracker');
define('MOVIE',               'tbl_movies');

//MEMCACHE KEY

define('MEMCACHE_KEY', 'Abcf755sfrd1dfddsa5');


define('IS_MOBILE', (bool)preg_match('#\b(ip(hone|od|ad)|android|opera m(ob|in)i|windows (phone|ce)|blackberry|tablet'.
					'|s(ymbian|eries60|amsung)|p(laybook|alm|rofile/midp|laystation portable)|nokia|fennec|htc[\-_]'.
					'|mobile|up\.browser|[1-4][0-9]{2}x[1-4][0-9]{2})\b#i', $_SERVER['HTTP_USER_AGENT'] ));


//MAILKOOT API CREDENTIALS
define('GAS_BASE_URL', 'https://mlkts.com/ga/api');
define('GAS_API_KEY',  '8:fc286971d2ab9b0387e712ce018ea9b3eb2e809c');


//MEMCACHE VARIABLE
define('USER_CACHE',   "userdsdsvcgbsdgfddfsdfsd");
define('USER_OLD_CACHE', "usdedrdfdgdfgdfdddddesed");
define('SUB_CACHE',    "subcacharraydata14");	
define('SUB_OLD_CACHE',    "subcacsdfharraydata14");
define('SUB_CACHE_NUM',"subskdfgdfgdggdfgwes5gfh14");
define('SUB_OLD_CACHE_NUM',    "subcacsdfharfghfga14");	
define('SUB_CACHE_HOR',"ewiretherkthretkrhetjrketh");
define('SUB_OLD_CACHE_HOR',    "subcacsgfhfhfgdata14");	
define('META_CACHE',   "metsaddat7sdf866sc78678");
define('CITY_CACHE',   "city99sdfwodman99thgjg99");	
define('NAMEDOB_CACHE',   "gdsgdgdfgofgdfgdfirt0rtertioetr");	

//define('ADMIN_DEFAULT_CONTROLLER', 'dashboard');
define('ADMIN_LOGOUT_CONTROLLER', 'logout_user/logout');
define('ADMIN_DEFAULT_CONTROLLER', 'changepassword');
/* End of file constants.php */
/* Location: ./application/config/constants.php */
