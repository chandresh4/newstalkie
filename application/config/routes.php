<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/


$route['default_controller'] = "home";
$route['games']             = "category/search/sports";
//$route['sports']             = "category/search/sports";
$route['politics']           = "category/search/politics";
$route['science-technology'] = "category/search/science-technology";
//$route['lifestyle']          = "category/search/lifestyle";
$route['entertainment']      = "category/search/entertainment";
$route['history-culture']    = "category/search/history-culture";
$route['health-fitness']     = "category/search/health-fitness";
$route['inspiration']        = "category/search/inspiration";
$route['trending-stories']   = "category/search/Trendy";
$route['viral-stories']      = "category/search/viral";
$route['travel-adventure']   = "category/search/travel-adventure";
$route['food']               = "category/search/food";
$route['environment']        = "category/search/environment";
$route['celebrity']          = "category/search/celebrity";
$route['corona-virus']	     = "tag/tagsearch/corona-virus";
$route['covid19']            = "tag/tagsearch/corona-virus";
$route['submit-content']	 = "submit_content";
$route['about-us']	         = "about_us";
$route['contact-us']	     = "contact_us";
$route['info/(:any)']	     = "/info/index/$1";
$route['404_override']       								 = 'page_notfound';
$route['animals']            = "category/search/animals";
$route['news']       		 = "category/search/news";
$route['world-news']         = "category/search/world-news";
$route['education']          = "category/search/education";
$route['tech-updates']       = "category/search/science-technology";
$route['viral-news']         = "category/search/viral";
$route['404_override']       = 'page_notfound/index';








$route['default_controller'] = "home";
$route['^(en-in|en-us|en-gb|en-uk|en-_)/games']             = "category/search/sports";
$route['^(en-in|en-us|en-gb|en-uk|en-_)/sports']             = "category/search/sports";
$route['^(en-in|en-us|en-gb|en-uk|en-_)/politics']           = "category/search/politics";
$route['^(en-in|en-us|en-gb|en-uk|en-_)/science-technology'] = "category/search/science-technology";
$route['^(en-in|en-us|en-gb|en-uk|en-_)/lifestyle']          = "category/search/lifestyle";
$route['^(en-in|en-us|en-gb|en-uk|en-_)/entertainment']      = "category/search/entertainment";
$route['^(en-in|en-us|en-gb|en-uk|en-_)/history-culture']    = "category/search/history-culture";
$route['^(en-in|en-us|en-gb|en-uk|en-_)/health-fitness']     = "category/search/health-fitness";
$route['^(en-in|en-us|en-gb|en-uk|en-_)/inspiration']        = "category/search/inspiration";
$route['^(en-in|en-us|en-gb|en-uk|en-_)/trending-stories']   = "category/search/Trendy";
$route['^(en-in|en-us|en-gb|en-uk|en-_)/viral-stories']      = "category/search/viral";
$route['^(en-in|en-us|en-gb|en-uk|en-_)/travel-adventure']   = "category/search/travel-adventure";
$route['^(en-in|en-us|en-gb|en-uk|en-_)/food']               = "category/search/food";
$route['^(en-in|en-us|en-gb|en-uk|en-_)/environment']        = "category/search/environment";
$route['^(en-in|en-us|en-gb|en-uk|en-_)/celebrity']          = "category/search/celebrity";
$route['^(en-in|en-us|en-gb|en-uk|en-_)/corona-virus']	     = "tag/tagsearch/corona-virus";
$route['^(en-in|en-us|en-gb|en-uk|en-_)/covid19']            = "tag/tagsearch/corona-virus";
$route['submit-content']	 = "submit_content";
$route['^(en-in|en-us|en-gb|en-uk|en-_)/about-us']	         = "about_us";
$route['^(en-in|en-us|en-gb|en-uk|en-_)/contact-us']	     = "contact_us";
$route['^(en-in|en-us|en-gb|en-uk|en-_)/info/(:any)']	     = "/info/index/$1";
$route['404_override']       								 = 'page_notfound';
$route['^(en-in|en-us|en-gb|en-uk|en-_)/animals']            = "category/search/animals";
$route['^(en-in|en-us|en-gb|en-uk|en-_)/news']       		 = "category/search/news";
$route['^(en-in|en-us|en-gb|en-uk|en-_)/world-news']         = "category/search/world-news";
$route['^(en-in|en-us|en-gb|en-uk|en-_)/education']          = "category/search/education";
$route['^(en-in|en-us|en-gb|en-uk|en-_)/tech-updates']       = "category/search/science-technology";
$route['^(en-in|en-us|en-gb|en-uk|en-_)/viral-news']         = "category/search/viral";
$route['^(en-in|en-us|en-gb|en-uk|en-_)/404_override']       = 'page_notfound/index';


$route['^(en-in|en-us|en-gb|en-uk|en-_)$'] = "home/index";
$route['^(en-in|en-us|en-gb|en-uk|en-_)$'] = $route['default_controller'];


/* End of file routes.php */
/* Location: ./application/config/routes.php */