<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class tag extends CI_Controller
{
    public $country = "";
	public function __construct(){
	   
		parent::__construct();
		$this->load->helper("general");
		$this->load->model("register_model");
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('utm_source'), $this->input->get('affid'), $this->input->get('utm_medium'), $this->input->get('utm_sub'));
		
		//$this->country = isset($_COOKIE["countrytest"]) != "" ? $_COOKIE["countrytest"] : 'in';

		$uri = current_url(true);

		$this->country = check_uri_for_country($uri);
	}
   
	public function index(){
	
		$query = $this->db->query("select t_id,tag_name,seourl from tbl_tags where status=1");
		if($query->num_rows() > 0){
			foreach($query->result() as $p){
			  $data['result'][]=$p;
			}
		}else{
			$data['result'][]="";
		}

	    $this->load->view("header");
		//$this->load->view("popup",$data1);
		$this->load->view("tags",$data);
		$this->load->view("footer"); 
	}
	 
	public function tagsearch($id){

		$data['tag_posts'] = array();
		$data['category_wise_ads'] = "";

		if($id !=""){

			$id= $id."-";
			 
			$query = $this->db->query("select t_id,tag_name,seourl from tbl_tags where seourl = '".$id."' and status=1");
			
			if($query->num_rows() > 0){
			
			    $p = $query->result();	
			  	
				$data['tag_res']  = $p[0];
				$data['tag_name'] = $p[0]->tag_name;
			    $tid = $p[0]->t_id;
					
				$query = $this->db->query("SELECT id,category,post_title,seourl,post_image FROM tbl_post WHERE country = '".$this->country."' and FIND_IN_SET('".$tid."',tag) and status=1 order by id desc");
					if($query->num_rows() > 0){
					    
					    foreach($query->result() as $p){
						   $data['tag_posts'][] = $p;
						   $category =$p->category;
					    }
					
					}
			}	
		
		}else{
		  $data['tag_res'] = "";
		  $data['tag_posts'] = "";
		}


		$this->load->view("header");
		//$this->load->view("popup",$data1);
		$this->load->view("tags-result",$data);
		$this->load->view("footer"); 
	}	
}
?>