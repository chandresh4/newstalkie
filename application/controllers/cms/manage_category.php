<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class manage_category extends CI_Controller
{
   public function __construct()
   {
        parent::__construct();
		global $page_details;
		$this->load->model(array( CMS_FOLDER.'common_model', CMS_FOLDER.'/db_function'));
		$this->db->cache_off();
		$this->tablename	= ADMIN_USERS;	
		$this->page_details['cur_controller'] = strtolower(__CLASS__);
		$this->page_details['menu']           = $this->common_model->Menu_Array();
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);  
		
		
   }

   public function index($perpage = 30, $offset = 0){
		
		$perpage = 30;
		
		// CALL PAGINATION FUNCTION
		$this->page($perpage, NULL, NULL, NULL, 0);
   }		
   
     public function page($perpage = 30, $btn_search = NULL, $from_date = NULL, $to_date = NULL, $offset = 0){
       // DECLARE NULL VARIABLES 
	    $tablename             =  CATEGORY;
		$data['page_title']    = "Category details ";
		$data['add_title']     = "Add Category";
		$query_string = NULL;
		$where = $whr = NULL;
		
		if($from_date == NULL && $to_date == NULL && $btn_search > 0) {
			$offset = $btn_search;
			$btn_search = NULL;
		}
		
		$data['from_date'] 	= "";
		$data['to_date'] 	= "";
		$data['status']     = "";
		
	   $cur_controller 	= strtolower(__CLASS__);
	   $query_string = NULL;
	   $full_path = FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
	   
	    if( (trim($this->input->post("btn_search")) != "" || $btn_search != "") || 
			((trim($this->input->post("from_date")) != "" || $from_date != "") && 
			(trim($this->input->post("to_date")) != "" || $to_date != ""))) {

			// STORE SEARCH VALUE IN DATA ARRAY
			$data['btn_search']		= ($btn_search != "") 	? $btn_search 	: $this->input->post("btn_search");
			$data['to_date']     	= ($to_date != "")      ? $to_date      : $this->input->post("to_date");
			$data['from_date']		= ($from_date != "") 	? $from_date 	: $this->input->post("from_date");
			 
			if($data['from_date'] !=	"" &&  $data['to_date']!= ""  && $data['from_date'] != '0'){
			    
				$query_string = $perpage."/".$data['btn_search']."/".$data['from_date']."/".$data['to_date'];
				$whr = "DATE_FORMAT(datecreated,'%Y-%m-%d') BETWEEN '".$data['from_date']."' AND '".$data['to_date']."'";
			      
			}
			// CHECK FOR EXISTING CLAUSE
			if($where == "") {
				$where = " where ".$whr;
			} else {
				$where .= " and ".$whr;
			}
		} else {
			$data['btn_search'] = "GO";
			$data['from_date']  = date('Y-m-d');
			$data['to_date']    = date('Y-m-d');
			
			$query_string = $perpage."/".$data['btn_search']."/".$data['from_date']."/".$data['to_date'];
			$where = " WHERE DATE_FORMAT(datecreated,'%Y-%m-%d') BETWEEN '".$data['from_date']."' AND '".$data['to_date']."'";
			      
		}
	   
	   
	    $order_by = " order by datecreated desc";
	   
		$sel_query="SELECT cat_id, name,seourl,status, datecreated, dateupdated FROM ".$tablename.$order_by;

	   if($this->uri->segment(8) == "") {
			$config['uri_segment'] 	=5;
		} else {
			$config['uri_segment'] 	= 8;
		}		
	   // MODIFY FULL PATH
		$full_path .= $query_string;		
		  $config['total_rows'] 		= $this->db_function->count_record($sel_query, false);
		  $config['per_page'] 			= $perpage;
		  $config['base_url'] 			= $full_path;
		  $choice 						= $config['total_rows'] / $config["per_page"];
		  $config['num_links'] 			= 2;
		  $config['full_tag_open'] 		= '<div id="paging" style="float:right; "><ul style="clear:left;">';
		  $config['full_tag_close'] 	= '</ul></div>';
		  $config['anchor_class'] 		= 'class="btn" ';
		  $config['first_tag_open'] 	= '<li class="num_off">';
		  $config['first_tag_close'] 	= '</li>';
		  $config['last_tag_open'] 		= '<li class="num_off">';
		  $config['last_tag_close'] 	= '</li>';
		  $config['cur_tag_open'] 		= '<li class="num_on">';
		  $config['cur_tag_close'] 		= '</li>';
		  $config['num_tag_open'] 		= '<li class="num_off">';
		  $config['num_tag_close'] 		= '</li>';
		  $config['prev_tag_open'] 		= '<li class="num_off">';
		  $config['prev_tag_close'] 	= '</li>';
		  $config['next_tag_open'] 		= '<li class="num_off">';
		  $config['next_tag_close'] 	= '</li>';
		  $config['prev_link'] 			= 'PREVIOUS';
		  $config['next_link'] 			= 'NEXT';
		  $config['use_page_numbers'] 	= FALSE;
		// PAGINATION PARAMETER VALUES END 				
		
		// PAGINATION PARAMETER INITIALIZE 
		$this->pagination->initialize($config);
		
		// SQL QUERY WITH OFFSET AND PERPAGE LIMIT
		$sql = $sel_query." limit ".$offset.", ".$perpage;
	
        $data["details"] = $this->db_function->get_data($sql);
		
		// GET THE DATA FROM PAGINATION
		$data["today_count"] = $config['total_rows'];
		
       if($this->uri->segment(8)){
			  $pages=$this->uri->segment(8);
		  } elseif($this->uri->segment(5)) {
			  $pages=$this->uri->segment(5);
		  } else{
			  $pages=0;
		  }
		  $sl_no = $pages +1;
		  $data['sl_no']=$sl_no;
		  $data["links"] = $this->pagination->create_links();
		
	    $this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $this->page_details);
	    $this->load->view(CMS_FOLDER."manage-category" ,  $data);
		$this->load->view(CMS_FOLDER.'footer');  

   }   
   
   public function add(){
	   
		
		$this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $this->page_details);
		$this->load->view(CMS_FOLDER."add-category");
		$this->load->view(CMS_FOLDER.'footer'); 
    	
   }
   
   public function add_action(){
	 
	 $names = "";
     $this->form_validation->set_rules('name', 'Category', 'required');
	 $this->form_validation->set_rules('meta_title', 'meta title', 'required');
	 $this->form_validation->set_rules('meta_description', 'meta description', 'required');
	 $this->form_validation->set_rules('meta_keyword', 'meta keyword', 'required');
     $this->form_validation->set_rules('status', 'Status', 'required');
     if($this->form_validation->run() == FALSE)
     {
		$this->session->set_flashdata('error', "Fields marked * are mandatory"); 
		redirect(FULL_CMS_URL."/manage_category/add"); 
     }else{
		   $name         = ($this->input->post('name'));
		if (preg_match('/&/', $name)){
		   $names = explode("&",$name); 
		   $seourl  = strtolower($names[0])."-".strtolower($names[1]);
		}elseif (preg_match("/\\s/", $myString)) {
		   $names = explode("",$name); 
		   $seourl  = strtolower($names[0])."-".strtolower($names[1]);
		}else{
			$seourl      = strtolower($name);
		}
		$date            = date('Y-m-d H:i:s');
		$meta_title      = ($this->input->post('meta_title'));
		$meta_description= ($this->input->post('meta_description'));
		$meta_keyword    = ($this->input->post('meta_keyword'));
		$status          = $this->input->post('status');
		if($name !=""){
           $query=$this->db->query("SELECT
								name  
							FROM
								tbl_category
							WHERE 
								name = '".$name."'");

		   if ( $query->num_rows() > 0) {
				$this->session->set_flashdata('error', 'This Category Name Is Already Created');
				redirect(SITE_URL.'cms/manage_category/add');
		   }else{
				$post_img = $_FILES['post_image']['name'];
				
				$res = $this->db->query("insert into tbl_category(name,seourl,image,meta_title,meta_keywords,meta_description,status,datecreated) values('".$name."','".$seourl."','".$post_img."','".$meta_title."','".$meta_keyword."','".$meta_description."',".$status.",'".$date."')");
				$pathAndName = CATEGORYBANNER_FOLDER.$post_img;
				move_uploaded_file($_FILES['post_image']['tmp_name'],$pathAndName);
				if($res!="")
				{
				 $this->session->set_flashdata('success', 'New Category Is Added');
				 redirect(SITE_URL.'cms/manage_category/add');
				}
		   } 
		}
     }
   }
	public function edit(){
	   
	     $id  = $_GET['id'];
		 
		 $query=$this->db->query("SELECT
										cat_id,name,image,meta_title,meta_description,meta_keywords,status 
									FROM
									 	tbl_category
									where cat_id=".$id."");
		   
		   if ( $query->num_rows() > 0) {
			  foreach($query->result() as $res){
				  $data['result'][]=$res;
			  }
		   }
		   //print_r($data);exit;
		$this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $this->page_details);
		$this->load->view(CMS_FOLDER."edit-category",$data);
		$this->load->view(CMS_FOLDER.'footer'); 
    	
   }
   
   public function edit_action(){
     $id     = $this->input->post('id');
	 $name   = $this->input->post('name');
     if(preg_match('/&/', $name)){
		   $names = explode("&",$name); 
		   $seourl  = strtolower($names[0])."-".strtolower($names[1]);
	}elseif (preg_match("/\\s/", $name)) {
		   $names = explode(" ",$name); 
		   $seourl  = strtolower($names[0])."-".strtolower($names[1]);
    }else{
			$seourl      = strtolower($name);
	}
	 
     $date   = date('Y-m-d H:i:s');
	 $meta_title      = ($this->input->post('meta_title'));
	 $meta_description= ($this->input->post('meta_description'));
	 $meta_keyword    = ($this->input->post('meta_keyword'));
	 $status = $this->input->post('status');
	 if($id){
		$this->db->where("cat_id",$_POST['id']);
		$Arr=array();
		$Arr['name'] = $name;
		$Arr['seourl'] = $seourl;
		$Arr['meta_title'] = $meta_title;
		$Arr['meta_description'] = $meta_description;
		$Arr['meta_keywords'] = $meta_keyword;
		$Arr['status'] = $status;
		$Arr['dateupdated'] = $date;
		if( $_FILES['post_image']['name']){
		  $post_img = $_FILES['post_image']['name'];
		  $Arr['image']   = $post_img;	
          $pathAndName = CATEGORYBANNER_FOLDER.$post_img;
		  move_uploaded_file($_FILES['post_image']['tmp_name'],$pathAndName);
		}		  
		  $this->db->update(CATEGORY,$Arr);			   
		  $this->session->set_flashdata('success', 'Succesfully updated', 'Succesfully updated');
		  redirect(FULL_CMS_URL."/manage_category/"); 
	 }else{   
	       $this->session->set_flashdata('error', 'Error Ocurred');
		   redirect(FULL_CMS_URL."/manage_category/");
	 }
  }
      
}

?>