<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class manage_admin extends CI_Controller
{
   public function __construct()
   {
        parent::__construct();
		global $page_details;
		$this->load->model(array( CMS_FOLDER.'common_model', CMS_FOLDER.'/db_function'));
		$this->db->cache_off();
		$this->tablename	= ADMIN_USERS;	
		$this->page_details['cur_controller'] = strtolower(__CLASS__);
		$this->page_details['menu']           = $this->common_model->Menu_Array();
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);  
		
		
   }

   public function index($perpage = 10, $offset = 0){
		
		$perpage = 1000;
		
		// CALL PAGINATION FUNCTION
		$this->page($perpage, NULL, NULL, NULL, NULL, 0);
   }		
   
    public function page($perpage = 1000, $btn_search = NULL, $search_field = NULL, $search_txt = NULL, $status=NULL, $offset = 0){
       
	   // DECLARE NULL VARIABLES 
		$cur_controller 		= strtolower(__CLASS__);
		$query_string       	= NULL;
		$full_path 				= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';	   
	    $query_string 			= NULL;
		$where = $whr 			= NULL;
		$tablename				= ADMIN_USERS;
		$data['status']         = "";
		$data['page_title']	 	= "Admin Users list";
		$data['add_page_title']	= "Add Admin";
		$data['add_edit_page'] 	= $cur_controller."/add_edit_form";
		$data['tablename']	 	= $tablename;
		$data['primary_field']	= "id";

	   //ROLE DATA
		$role_query =$this->db->query("SELECT id, role_name FROM ".ADMIN_ROLE." WHERE status = 1");
		
		if ( $role_query->num_rows() > 0) {
			 foreach ($role_query->result() as $p) {
			   $data['role_data'][$p->id] = $p->role_name;
			 }
		}else{
			 $data['role_data'][] = NULL;
		}
		 
	    $order_by = " order by date_created desc";
	   
		$sel_query="SELECT id, adm_role_id, username, email, passwd, status, date_created, date_updated FROM ".$tablename.$order_by;
		
	    if($this->uri->segment(9) == "") {
			$config['uri_segment'] 	= 5;
		} else {
			$config['uri_segment'] 	= 9;
		}
		
	   
		$sql = $sel_query." limit ".$offset.", ".$perpage;
	
        $data["details"] = $this->db_function->get_data($sql);
		
		$data["today_count"] = $this->db_function->count_record($sel_query, false);
		
	    $this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $this->page_details);
	    $this->load->view(CMS_FOLDER."manage-admin" ,  $data);
		$this->load->view(CMS_FOLDER.'footer'); 

   }   
   
   public function add_edit_form($mode = "add", $id = NULL){
	   
		// DETAILS WE WANT TO SEND IN VIEW
		$data['page_name']	 	 	= "Admin Users list";
		$data['manage_page_title']	= "Manage ".$data['page_name'];
		$data['page_title']	 		= ucfirst($mode)." ".$data['page_name'];
		$data['manage_page'] 		= $this->page_details['cur_controller'];
		$data['add_page'] 			= $this->page_details['cur_controller']."/add_edit_form";
		$data['tablename']	 		= $this->tablename;
		$data['primary_field']		= "id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$this->page_details['cur_controller']."/add_edit_action";
		$data['mode'] 				= ($mode == "add") ? ucfirst($mode) : 'Edit';
		
		// INITIALIZE ARRAY VARIABLES
		$data['result_data'] = NULL;
		$data['result_data'][$data['primary_field']]	    = NULL;
		$data['result_data']['adm_role_id'] 				= NULL;
		$data['result_data']['username']					= NULL;
		$data['result_data']['email']				    	= NULL;
		$data['result_data']['status']					    = NULL;
		$data['result_data']['date_created']				= NULL;
		$data['result_data']['date_updated']				= NULL;
		
		$data['role'] = $this->db_function->get_data("SELECT id, role_name, role_details FROM ".ADMIN_ROLE." WHERE status =1");
		
		$data['get_teamleader'] = $this->db_function->get_data("SELECT a.id, a.username FROM ".$this->tablename." a LEFT JOIN ".ADMIN_ROLE." r On (a.adm_role_id= r.id)  WHERE a.status =1 and r.role_name = 'team leader'");
		
		if ($mode == 'edit' && is_numeric($id)) {
			$val = $this->db_function->get_single_row($this->tablename, '	id, 	adm_role_id, username,  email, 	passwd,	admin_image,
																			status, date_created, 	date_updated', 
																	        $data['primary_field'].'='.$id);
			$data['result_data'] = $val;	
		}
				
		$this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $this->page_details);
		$this->load->view(CMS_FOLDER."add-admin", $data);
		$this->load->view(CMS_FOLDER.'footer'); 
   }
   
   public function add_edit_action(){
		
		$this->form_validation->set_rules('role', 'role', 'trim|required|xss_clean');
	   	$this->form_validation->set_rules('username', 'username', 'required');
	    $this->form_validation->set_rules('email', 'email', 'required');
		
		if( ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
				$admin_id = (isset($_POST['id']) && is_numeric($_POST['id'])) ? $_POST['id'] : NULL;
				$_POST['mode'] = strtolower($_POST['mode']);
				if( trim ($_POST['mode']) == 'edit' && is_numeric ($_POST['id'])) { 		
					// SET THE WHERE CLAUSE
					$this->db->where("id",$_POST['id']);
					
					// SET THE DATA
					$data = array(
									'adm_role_id'       => $_POST['role'],
									'username'          => $_POST['username'],
									'passwd'            => md5(trim($_POST['password'])),
									'email'				=> $_POST['email'],
									'status'            => $_POST['status'],
									'date_updated'      => date('Y-m-d H:i:s')
								);
					
					// UPDATE QUERY
					 
					$this->db->update($this->tablename,$data);
				
					$this->session->set_flashdata('success', 'Updated successfully');
					
				} else if ( trim ($_POST['mode']) == 'add') { 			
					
					// SET THE DATA FOR INSERTION
					$data = array(
									'adm_role_id'       => $_POST['role'],									
									'username'          => $_POST['username'],
									'passwd'            => md5(trim($_POST['password'])),
									'email'				=> $_POST['email'],
									'status'            => $_POST['status'],
									'date_created'      => date('Y-m-d H:i:s')
								);
								
				    // INSERT QUERY
					$this->db->insert($this->tablename,$data);
					$admin_id = $this->db->insert_id();
					$this->session->set_flashdata('success', 'Added successfully');
				}
				
				
				if (isset ($_FILES['admin_image']['name'])) {
					
					$image_ext = allowed_extensions($_FILES['admin_image']['name']);
					if($image_ext != false) {
						if($_FILES['admin_image']['size'] > IMAGE_UPLOAD_MAX_SIZE) {
							// IF MORE THAN GENERATE FLASH MESSAGE
							$this->session->set_flashdata('error', 'File size must be less than or equals to '.
																	(IMAGE_UPLOAD_MAX_SIZE/1024/1024).' MB');
						} else {
							// CHECK IMAGE WE ARE TRYING TO UPLOAD THAT DIRECTORY IS AVAILABLE
							if (is_dir("../".ADMIN_IMAGE_FOLDER) == false) {
								
								// DELETE THE CURRENT IMAGE FILE FROM HDD
								$admin_image = $this->db_function->get_single_value($this->tablename, 'admin_image', "id=".$admin_id, false, false);
								
								// FINALLY UPLOAD THE FILE AFTER CHECKING OF EVERYTHING 
								$new_img_name = get_random_chracter('12','3').".".$image_ext;
								
								move_uploaded_file($_FILES['admin_image']['tmp_name'],"./".ADMIN_IMAGE_FOLDER.$new_img_name);
								
								// DELETE IMAGE FROM HDD
								unlink("./".ADMIN_IMAGE_FOLDER.$admin_image);
								
								$admin_image = NULL;
								$admin_image['admin_image'] = $new_img_name;
								
								// SET THE WHERE CLAUSE
								$this->db->where("id", $admin_id);
								$this->db->update($this->tablename, $admin_image);
							}
						}
					}
				}
				
				
				//PAGE REDIRECTION
				if( trim ($_POST['mode']) == 'edit' && is_numeric ($_POST['id'])) {
					redirect(CMS_FOLDER.$_POST['add_page']."/edit/".$_POST['id']);
				} else if ( trim ($_POST['mode']) == 'add') {
					redirect(CMS_FOLDER.$_POST['add_page']."/add/");
				}
				
			} else {
				
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect(CMS_FOLDER.$_POST['add_page']);
				
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong please try again');
			redirect(CMS_FOLDER.$_POST['add_page']);	
		}
	}

      
}

?>