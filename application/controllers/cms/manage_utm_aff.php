<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class manage_utm_aff extends CI_Controller
{
   public function __construct()
   {
        parent::__construct();
		$this->load->model(array(CMS_FOLDER.'common_model',CMS_FOLDER.'db_function'));
		$this->page_details['cur_controller'] = strtolower(__CLASS__);
		$this->page_details['menu']           = $this->common_model->Menu_Array();
		$this->db->cache_off();
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);  
   }

      public function index($perpage = 10, $offset = 0){
	   
	   $perpage = 10;
		
		// CALL PAGINATION FUNCTION
		$this->page($perpage, NULL, NULL, NULL, 0);
   }		
   
    public function page($perpage = 10, $btn_search = NULL, $search_field = NULL, $search_txt = NULL, $offset = 0){
       
	   $global_country =  ($this->session->userdata('cms_country') != "" ) ? $this->session->userdata('cms_country') : '';
	   // DECLARE NULL VARIABLES 
	    $tablename 	  = PUBLISHER;
		$query_string = NULL;
		$where = $whr = NULL;
		
		if($search_field == NULL && $search_txt == NULL && $btn_search > 0) {
			$offset = $btn_search;
			$btn_search = NULL;
		}
		
		$data['search_field'] 	= "";
		$data['search_txt'] 	= "";
		
	   $cur_controller 	= strtolower(__CLASS__);
	   $query_string = NULL;
	   $full_path = FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
	   
	   if( (trim($this->input->post("btn_search")) != "" || $btn_search != "") && 
			(trim($this->input->post("search_field")) != "" || $search_field != "") && 
			(trim($this->input->post("search_txt")) != "" || $search_txt != "")) {
			
			// STORE SEARCH VALUE IN DATA ARRAY
			$data['btn_search']		= ($btn_search != "") 	? $btn_search 	: $this->input->post("btn_search");
			$data['search_field'] 	    = ($search_field != "") ? $search_field : $this->input->post("search_field");
			$data['search_txt']		= ($search_txt != "") 	? $search_txt 	: $this->input->post("search_txt");
			
			// PREPARE QUERY STRING
			$query_string = $perpage."/".$data['btn_search']."/".$data['search_field']."/".$data['search_txt'];
			
			// CREATE THE WHERE CLAUSE
			$whr = $data['search_field']." like '%".$data['search_txt']."%'";
			
			// CHECK FOR EXISTING CLAUSE
			if($where == "") {
				$where = " where ".$whr;
			} else {
				$where .= " and ".$whr;
			}
		} else {
			$query_string = $perpage."/";
			$where = "";
		}
	   
	   
	    $order_by = " order by datecreated desc";
	   		
		$sel_query="SELECT id, location, pubs, pixel, handle_type, cpl, fire_type, goal, net_id, status FROM ".$tablename.$where.$order_by;
		
	   if($this->uri->segment(8) == "") {
			$config['uri_segment'] 	= 5;
		} else {
			$config['uri_segment'] 	= 8;
		}
		
	   // MODIFY FULL PATH
		$full_path .= $query_string;
		
		$config['total_rows'] 		= $this->db_function->count_record($sel_query, false);
		$config['per_page'] 		= $perpage;
		$config['base_url'] 		= $full_path;
		$choice 					= $config['total_rows'] / $config["per_page"];
		$config['num_links'] 		= 2;
		$config['full_tag_open'] 	= '<div id="paging" style="float:right; "><ul style="clear:left;">';
		$config['full_tag_close'] 	= '</ul></div>';
		$config['anchor_class'] 	= 'class="btn" ';
		$config['first_tag_open'] 	= '<li class="num_off">';
		$config['first_tag_close'] 	= '</li>';
		$config['last_tag_open'] 	= '<li class="num_off">';
		$config['last_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="num_on">';
		$config['cur_tag_close'] 	= '</li>';
		$config['num_tag_open'] 	= '<li class="num_off">';
    	$config['num_tag_close'] 	= '</li>';
		$config['prev_tag_open'] 	= '<li class="num_off">';
    	$config['prev_tag_close'] 	= '</li>';
		$config['next_tag_open'] 	= '<li class="num_off">';
    	$config['next_tag_close'] 	= '</li>';
		$config['prev_link'] 		= 'PREVIOUS';
    	$config['next_link'] 		= 'NEXT';
		$config['use_page_numbers'] = FALSE;
		// PAGINATION PARAMETER VALUES END 				
		
		// PAGINATION PARAMETER INITIALIZE 
		$this->pagination->initialize($config);
		
		// SQL QUERY WITH OFFSET AND PERPAGE LIMIT
		$sql = $sel_query." limit ".$offset.", ".$perpage;
	
        $data["details"] = $this->db_function->get_data($sql);
		
		// GET THE DATA FROM PAGINATION
		$data["today_count"] = $config['total_rows'];
		
        $data["links"] = $this->pagination->create_links();
		
	    $this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $this->page_details);
	    $this->load->view(CMS_FOLDER."manage-utm-aff" ,  $data);
		$this->load->view(CMS_FOLDER.'footer'); 
   

   }
   
   public function add_form(){
	   $this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $this->page_details);
	   
	   $data['net_data'] = $this->db->query("select id, net from ".NETWORK." where status = '1' order by net asc");
	   
	   $this->load->view(CMS_FOLDER."add-utm-aff" , $data);
	   $this->load->view(CMS_FOLDER.'footer'); 
   }
   
   public function add(){
	    
       $this->form_validation->set_rules('net', 'Net', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('pub_name', 'UTM Affiliate name', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('pub_pixel', 'UTM Affiliate', 'required');
	   $this->form_validation->set_rules('handle_type', 'handle type', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('cpl', 'cpl', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('fire_type', 'Fire Type', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('goal', 'Goal', 'trim|required|xss_clean');
	
	   if($this->form_validation->run() == FALSE){
		   $this->session->set_flashdata('error', "Fields marked * are mandatory"); 
       
	   }else{
		   	   $pub_details=array();
			   $pub_details['pubs']=addslashes(trim(strtolower($this->input->post('pub_name'))));
			   $pub_details['pixel']=$this->input->post('pub_pixel');
			   $pub_details['handle_type']=addslashes(trim($this->input->post('handle_type')));
			   $pub_details['cpl']=addslashes(trim($this->input->post('cpl')));
			   $pub_details['fire_type']=addslashes(trim($this->input->post('fire_type')));
			   $pub_details['goal']=addslashes(trim($this->input->post('goal')));
			   $pub_details['net_id']=addslashes(trim($this->input->post("net")));
			      
			   $this->db->insert(PUBLISHER,$pub_details);
			      
			   $this->session->set_flashdata('success', 'UTM Affiliate added ');              
			   redirect(SITE_URL."cms/manage_utm_aff/add_form"); 
		 }
			   
		redirect(SITE_URL."cms/manage_utm_aff");
   }
   
   
   public function edit_form($id=''){
     
	   $tablename = PUBLISHER;
	   if($id !=""){
		   $query= $this->db->query("SELECT
										id, location, pubs, pixel, handle_type, cpl, fire_type, goal, net_id, status
									FROM
									 	".$tablename."
									WHERE
										id= ".$id);
		
		   if ($query->num_rows() > 0) {
				foreach($query->result() as $p) {
						$data['pub_pixel'][] = $p;
						
						
						$data['net_data'] = $this->db->query("select id, net from ".NETWORK." where status = '1' order by net asc");
						$this->load->view(CMS_FOLDER."header");
						$this->load->view(CMS_FOLDER."sidebar", $this->page_details);
						$this->load->view(CMS_FOLDER."edit-utm-aff" , $data);
						$this->load->view(CMS_FOLDER.'footer'); 
				}
			}else{
			   redirect(CMS_FOLDER.'manage_utm_aff');
		   }
	   }
   }
   
   public function edit(){
 
	   $id = $this->input->post('id');
	   if($id){
		   
		   $this->form_validation->set_rules('pub_name', 'UTM Affiliate name', 'trim|required|xss_clean');
		   $this->form_validation->set_rules('pub_pixel', 'UTM Affiliate', 'required');
		   $this->form_validation->set_rules('handle_type', 'handle type', 'trim|required|xss_clean');
		   $this->form_validation->set_rules('cpl', 'cpl', 'trim|required|xss_clean');
		   $this->form_validation->set_rules('fire_type', 'Fire Type', 'trim|required|xss_clean');
		   $this->form_validation->set_rules('goal', 'Goal', 'trim|required|xss_clean');
		   $this->form_validation->set_rules('net', 'Net', 'trim|required|xss_clean');
		   $this->form_validation->set_rules('status', 'status', 'trim|required|xss_clean');
		   
		   if( $this->form_validation->run() == FALSE){
			   $this->session->set_flashdata('error', validation_errors());
			   redirect(CMS_FOLDER.'manage_utm_source/edit_form/'.$id);
		   
		   }else{
			   $this->db->where("id",$_POST['id']);
			   
			   $pub_details=array();
			   $pub_details['pubs']		=addslashes(trim(strtolower($this->input->post('pub_name'))));
			   $pub_details['pixel']	=$this->input->post('pub_pixel');
			   $pub_details['handle_type']=addslashes(trim($this->input->post('handle_type')));
			   $pub_details['cpl']		=addslashes(trim($this->input->post('cpl')));
			   $pub_details['fire_type']=addslashes(trim($this->input->post('fire_type')));
			   $pub_details['goal']		=addslashes(trim($this->input->post('goal')));
			   $pub_details['net_id']	=addslashes(trim($this->input->post("net")));
			   $pub_details['status']	=addslashes(trim($this->input->post("status")));
			   
			          
			   $this->db->update(PUBLISHER,$pub_details);
			   
			   $this->session->set_flashdata('success', 'Succesfully updated' , 'Succesfully updated');
		       
			   redirect(CMS_FOLDER.'manage_utm_aff');  
	      }
	   }
	   else
	   {   
		   redirect(CMS_FOLDER.'manage_utm_source');
	   }
   }
   
   public function get_ajax_net_dropdown(){
	   
	$data['net_id']= $this->db_function->get_data("SELECT id, net, status FROM ".NETWORK." WHERE status = 1");	
		
		foreach($data['net_id'] as $r ){ 
		
		   echo "<option value=".$r->id.">".$r->net."</option>";
		
		 }
	
	exit;
   }
   
}

?>
