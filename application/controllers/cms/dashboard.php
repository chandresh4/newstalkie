<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class dashboard extends CI_Controller
{
   public function __construct()
   {
        parent::__construct();
		$this->db->cache_off();
		//$this->output->enable_profiler(TRUE);
		$this->load->model(array(CMS_FOLDER.'common_model'));
		$this->load->library('memcached');
		$this->page_details['cur_controller'] = strtolower(__CLASS__);
		$this->page_details['menu']           = $this->common_model->Menu_Array();
        
   }
	//INDEX
	public function index(){
		//to get all category
		
		$query=$this->db->query("SELECT cat_id,name FROM tbl_category WHERE status = 1 ");
		   if ( $query->num_rows() > 0) {
				foreach ($query->result() as $p) {
				  $data['category'][] = $p;
				 
				}
		   }else{
			    $data['category'] ="";
		   }
		   $query=$this->db->query("SELECT id,net FROM tbl_network WHERE status = 1 ");
		   if ( $query->num_rows() > 0) {
				foreach ($query->result() as $p) {
				  $data['source'][] = $p;
				 
				}
		   }else{
			    $data['source'] ="";
		   }
		   $query=$this->db->query("SELECT id,medium FROM tbl_medium WHERE status = 1 ");
		   if ( $query->num_rows() > 0) {
				foreach ($query->result() as $p) {
				  $data['medium'][] = $p;
				 
				}
		   }else{
			    $data['medium'] ="";
		   }
		   $query=$this->db->query("SELECT id,sub FROM tbl_sub WHERE status = 1 ");
		   if ( $query->num_rows() > 0) {
				foreach ($query->result() as $p) {
				  $data['sub'][] = $p;
				 
				}
		   }else{
			    $data['sub'] ="";
		   }
		$this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $this->page_details);
		$this->load->view(CMS_FOLDER.'dashboard',$data); 
		$this->load->view(CMS_FOLDER.'footer'); 
   }
    public function get_val_count($from="",$to="",$category="",$status="", $optin="",$source ="",$medium="",$sub=""){
		 
		$status1 = $optin1= $where=$source1=$medium1=$sub1="";
		
		if($status == 1){
			$status1 = "status = 1";
		}else if($status == 2){
			$status1 = "status = 0";
		}else{
			$status1 = "status in(0,1)";
		}
        if($optin == 1){
			$optin1 = "is_optin = 1";
		}else if($optin == 2){
			$optin1 = "is_optin = 0";
		}else{
			$optin1 = "is_optin in(0,1)";
		}
		if($category == "all"){
			$where = $status1;
		}else{
			$where = "category = ".$category. " and ".$status1;
		}
	    if($source == "sel"){
		   $source1 = "";
	    }else{
		   $source1 = "utm_source='".$source."'";
	    }
		if($medium == "sel"){
		   $medium1 = "";
	    }else{
		   $medium1 = "utm_medium='".$medium."'";
	    }
		if($sub == "sel"){
		   $sub1 = "";
	    }else{
		   $sub1 = "utm_sub='".$sub."'";
	    }
		$sub_array=$tracker_array="";
	    //subscribers count
		if($sub1 !="" && $source1 !="" && $medium1 !=""){
			$query=$this->db->query("SELECT subscribe_id FROM `tbl_subscribers` WHERE ".$optin1." and DATE_FORMAT(datecreated, '%Y-%m-%d')  between '".$from."' and '".$to."'");
		    if ( $query->num_rows() > 0) {
				foreach ($query->result() as $p) {
					$sub_array[] = $p->subscribe_id;
				}
			}
			$query=$this->db->query("select userid from tbl_utm_tracker where ".$source1." and ".$medium1." and ".$sub1);
		    if ( $query->num_rows() > 0) {
				foreach ($query->result() as $p) {
					$tracker_array[] = $p->userid;
				}
			}
		$result=array_intersect($sub_array,$tracker_array);
		$data['subscribers'] =count($result);
		
		}else{
			$sql= "SELECT subscribe_id FROM tbl_subscribers WHERE ".$optin1." and DATE_FORMAT(datecreated, '%Y-%m-%d')  between '".$from."' and '".$to."'";
		    $query=$this->db->query($sql);
	        $data['subscribers'] =$query->num_rows() ;
		}
	   // writers count 
	   $query1=$this->db->query("SELECT id FROM tbl_contentwriter WHERE ".$optin1." and DATE_FORMAT(date_created, '%Y-%m-%d')  between '".$from."' and '".$to."'");
	   $data['writers'] = $query1->num_rows() ;
	   //writers post count
	   $query2=$this->db->query("SELECT id FROM tbl_post WHERE ".$status1." and (writer_id !='admin' and writer_id !='0') and DATE_FORMAT(date_created, '%Y-%m-%d')  between '".$from."' and '".$to."' ");
	   $data['writers_posts']= $query2->num_rows() ;
	  //posts count
	   $query3=$this->db->query("SELECT id FROM tbl_post WHERE ".$where." and DATE_FORMAT(date_created, '%Y-%m-%d')  between '".$from."' and '".$to."' ");
	   $data['total_posts'] =	$query3->num_rows() ;
	   echo json_encode($data);
       exit();
	}
	
	public function get_graph(){
	 $query=$this->db->query("SELECT cat_id,name FROM tbl_category WHERE  status = 1");
		   if ( $query->num_rows() > 0) {
				foreach ($query->result() as $p) {
				  $query1=$this->db->query("SELECT count(id) as count FROM tbl_post WHERE category = ".$p->cat_id);
		   
					   if ( $query1->num_rows() > 0) {
							foreach ($query1->result() as $p1) {
							   $data[$p->name]= $p1->count;
							}
					   }
				}
		   } 
		  foreach($data as $key => $val){
			 $arr[] = array($key ,(int)$val);
	  	  }
		 echo json_encode($arr);
	  exit;
	}
	 
	public function get_graphcount($smiley){
	$query=$this->db->query("select post_id,".$smiley." from `tbl_postreview` order by ".$smiley." desc limit 5");
	if ( $query->num_rows() > 0) {
	  foreach ($query->result() as $p) {
         $id[] = $p->post_id;
         $review[$p->post_id] = $p->$smiley;	  
      }
	}else{
	   $id[] = "";
	   $review[] ="";
	}
	if(!empty($id)){
	$ids= "";
	for($i=0;$i<count($id);$i++){
	   $ids.=$id[$i].",";
	}
	$ids = rtrim($ids,",");
	$query=$this->db->query("SELECT id,post_title from tbl_post where id in (".$ids.")");
	if ( $query->num_rows() > 0) {
	   foreach ($query->result() as $p) {
	     $data[$p->id] = $p->post_title;
 	   }
	}else{
	    $data[] ="";
	}
    foreach($review as $key => $val){
	  foreach($data as $key1 => $val1){
	    if($key == $key1){
	       $arr[] = array($val1 , (int)$val);
	    }
	  }
	}
	echo json_encode($arr);
	}else{
		echo json_encode("nodata");
	}
	
	exit;
		 
	 }
}
?>	

