<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class manage_utm_source extends CI_Controller
{
   public $global_country;	
   public function __construct()
   {
        parent::__construct();
		$this->load->model(array(CMS_FOLDER.'common_model', CMS_FOLDER.'db_function'));
		$this->page_details['cur_controller'] = strtolower(__CLASS__);
		$this->page_details['menu']           = $this->common_model->Menu_Array();
		$this->db->cache_off();
		$this->global_country =  ($this->session->userdata('cms_country') != "" ) ? $this->session->userdata('cms_country') : '';
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);  
   }

   public function index($perpage = 10, $offset = 0){
	   
	   $perpage = 10;
		
		// CALL PAGINATION FUNCTION
		$this->page($perpage, NULL, NULL, NULL, 0);
   }		
   
    public function page($perpage = 10, $btn_search = NULL, $search_field = NULL, $search_txt = NULL, $offset = 0){
       
	   // DECLARE NULL VARIABLES 
		$tablename = NETWORK;
		$query_string = NULL;
		$where = $whr = NULL;
		
		if($search_field == NULL && $search_txt == NULL && $btn_search > 0) {
			$offset = $btn_search;
			$btn_search = NULL;
		}
		
		$data['search_field'] 	= "";
		$data['search_txt'] 	= "";
		
		$cur_controller 	= strtolower(__CLASS__);
		$query_string = NULL;
		$full_path = FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
	    if( (trim($this->input->post("btn_search")) != "" || $btn_search != "") && 
			(trim($this->input->post("search_field")) != "" || $search_field != "") && 
			(trim($this->input->post("search_txt")) != "" || $search_txt != "")) {
			
			// STORE SEARCH VALUE IN DATA ARRAY
			$data['btn_search']		= ($btn_search != "") 	? $btn_search 	: $this->input->post("btn_search");
			$data['search_field'] 	= ($search_field != "") ? $search_field : $this->input->post("search_field");
			$data['search_txt']		= ($search_txt != "") 	? $search_txt 	: $this->input->post("search_txt");
			
			// PREPARE QUERY STRING
			$query_string = $perpage."/".$data['btn_search']."/".$data['search_field']."/".$data['search_txt'];
			
			// CREATE THE WHERE CLAUSE
			$whr = $data['search_field']." like '%".$data['search_txt']."%'";
			
			// CHECK FOR EXISTING CLAUSE
			if($where == "") {
				$where = " where ".$whr;
			} else {
				$where .= " and ".$whr;
			}
		} else {
			$query_string = $perpage."/";
			$where = "";
		}
	   
	   
	    $order_by = " order by datecreated desc";
	   		
		$sel_query="SELECT id, location, net, pixel, handle_type, cpl, fire_type, app_offer_form, goal, status FROM ".$tablename.$where.$order_by;
		
	    if($this->uri->segment(8) == "") {
			$config['uri_segment'] 	= 5;
		} else {
			$config['uri_segment'] 	= 8;
		}
		
	   // MODIFY FULL PATH
		$full_path .= $query_string;
		
		$config['total_rows'] 		= $this->db_function->count_record($sel_query, false);
		$config['per_page'] 		= $perpage;
		$config['base_url'] 		= $full_path;
		$choice 					= $config['total_rows'] / $config["per_page"];
		$config['num_links'] 		= 2;
		$config['full_tag_open'] 	= '<div id="paging" style="float:right; "><ul style="clear:left;">';
		$config['full_tag_close'] 	= '</ul></div>';
		$config['anchor_class'] 	= 'class="btn" ';
		$config['first_tag_open'] 	= '<li class="num_off">';
		$config['first_tag_close'] 	= '</li>';
		$config['last_tag_open'] 	= '<li class="num_off">';
		$config['last_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="num_on">';
		$config['cur_tag_close'] 	= '</li>';
		$config['num_tag_open'] 	= '<li class="num_off">';
    	$config['num_tag_close'] 	= '</li>';
		$config['prev_tag_open'] 	= '<li class="num_off">';
    	$config['prev_tag_close'] 	= '</li>';
		$config['next_tag_open'] 	= '<li class="num_off">';
    	$config['next_tag_close'] 	= '</li>';
		$config['prev_link'] 		= 'PREVIOUS';
    	$config['next_link'] 		= 'NEXT';
		$config['use_page_numbers'] = FALSE;
		// PAGINATION PARAMETER VALUES END 				
		
		// PAGINATION PARAMETER INITIALIZE 
		$this->pagination->initialize($config);
		
		// SQL QUERY WITH OFFSET AND PERPAGE LIMIT
		$sql = $sel_query." limit ".$offset.", ".$perpage;
	
        $data["details"] = $this->db_function->get_data($sql);
		
		// GET THE DATA FROM PAGINATION
		$data["today_count"] = $config['total_rows'];
		
        $data["links"] = $this->pagination->create_links();
		
	    $this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $this->page_details);
	    $this->load->view(CMS_FOLDER."manage-utm-source" ,  $data);
		$this->load->view(CMS_FOLDER.'footer'); 
   

   }
   
   public function add_form(){
	   
	   $this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $this->page_details);
	   $this->load->view(CMS_FOLDER."add-utm-source");
	   $this->load->view(CMS_FOLDER.'footer'); 
   }
   
   public function add(){
	    
       $this->form_validation->set_rules('net_name', 'Net name', 'trim|required|xss_clean');
	   //$this->form_validation->set_rules('net_pixel', 'Net Pixel', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('handle_type', 'handle type', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('cpl', 'cpl', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('fire_type', 'Fire Type', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('goal', 'Goal', 'trim|required|xss_clean');
	   
	   if($this->form_validation->run() == FALSE){
		    $this->session->set_flashdata('error', "Fields marked * are mandatory"); 
       }else{
		    if($this->db_function->get_data("SELECT net, location FROM ".NETWORK." WHERE location = '".trim($this->input->post('location'))."' and net ='".trim($this->input->post('net_name'))."'")) {
		   
			   $this->session->set_flashdata('error', 'Net name Alredy existed, ');              
			   redirect(SITE_URL."index.php/cms/manage_utm_source/add_form"); 
			   
		    }else{
				   $net_details=array();
				   $net_details['net']			=addslashes(trim(strtolower($this->input->post('net_name'))));
				   $net_details['pixel']		=trim($this->input->post('net_pixel'));
				   $net_details['handle_type']	=addslashes(trim($this->input->post('handle_type')));
				   $net_details['cpl']			=addslashes(trim($this->input->post('cpl')));
				   $net_details['fire_type']	=addslashes(trim($this->input->post('fire_type')));
				   $net_details['goal']			=addslashes(trim($this->input->post('goal')));
				   
				   $this->db->insert(NETWORK,$net_details);
					  
				   $this->session->set_flashdata('success', 'Network added , ');              
				   redirect(SITE_URL."index.php/cms/manage_utm_source/add_form"); 
			}
		 }
			   
		redirect(SITE_URL."index.php/cms/manage_utm_source");
   }
   
   public function edit_form($id='')
   {
	   $tablename = NETWORK;
	   if($id !=""){
		   $query= $this->db->query("SELECT
										id, net, pixel, handle_type, cpl, fire_type, goal, status
									FROM
									 	".$tablename."
									WHERE
										id=".$id);
		
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $p) {
					$data['net_pixel'][] = $p;
					
					$this->load->view(CMS_FOLDER."header");
					$this->load->view(CMS_FOLDER."sidebar", $this->page_details);
					$this->load->view(CMS_FOLDER."edit-utm-source" , $data);
					$this->load->view(CMS_FOLDER.'footer'); 
				}
			}else{
			   redirect(CMS_FOLDER.'manage_utm_source');
			}
	   }
   }
   
   public function edit(){
	   
	   $id=$this->input->post('id');
	   if($id){
		   
		   $this->form_validation->set_rules('net_name', 'Net name', 'trim|required|xss_clean');
		   //$this->form_validation->set_rules('net_pixel', 'Net Pixel', 'trim|required|xss_clean');
		   $this->form_validation->set_rules('handle_type', 'handle type', 'trim|required|xss_clean');
		   $this->form_validation->set_rules('cpl', 'cpl', 'trim|required|xss_clean');
		   $this->form_validation->set_rules('fire_type', 'Fire Type', 'trim|required|xss_clean');
		   $this->form_validation->set_rules('goal', 'Goal', 'trim|required|xss_clean');
		   $this->form_validation->set_rules('status', 'status', 'trim|required|xss_clean');
		   
		   if($this->form_validation->run() == FALSE){
			   
			   $this->session->set_flashdata('error', validation_errors());
			   redirect(CMS_FOLDER.'manage_utm_source/edit_form/'.$id);
		   
		   }else{
			   
			   $this->db->where("id",$_POST['id']);
			   
			   $net_details=array();
			   $net_details['net']			=addslashes(trim(strtolower($this->input->post('net_name'))));
			   $net_details['pixel']		=$this->input->post('net_pixel');
			   $net_details['handle_type']	=addslashes(trim($this->input->post('handle_type')));
			   $net_details['cpl']			=addslashes(trim($this->input->post('cpl')));
			   $net_details['fire_type']	=addslashes(trim($this->input->post('fire_type')));
			   $net_details['goal']			=addslashes(trim($this->input->post('goal')));
			   $net_details['status']		=addslashes(trim($this->input->post("status")));
				   
			   $this->db->update(NETWORK,$net_details);
			   
			   $this->session->set_flashdata('success', 'Succesfully updated' , 'Succesfully updated');
		       
			   redirect(CMS_FOLDER.'manage_utm_source');
	      }
	   
	   }else{   
		   redirect(CMS_FOLDER.'manage_utm_source');
	   }
   }
   

   
   
}

?>