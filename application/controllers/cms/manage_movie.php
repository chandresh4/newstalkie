<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class manage_movie extends CI_Controller
{
   public function __construct()
   {
        parent::__construct();
		global $page_details;
		$this->load->model(array( CMS_FOLDER.'common_model', CMS_FOLDER.'/db_function'));
		$this->db->cache_off();
		$this->tablename	= ADMIN_USERS;	
		$this->page_details['cur_controller'] = strtolower(__CLASS__);
		$this->page_details['menu']           = $this->common_model->Menu_Array();
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);  
		
		
   }

   public function index($perpage = 30, $offset = 0){
		
		$perpage = 30;
		
		// CALL PAGINATION FUNCTION
		$this->page($perpage, NULL, NULL, NULL, NULL, 0);
   }		
   
    public function page($perpage = 30, $btn_search = NULL, $search_field = NULL, $search_txt = NULL, $status=NULL, $offset = 0){
   
	    $cur_controller 		= strtolower(__CLASS__);
		$query_string       	= NULL;
		$full_path 				= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';	   
	    $query_string 			= NULL;
		$where = $whr 			= NULL;
	    $tablename              = MOVIE;
		$data['page_title']     = "MOVIES LIST";
		$data['add_title']      = "Add Movie";
		$data['add_edit_page'] 	= $cur_controller."/add";
		$query_string = NULL;
		$where = $whr = NULL;
		$from_date = $to_date= "";
		if($from_date == NULL && $to_date == NULL && $btn_search > 0) {
			$offset = $btn_search;
			$btn_search = NULL;
		}
		
		$data['from_date'] 	= "";
		$data['to_date'] 	= "";
		$data['status']     = "";
		
	   $cur_controller 	= strtolower(__CLASS__);
	   $query_string = NULL;
	   $full_path = FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
	   
	    if( (trim($this->input->post("btn_search")) != "" || $btn_search != "") || 
			((trim($this->input->post("from_date")) != "" || $from_date != "") && 
			(trim($this->input->post("to_date")) != "" || $to_date != "")) || 
			(trim($this->input->post("status")) != "" || $status != "")) {
			
			// STORE SEARCH VALUE IN DATA ARRAY
			$data['btn_search']		= ($btn_search != "") 	? $btn_search 	: $this->input->post("btn_search");
			$data['status']		    = ($status != "") 	    ? $status 	    : $this->input->post("status");
			$data['to_date'] 	    = ($to_date != "")      ? $to_date      : $this->input->post("to_date");
			$data['from_date']		= ($from_date != "") 	? $from_date 	: $this->input->post("from_date");
			 
			
			if($data['status'] != "" ){
			   
			   // PREPARE QUERY STRING
			   $query_string = $perpage."/".$data['btn_search']."/".$data['from_date']."/".$data['to_date']."/".$data['status']."/";
			   // CREATE THE WHERE CLAUSE
			   $whr = "DATE_FORMAT(date_created,'%Y-%m-%d') BETWEEN '".$data['from_date']."' AND '".$data['to_date']."' AND  status=".$data['status'];
			   
			}else{
	           // PREPARE QUERY STRING
			   $query_string = $perpage."/".$data['btn_search']."/".$data['from_date']."/".$data['to_date']."/".$data['status']."/";
			   
			   // CREATE THE WHERE CLAUSE
			   $whr = "DATE_FORMAT(date_created,'%Y-%m-%d') BETWEEN '".$data['from_date']."' AND '".$data['to_date']."' AND status = ".$data['status'];
			}
			
			// CHECK FOR EXISTING CLAUSE
			if($where == "") {
				$where = " where ".$whr;
			} else {
				$where .= " and ".$whr ;
			}
		} else {
			$data['status'] = '1';
			$query_string = $perpage."/";
			$where = " where status =".$data['status'] ;
		}
	   
	   
	    $order_by = " order by date_created desc";
	   
		$sel_query="SELECT mid, movie_name, language,status FROM ".$tablename.$where.$order_by;

	   if($this->uri->segment(9) == "") {
			$config['uri_segment'] 	= 5;
		} else {
			$config['uri_segment'] 	= 9;
		}
		
	   // MODIFY FULL PATH
		$full_path .= $query_string;
		
		$config['total_rows'] 		= $this->db_function->count_record($sel_query, false);
		$config['per_page'] 		= $perpage;
		$config['base_url'] 		= $full_path;
		$choice 					= $config['total_rows'] / $config["per_page"];
		$config['num_links'] 		= 2;
		$config['full_tag_open'] 	= '<div id="paging" style="float:right; "><ul style="clear:left;">';
		$config['full_tag_close'] 	= '</ul></div>';
		$config['anchor_class'] 	= 'class="btn" ';
		$config['first_tag_open'] 	= '<li class="num_off">';
		$config['first_tag_close'] 	= '</li>';
		$config['last_tag_open'] 	= '<li class="num_off">';
		$config['last_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="num_on">';
		$config['cur_tag_close'] 	= '</li>';
		$config['num_tag_open'] 	= '<li class="num_off">';
    	$config['num_tag_close'] 	= '</li>';
		$config['prev_tag_open'] 	= '<li class="num_off">';
    	$config['prev_tag_close'] 	= '</li>';
		$config['next_tag_open'] 	= '<li class="num_off">';
    	$config['next_tag_close'] 	= '</li>';
		$config['prev_link'] 		= 'PREVIOUS';
    	$config['next_link'] 		= 'NEXT';
		$config['use_page_numbers'] = FALSE;
		// PAGINATION PARAMETER VALUES END 				
		$query =$this->db->query("SELECT cat_id, name FROM ".CATEGORY." WHERE status = 1 ");
		
		if ( $query->num_rows() > 0) {
			 foreach ($query->result() as $p) {
			   $data['res'][] = $p;
			 }
		}
		
		// PAGINATION PARAMETER INITIALIZE 
		$this->pagination->initialize($config);
		
		// SQL QUERY WITH OFFSET AND PERPAGE LIMIT
		$sql = $sel_query." limit ".$offset.", ".$perpage;
	
        $data["details"] = $this->db_function->get_data($sql);
		
		// GET THE DATA FROM PAGINATION
		$data["today_count"] = $config['total_rows'];
		
        $data["links"] = $this->pagination->create_links();
		
	    $this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $this->page_details);
	    $this->load->view(CMS_FOLDER."manage-movie" ,  $data);
		$this->load->view(CMS_FOLDER.'footer'); 

   }   
   
   public function add(){
	   
	    $this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $this->page_details);
		$this->load->view(CMS_FOLDER."add-movie");
		$this->load->view(CMS_FOLDER.'footer'); 
    	
   }
   
   public function add_action(){
	 
		$this->form_validation->set_rules('language', 'language', 'required');
	    $this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('url', 'URL', 'required');
	    
		$this->form_validation->set_rules('status', 'Status', 'required');
	    
	
	   if($this->form_validation->run() == FALSE)
	   {
		  $this->session->set_flashdata('error', "Fields marked * are mandatory"); 
		  redirect(FULL_CMS_URL."/manage_posts/add"); 
       }else{
		   if($this->db_function->get_data("SELECT mid, movie_name  FROM ".MOVIE." WHERE movie_name = '".trim($this->input->post('title'))."'")) {
		   
			   $this->session->set_flashdata('error', 'Movie is Already existed, ');              
			   redirect(FULL_CMS_URL."/manage_movie/add"); 
			   
		    }else{
				 
			   $path = $_FILES['post_image']['name'];   
			   $file_extention = pathinfo($path, PATHINFO_EXTENSION);
			   $allowed_types = array('jpg', 'gif', 'png' , 'bmp' , 'jpeg');  
		       $post_img = $_FILES['post_image']['name'];
			   $post_Arr=array();
               $post_Arr['mid']              = "";
			   $post_Arr['language']   		= addslashes(trim($this->input->post('language')));
			   $post_Arr['movie_name']     	= stripslashes(trim($this->input->post('title')));	
			   $post_Arr['seourl']     	    = stripslashes(trim($this->input->post('url')));	 
			   $post_Arr['details'] = stripslashes(trim($this->input->post('content')));			 
			   $post_Arr['status']   		= addslashes(trim($this->input->post('status')));
			   $post_Arr['date_created']    = date('Y-m-d H:i:s');
			   $post_Arr['link']   		= addslashes(trim($this->input->post('link')));
			   $post_Arr['image']     	= $path;	
			  
			   if(in_array($file_extention, $allowed_types)){
				  
			       $this->db->insert(MOVIE,$post_Arr);
				   $pathAndName = MOVIE_FOLDER.$post_img;
				   $croppathAndName =LARGE_MOVIE_FOLDER.THUMB_PREFIX.$post_img;
				   $croppathAndName1 =MEDIUM_MOVIE_FOLDER.THUMB_PREFIX.$post_img;
				   $croppathAndName2 =SMALL_MOVIE_FOLDER.THUMB_PREFIX.$post_img;
				   $croppathAndName3 =CROP_MOVIE_FOLDER.THUMB_PREFIX.$post_img;
				   move_uploaded_file($_FILES['post_image']['tmp_name'],$pathAndName);
				   make_img_thumb("./".$pathAndName, "./".$croppathAndName,756);
				   make_img_thumb("./".$pathAndName, "./".$croppathAndName1,378);
				   make_img_thumb("./".$pathAndName, "./".$croppathAndName2,303);
				   make_img_thumb("./".$pathAndName, "./".$croppathAndName3,850);
				   $this->session->set_flashdata('success', $lb_Arr['title'].' Post  Added Successfully');           
				   redirect(FULL_CMS_URL."/manage_movie/add"); 
			   }else{
				   				  
					$this->session->set_flashdata('error', 'Please upload correct image only');
		            redirect(FULL_CMS_URL."/manage_movie/add"); 
			   }	
		   }
	   }
  
   }
	
	public function edit(){
	 
	   $id=$_GET['id'];
	    
	   $tablename = MOVIE;
	   if( $id !=""){
		   $query=$this->db->query("SELECT
										mid,language,movie_name,seourl,image,details,status,link
									FROM
									 	".$tablename."
									WHERE 
										mid = ".$id);
		   
		   if ( $query->num_rows() > 0) {
				foreach ($query->result() as $p) {
				  $data['val'][] = $p;
				
				  $this->load->view(CMS_FOLDER."header");
		          $this->load->view(CMS_FOLDER."sidebar", $this->page_details);
				  $this->load->view(CMS_FOLDER."edit-movie" , $data);
				  $this->load->view(CMS_FOLDER.'footer'); 
				}
			}else{
		       
			    redirect(FULL_CMS_URL."/manage_posts/");
	        }
	   }
   }
      
   public function edit_action(){
	 
     $id=$this->input->post('id');
	  
	   if($id){
			  
			   $this->form_validation->set_rules('title', 'Title', 'required');
			    $this->form_validation->set_rules('url', 'URL', 'required');
			   $this->form_validation->set_rules('status', 'Status', 'required');
			   $this->form_validation->set_rules('language', 'Language', 'required');
			   
				
		   if( $this->form_validation->run() == FALSE){
			   $this->session->set_flashdata('message', validation_errors());
			   redirect(CMS_FOLDER.'manage_movie/edit/?id='.$id);
		   
		   }else{
			
			 $this->db->where("mid",$_POST['id']);
			 $path = $_FILES['post_image']['name'];
			 $file_extention = pathinfo($path, PATHINFO_EXTENSION);
			 $allowed_types = array('jpg', 'gif', 'png' , 'bmp' , 'jpeg');
		  	 $lb_Arr=array();	
			 $lb_Arr['language']     	= stripslashes(trim($this->input->post('language')));	
            				 
			 $lb_Arr['movie_name']     	= stripslashes(trim($this->input->post('title')));	 
			 $lb_Arr['seourl']     	    = stripslashes(trim($this->input->post('url')));	 
			 $lb_Arr['details'] = stripslashes(trim($this->input->post('content')));
			 $lb_Arr['status']   		= addslashes(trim($this->input->post('status')));
			 $lb_Arr['date_created']  	= date('Y-m-d H:i:s');
			 $lb_Arr['link']     	    = stripslashes(trim($this->input->post('link')));				 
             if( $_FILES['post_image']['name']){
				if(in_array($file_extention, $allowed_types)){
					$post_img = $_FILES['post_image']['name'];
					$lb_Arr['image']   = $post_img;				
					$pathAndName = MOVIE_FOLDER.$post_img;
					$croppathAndName =LARGE_MOVIE_FOLDER.THUMB_PREFIX.$post_img;
				    $croppathAndName1 =MEDIUM_MOVIE_FOLDER.THUMB_PREFIX.$post_img;
				    $croppathAndName2 =SMALL_MOVIE_FOLDER.THUMB_PREFIX.$post_img;
					 $croppathAndName3 =CROP_MOVIE_FOLDER.THUMB_PREFIX.$post_img;
				    move_uploaded_file($_FILES['post_image']['tmp_name'],$pathAndName);
				    make_img_thumb("./".$pathAndName, "./".$croppathAndName,756);
				    make_img_thumb("./".$pathAndName, "./".$croppathAndName1,378);
				    make_img_thumb("./".$pathAndName, "./".$croppathAndName2,303);
					make_img_thumb("./".$pathAndName, "./".$croppathAndName3,850);
					
				}else{
					$this->session->set_flashdata('error', 'Please upload correct image only');
		            redirect(CMS_FOLDER.'manage_movie/edit/?id='.$id);
				}		
			 }
			 $this->db->update(MOVIE,$lb_Arr);
			 $this->session->set_flashdata('success', 'Succesfully updated', 'Succesfully updated');
			 redirect(CMS_FOLDER.'manage_movie/edit/?id='.$id); 
	      }
	   }else{   
	       $this->session->set_flashdata('error', 'Error Ocurred');
		   redirect(CMS_FOLDER.'manage_movie/edit/?id='.$id);
	   }
   }
   
   
}

?>