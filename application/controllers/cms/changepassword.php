<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class changepassword extends CI_Controller
{
   public function __construct()
   {
        parent::__construct();
		$this->load->model(array( CMS_FOLDER.'common_model', CMS_FOLDER.'/db_function'));
		$this->page_details['cur_controller'] = strtolower(__CLASS__);
		$this->page_details['menu']           = $this->common_model->Menu_Array();
		$this->db->cache_off();
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);  
   }

   public function index($perpage = 30, $offset = 0){
	  
	
				
		      $this->load->view(CMS_FOLDER."header");
			  $this->load->view(CMS_FOLDER."sidebar", $this->page_details);
		      $this->load->view(CMS_FOLDER."change-password" );
			  $this->load->view(CMS_FOLDER.'footer'); 
			}
			
	     
		
   
   
   public function update(){
	   $tablename = "tbl_admin_user";
       if($this->session->userdata('uid') != ""){
		$pwd=md5(trim($_POST['password']));
        $npwd=md5(trim($_POST['newpassword']));	
        $cpwd=md5(trim($_POST['cpassword']));			
		$query=$this->db->query("SELECT
										id, passwd, status
									FROM
									 	".$tablename."
									WHERE 
										id=".$this->session->userdata('uid'));
		   
	    if ($query->num_rows() > 0) {
			foreach($query->result() as $val){
				$password= $val->passwd;
			}
	    }
		
		if($password == $pwd)
			{
				if($npwd == $cpwd){
				$data = array(
	               "passwd" => $npwd
	            );
	           $this->db->where('id',$this->session->userdata('uid'));
	           $this->db->update($tablename, $data);
	           $this->session->set_flashdata('success', ' Password Updated Successfully');redirect(CMS_FOLDER.'changepassword'); 
			}else{
			$this->session->set_flashdata('error', 'both  password are not matching' );
	        redirect(CMS_FOLDER.'changepassword');
			}
			
		}else{
		   $this->session->set_flashdata('error', 'wrong password u have given' );
	        redirect(CMS_FOLDER.'changepassword');
		}
	                
    }else{
		  $this->session->set_flashdata('error', 'Something went is wrong' );
	  redirect(CMS_FOLDER.'changepassword');
	}
   }
    
}

?>