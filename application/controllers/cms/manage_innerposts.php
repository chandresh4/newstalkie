<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class manage_innerposts  extends CI_Controller
{
   public function __construct()
   {
        parent::__construct();
		global $page_details;
		$this->load->model(array( CMS_FOLDER.'common_model', CMS_FOLDER.'/db_function'));
		$this->db->cache_off();
		$this->tablename	= ADMIN_USERS;	
		$this->page_details['cur_controller'] = strtolower(__CLASS__);
		$this->page_details['menu']           = $this->common_model->Menu_Array();
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);  
		
		
   }

   public function index($perpage = 30, $offset = 0){
		$id = $_GET['id'];
		if($id !=""){
		$perpage = 30;
		// CALL PAGINATION FUNCTION
		$this->page($id,$perpage, NULL, NULL, NULL, NULL, 0);
		}else{
			 redirect(FULL_CMS_URL."/manage_posts");
		}
   }		
   
    public function page($id,$perpage = 30, $btn_search = NULL, $search_field = NULL, $search_txt = NULL, $status=NULL, $offset = 0){
       
	    $cur_controller 		= strtolower(__CLASS__);
		$query_string       	= NULL;
		$full_path 				= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';	   
	    $query_string 			= NULL;
		$where = $whr 			= NULL;
	    $tablename              = INNERPOSTS;
		$data['page_title']     = "INNERPOST LISTS";
		$data['add_title']      = "Add InnerPost";
		$data['add_edit_page'] 	= $cur_controller."/add";
		$data['pid']        = $id;
		$query_string = NULL;
		$where = $whr = NULL;
		$from_date = $to_date= "";
		if($from_date == NULL && $to_date == NULL && $btn_search > 0) {
			$offset = $btn_search;
			$btn_search = NULL;
		}
		
		$data['from_date'] 	= "";
		$data['to_date'] 	= "";
		$data['status']     = "";
		
	   $cur_controller 	= strtolower(__CLASS__);
	   $query_string = NULL;
	   $full_path = FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
	   
	    if( (trim($this->input->post("btn_search")) != "" || $btn_search != "") || 
			((trim($this->input->post("from_date")) != "" || $from_date != "") && 
			(trim($this->input->post("to_date")) != "" || $to_date != "")) || 
			(trim($this->input->post("status")) != "" || $status != "")) {
			
			// STORE SEARCH VALUE IN DATA ARRAY
			$data['btn_search']		= ($btn_search != "") 	? $btn_search 	: $this->input->post("btn_search");
			$data['status']		    = ($status != "") 	    ? $status 	    : $this->input->post("status");
			$data['to_date'] 	    = ($to_date != "")      ? $to_date      : $this->input->post("to_date");
			$data['from_date']		= ($from_date != "") 	? $from_date 	: $this->input->post("from_date");
			 
			
			if($data['status'] != "" ){
			   
			   // PREPARE QUERY STRING
			   $query_string = $perpage."/".$data['btn_search']."/".$data['from_date']."/".$data['to_date']."/".$data['status']."/";
			   // CREATE THE WHERE CLAUSE
			   $whr = "parent_post_id = ".$id." and DATE_FORMAT(date_created,'%Y-%m-%d') BETWEEN '".$data['from_date']."' AND '".$data['to_date']."' AND  status=".$data['status'];
			   
			}else{
	           // PREPARE QUERY STRING
			   $query_string = $perpage."/".$data['btn_search']."/".$data['from_date']."/".$data['to_date']."/".$data['status']."/";
			   
			   // CREATE THE WHERE CLAUSE
			   $whr = " parent_post_id = ".$id." and DATE_FORMAT(date_created,'%Y-%m-%d') BETWEEN '".$data['from_date']."' AND '".$data['to_date']."' AND status = ".$data['status'];
			}
			
			// CHECK FOR EXISTING CLAUSE
			if($where == "") {
				$where = " where ".$whr;
			} else {
				$where = " and ".$whr;
			}
			
		} else {
			$data['status'] = '1';
			$query_string = $perpage."/";
			$where = " where parent_post_id = ".$id." and status =".$data['status'];
		}
	   
	   
	    $order_by = " order by date_created desc";
	   
		$sel_query="SELECT id,parent_post_id, inner_post_title, inner_post_desc,status FROM ".$tablename.$where.$order_by;
        
	   if($this->uri->segment(9) == "") {
			$config['uri_segment'] 	= 5;
		} else {
			$config['uri_segment'] 	= 9;
		}
		
	   // MODIFY FULL PATH
		$full_path .= $query_string;
		
		$config['total_rows'] 		= $this->db_function->count_record($sel_query, false);
		$config['per_page'] 		= $perpage;
		$config['base_url'] 		= $full_path;
		$choice 					= $config['total_rows'] / $config["per_page"];
		$config['num_links'] 		= 2;
		$config['full_tag_open'] 	= '<div id="paging" style="float:right; "><ul style="clear:left;">';
		$config['full_tag_close'] 	= '</ul></div>';
		$config['anchor_class'] 	= 'class="btn" ';
		$config['first_tag_open'] 	= '<li class="num_off">';
		$config['first_tag_close'] 	= '</li>';
		$config['last_tag_open'] 	= '<li class="num_off">';
		$config['last_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="num_on">';
		$config['cur_tag_close'] 	= '</li>';
		$config['num_tag_open'] 	= '<li class="num_off">';
    	$config['num_tag_close'] 	= '</li>';
		$config['prev_tag_open'] 	= '<li class="num_off">';
    	$config['prev_tag_close'] 	= '</li>';
		$config['next_tag_open'] 	= '<li class="num_off">';
    	$config['next_tag_close'] 	= '</li>';
		$config['prev_link'] 		= 'PREVIOUS';
    	$config['next_link'] 		= 'NEXT';
		$config['use_page_numbers'] = FALSE;
		// PAGINATION PARAMETER VALUES END 				
		$query =$this->db->query("SELECT id,post_title FROM ".POSTS."   WHERE  id=".$id." and status = 1");
		
		if ( $query->num_rows() > 0) {
			 foreach ($query->result() as $p) {
			   $data['res'][] = $p;
			 }
		}
		// PAGINATION PARAMETER INITIALIZE 
		$this->pagination->initialize($config);
		
		// SQL QUERY WITH OFFSET AND PERPAGE LIMIT
		$sql = $sel_query." limit ".$offset.", ".$perpage;
	
        $data["details"] = $this->db_function->get_data($sql);
		
		// GET THE DATA FROM PAGINATION
		$data["today_count"] = $config['total_rows'];
		
        $data["links"] = $this->pagination->create_links();
		
	    $this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $this->page_details);
	    $this->load->view(CMS_FOLDER."manage-innerpost" ,  $data);
		$this->load->view(CMS_FOLDER.'footer'); 
	}
   
   public function add(){
	   
		
		$data['id'] = $_GET['id'];
		$this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $this->page_details);
		$this->load->view(CMS_FOLDER."add-innerpost",$data);
		$this->load->view(CMS_FOLDER.'footer'); 
    	
   }
   
   public function add_action(){
	 
	     $id = $this->input->post('id');
	    $this->form_validation->set_rules('title', 'Title', 'required');
	   
	   $this->form_validation->set_rules('status', 'Status', 'required');
      
	
	   if($this->form_validation->run() == FALSE)
	   {
		  $this->session->set_flashdata('error', "Fields marked * are mandatory"); 
		  redirect(FULL_CMS_URL."/manage_innerposts/add?id=".$id); 
       }else{
		   if($this->db_function->get_data("SELECT id, inner_post_title  FROM ".INNERPOSTS." WHERE inner_post_title = '".addslashes(trim($this->input->post('title')))."'")) {
		   
			   $this->session->set_flashdata('error', 'POST is Already existed, ');              
			  redirect(FULL_CMS_URL."/manage_innerposts/add?id=".$id); 
			   
		    }else{
				 
			   $path = $_FILES['post_image']['name'];   
			   $file_extention = pathinfo($path, PATHINFO_EXTENSION);
			   $allowed_types = array('jpg', 'gif', 'png' , 'bmp' , 'jpeg');  
		    
			   $post_Arr=array();
             
			   $post_Arr['id']        = "";
			   $post_Arr['parent_post_id'] = $this->input->post('id');
			   $post_Arr['inner_post_title']     	= addslashes(trim($this->input->post('title')));	
			   if($this->input->post('content')!= ""){
			        $post_Arr['inner_post_desc']  	= stripslashes(trim($this->input->post('content')));
			   }else{
				    $post_Arr['inner_post_desc']  	= "";
			   }				   
			   $post_Arr['status']   		= addslashes(trim($this->input->post('status')));
			   $post_img                = $_FILES['post_image']['name'];
			   $newimage= end(explode(".", $post_img ));
			   $newfilename=preg_replace('/\s+/', '', $this->input->post('title')).".".$newimage;
			   //echo $newfilename;exit;
			   $post_Arr['inner_post_image']    = $post_img;
		       $post_Arr['date_created']  = date('Y-m-d H:i:s');
			   if(in_array($file_extention, $allowed_types)){
				   $this->db->insert(INNERPOSTS,$post_Arr);
				   $pathAndName = INNERPOSTS_FOLDER.$post_img;
				   $croppathAndName  = LARGE_INNERPOSTS_FOLDER.THUMB_PREFIX.$post_img;
				   $croppathAndName1 =MEDIUM_INNERPOSTS_FOLDER.THUMB_PREFIX.$post_img;
				   $croppathAndName2 =SMALL_INNERPOSTS_FOLDER.THUMB_PREFIX.$post_img;
				   $croppathAndName3 =CROP_INNERPOSTS_FOLDER.THUMB_PREFIX.$post_img;
				   move_uploaded_file($_FILES['post_image']['tmp_name'],$pathAndName);
				   make_img_thumb("./".$pathAndName, "./".$croppathAndName,756);
				   make_img_thumb("./".$pathAndName, "./".$croppathAndName1,378);
				   make_img_thumb("./".$pathAndName, "./".$croppathAndName2,303);
				   make_img_thumb("./".$pathAndName, "./".$croppathAndName3,850);
				   $this->session->set_flashdata('success', $lb_Arr['title'].' Post  Added Successfully');              
				   redirect(FULL_CMS_URL."/manage_innerposts/add?id=".$id);
               }else{
				   $this->session->set_flashdata('error', 'Please upload correct image only');
		           redirect(FULL_CMS_URL."/manage_innerposts?id=".$pid); 
			   }				   
		   }
	   }
  
   }
	
	public function edit(){
	 
	   $res=explode("_",$_GET['id']);
	   $id= $res[0];
	   $p_id=$res[1];
	    
	   $tablename = INNERPOSTS;
	   if( $id !=""){
		   $query=$this->db->query("SELECT
										id,parent_post_id,inner_post_title,inner_post_desc,	inner_post_image,status
									FROM
									 	".$tablename."
									WHERE 
										id = ".$id);
		   
		   if ( $query->num_rows() > 0) {
				foreach ($query->result() as $p) {
				  $data['val'][] = $p;
					
				  $this->load->view(CMS_FOLDER."header");
		          $this->load->view(CMS_FOLDER."sidebar", $this->page_details);
				  $this->load->view(CMS_FOLDER."edit-innerpost" , $data);
				  $this->load->view(CMS_FOLDER.'footer'); 
				}
			}else{
		       
			    redirect(FULL_CMS_URL."/manage_innerposts?id=". $p_id);
	        }
	   }
   }
      
   public function edit_action(){
	  
     $id=$this->input->post('id');
	 $pid= $this->input->post('pid');
	   if($id){
			  
			   $this->form_validation->set_rules('title', 'Title', 'required');
			   
			   $this->form_validation->set_rules('status', 'Status', 'required');
			  
			   
				
		   if( $this->form_validation->run() == FALSE){
			   $this->session->set_flashdata('message', validation_errors());
			   redirect(CMS_FOLDER.'manage_innerposts/edit?id='.$id."_".$pid);
		   
		   }else{
			
			 $this->db->where("id",$_POST['id']);
			
			 $path = $_FILES['post_image']['name'];
			 $file_extention = pathinfo($path, PATHINFO_EXTENSION);
			 $allowed_types = array('jpg', 'gif', 'png' , 'bmp' , 'jpeg');
		     $post_Arr=array();
             $post_Arr['parent_post_id'] = $this->input->post('pid');
			 $post_Arr['inner_post_title']     	= addslashes(trim($this->input->post('title')));	
			 if($this->input->post('content')!= ""){
			        $post_Arr['inner_post_desc']  	= stripslashes(trim($this->input->post('content')));
			 }else{
				    $post_Arr['inner_post_desc']  	= "";
			 }				   
			 $post_Arr['status']   		= addslashes(trim($this->input->post('status')));
			 $post_img                = $_FILES['post_image']['name'];
			 $newimage= end(explode(".", $post_img ));
			 $newfilename=preg_replace('/\s+/', '', $this->input->post('title')).".".$newimage;
			 $post_Arr['date_created']  = date('Y-m-d H:i:s');
			 if( $_FILES['post_image']['name']){
				if(in_array($file_extention, $allowed_types)){
				$post_img = $_FILES['post_image']['name'];
				$newimage= end(explode(".", $post_img ));
				$newfilename=preg_replace('/\s+/', '', $this->input->post('title')).".".$newimage;
				$post_Arr['inner_post_image']    =$post_img;		   
				$pathAndName = INNERPOSTS_FOLDER.$post_img;
			    $croppathAndName  = LARGE_INNERPOSTS_FOLDER.THUMB_PREFIX.$post_img;
			    $croppathAndName1 =MEDIUM_INNERPOSTS_FOLDER.THUMB_PREFIX.$post_img;
			    $croppathAndName2 =SMALL_INNERPOSTS_FOLDER.THUMB_PREFIX.$post_img;
				$croppathAndName3 =CROP_INNERPOSTS_FOLDER.THUMB_PREFIX.$post_img;
				//echo $croppathAndName3;exit;
			    move_uploaded_file($_FILES['post_image']['tmp_name'],$pathAndName);
			    make_img_thumb("./".$pathAndName, "./".$croppathAndName,756);
			    make_img_thumb("./".$pathAndName, "./".$croppathAndName1,378);
			    make_img_thumb("./".$pathAndName, "./".$croppathAndName2,303);
				make_img_thumb("./".$pathAndName, "./".$croppathAndName3,850);
				}else{
					$this->session->set_flashdata('error', 'Please upload correct image only');
		            redirect(FULL_CMS_URL."/manage_innerposts?id=".$pid); 
				 }					 
			 }
			 //print_r($post_Arr);exit;
			 $this->db->update(INNERPOSTS,$post_Arr);
			 $this->session->set_flashdata('success', 'Succesfully updated', 'Succesfully updated');
			 redirect(FULL_CMS_URL."/manage_innerposts?id=".$pid);  
	      }
	   }else{   
	       $this->session->set_flashdata('error', 'Error Ocurred');
		   redirect(FULL_CMS_URL."/manage_innerposts?id=".$pid);
	   }
   }
}

?>