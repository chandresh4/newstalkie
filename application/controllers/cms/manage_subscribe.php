<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class manage_subscribe extends CI_Controller
{
   public $global_country;
   public function __construct(){
        
		parent::__construct();
		$this->load->library('memcached');
		$this->load->model(array(CMS_FOLDER.'common_model', CMS_FOLDER.'db_function'));
		$this->page_details['cur_controller'] = strtolower(__CLASS__);
		$this->page_details['menu']           = $this->common_model->Menu_Array();
		$this->db->cache_off();
		$this->global_country =  ($this->session->userdata('cms_country') != "" ) ? $this->session->userdata('cms_country') : '';
		
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);  
   }

      public function index($perpage = 30, $offset = 0){
	   
	   $perpage = 30;
		
		// CALL PAGINATION FUNCTION
		$this->page($perpage, NULL, NULL, NULL, 0);
   }		
   
    public function page($perpage = 30, $btn_search = NULL, $from_date = NULL, $to_date = NULL, $offset = 0){
      
	   // DECLARE NULL VARIABLES 
	    $tablename             =  SUBSCRIBE;
		$data['page_title']    = "User details ";
		$data['add_title']     = "Add user details";
		$query_string = NULL;
		$where = $whr = NULL;
		
		if($from_date == NULL && $to_date == NULL && $btn_search > 0) {
			$offset = $btn_search;
			$btn_search = NULL;
		}
		
		$data['from_date'] 	= "";
		$data['to_date'] 	= "";
		$data['status']     = "";
		
	   $cur_controller 	= strtolower(__CLASS__);
	   $query_string = NULL;
	   $full_path = FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
	   
	    if( (trim($this->input->post("btn_search")) != "" || $btn_search != "") || 
			((trim($this->input->post("from_date")) != "" || $from_date != "") && 
			(trim($this->input->post("to_date")) != "" || $to_date != ""))) {

			// STORE SEARCH VALUE IN DATA ARRAY
			$data['btn_search']		= ($btn_search != "") 	? $btn_search 	: $this->input->post("btn_search");
			$data['to_date']     	= ($to_date != "")      ? $to_date      : $this->input->post("to_date");
			$data['from_date']		= ($from_date != "") 	? $from_date 	: $this->input->post("from_date");
			 
			if($data['from_date'] !=	"" &&  $data['to_date']!= ""  && $data['from_date'] != '0'){
			    
				$query_string = $perpage."/".$data['btn_search']."/".$data['from_date']."/".$data['to_date'];
				$whr = "DATE_FORMAT(datecreated,'%Y-%m-%d') BETWEEN '".$data['from_date']."' AND '".$data['to_date']."'";
			      
			}
			// CHECK FOR EXISTING CLAUSE
			if($where == "") {
				$where = " where ".$whr;
			} else {
				$where .= " and ".$whr;
			}
		} else {
			$data['btn_search'] = "GO";
			$data['from_date']  = date('Y-m-d');
			$data['to_date']    = date('Y-m-d');
			
			$query_string = $perpage."/".$data['btn_search']."/".$data['from_date']."/".$data['to_date'];
			$where = " WHERE DATE_FORMAT(datecreated,'%Y-%m-%d') BETWEEN '".$data['from_date']."' AND '".$data['to_date']."'";
			      
		}
	   
	   
	    $order_by = " order by datecreated desc";
	   
		$sel_query="SELECT subscribe_id, name ,email,location,is_optin,status FROM ".$tablename.$where.$order_by;

	   if($this->uri->segment(8) == "") {
			$config['uri_segment'] 	=5;
		} else {
			$config['uri_segment'] 	= 8;
		}		
	   // MODIFY FULL PATH
		$full_path .= $query_string;		
		  $config['total_rows'] 		= $this->db_function->count_record($sel_query, false);
		  $config['per_page'] 			= $perpage;
		  $config['base_url'] 			= $full_path;
		  $choice 						= $config['total_rows'] / $config["per_page"];
		  $config['num_links'] 			= 2;
		  $config['full_tag_open'] 		= '<div id="paging" style="float:right; "><ul style="clear:left;">';
		  $config['full_tag_close'] 	= '</ul></div>';
		  $config['anchor_class'] 		= 'class="btn" ';
		  $config['first_tag_open'] 	= '<li class="num_off">';
		  $config['first_tag_close'] 	= '</li>';
		  $config['last_tag_open'] 		= '<li class="num_off">';
		  $config['last_tag_close'] 	= '</li>';
		  $config['cur_tag_open'] 		= '<li class="num_on">';
		  $config['cur_tag_close'] 		= '</li>';
		  $config['num_tag_open'] 		= '<li class="num_off">';
		  $config['num_tag_close'] 		= '</li>';
		  $config['prev_tag_open'] 		= '<li class="num_off">';
		  $config['prev_tag_close'] 	= '</li>';
		  $config['next_tag_open'] 		= '<li class="num_off">';
		  $config['next_tag_close'] 	= '</li>';
		  $config['prev_link'] 			= 'PREVIOUS';
		  $config['next_link'] 			= 'NEXT';
		  $config['use_page_numbers'] 	= FALSE;
		// PAGINATION PARAMETER VALUES END 				
		
		// PAGINATION PARAMETER INITIALIZE 
		$this->pagination->initialize($config);
		
		// SQL QUERY WITH OFFSET AND PERPAGE LIMIT
		$sql = $sel_query." limit ".$offset.", ".$perpage;
	
        $data["details"] = $this->db_function->get_data($sql);
		
		// GET THE DATA FROM PAGINATION
		$data["today_count"] = $config['total_rows'];
		
       if($this->uri->segment(8)){
			  $pages=$this->uri->segment(8);
		  } elseif($this->uri->segment(5)) {
			  $pages=$this->uri->segment(5);
		  } else{
			  $pages=0;
		  }
		  $sl_no = $pages +1;
		  $data['sl_no']=$sl_no;
		  $data["links"] = $this->pagination->create_links();
		
	    $this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $this->page_details);
	    $this->load->view(CMS_FOLDER."manage-subscribe" ,  $data);
		$this->load->view(CMS_FOLDER.'footer'); 
   

   } 
   
  
   
   public function edit(){
	   
	   $id=$this->input->post('id');
	   if($id){
		   $this->form_validation->set_rules('status', 'status', 'required');


		   if( $this->form_validation->run() == FALSE){
			   $this->session->set_flashdata('message', validation_errors());
			   
			   redirect(CMS_FOLDER.'user_subscribe/edit_form/'.$id);
		   
		   }else{
			   $this->db->where("id",$_POST['id']);
			  
			   $page_arr=array();
			   $page_arr['status']             = addslashes(trim($this->input->post('status')));
			   $page_arr['date_created']       = date('Y-m-d H:i:s');
			   
			   $this->db->update(SUBSCRIBE,$page_arr);
			   
			   $this->session->set_flashdata('success', 'Succesfully updated', 'Succesfully updated');
		       
			   redirect(CMS_FOLDER.'user_subscribe');  
	      }
	   }else{   
	           redirect(CMS_FOLDER.'user_subscribe');
	   }

   }
   
   
}

?>