<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class logout_user extends CI_Controller
{
   public function __construct()
   {
        parent::__construct();
   }
    public function logout()
    {
       $newdata = array(
                          'user_id'   =>'',
                          'user_name'  =>'',
                          'user_email'     => '',
                          'logged_in' => FALSE,
                       );

       $this->session->unset_userdata($newdata );
       $this->session->sess_destroy();
       redirect(CMS_FOLDER.'adminlogin');
    }
}
?>