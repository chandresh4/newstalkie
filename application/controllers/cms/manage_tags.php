<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class manage_tags extends CI_Controller
{
   public function __construct()
   {
        parent::__construct();
		global $page_details;
		$this->load->model(array( CMS_FOLDER.'common_model', CMS_FOLDER.'/db_function'));
		$this->db->cache_off();
		$this->tablename	= ADMIN_USERS;	
		$this->page_details['cur_controller'] = strtolower(__CLASS__);
		$this->page_details['menu']           = $this->common_model->Menu_Array();
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);  
		
		
   }

   public function index($perpage = 30, $offset = 0){
		
		$perpage = 30;
		
		// CALL PAGINATION FUNCTION
		$this->page($perpage, NULL, NULL, NULL, NULL, 0);
   }		
   
    public function page($perpage = 30, $btn_search = NULL, $search_field = NULL, $search_txt = NULL, $status=NULL, $offset = 0){
       
	    $cur_controller 		= strtolower(__CLASS__);
		$query_string       	= NULL;
		$full_path 				= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';	   
	    $query_string 			= NULL;
		$where = $whr 			= NULL;
	    $tablename              = TAGS;;
		$data['page_title']     = "Tag Details";
		$data['add_title']      = "Add Tag";
		$data['add_edit_page'] 	= $cur_controller."/add";
		$query_string = NULL;
		$where = $whr = NULL;
		$from_date = $to_date= "";
		if($from_date == NULL && $to_date == NULL && $btn_search > 0) {
			$offset = $btn_search;
			$btn_search = NULL;
		}
		
		$data['from_date'] 	= "";
		$data['to_date'] 	= "";
		$data['status']     = "";
		
	    $cur_controller 	= strtolower(__CLASS__);
	    $query_string = NULL;
	    $full_path = FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
	   
	    if( (trim($this->input->post("btn_search")) != "" || $btn_search != "") || 
			((trim($this->input->post("from_date")) != "" || $from_date != "") && 
			(trim($this->input->post("to_date")) != "" || $to_date != "")) || 
			(trim($this->input->post("status")) != "" || $status != "")) {
			
			// STORE SEARCH VALUE IN DATA ARRAY
			$data['btn_search']		= ($btn_search != "") 	? $btn_search 	: $this->input->post("btn_search");
			$data['status']		    = ($status != "") 	    ? $status 	    : $this->input->post("status");
			$data['to_date'] 	    = ($to_date != "")      ? $to_date      : $this->input->post("to_date");
			$data['from_date']		= ($from_date != "") 	? $from_date 	: $this->input->post("from_date");
			 
			
			if($data['status'] != "" ){
			   
			   // PREPARE QUERY STRING
			   $query_string = $perpage."/".$data['btn_search']."/".$data['from_date']."/".$data['to_date']."/".$data['status']."/";
			   // CREATE THE WHERE CLAUSE
			   $whr = "DATE_FORMAT(date_created,'%Y-%m-%d') BETWEEN '".$data['from_date']."' AND '".$data['to_date']."' AND  status=".$data['status'];
			   
			}else{
	           // PREPARE QUERY STRING
			   $query_string = $perpage."/".$data['btn_search']."/".$data['from_date']."/".$data['to_date']."/".$data['status']."/";
			   
			   // CREATE THE WHERE CLAUSE
			   $whr = "DATE_FORMAT(date_created,'%Y-%m-%d') BETWEEN '".$data['from_date']."' AND '".$data['to_date']."' AND status = ".$data['status'];
			}
			
			// CHECK FOR EXISTING CLAUSE
			if($where == "") {
				$where = " where ".$whr." and( writer_id = 'admin' or writer_id = '0')";
			} else {
				$where .= " and ".$whr." and( writer_id = 'admin' or writer_id = '0')";
			}
		} else {
			$data['status'] = '1';
			$query_string = $perpage."/";
			$where = " where status =".$data['status'] ." and( writer_id = 'admin' or writer_id = '0')";
		}
	   
	   
	    $order_by = " order by datecreated desc";
	   
		$sel_query="SELECT t_id, tag_name,seourl,status, datecreated FROM ".$tablename.$order_by;

	   if($this->uri->segment(9) == "") {
			$config['uri_segment'] 	= 5;
		} else {
			$config['uri_segment'] 	= 9;
		}
		
	   // MODIFY FULL PATH
		$full_path .= $query_string;
		
		$config['total_rows'] 		= $this->db_function->count_record($sel_query, false);
		$config['per_page'] 		= $perpage;
		$config['base_url'] 		= $full_path;
		$choice 					= $config['total_rows'] / $config["per_page"];
		$config['num_links'] 		= 2;
		$config['full_tag_open'] 	= '<div id="paging" style="float:right; "><ul style="clear:left;">';
		$config['full_tag_close'] 	= '</ul></div>';
		$config['anchor_class'] 	= 'class="btn" ';
		$config['first_tag_open'] 	= '<li class="num_off">';
		$config['first_tag_close'] 	= '</li>';
		$config['last_tag_open'] 	= '<li class="num_off">';
		$config['last_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="num_on">';
		$config['cur_tag_close'] 	= '</li>';
		$config['num_tag_open'] 	= '<li class="num_off">';
    	$config['num_tag_close'] 	= '</li>';
		$config['prev_tag_open'] 	= '<li class="num_off">';
    	$config['prev_tag_close'] 	= '</li>';
		$config['next_tag_open'] 	= '<li class="num_off">';
    	$config['next_tag_close'] 	= '</li>';
		$config['prev_link'] 		= 'PREVIOUS';
    	$config['next_link'] 		= 'NEXT';
		$config['use_page_numbers'] = FALSE;
		// PAGINATION PARAMETER VALUES END 				
		
		
		// PAGINATION PARAMETER INITIALIZE 
		$this->pagination->initialize($config);
		
		// SQL QUERY WITH OFFSET AND PERPAGE LIMIT
		$sql = $sel_query." limit ".$offset.", ".$perpage;
	
        $data["details"] = $this->db_function->get_data($sql);
		
		// GET THE DATA FROM PAGINATION
		$data["today_count"] = $config['total_rows'];
		
        $data["links"] = $this->pagination->create_links();
		
	    $this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $this->page_details);
	    $this->load->view(CMS_FOLDER."manage-tags" ,  $data);
		$this->load->view(CMS_FOLDER.'footer'); 

   }   
   
   public function add(){
	   
		
		$this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $this->page_details);
		$this->load->view(CMS_FOLDER."add-tags");
		$this->load->view(CMS_FOLDER.'footer'); 
    	
   }
   
   public function add_action(){

		$name   = $this->input->post('name');
		$seourl = $this->input->post('seourl');
		$date   = date('Y-m-d H:i:s');
	    $status = $this->input->post('status');
	    if($name !=""){
			
		   $query=$this->db->query("SELECT
										tag_name  
									FROM
									 	tbl_tags
									WHERE 
										tag_name = '".$name."'");
		   
		   if ( $query->num_rows() > 0) {
			  
			   $this->session->set_flashdata('error', 'This Tag Name Is Already Created');
			       redirect(SITE_URL.'cms/manage_tags/add');
		   }
		   else
		   {
			 
				$res = $this->db->query("insert into tbl_tags(tag_name,seourl,status,datecreated) values('".$name."','".$seourl."',".$status.",'".$date."')");
				if($res!="")
				{
				 $this->session->set_flashdata('success', 'New Tag  Is Added');
			     redirect(SITE_URL.'cms/manage_tags/add');
				}
		   }
	
			
		 }
	}
	
	public function edit(){
	   
	     $id  = $_GET['id'];
		 
		 $query=$this->db->query("SELECT
										t_id,tag_name ,seourl,status 
									FROM
									 	tbl_tags
									where  t_id=".$id."");
		   
		   if ( $query->num_rows() > 0) {
			  foreach($query->result() as $res){
				  $data['result'][]=$res;
			  }
		   }
		   //print_r($data);exit;
		$this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $this->page_details);
		$this->load->view(CMS_FOLDER."edit-tags",$data);
		$this->load->view(CMS_FOLDER.'footer'); 
    	
   }
   
   public function edit_action(){
	 
     $id     = $this->input->post('id');
	 $name   = $this->input->post('name');
	 $seourl = $this->input->post('seourl');
	
     $date   = date('Y-m-d H:i:s');
	 $status = $this->input->post('status');
	 if($id){
		$this->db->where("t_id",$_POST['id']);
		$Arr=array();
		$Arr['tag_name'] = $name;
		$Arr['seourl'] = $seourl;
		$Arr['status'] = $status;
		
		$this->db->update(TAGS,$Arr);			   
		$this->session->set_flashdata('success', 'Succesfully updated', 'Succesfully updated');
		redirect(FULL_CMS_URL."/manage_tags/"); 
	 }else{   
	       $this->session->set_flashdata('error', 'Error Ocurred');
		   redirect(FULL_CMS_URL."/manage_tags/");
	 }
  }
      
}

?>