<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class contact_us extends CI_Controller
{
    public $country = "";
	public function __construct(){
	   
		parent::__construct();
		$this->load->helper("general");
		
		
	}
   
	public function index(){
		
		$this->load->view("header");
	    $this->load->view("contact-us");
	    $this->load->view("footer");	
	}
   
}
?>
