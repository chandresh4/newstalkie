<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class submit_content extends CI_Controller
{
    public $country = "";
	public function __construct(){
	   
		parent::__construct();
		$this->load->helper("general");
		$this->load->model("register_model");
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('utm_source'), $this->input->get('affid'), $this->input->get('utm_medium'), $this->input->get('utm_sub'));
		
	}
   
	public function index(){
		
	 $query = $this->db->query("select cat_id,name from tbl_category where status= 1 ORDER BY name ASC;");
     if($query->num_rows()>0){
       foreach($query->result()  as $res){
          $data['result'][]= $res;
	   }
	 }
        $data['result1'] = "new";	   
		//print_r($data);exit;
	 $this->load->view("header");
    
	 $this->load->view("submit_story",$data);
	 $this->load->view("footer");	
	}
	 
	 public function check_user(){
/* 	  echo"hi";exit; */
		if ($this->input->post('email') != "") {
			
					
			$this->form_validation->set_rules('name', 'Name', 'trim|required|');
			$this->form_validation->set_rules('email', 'Mobile', 'trim|required|valid_email');
			$this->form_validation->set_rules('ptitle', 'Title', 'trim|required');
			$this->form_validation->set_rules('category', 'category', 'trim|required');
			$this->form_validation->set_rules('comment', 'Comment', 'trim|required');
			if ($this->form_validation->run() != false) {
				$query=$this->db->query("select email ,is_optin from tbl_contentwriter where email='".$this->input->post('email')."'");
				if($query->num_rows()>0){
					$pro_arr = $this->input->post();
					 foreach($query->result() as $p){
						 $optin = $p->is_optin;
					 }
					 if($optin == 1){
						$result = $this->register_model->update($pro_arr);
					
						if($result == true){
							$data['result1'] = "done";
						}
					   elseif($result == "duplicate"){
							 $data['result1'] ="duplicate";
						}else{
							$data['result1'] = "error";
						}
					 }else{
						 $data['result1'] = "Nonoptin";
					}
	   
				}else{
					$reg_arr = $this->input->post();
					$result = $this->register_model->register($reg_arr);
					if($result == true){
						$data['result1'] = "new_user";
					}
					elseif($result == "duplicate"){
					   $data['result1']="duplicate";
					}else{
					   $data['result1'] = "error";
					}
									
				}
			}else{
			$data['result1'] = "validation_error";
		   }
	    }else{
			$data['result1'] = "error";
		}
		
	 $this->load->view("header");
	
	 $this->load->view("submit_story",$data);
	 $this->load->view("footer");	
	 
	 }
	 
}
?>