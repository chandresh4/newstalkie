<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class category extends CI_Controller
{
    public $country = "";
	public function __construct(){
	   
		parent::__construct();
		$this->load->helper("general");
		$this->load->model("register_model");
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('utm_source'), $this->input->get('affid'), $this->input->get('utm_medium'), $this->input->get('utm_sub'));
		$this->country = isset($_COOKIE["countrytest"]) != "" ? $_COOKIE["countrytest"] : 'in';

	}
   
	public function index(){
	   redirect(SITE_URL);
	   
	}


	public function search($category){
		
	    //Declared Variable
	    $id = $seourl = $data['res']= $data['class']= $headers="";
		$data['result'] = "";		
		$header['metadata'] = "";
		$data['seourl'] = "";
		$data['cat_id'] = "";
		$data['category_name'] = "";
		$data['category_wise_ads'] = "";

		//START CATEGORY QUERY

		$query = $this->db->query("select cat_id,name,seourl,meta_description,meta_keywords from tbl_category where seourl='".$category."' and status = 1 ");
	    
	    if ( $query->num_rows() > 0) {
	    	
	           $category_result	= $query->result();

	           $res = $category_result[0];

			   $data['category_name']  = $res->seourl;
			   $data['cat_id']         = $res->cat_id;
			   $metadata['title']      = $res->name;
	           $metadata['keywords']   = $res->meta_keywords;
	           $metadata['seourl']     = $res->seourl;
			   $metadata['description']= $res->meta_description;

			   $category_wise_ads =  $this->db_function->get_data("select ads_position, web_ads, mob_ads from tbl_category_ads where category_id = ".$res->cat_id." and status =1");

			   if(is_array($category_wise_ads) && count($category_wise_ads) > 0 ){
			   	
			   		 $data['category_wise_ads'] =  (array)$category_wise_ads;
			   }
	        
		}		
		

			if($data['cat_id'] != ""){
			
				$query = $this->db->query("SELECT id,post_title,seourl,post_image,date_created FROM tbl_post WHERE country = '".$this->country."' and category = ".$data['cat_id']." and status = 1 ORDER BY id DESC limit 25" );
			}else if($category == "viral"){

				$data['category_name'] = "Viral News";

				$query = $this->db->query("SELECT id,post_title,seourl,post_image,date_created FROM tbl_post WHERE country = '".$this->country."' and viral_status=1 and status = 1 ORDER BY id DESC limit 25" );
			}
				
				
			if ( $query->num_rows() > 0) {
			 	$count=$query->num_rows();
			 	foreach($query->result() as $res){
			  		$data['result'][] = $res;
				}
			}


			
		
		
		$this->load->view("header",$headers);

		$this->load->view("category",$data);
		
		$this->load->view("footer");
			    
	}

	
	public function story($id,$seourl){

		$keywords=$headers="";
		$data['newTime'] = "";
		if($id != "" && $seourl != ""){

	    $query    = $this->db->query("select id,category,tag,post_title,seourl,post_sub_title,post_image,post_desciption,date_created,link, meta_keywords, meta_description, alt_text from tbl_post where country = '".$this->country."' and id='".$id ."' and seourl ='".$seourl."'and status = 1");
	    
	    if ( $query->num_rows() > 0) {
		   
	       $res = $query->result();
		   $data['details'] = $res[0];
		   $title = $res[0]->post_title;
		   $sub_title = $res[0]->post_sub_title;
		   $category = $res[0]->category;
		   $date    = $res[0]->date_created;
		   $startTimeStamp = strtotime($date);
    	   $data['newTime'] = date('l, jS F Y', $startTimeStamp);
    	   $meta_keywords = $res[0]->meta_keywords;
		   $meta_description = $res[0]->meta_description;
		   $meta_og_image = $res[0]->post_image;
		   
	    
	    }else{
		   $data['details']= "";
		   redirect(SITE_URL);
		}
		
	    
		$query    = $this->db->query("select cat_id,name,seourl from tbl_category where cat_id = ".$category." and status = 1");
	    if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			  
			   $data['category']= $res->name;
			   $data['seourl']= $res->seourl;
		   }
	    }
		
		$query1    = $this->db->query("select id,inner_post_title,inner_post_desc,inner_post_image from tbl_inner_post where parent_post_id='".$id ."' and status = 1");
	    if ( $query1->num_rows() > 0) {
		   foreach($query1->result() as $res1){
			   $data['result'][] = $res1;
			   
		   }
	    }else{
		   $data['result']= "";

		}
			 
		//extra posts
		$query    = $this->db->query("SELECT id,post_title,seourl,post_image, alt_text FROM  `tbl_post` WHERE country = '".$this->country."' and id!=".$id." and status = 1 order by id desc limit 10 offset 0");
	    if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			   $data['result1'][]= $res;
		   }
	    }
	    else{
		   $data['result1']= "";
	    }
	    }else{
	    	//echo $id."--".$seourl;
	    	//die();
			redirect(SITE_URL);
		}
		
		$metadata['title'] = $title;
		$metadata['meta_keywords'] = $meta_keywords;
		$metadata['meta_description'] = $meta_description;
		$metadata['meta_og_image'] = $meta_og_image;
		$headers = $metadata;
		
		
		//ads
		/*
		$query1    = $this->db->query("select id,ad_name,banner,link from tbl_post_ads where post_id='".$id ."' and status = 1");
	    if ( $query1->num_rows() > 0) {
		   foreach($query1->result() as $res1){
			   $data['ads_res'][] = $res1;
			   
		   }
	    }
	    else{
		   $data['ads_res']= "";

		} */
		
		$this->load->view("header",$headers);
        //$this->load->view("popup",$data1);
	    $this->load->view("story-result",$data);
	    $this->load->view("footer");
      }
	
    
    /*  
    public function post_review(){
	   $v = 0;
	  $id = $this->input->post('id');
	  $val = $this->input->post('value');
	  $query = $this->db->query("select id,post_id,happy,angry,sad,neutral from tbl_postreview where post_id =".$id);
	  $happy = $angry = $sad = $neutral ="";
	  if($query->num_rows() >0){
		 foreach($query->result() as $p){
			if($val == "happy"){
			    $happy = $p->happy+1;
				$v = $p->happy+1;
		    }else{
			    $happy =$p->happy;
			}
			if($val == "angry"){
				$angry = $p->angry+1;
				$v = $p->angry+1;
			}else{
				$angry =$p->angry;
			}
			if($val == "sad"){
				$sad = $p->sad+1;
				$v = $p->sad+1;
			}else{
				$sad =$p->sad;
			}
			if($val == "neutral"){
				$neutral = $p->neutral+1;
				$v = $p->neutral+1;
			}else{
				$neutral =$p->neutral;
			}
		 }
		 $this->db->where("post_id",$id);
		 $array['post_id'] = $id;
		 $array['happy']   = $happy;
		 $array['angry']   = $angry;
         $array['sad']     = $sad;
		 $array['neutral'] = $neutral;
         $array['date_created']	= date('Y-m-d H:i:s');	 
		 $this->db->update(REVIEW,$array);
		 echo $val."_".$v;
		
	  }else{
		   
		 $happy = $angry = $sad = $neutral = 0;
		 if($val == "happy"){
			 $happy = 1;
			 $v = 1;
		 }
		 if($val == "angry"){
			 $angry = 1;
			 $v = 1;
		 }
		 if($val == "sad"){
			 $sad = 1;
			 $v = 1;
		 }
		 if($val == "neutral"){
			 $neutral = 1;
			 $v = 1;
		 }
		 $array['id']      = '';
		 $array['post_id'] = $id;
		 $array['happy']   = $happy;
		 $array['angry']   = $angry;
         $array['sad']     = $sad;
		 $array['neutral'] = $neutral;
         $array['date_created']	= date('Y-m-d H:i:s');	 
         $this->db->insert(REVIEW,$array);	 
		  echo $val."_".$v;
	  }
	   exit;
   }*/

   
   public function getdata(){
	  
		$limit=$_GET['limit'];
	    $offset=$_GET['offset'];
	    $category=$_GET['category'];
		$data=$id=$count=$total_count="";
		$query = $this->db->query("select cat_id,seourl from tbl_category where seourl='".$category."' and status = 1 ");
		if ( $query->num_rows() > 0) {
			foreach($query->result() as $res){
				 $id = $res->cat_id;
				 $seourl = $res->seourl;
			}
	    }
	    
	    $query = $this->db->query("SELECT id,post_title,seourl,post_image FROM  `tbl_post` WHERE  country = '".$this->country."' and `category` = ".$id." and status = 1");
		$total_count=$query->num_rows();

			if($id != ""){
			  $query = $this->db->query("SELECT id,post_title,seourl,post_image FROM  `tbl_post` WHERE country = '".$this->country."' and `category` = ".$id." and status = 1 ORDER BY id DESC limit ".$limit." offset ".$offset);
			  if ( $query->num_rows() > 0) {
				 $count=$query->num_rows();
				 foreach($query->result() as $res){
				  $data[]= $res;
				}
			  }else{
				  $data= "";
			  }
		   }
	       
		exit;
   }
  
	   
}
?>