<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class movies extends CI_Controller
{
    public $country = "";
	public function __construct(){
	   
		parent::__construct();
		$this->load->helper("general");
		$this->load->model("register_model");
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('utm_source'), $this->input->get('affid'), $this->input->get('utm_medium'), $this->input->get('utm_sub'));
			 
	}
   
	public function index(){
	   $query = $this->db->query("select mid,movie_name,seourl,image from tbl_movies where status = '1' order by date_created  ");
	   if($query->num_rows()>0){
		   foreach($query->result() as $p){
			   $data['results'][]= $p;
			   
		   }
	   }
	   $query = $this->db->query("SELECT id,name from tbl_cities  ORDER BY name ");
	    if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			   $data1['cities'][]= $res;
			   
		   }
	    }else{
			$data1['cities']= "";
			
		}
	   
	   $this->load->view("header");
	    $this->load->view("popup",$data1);
	   $this->load->view("movies",$data);
	   $this->load->view("footer");
	   
	}
	
	
	public function movie_details($seourl){
		
		$query    = $this->db->query("select mid,language,movie_name,details,seourl,image,link from tbl_movies where seourl ='".$seourl."'and status = 1");
	    if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			   $data['movie_res'][]= $res;
			   $metadata['title'] = $res->movie_name;
			   $metadata['keywords'] = $res->movie_name;
			   $metadata['description'] = "";
		   }
		}else{
			$data['movie_res']="";
		}
		$query = $this->db->query("SELECT id,name from tbl_cities  ORDER BY name ");
	    if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			   $data1['cities'][]= $res;
			   
		   }
	    }else{
			$data1['cities']= "";
			
		}
		
	   $headers['metadata'] =$metadata;
	   $this->load->view("header",$headers);
       $this->load->view("popup",$data1);
	   $this->load->view("movie-details",$data);
	   $this->load->view("footer");
	}
	
	 
	 
}
?>