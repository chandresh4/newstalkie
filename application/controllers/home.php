<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class home extends CI_Controller
{
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper("general");
		$this->load->model("register_model");
	    $this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('utm_source'), $this->input->get('affid'), $this->input->get('utm_medium'), $this->input->get('utm_sub'));
		
		$uri = current_url(true);

		//$this->country = isset($_COOKIE["countrytest"]) != "" ? $_COOKIE["countrytest"] : check_uri_for_country($uri);
		$this->country = check_uri_for_country($uri);

		
	}
   
    public function index($optin_val= ""){ 
	  
	   $data['news'] = array();

	   $query  = $this->db->query("SELECT id,post_title,post_sub_title,seourl,post_image FROM  `tbl_post` where country = '".$this->country."' and status = 1 and category != 14 and  (writer_id = 'admin' or writer_id = '0') order by date_created desc limit 35 ");
	    if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			  $result[] = $res;
		   }
		   $data['news'] = $result;
	    }

	    //FOR ENTERTAINMENT POST

	    $data['entertainment'] = array();

	    $query_entertainment  = $this->db->query("SELECT id,post_title,seourl,post_image FROM  `tbl_post` where country = '".$this->country."' and status = 1 and category = 14 and (writer_id = 'admin' or writer_id = '0') order by date_created desc limit 7 ");
	    
	    if ( $query_entertainment->num_rows() > 0) {
		   foreach($query_entertainment->result() as $res_entrtnmt){
			  $result1[] = $res_entrtnmt;
		   }
		   $data['entertainment'] = $result1;
	    }

		
	   $this->load->view("header");
	   //$this->load->view("popup",$data1);
	   
	   //$this->load->view("home",$data);

	   $this->load->view("home",$data);
	   
	   $this->load->view("footer");
	}
   
   
   
   
}
?>