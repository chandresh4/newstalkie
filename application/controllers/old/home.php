<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class home extends CI_Controller
{
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper("general");
		$this->load->model("register_model");
	    $this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('utm_source'), $this->input->get('affid'), $this->input->get('utm_medium'), $this->input->get('utm_sub'));
		
		$uri = current_url(true);

		//$this->country = isset($_COOKIE["countrytest"]) != "" ? $_COOKIE["countrytest"] : check_uri_for_country($uri);
		$this->country = check_uri_for_country($uri);

		
	}
   
    public function index($optin_val= ""){ 
	  
	  
	  //to get news id 
       $query    = $this->db->query("SELECT cat_id FROM `tbl_category` where status = 1 and name = 'news'");
	    if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			   $news_id= $res->cat_id;
		   }
	    }
       // to get news 
       $result="";
       $data['news'] = "";
       $data['news1'] = "";	   
	   
	   $query  = $this->db->query("SELECT id,post_title,seourl,post_image FROM  `tbl_post` where country = '".$this->country."' and status = 1 and category =".$news_id." and (writer_id = 'admin' or writer_id = '0') order by date_created desc limit 6 ");
	    if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			  $result[] = $res;
		   }
		   	$data['news'] = array_slice($result, 0, 3);
        	$data['news1'] = array_slice($result, 3, 6); 
	    }

		
	   
     //viral stories	  
	  $query    = $this->db->query("SELECT id,post_title,seourl,post_image FROM  `tbl_post` where country = '".$this->country."' and status = 1 and trendy_status = 1 and category != ".$news_id." and (writer_id = 'admin' or writer_id = '0 ') order by date_created desc limit 3");
	    if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			   $data['details'][]= $res;
		   }
	    }else{
			$data['details']= "";
		}
		
		//viral stories start
		$data['res']  = "";
		$data['res1'] = "";
		$result="";
		$query    = $this->db->query("SELECT id,post_title,seourl,post_image FROM  `tbl_post` where country = '".$this->country."' and status = 1 and viral_status = 1 and category != ".$news_id." and (writer_id = 'admin' or writer_id = '0') order by date_created desc limit 8 ");
	    if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			   $result[]= $res;
		   }

		   	$data['res'] = array_slice($result, 0, 4);
        	$data['res1'] = array_slice($result, 4, 8);
	    }

		
		//to get writers content
		//echo "SELECT id,post_title,writer_id,seourl,post_image FROM  `tbl_post` where status = 1 and live_status = 1 and (writer_id != 'admin' or writer_id != '0') order by id desc limit 6 offset 0";echo"<br>";
		/* $query    = $this->db->query("SELECT id, post_title, writer_id, seourl, post_image FROM  `tbl_post` WHERE STATUS =1 AND live_status =1 AND (writer_id !=  'admin' OR writer_id !=  '0')ORDER BY date_created DESC LIMIT 6 ");

	    if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			   $data['content'][]= $res;
		   }
	    
		}else{
			$data['content'] = "";
		}
		
		//to get writers details
		$query    = $this->db->query("SELECT id,name from tbl_contentwriter where is_optin = 1");
	    if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			   $data['writer'][$res->id]= $res->name;
		   }
	    } */
	    $data['movies'] = "";
		$query    = $this->db->query("SELECT mid,movie_name,language,seourl,image from tbl_movies where status = 1 order by date_created desc limit 6");
	    if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			   $data['movies'][]= $res;
		   }
	    }

		$result="";
		$data['videos'] = "";
		$data['video'] = "";
		
		$query = $this->db->query("SELECT id,post_title,link,seourl,post_image from tbl_post where country = '".$this->country."' and (link != ' ' or  link= 'no link') and status = 1 order by date_created desc limit 6");
	    if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			   $result[]= $res;
		
			}

			$data['videos'] = array_slice($result, 0, 1);
	        $data['video'] = array_slice($result, 1, 5);	

	    }
		
		
		
		/*$query = $this->db->query("SELECT id,name from tbl_cities  ORDER BY name ");
	    if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			   $data1['cities'][]= $res;
			   
		   }
	    }else{
			$data1['cities']= "";
			
		}
		*/
	   $this->load->view("header");
	   //$this->load->view("popup",$data1);
	   $this->load->view("home",$data);
	   $this->load->view("footer");
	}
   
   
   
   
}
?>