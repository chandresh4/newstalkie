<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class thankyou extends CI_Controller
{
    public $country = "";
	public function __construct(){
	   
		parent::__construct();
		$this->load->helper("general");
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('utm_source'), $this->input->get('affid'), $this->input->get('utm_medium'), $this->input->get('utm_sub'));
		
		
	}
   
	public function index($optin_val = ""){
		if($optin_val != ""){
			$query = $this->db->query("select id,name,email from tbl_contentwriter where email_token='".$optin_val."'");
			if($query->num_rows()>0){
				foreach($query->result() as $p){
					$id=$p->id;
				}
			}
			$this->db->where("email_token", $optin_val);
			$user_verify = array(
								'is_optin'   => 1,		  
								
						  );
			$this->db->update(CONTENTS,$user_verify);
			/*$this->db->where("writer_id", $id);
			$post_verify = array(
								'status'   => 1,		  
								
						  );
			$this->db->update(POSTS,$post_verify);*/
			$data['result1']="done";
		}else{
			$data['result1']="error";
		}
		$query = $this->db->query("SELECT id,name from tbl_cities  ORDER BY name ");
	    if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			   $data1['cities'][]= $res;
			   
		   }
	    }else{
			$data1['cities']= "";
			
		}
		$this->load->view("header");
	    $this->load->view("popup",$data1);
	    $this->load->view("submit_story",$data);
	    $this->load->view("footer");	
	}
   public function subscribe($optin_val = ""){
		if($optin_val != ""){
			$query = $this->db->query("select subscribe_id,name,email from tbl_subscribers where email_token='".$optin_val."'");
			if($query->num_rows()>0){
				
				$this->db->where("email_token", $optin_val);
			    $user_verify = array(
								'is_optin'   => 1,		  
								
						  );
			    $this->db->update("tbl_subscribers",$user_verify);
			    $data['result1']="subscribe";
			}else{
				
				$data['result1']="error";
			}
		
		}else{
			$data['result1']="error";
		}
		$query = $this->db->query("SELECT id,name from tbl_cities  ORDER BY name ");
	    if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			   $data1['cities'][]= $res;
			   
		   }
	    }else{
			$data1['cities']= "";
			
		}
		$this->load->view("header");
	    //$this->load->view("popup",$data1);
	    $this->load->view("submit_story",$data);
	    $this->load->view("footer");	
	}
}
?>
