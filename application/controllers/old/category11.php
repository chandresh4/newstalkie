<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class category extends CI_Controller
{
    public $country = "";
	public function __construct(){
	   
		parent::__construct();
		$this->load->helper("general");
		$this->load->model("register_model");
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('utm_source'), $this->input->get('affid'), $this->input->get('utm_medium'), $this->input->get('utm_sub'));
		
			
	}
   
	public function index(){
	   redirect(SITE_URL);
	   
	}
	 public function search($category){
		
	    //all category pages
	    $id = $seourl = $data['res']= $data['class']= $headers="";
		if($category == "science-technology"){
	      $data['res'] = "Science & Technology";
		  $data['class'] = "science_banner";
		}
		if($category == "lifestyle"){
	      $data['res'] = "Lifestyle";
		  $data['class'] = "lifestyle_banner";
		}
		if($category == "travel-adventure"){
	      $data['res'] = "Travel & Adventure";
		  $data['class'] = "travel_banner";
		}
		if($category == "animals"){
	      $data['res'] = "Animals";
		  $data['class'] = "animals_banner";
		}
		if($category == "history-culture"){
	      $data['res'] = "History & Culture";
		  $data['class'] = "history_banner";
		}
		if($category == "health-fitness"){
	      $data['res'] = "Health & Fitness";
		  $data['class'] = "health_banner";
		}
		if($category == "inspiration"){
	      $data['res'] = "Inspiration";
		  $data['class'] = "inspiration_banner";
		}
		if($category == "sports"){
	      $data['res'] = "Sports";
		  $data['class'] = "sports_banner";
		}
		if($category == "politics"){
	      $data['res'] = "Politics";
		  $data['class'] = "politics_banner";
		}
		if($category == "entertainment"){
	      $data['res'] = "Entertainment";
		  $data['class'] = "entertainment_banner";
		}
		if($category == "environment"){
	      $data['res'] = "Environment";
		  $data['class'] = "Environment_banner";
		}
		if($category == "celebrity"){
	      $data['res'] = "celebrity";
		  $data['class'] = "celebrity_banner";
		}
		if($category == "food"){
	      $data['res'] = "Food";
		  $data['class'] = "food_banner";
		}
		
		if($category != "viral" ){
		  if($category !="Trendy"){
			// echo"select cat_id,name,seourl,meta_description,meta_keywords from tbl_category where seourl='".$category."' and status = 1";exit;
			 $query = $this->db->query("select cat_id,name,seourl,meta_description,meta_keywords from tbl_category where seourl='".$category."' and status = 1 ");
		     if ( $query->num_rows() > 0) {
		         foreach($query->result() as $res){
				   $data['seourl']= $res->seourl;
				   $metadata['title'] = $res->name;
		           $metadata['keywords'] = $res->meta_keywords;
		           $metadata['seourl'] = $res->seourl;
				   $metadata['description'] = $res->meta_description;
		         }
		      }else{
				  $metadata = "";
			  }
			
			  $headers['metadata'] =$metadata;
		      
			$query = $this->db->query("SELECT id,name from tbl_cities  ORDER BY name ");
			if ( $query->num_rows() > 0) {
			   foreach($query->result() as $res){
				   $data1['cities'][]= $res;
				   
			   }
			}else{
				$data1['cities']= "";
				
			}
			$this->load->view("header",$headers);
			$this->load->view("popup",$data1);
			$this->load->view("category",$data);
			$this->load->view("footer");
		    }
			
		}
		
		///for viral page
	   if($category == "viral"){
		$data['seourl']="viral";
		$query    = $this->db->query("SELECT id,post_title,seourl,post_image FROM  `tbl_post` WHERE  `trendy_status` = 1 and status = 1 ORDER BY id DESC limit 8 offset 0");
	    if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			   $results[]= $res;
		   }
	    }
	    else{
		   $results[]= "";
	    }
		$data['result'] = array_slice($results, 0, 4);
        $data['result1'] = array_slice($results, 4, 8); 
		$data['check']="trendy";
		$headers['metadata'] = "";
		$query = $this->db->query("SELECT id,name from tbl_cities  ORDER BY name ");
	    if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			   $data1['cities'][]= $res;
			   
		   }
	    }else{
			$data1['cities']= "";
			
		}
		$this->load->view("header",$headers);
        $this->load->view("popup",$data1);
	    $this->load->view("category-result",$data);
	    $this->load->view("footer");	   
	   }
	   ///for trendy page
	   if($category == "Trendy"){
		 $data['seourl']="trendy";
		 $query    = $this->db->query("SELECT id,post_title,category,seourl,post_image FROM  `tbl_post` WHERE  `trendy_status` = 1 and status = 1 ORDER BY id DESC");
	     if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			   $data['details'][]= $res;
		   }
	     }else{
		   $data['deatils']= "";
		   $headers['metadata'] ="";
	     }
		 $query    = $this->db->query("SELECT id,post_title,seourl,post_image FROM  `tbl_post` WHERE  `viral_status` = 1 and status = 1 ORDER BY id DESC limit 8 offset 0");
	     if ( $query->num_rows() > 0) {
		    foreach($query->result() as $res){
			   $results[]= $res;
		    }
	     }else{
		   $results[]= "";
	     }
		 $data['result'] = array_slice($results, 0, 4);
         $data['result1'] = array_slice($results, 4, 8); 
		 $data['check']="";
		 $data['search_res'] = "trendy";
		 $headers['metadata'] = "";
		 $query = $this->db->query("SELECT id,name from tbl_cities  ORDER BY name ");
	     if ( $query->num_rows() > 0) {
		    foreach($query->result() as $res){
			   $data1['cities'][]= $res;
            }
	     }else{
			$data1['cities']= "";
		 }
		 $this->load->view("header",$headers);
         $this->load->view("popup",$data1);
	     $this->load->view("category-result",$data);
	     $this->load->view("footer");
	    }
	    
	  }
	
	 public function story($id,$seourl){
		
		$keywords=$headers="";
		if($id != "" && $seourl != ""){
	    $query    = $this->db->query("select id,category,tag,post_title,seourl,post_sub_title,post_image,post_desciption,date_created,link,writer_id from tbl_post where id='".$id ."' and seourl ='".$seourl."'and status = 1");
	    if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			   $data['details'][] = $res;
			   $title = $res-> post_title;
			   $sub_title = $res-> post_sub_title;
			   $category = $res->category;
			   $tag = $res->tag;
			   $date = $res->date_created;
			   $writer = $res->writer_id;
		   }
	    }
	    else{
		   $data['details']= "";
		   redirect(SITE_URL);
		}
		
		$query    = $this->db->query("select id,name from tbl_contentwriter where id='".$writer ."' and status = 1");
	    if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			   $data['writer_name']= $res->name;
		   }
		}else{
			 $data['writer_name']= "Locallaunde";
		}
	    $startTimeStamp = strtotime($date);
	    $endTimeStamp = strtotime(date('Y-m-d H:i:s'));
	    $timeDiff = abs($endTimeStamp - $startTimeStamp);
	    $numberDays = $timeDiff/86400;  // 86400 seconds in one day
	    // and you might want to convert to integer
	    $numberDays = intval($numberDays);
		
		$data['days']= $numberDays;
	    $query    = $this->db->query("select cat_id,name,seourl from tbl_category where cat_id = ".$category." and status = 1");
	    if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			  
			   $data['category']= $res->name;
			   $data['seourl']= $res->seourl;
		   }
	    }
		if($tag != ""){
		$query1    = $this->db->query("select t_id,tag_name,seourl from tbl_tags where status = 1 and t_id in (".$tag.")");
	    if ( $query1->num_rows() > 0) {
		   foreach($query1->result() as $q){
			   $data['tag'][] = $q;
			   $keywords.=$q->tag_name.",";  
		   }
	    }
	    else{
		   $data['tag']= "";

		   }
		}else{
		   $data['tag']= "";

		   }
		$query1    = $this->db->query("select id,inner_post_title,inner_post_desc,inner_post_image from tbl_inner_post where parent_post_id='".$id ."' and status = 1");
	    if ( $query1->num_rows() > 0) {
		   foreach($query1->result() as $res1){
			   $data['result'][] = $res1;
			   
		   }
	    }
	    else{
		   $data['result']= "";

		   }
		 //review 
         $query = $this->db->query("select id,post_id,happy,angry,sad,neutral from tbl_postreview where post_id =".$id);
	     if($query->num_rows() >0){	
		   foreach($query->result() as $value){
			   $data['happy'] = $value->happy;
			   $data['sad'] = $value->sad;
			   $data['neutral'] = $value->neutral;
			   $data['angry'] = $value->angry;
		   }
		 }else{
			   $data['happy'] = 0;
			   $data['sad'] = 0;
			   $data['neutral'] = 0;
			   $data['angry'] = 0;
         }			 
		//extra posts
		$query    = $this->db->query("SELECT id,post_title,seourl,post_image FROM  `tbl_post` WHERE id!=".$id." and status = 1 order by id desc limit 6 offset 0");
	    if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			   $data['result1'][]= $res;
		   }
	    }
	    else{
		   $data['result1']= "";
	    }
	    }else{
			redirect(SITE_URL);
		}
		
		$metadata['title'] = $title;
		$metadata['keywords'] = rtrim($keywords,',');
		$metadata['description'] = $sub_title;
		$headers['metadata'] =$metadata;
		$query = $this->db->query("SELECT id,name from tbl_cities  ORDER BY name ");
	    if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			   $data1['cities'][]= $res;
			   
		   }
	    }else{
			$data1['cities']= "";
			
		}
		$this->load->view("header",$headers);
        $this->load->view("popup",$data1);
	    $this->load->view("story-result",$data);
	    $this->load->view("footer");
      }
	
   public function post_review(){
	   $v = 0;
	  $id = $this->input->post('id');
	  $val = $this->input->post('value');
	  $query = $this->db->query("select id,post_id,happy,angry,sad,neutral from tbl_postreview where post_id =".$id);
	  $happy = $angry = $sad = $neutral ="";
	  if($query->num_rows() >0){
		 foreach($query->result() as $p){
			if($val == "happy"){
			    $happy = $p->happy+1;
				$v = $p->happy+1;
		    }else{
			    $happy =$p->happy;
			}
			if($val == "angry"){
				$angry = $p->angry+1;
				$v = $p->angry+1;
			}else{
				$angry =$p->angry;
			}
			if($val == "sad"){
				$sad = $p->sad+1;
				$v = $p->sad+1;
			}else{
				$sad =$p->sad;
			}
			if($val == "neutral"){
				$neutral = $p->neutral+1;
				$v = $p->neutral+1;
			}else{
				$neutral =$p->neutral;
			}
		 }
		 $this->db->where("post_id",$id);
		 $array['post_id'] = $id;
		 $array['happy']   = $happy;
		 $array['angry']   = $angry;
         $array['sad']     = $sad;
		 $array['neutral'] = $neutral;
         $array['date_created']	= date('Y-m-d H:i:s');	 
		 $this->db->update(REVIEW,$array);
		 echo $val."_".$v;
		
	  }else{
		   
		 $happy = $angry = $sad = $neutral = 0;
		 if($val == "happy"){
			 $happy = 1;
			 $v = 1;
		 }
		 if($val == "angry"){
			 $angry = 1;
			 $v = 1;
		 }
		 if($val == "sad"){
			 $sad = 1;
			 $v = 1;
		 }
		 if($val == "neutral"){
			 $neutral = 1;
			 $v = 1;
		 }
		 $array['id']      = '';
		 $array['post_id'] = $id;
		 $array['happy']   = $happy;
		 $array['angry']   = $angry;
         $array['sad']     = $sad;
		 $array['neutral'] = $neutral;
         $array['date_created']	= date('Y-m-d H:i:s');	 
         $this->db->insert(REVIEW,$array);	 
		  echo $val."_".$v;
	  }
	   exit;
   }
   
   public function getdata(){
	  
	$limit=$_GET['limit'];
    $offset=$_GET['offset'];
    $category=$_GET['category'];
	$data=$id=$count=$total_count="";
	$query = $this->db->query("select cat_id,seourl from tbl_category where seourl='".$category."' and status = 1 ");
	if ( $query->num_rows() > 0) {
		foreach($query->result() as $res){
			 $id = $res->cat_id;
			 $seourl = $res->seourl;
		}
    }
    $query = $this->db->query("SELECT id,post_title,seourl,post_image FROM  `tbl_post` WHERE  `category` = ".$id." and status = 1");
	$total_count=$query->num_rows();

	if($id != ""){
	  $query = $this->db->query("SELECT id,post_title,seourl,post_image FROM  `tbl_post` WHERE  `category` = ".$id." and status = 1 ORDER BY id DESC limit ".$limit." offset ".$offset);
	  if ( $query->num_rows() > 0) {
		 $count=$query->num_rows();
		 foreach($query->result() as $res){
		  $data[]= $res;
		}
	  }else{
		  $data= "";
	  }
   }
   $value = "";
   if($count == $total_count ){
	   $value = "less";
   }else{
	   if($count < $limit ){
		    $value = "less";
	   }else{$value = "";}
	   
   }
  
    $i=0;$val="";
   if(!empty($data)){
	  
     foreach($data as $p1){
		 $img ='thumb-'.$p1->post_image;
		 $val .="<div class='col-sm-4'>
						<div class='col-item'>
						<a href='".SITE_URL."category/story/".$p1->id."/".$p1->seourl."'>
							<div class='photo'>
								 <img src='".S3_URL."site/images/posts/medium_postimage_crop/".$img."' class='img-responsive' alt='".$p1->post_title."' />
							</div>
							<div class='info'>
								<div class='row'>
									<div class='price col-md-12'>
									".substr($p1->post_title,0,40)."
									</div> 
								</div> 
								<div class='clearfix'>
								</div>
							</div>
							</a>
						</div>
					</div>
		 ";
	}
	echo $val."|".$value;
   }else{
	 echo $data ;
   }
	exit;
   }
   
   public function get_result(){
	$limit=$_GET['limit'];
    $offset=$_GET['offset'];
    $category=$_GET['category'];
	$data=$id=$count=$total_count=$val=$value="";  
	if($category =="viral"){
		$query = $this->db->query("SELECT id,post_title,category,seourl,post_image FROM  `tbl_post` WHERE  `viral_status` = 1 and status = 1");
	    $total_count=$query->num_rows();
		$query    = $this->db->query("SELECT id,post_title,category,seourl,post_image FROM  `tbl_post` WHERE  `viral_status` = 1 and status = 1 ORDER BY id DESC limit ".$limit." offset ".$offset);
	    if ( $query->num_rows() > 0){
			 $count=$query->num_rows();
		     foreach($query->result() as $res){
			   $data[]= $res;
			 } 
	    }else{
		   $data= "";
		}
		if($count == $total_count ){
	       $value = "less";
        }else{
	       if($count < $limit ){
		      $value = "less";
	       }else{
			  $value = "";
		   }
	    }
	}
	if($category == "trendy"){
		$query = $this->db->query("SELECT id,post_title,category,seourl,post_image FROM  `tbl_post` WHERE  `trendy_status` = 1 and status = 1");
	    $total_count=$query->num_rows();
		$query    = $this->db->query("SELECT id,post_title,category,seourl,post_image FROM  `tbl_post` WHERE  `trendy_status` = 1 and status = 1 ORDER BY id DESC limit ".$limit." offset ".$offset);
	    if ( $query->num_rows() > 0) {
		   $count=$query->num_rows();
		   foreach($query->result() as $res){
			   $data[]= $res;
		   }  
	    }
	    else{
		   $data= ""; 
	    }
		if($count == $total_count ){
	       $value = "less";
        }else{
	       if($count < $limit ){
		      $value = "less";
	       }else{
			  $value = "";
		   }
	    }
	}
	
	if(!empty($data)){
     foreach($data as $p){
		 $img ='thumb-'.$p->post_image;
		 $val.= "<a class='search_items' href='".SITE_URL."category/story/".$p->id."/".$p->seourl."'>
				<div class='search_item row'>
					<div class='col-sm-3'>
						<img src='".S3_URL."site/images/posts/medium_postimage_crop/".$img."' class='img-responsive' alt='".$p->post_title."' />
					</div>
					<div class='col-sm-8' style='padding-left: 0px'>
						<h1 class='items_name'>".substr($p->post_title,0,40)."</h1>
					</div>
				</div>
			</a>
		 ";
	}
	 echo $val."|".$value;
   }else{
	 echo $data ;
   }
	exit;
   }
	   
}
?>