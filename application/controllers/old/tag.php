<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class tag extends CI_Controller
{
    public $country = "";
	public function __construct(){
	   
		parent::__construct();
		$this->load->helper("general");
		$this->load->model("register_model");
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('utm_source'), $this->input->get('affid'), $this->input->get('utm_medium'), $this->input->get('utm_sub'));
		
		$this->country = $_COOKIE["countrytest"] != "" ? $_COOKIE["countrytest"] : 'in';
		
	}
   
	public function index(){
	  $query = $this->db->query("select t_id,tag_name,seourl from tbl_tags where status=1");
	  if($query->num_rows() > 0){
		  foreach($query->result() as $p){
			  $data['result'][]=$p;
		  }
	  }else{
		    $data['result'][]="";
	  }
	  /*$query = $this->db->query("SELECT id,name from tbl_cities  ORDER BY name ");
	    if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			   $data1['cities'][]= $res;
			   
		   }
	    }else{
			$data1['cities']= "";
			
		}*/
	    $this->load->view("header");
		//$this->load->view("popup",$data1);
		$this->load->view("tags",$data);
		$this->load->view("footer"); 
	}
	 
	public function tagsearch($id){

	  if($id !=""){
		  $id= $id."-";
		 
		$query = $this->db->query("select t_id,tag_name,seourl from tbl_tags where seourl = '".$id."' and status=1");
		
		if($query->num_rows() > 0){
			
		  foreach($query->result() as $p){
			$tid = $p->t_id;
			
			$data['tag_res'][] = $p;
			
		  }
		
			$query = $this->db->query("SELECT id,category,post_title,seourl,post_image FROM tbl_post WHERE country = '".$this->country."' and FIND_IN_SET('".$tid."',tag) and status=1 order by id desc");
				if($query->num_rows() > 0){
				  foreach($query->result() as $p){
					 $data['results'][] = $p;
					 $category =$p->category;
				  }
				  $query = $this->db->query("select cat_id,name,seourl from tbl_category where cat_id='".$category."' and status = 1");
				  if ( $query->num_rows() > 0) {
					foreach($query->result() as $res){
						$data['seourl']= $res->seourl;
						$data['res']= $res->name;
					}
				   }
				}else{
				  $data['results'] = "";
				}
		}else{
			
		   $data['tag_res'] = "";
		}	
		
		}else{
		  $data['tag_res'] = "";
		  $data['results'] = "";
		}


		$query = $this->db->query("SELECT id,name from tbl_cities  ORDER BY name ");
	    if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			   $data1['cities'][]= $res;
			   
		   }
	    }else{
			$data1['cities']= "";
			
		}


		$this->load->view("header");
		//$this->load->view("popup",$data1);
		$this->load->view("tags-result",$data);
		$this->load->view("footer"); 
	}	
}
?>