<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class page_notfound extends CI_Controller
{
    public $country = "";
	public function __construct(){
	   
		parent::__construct();
		$this->load->helper("general");
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('utm_source'), $this->input->get('affid'), $this->input->get('utm_medium'), $this->input->get('country'),  $this->input->get('zoneid'),  $this->input->get('mobile_track'), $this->input->get('ClickID'));
		
		$this->country = $this->session->userdata('country') != "" ? $this->session->userdata('country') : 'in';
		
	}
   
	public function index(){
	   $this->load->view("header");
	   $this->load->view("page-notfound");
	   $this->load->view("footer");
	}
   
   

   
}
?>