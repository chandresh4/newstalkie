<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class news extends CI_Controller
{
    public $country = "";
	public function __construct(){
	   
		parent::__construct();
		$this->load->helper("general");
		$this->load->model("register_model");
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('utm_source'), $this->input->get('affid'), $this->input->get('utm_medium'), $this->input->get('utm_sub'));
			 
	}
   
	public function index(){
	   $query = $this->db->query("select cat_id,name,seourl,meta_keywords,meta_description from tbl_category where name = 'news'  ");
	   if($query->num_rows()>0){
		   foreach($query->result() as $p){
			
			   $data['seourl']= $p->seourl;
			   $metadata['title'] = $p->name;
		       $metadata['keywords'] = $p->meta_keywords;
		       $metadata['description'] = $p->meta_description;
			   $metadata['seourl'] = $p->seourl;
		   }
		   
	   }else{
		   $metadata ="";
	   }
	   $headers['metadata'] =$metadata;
	   
	   $query = $this->db->query("SELECT id,name from tbl_cities  ORDER BY name ");
	    if ( $query->num_rows() > 0) {
		   foreach($query->result() as $res){
			   $data1['cities'][]= $res;
			   
		   }
	    }else{
			$data1['cities']= "";
			
		}
	
	   $this->load->view("header",$headers);
       $this->load->view("popup",$data1);
	   $this->load->view("news",$data);
	   $this->load->view("footer");
	   
	}
    public function getdata(){
	$limit=$_GET['limit'];
    $offset=$_GET['offset'];
    $category=$_GET['category'];
	$data=$id=$count=$total_count=$val= $value="";
	$query = $this->db->query("select cat_id,seourl from tbl_category where name='".$category."' and status = 1 ");
	if ( $query->num_rows() > 0) {
		foreach($query->result() as $res){
			 $id = $res->cat_id;
			 $seourl = $res->seourl;
		}
    }
	if($id != ""){
	  $query = $this->db->query("SELECT id,post_title,seourl,post_image FROM  `tbl_post` WHERE  `category` = ".$id." and status = 1 ");
      $total_count=$query->num_rows();
	  $query = $this->db->query("SELECT id,post_title,seourl,post_image FROM  `tbl_post` WHERE  `category` = ".$id." and status = 1 ORDER BY id DESC limit ".$limit." offset ".$offset);
	  if ( $query->num_rows() > 0) {
		$count =$query->num_rows();
		foreach($query->result() as $res){
		   $data[]= $res;
		}
	  }else{
		  $data= "";
	  }
	  if($count == $total_count ){
	       $value = "less";
        }else{
	       if($count < $limit ){
		      $value = "less";
	       }else{
			  $value = "";
		   }
	    }
   }
   if(!empty($data)){
     foreach($data as $p1){
		 $img ='thumb-'.$p1->post_image;
		 $val.= "<div class='col-sm-4'>
						<div class='col-item'>
						<a href='".SITE_URL."category/story/".$p1->id."/".$p1->seourl."'>
							<div class='photo'>
								 <img src='".S3_URL."site/images/posts/medium_postimage_crop/".$img."' class='img-responsive' alt='".$p1->post_title."' />
							</div>
							<div class='info'>
								<div class='row'>
									<div class='price col-md-12'>
									".$p1->post_title."
									</div> 
								</div> 
								<div class='clearfix'>
								</div>
							</div>
							</a>
						</div>
					</div>
		 ";
	}
	
	echo $val."|".$value;
   }else{
	 echo $data ;
   }
	exit;
   }	
	
	 
	 
}
?>