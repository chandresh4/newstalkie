<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class subscribe extends CI_Controller
{
    public $country = "";
	public function __construct(){
	   
		parent::__construct();
		$this->load->helper("general");
		$this->load->model("register_model");
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('utm_source'), $this->input->get('affid'), $this->input->get('utm_medium'), $this->input->get('utm_sub'));
		
		
		
	}
   
	public function index(){

	    // print_r($_POST);exit;
		$email = $this->input->post('email'); 
	
		$name 		  = $this->input->post('name');
		$location 	  = $this->input->post('location');	
		$email_tokens = md5(date('Y-m-d H:i:s'));
		
		// CHECK FOR USER REQUEST OF REGISTRATION - START
		if ($email != "") {
			// SET THE VALIDATION FOR FORM SUBMITTION - START
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			
			// SET THE VALIDATION FOR FORM SUBMITTION - END
			if ($this->form_validation->run() != false) {
			  $query=$this->db->query("select email from tbl_subscribers where email='".$this->input->post('email')."'");
              if($query->num_rows()>0){
				 echo "olduser";
			  }else{
				  
				 $register    = array(
								 "name"        => $name,
								 "email"       => $email,
								 "location"    => $location,
								 "email_token" => $email_tokens,
								  "datecreated" =>date('Y-m-d H:i:s')
			     );
			     
				 $res = $this->insert($register);
				
				 $this->locallaunde_mailer->subscribe($email , $name, $email_tokens);

				 if($res == true){
			         echo "done";
				 }else{
					 echo"something went wrong";
				 }	 
			  }
			 }else{
				echo"validation_error";
			}
		}else{
			echo "error";
		}
				
		exit;		
	}
	
	public function insert($array){
		
		$this->db->insert(SUBSCRIBE, $array);
        $insert_id = $this->db->insert_id();
		$utm_source = $utm_medium = $utm_sub ="";
		if($this->session->userdata('utm_source') != '' ) {
			   $utm_source = addslashes(trim($this->session->userdata('utm_source')));
		}
		if($this->session->userdata('utm_medium') != '' ){
			   $utm_medium = addslashes(trim( $this->session->userdata('utm_medium')));
		}
		if($this->session->userdata('utm_sub') != '' ){
			   $utm_sub = addslashes(trim( $this->session->userdata('utm_sub')));
		}
		$sources    = array(
								 "utm_source"  => $utm_source,
								 "utm_medium"  => $utm_medium,
								 "utm_sub"  => $utm_sub,
								 "userid"      => $insert_id,
								 "date_created" =>date('Y-m-d H:i:s')
			     );
			
		$this->db->insert(TRACKER, $sources);
		//$id = $this->db->insert_id();		 
		if($insert_id != ""){
			
			setcookie("lol_newltr", "already", time() + (365 * 24 * 60 * 60), "/");
		}
		
		
        return true;	
		
	}
	public function search()
	{
	  $key =$_POST['keyword'];
	  $tablename= "tbl_cities";
	?>
	<ul id="country-list">
	<?php
	  $query1=$this->db->query("SELECT id,name from 
									 	".$tablename."
									WHERE name like('".$key."%')");
	  if ( $query1->num_rows() > 0) {
		 foreach ($query1->result() as $p1) {
	?>
	<li onClick="selectcamp('<?php echo $p1->name;  ?>');"><?php echo $p1->name; ?></li>
	<?php }} ?>
	</ul>
	<?php 
	  exit;
	}
	
}
?>