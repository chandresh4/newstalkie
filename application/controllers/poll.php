<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class poll extends CI_Controller
{
    public $country = "";
	public function __construct(){
	   
		parent::__construct();
		$this->load->helper("general");
		$this->load->model("register_model");
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('utm_source'), $this->input->get('affid'), $this->input->get('utm_medium'), $this->input->get('utm_sub'));
		
		//$this->country = isset($_COOKIE["countrytest"]) != "" ? $_COOKIE["countrytest"] : 'in';

		$uri = current_url(true);

		$this->country = check_uri_for_country($uri);
	}
   
	public function index(){
	
		$data = "";

	    $this->load->view("header");
		//$this->load->view("popup",$data1);
		$this->load->view("poll",$data);
		$this->load->view("footer"); 
	}
	 
}
?>