<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class about_us extends CI_Controller
{
    public $country = "";
	public function __construct(){
	   
		parent::__construct();
		$this->load->helper("general");
		
		
	}
   
	public function index(){
		
		$this->load->view("header");
	    $this->load->view("about-us");
	    $this->load->view("footer");	
	}
   
}
?>
