<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cities extends CI_Controller
{
    public $country = "";
	public function __construct(){
	   
		parent::__construct();
		$this->load->helper("general");
		$this->load->model("register_model");
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('utm_source'), $this->input->get('affid'), $this->input->get('utm_medium'), $this->input->get('utm_sub'));
		$this->country = isset($_COOKIE["countrytest"]) != "" ? $_COOKIE["countrytest"] : 'in';

	}
   

	public function index(){
		
	    //Declared Variable
	    $id = $seourl = $data['res']= $data['class']= $headers="";
		$data['result'] = "";		
		$header['metadata'] = "";
		$data['seourl'] = "";
		$data['cat_id'] = "";
		$data['category_name'] = "";

		
		$this->load->view("header",$headers);
		$this->load->view("cities",$data);
		$this->load->view("footer");
			    
	}

  
	   
}
?>