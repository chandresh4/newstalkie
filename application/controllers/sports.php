<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class sports extends CI_Controller
{
    public $country = "";
	public function __construct(){
	   
		parent::__construct();
		$this->load->helper("general");
		$this->load->model("register_model");
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('utm_source'), $this->input->get('affid'), $this->input->get('utm_medium'), $this->input->get('utm_sub'));
		$this->country = isset($_COOKIE["countrytest"]) != "" ? $_COOKIE["countrytest"] : 'in';

	}
   

	public function index($currentMatchState=""){

		//Declared Variable
	    $id = $seourl = $data['res']= $data['class']= $headers="";
		$data['result'] = "";		
		$header['metadata'] = "";
		$data['seourl'] = "";
		$data['cat_id'] = "";
		$data['category_name'] = "";
		$data['category_wise_ads'] = "";
		$data['match_list'] = "";
		$data['matches_list'] = array();


		$query = $this->db->query("SELECT id,post_title,post_sub_title,seourl,post_image,date_created FROM tbl_post WHERE country = '".$this->country."' and category = 12 and status = 1 ORDER BY id DESC limit 25" );
		
		if ( $query->num_rows() > 0) {
		 	$count=$query->num_rows();
		 	foreach($query->result() as $result_post){
		  		$data['result'][] = $result_post;
			}
		}

		//Fetch ADS FROM DB  
		$category_wise_ads =  $this->db_function->get_data("select ads_position, web_ads, mob_ads from tbl_category_ads where category_id = 12 and status =1");

	   if(is_array($category_wise_ads) && count($category_wise_ads) > 0 ){
	   	
	   		 $data['category_wise_ads'] =  (array)$category_wise_ads;
	   }
		
		//CALL SCORECARD API

		$currentMatchState = $currentMatchState != "" ? $currentMatchState : "live";

		$data['currentMatchState'] = $currentMatchState;

		$match_list = get_series_match_list();

		if(is_object($match_list->matchList)){

			foreach($match_list->matchList->matches as $matches) {

				if($currentMatchState == "live"){
					
					if(strtolower($matches->currentMatchState) == "live"){

						$data['matches_list'][]	= $matches;
					
					}else if(time() - strtotime($matches->startDateTime) < 43200 && time() - strtotime($matches->startDateTime) > 0) {

						$data['matches_list'][]	= $matches;
					}
					
				}else if(strtolower($matches->status) == $currentMatchState ){

					$data['matches_list'][]	= $matches;
					
				}
			}
		}
		

		
		$this->load->view("header",$headers);
		$this->load->view("sports",$data);
		$this->load->view("footer");
			    
	}

	public function live_cricket_score(){


	}

	public function live_cricket_scorecard($series_id, $match_id){


		//Declared Variable
		$id = $seourl = $data['res']= $data['class']= $headers="";
		$data['full_scorecard'] = "";
		$data['result'] = "";		
		$header['metadata'] = "";
		$data['seourl'] = "";
		$data['cat_id'] = "";
		$data['category_name'] = "";

		$query = $this->db->query("SELECT id,post_title,post_sub_title,seourl,post_image,date_created FROM tbl_post WHERE country = '".$this->country."' and category = 12 and status = 1 ORDER BY id DESC limit 25" );
		
		if ( $query->num_rows() > 0) {
				$count=$query->num_rows();
				foreach($query->result() as $result_post){
					$data['result'][] = $result_post;
			}
		}
		

		if($series_id != "" && $match_id != ""){

			//CALL SCORECARD API

			$scorecard = get_scorecard($series_id, $match_id);

			
			if($scorecard->status != "no results"){

				if(is_object($scorecard->fullScorecard)){

					$data['full_scorecard'] =  $scorecard->fullScorecard->innings;

				}
			}
		}


		$headers['metadata'] = "";
		$this->load->view("header",$headers);
		$this->load->view('cricket/scorecard', $data);
		$this->load->view("footer");		

	}

	public function live_cricket_full_commentary(){

	}

  
	   
}
?>