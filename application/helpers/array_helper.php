<?php
if( ! function_exists("all_arrays")) {
	  function all_arrays() {
		
		$coapp_emptype =  array("1"     =>  "Salaried",
							    "2"     =>  "Self Employed"); 
		
		$pl_full_status = array('1' => "Incomplete",
								'2' => "Submitted",
								'3' => "Reviewed",
								'4' => "Verified",
								'5' => "Bank approval",
								'6' => "Loan Disbursed",
								'7' => "Rejected",
								'8' => "Bank Rejected",
								'9' => "Criteria not match",
								'10' => "Bank Processing",
								'11' => "Documents awaited",
								'12' => "Documents received",
								'13' => "Bank Login",
								);
														
		$caller_status = array("1" => "Qualified for Appointment",
							   "2" => "Appointment rejected",
							   "3" => "Irrelevant documents",
							   "4" => "Irrelevant leads");
		
		$fire_limit_arr = array('1' => "1",
								'2' => "2",
								'3' => "3",
				   			    '4' => "4",
								'5' => "5",
								'6' => "6",
								'7' => "7");
							
		$employe_type   = array('1' => "Salaried",
								'2' => "Self Employed Professional",
								'3' => "Self Employed Business"); 																																			 
		$job_exp  	  = array( "1"  => "0 - 1  years",
							   "2"  => "1 - 2  years",
							   "3"  => "2 - 3  years",
							   "4"  => "3 - 4  years",
							   "5"  => "4 - 5  years",
							   "6"  => "5 - 6  years",
							   "7"  => "6 - 7  years",
							   "8"  => "7 - 8  years",
							   "9"  => "8 - 9  years",
							   "10" => "9 - 10 years",
							   "11" => "10- 11 years",
							   "12" => "11- 12 years",
							   "13" => "12 and above");
							
		$residence_type = array("1"  =>  array("Owned by Spouse","OwnedbySpouse"), 
								"2"  =>  array("Owned by Parents", "Ownedbyparents"),
								"3"	 =>  array("Owned by Me","Ownedbyme"),
								"4"	 =>  array("Rented Family","rentedfamily"),
								"5"	 =>  array("Rented Friends","rentedfriends"), 
								"6"	 =>  array("Rented Staying alone","rentedpg"),
								"7"  =>  array("Company lease", "companylease"), 
								"8"  =>  array("Owned by relative", "ownedbyrelative")
								);
								
		
		$gender  		= array("1"  =>  array("Male","male"), 
								"2"  =>  array("Female","female")); 
								//"3"	 =>  array("Other","other"));						 					
								
		$education     =   array(
								"1" => "10th Std",
								"2" => "12th Std",
								"3" => "BA",
								"4" => "BBA",
								"5" => "BCA",
								"6" => "BCom",
								"7" => "BE/BTech",
								"8" => "BSc",
								"9" => "MA",
								"10" => "MBA",
								"11" => "MCA",
								"12" => "MCom",
								"13" => "MSc",
								"14" => "ME/MTech",
								"15" => "Phd",
								"16" => "Other UG",
								"17" => "Other PG",
								"18" => "Others");	
								
		
		
		$dependents     = array('1' => "1  ",
								'2' => "2  ",
								'3' => "3  ",
				   			    '4' => "4  ",
								'5' => "5+ ");
							
		$industry_type  = array('1' => "Manufacturing/Engg",
								'2' => "Trading",
								'3' => "Services"); 															   
		
		$emp_role	   = array( '1' => "Proprietor",
								'2' => "Partnership Firm",
								'3' => "Pvt Ltd Company",
								'4' => "Director applying as an individual",
								'5' => "Partner applying as an individual",
								'6' => "Others-Business");
								
		$loan_purpose   = array('1' => array("Home Renovation","pl-for-homerelovution"), 
								'2' => array("Marriage","pl-for-marriage"), 
								'3' => array("Medical Emergency","pl-for-medicalemergency"), 
								'4' => array("Higher Education","pl-for-highereducation"), 
								'5' => array("Business","pl-for-business"), 
								'6' => array("Buy Car/Bike","pl-for-carbike"),
								'7' => array("Debts","pl-for-debts"),
								'8' => array("Others","pl-for-others"));						
		
		$app_status    = array( 
								//'1' => "Incomplete",
								'2' => "Submitted",
								'3' => "Reviewed",
								'4' => "Verified",
								'5' => "Bank approval",
								'6' => "Loan Disbursed",
								); 
								
		$salary_bank  =	array(  '1' => array("I receive by cheque","salary-acc-cash"),
								'2' => array("Other Bank","salary-acc-other"),
								'3' => array("RBL Bank","salary-acc-icici"),
								'4' => array("SBI Bank","salary-acc-sbi"),
								'5' => array("Kotak Bank","salary-acc-kotak"),
								'6' => array("HDFC Bank","salary-acc-hdfc"),
								'7' => array("Axis Bank","salary-acc-axis"),
								'8' => array("Standard Chartered","salary-acc-Standard"),
								);
								
		$counts      = array(   '1' => '1',
								'2' => '2',
								'3' => '3',
								'4' => '4',
								'5' => '5');
								//'6' => '6');
								
		$staying_yr  = array(   '1' => '1',
								'2' => '2',
								'3' => '3',
								'4' => '4',
								'5' => '5+');						
								
		$client_type = array(	'1' => 'DSA',
								'2' => 'BANK',
								'3' => 'NBFC');	
								
		$payout_type = array(	'1' => 'CPL',
								'2' => 'COMMISSION',
								'3' => 'REV SHARE');
								
		$user_review = array(	'1' => 'Received',
								'2' => 'Pending', 
								'3' => 'Answered');	
								
		$city_alias  = array(   '4'  => 'simla',
								'32' => 'gurugram',
								'50' => 'delhi',
								'81' => 'cawnpore',
								'85' => 'benares',
								'177'=> 'gauhati',
								'184'=> 'calcutta',
								'242'=> 'indhur',
								'244'=> 'jubbulpore',
								'277'=> 'baroda',
								'305'=> 'bombay',
								'306'=> 'poona',
								'388'=> 'bengaluru',
								'389'=> 'mysuru',
								'391'=> 'mangaluru',
								'392'=> 'belagavi',
								'414'=> 'panjim',
								'417'=> 'cochin',
								'418'=> 'calicut',
								'421'=> 'trivandrum',
								'428'=> 'allepey',
								'435'=> 'madras',
								'446'=> 'tanjore',
								'450'=> 'ootacamund',
								'450'=> 'ooty',
								'467'=> 'pondicherry',
								'469'=> 'tiruchirapalli');												
		
		$creditcard_purpose = array('1' => array("Shopping","cc-Purpose-shopping","shopping.jpg"), 
								'2' => array("Premium","cc-Purpose-premium","prime.jpg"),  
								'3' => array("Cash back","cc-Purpose-cashback","cashback.jpg"),  
								'4' => array("Lifestyle","cc-Purpose-lifestyle","lifestyle.jpg"),  
								'5' => array("Fuel","cc-Purpose-fuel","fuel.jpg"),  
								'6' => array("Airline","cc-Purpose-airline","airline.jpg"), 
								'7' => array("Others","cc-Purpose-other","others.jpg"), 
								//'8' => array("All","cc-Purpose-all","all.jpg")
								);
								
		$creditcard_existing = array('1' => array("Sbi","cc-Purpose-sbi"), 
								'2' => array("City Bank","cc-Purpose-city"), 
								'3' => array("Axis Bank","cc-Purpose-axis"), 
								'4' => array("Icici Bank","cc-Purpose-icici"), 
								'5' => array("Hdfc Bank","cc-Purpose-hdfc"), 
								'6' => array("Standard Chartered","cc-Purpose-standard"),
								'7' => array("I Don't Have","cc-Purpose-null"));
								
		$yes_no             = array( "1" => "Yes", "2" => "No");
		
		$hl_purpose         = array( "1" => array("Construct or Purchase the Property", "purchase-property"), 
									 "2" => array("Transfer Existing home loan", "transfer-loan"),
									 "3" => array("Yet to identify<br>Property", "identify-property"));	
		
		$hl_proposed_property = array( "1" => array("Buying completed project", "buying-project"), 
									   "2" => array("Buying a Plot", "buying-plot"),
									   "3" => array("Build on Land I Own", "buildonland"),
									   "4" => array("Buy Land and Build", "buyland"),
									   "5" => array("Buy under construction project", "under-construction_project"),
									   "6" => array("Others", "other-buyld"));						
		
		$hl_coaplicant       = array( "1" => "Parent",
									  "2" => "Spouse",
									  "3" => "Children");
		
		$rbl_qualification = array( "1" => "1",  "2"  => "1", "3" =>  "2", "4"  => "2", "5"  => "2", "6" => "2", 
									"7" => "2",  "8"  => "2", "9" =>  "3", "10" => "3", "11" => "3", "12" => "3", 
									"13" => "3", "14" => "3", "15" => "3", "16" => "1", "17" => "3", "18" => "9999");
									
		$rbl_city	      = array("gurgaon" =>"7", "hyderabad" =>"15", "bengaluru"=>"19", "bangalore"=>"19",
								  "chennai" =>"21",  "mumbai" =>"25",
						    	  "pune"    =>"26", "noida"    =>"78", "ghaziabad"=>"87", "navi mumbai"=>"163", "new delhi"=>"318",
								  "thane"   =>"640", "faridabad"=>"981", "panvel" =>"1470", "greater noida"=>"704");
								  
								  						   
		$rbl_residence    = array( "1" => "1", "2" => "2", "3" => "1", "4" => "4");	
		
		$rbl_err_code	  = array("1" => "INPUT OUT OF MASTERS RANGE", "2" => "VALIDATION ERROR (FRONT-END RULE FAILURE)", 
								  "3" => "INPUT NOT IN VALID DATA FORMAT (SPECIAL CHARACTERS etc)",
								  "4" => "SYSTEM UNAVAILABLE",         "5" => "DECISION CENTER ERROR (TU ERROR)",
								  "6" => "DUPLICATE APPLICATION");
		
		$rbl_status_code =  array("0" => "FAILURE/ERROR", "1" => "AIP APPROVED", "2" => "Eligible for Lower Emi", 
								  "4" => "AIP REFER", "3" => "AIP REJECT");					   		
		
		$feature_catetogy = array( "1" => "Frills", "2" => "Documents", "3" => "Fee Details");  
		
		$bank_criteria    = array( 	'1' => array("dob","Dob"), 
									'2' => array("total_exp","Total exp"), 
									'3' => array("annual_salary", "Salary"), 
									'4' => array("employment_type","employment type"), 
									'5' => array("joined_start_date","current exp"),
									'6' => array("staying_year","staying year"),
									'7' => array("residence_type","residence type"),
									'8' => array("salary_bank","salary mode"));
									
		$kotak_comp_sal_cat  = array('a' => "30000", 'b' => "30000", 'c' => "30000", 'd' => "40000");
		
		$kotak_qualification = array( "1" => "15",  "2"  => "15", "3" =>  "11", "4"  => "11", "5"  => "11", "6" => "11", 
									"7" => "14",  "8"  => "11", "9" =>  "8", "10" => "14", "11" => "8", "12" => "8", 
									"13" => "8", "14" => "14", "15" => "13", "16" => "11", "17" => "8", "18" => "16");					
		 													   		
		
		$kotak_residence    = array( "1" => "2", "2" => "4", "3" => "2", "4" => "5");
		
		$year_arr           = array ("1" => "2010 before", "2" => "2010", "3" => "2011", "4" => "2015",
									 "5" => "2010", "6" => "2016", "7" => "2017");
		
		$car_brands			= array("1" => array("Other Car", "other_car"),
									"2" => array("Baleno", 	  "baleno"),
									"3" => array("Hyundai I20", "Hyundai_I20"),		
									"4" => array("Mahindra KUV100", "Mahindra_KUV100"),
									"5" => array("Renault kwid", "Tata_Zest"),
									"6" => array("Renault kwid", "Renault_kwid"),
									"7" => array("Hyundai Eon", "Hyundai_Eon"),
									"8" => array("Swift", "Santro"));
		
		$main_arr['EMPLOYEE_TYPE']	  = $employe_type;
		$main_arr['CALLER_STATUS']    = $caller_status;	
		$main_arr['FIRE_LIMIT']  	  = $fire_limit_arr;
		$main_arr['JOB_EXP']	  	  = $job_exp;
		$main_arr['RESIDENCE_TYPE']	  = $residence_type;
		$main_arr['GENDER']	  		  = $gender;
		$main_arr['EDUCATION']	  	  = $education;
		$main_arr['DEPENDENTS']	  	  = $dependents;
		$main_arr['INDUSTRY_TYPE']	  = $industry_type;
		$main_arr['EMP_ROLE']	  	  = $emp_role;
		$main_arr['LOAN_PURPOSE']	  = $loan_purpose;
		$main_arr['APPLICATION_STATUS']= $app_status;
		$main_arr['SALARY_BANK']	  = $salary_bank;
		$main_arr['COUNTS_ARR']       = $counts;
		$main_arr['CLIENT_TYPE']      = $client_type;
		$main_arr['PAYOUT_TYPE']	  = $payout_type;
		$main_arr['STAYING_YEAR']     = $staying_yr;
		$main_arr['REVIEW_STATUS']	  = $user_review;	
		$main_arr['CITY_ALIAS']		  = $city_alias;
		$main_arr['CREDITCARD_PURPOSE']	= $creditcard_purpose;
		$main_arr['CREDITCARD_EXISTING']= $creditcard_existing;
		$main_arr['RBL_QUALIFICATION']= $rbl_qualification;
		$main_arr['RBL_CITY'] 		  = $rbl_city; 	
		$main_arr['RBL_RESIDENCE'] 	  = $rbl_residence;	
		$main_arr['RBL_ERRCODE']      = $rbl_err_code;	
		$main_arr['RBL_STATUS_CODE']  = $rbl_status_code;
		$main_arr['FEATURES_CATEGORY']= $feature_catetogy;
		$main_arr['BANK_CRITERIA']    = $bank_criteria;	
		$main_arr['KOTAL_SALARY']     = $kotak_comp_sal_cat;	
		$main_arr['KOTAL_RESIDENCE']  = $kotak_residence;	
		$main_arr['KOTAK_QUALIFICATION']= $kotak_qualification;
		$main_arr['YES_NO']			  = $yes_no;
		$main_arr['PL_FULL_STATUS']	  = $pl_full_status;
		$main_arr['HL_PURPOSE']	  	  = $hl_purpose;
		$main_arr['HL_PROPOSED_PROPERTY'] = $hl_proposed_property;
		$main_arr['COAPLICANT']		  = $hl_coaplicant;
		$main_arr['COAPLICANT_EMPT']  = $coapp_emptype;
		$main_arr['YEAR_ARR']		  = $year_arr;
		$main_arr['CAR_BRANDS']		  = $car_brands;
		
			
		return $main_arr;					
	}
}
	

?>