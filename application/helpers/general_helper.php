<?php
if(! function_exists ('is_Admin_Login') ) {
	function is_Admin_Login($username, $uid, $admin_role, $admin_role_id, $admin_role_details, $cur_controller) {
		
		// CHECK IP ADDRESS IS ALLOWED FOR VIEW
		//is_ip_allowed();
		
		if( strlen (trim ($username)) > 0 && is_numeric($uid) != "" && 
			strlen (trim ($admin_role)) > 0 && is_numeric($admin_role_id) != "" &&  
			strlen (trim ($admin_role_details)) > 0 && trim($cur_controller) != ""){
			
			if(strtolower($cur_controller) == 'extra_call') {
				// IF ITS AJAX CALL THEN DONT CHECK FOR PERMISSION
				return true;
			} else {
				// OTHERWISE CHECK FOR PERMISSION
				if (check_role_permission ($admin_role_details, $cur_controller) != false) {
					// IF USER IS ALLOWED THEN KEEP HE/SHE TO THAT PAGE	
					return true;
				} else {
					return "404 not found";
					// ELSE REDIRECT TO 404 PAGE
					//redirect(FULL_CMS_URL."/".ADMIN_DEFAULT_CONTROLLER);		
				}
			}
		}else{
			// THEN REDIRECT
			redirect(FULL_CMS_URL."/".ADMIN_LOGOUT_CONTROLLER);
		}
	}
}
if(! function_exists('check_role_permission')) {
	function check_role_permission($role_details, $cur_controller) {
		
		// CONVERT PERMISSION IN SINGLE ARRAY AND MAKE IT CLEAN PER ELEMENT ONE CONTROLLER
		$arr_permission = create_permission_array($role_details);
		
			
		// CHECK USER REQUESTED PAGE IS PERMITTED TO THAT USER OR ROLE WHICH IS GRANTED TO USER
		if (in_array(strtolower($cur_controller), $arr_permission)) {
			return true;	
		} else {
			return false;
		}
	}
}
if(! function_exists('create_permission_array')) {
	function create_permission_array($role_details) {
		// UNSERIALIZE ARRAY AND STORE IN VARIABLE
		$arr_role_details = unserialize($role_details);
		
		// SEPERATE THE DOUBLE VARIALE
		foreach($arr_role_details as $k => $v) {
			
			// EXTRACT THE STRIG FROM COMMA
			$arr_cur_val = explode(",", $v);
			foreach ($arr_cur_val as $k1 => $v1) {
				//$arr_permission[] = strtolower($v1);
				$arr_permission[] = strtolower($v1);
			}
		}
		return $arr_permission;
	}	
}
if(! function_exists ('is_Admin_Login_Redirect') ) {
	function is_Admin_Login_Redirect ($username, $uid, $admin_role, $admin_role_id, $admin_role_details, $cur_controller) {	
		
		// CHECK IP ADDRESS IS ALLOWED FOR VIEW
		is_ip_allowed();
		
		if( strlen (trim ($username)) > 0 && is_numeric($uid) != "" && 
			strlen (trim ($admin_role)) > 0 && is_numeric($admin_role_id) != "" &&  
			strlen (trim ($admin_role_details)) > 0 && trim($cur_controller) != ""){
		
			// THEN REFIRECT 
			redirect(FULL_CMS_URL."/dashboard");
		} else {
			// DO NOTHING
			return true;
		}
	}	
}


if(! function_exists('is_user_login')) {
	function is_user_login($user_id){
		if(strlen (trim ($user_id))  > 0 ) {
			return true;
		} else {
			return false;
		}
	}
}


	
function get_random_chracter($chars_min=6, $chars_max=8, $use_upper_case=false, $include_numbers=false, $include_special_chars=false) {
	$length = rand($chars_min, $chars_max);
	$selection = 'aeuoyibcdfghjklmnpqrstvwxz';
	if($include_numbers) {
		$selection .= "1234567890";
	}
	if($include_special_chars) {
		$selection .= "!@04f7c318ad0360bd7b04c980f950833f11c0b1d1quot;#$%&[]{}?|";
	}
							
	$password = "";
	for($i=0; $i<$length; $i++) {
		$current_letter = $use_upper_case ? (rand(0,1) ? strtoupper($selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))];            
		$password .=  $current_letter;
	}
	return $password;
}	

  
  
function __serialize__($str) {
 return json_encode($str);
}

function __unserialize__($str) {
 return json_decode($str);
}

function object_to_array($arr) {
    if (is_object($arr)) {
        $arr = get_object_vars($arr);
    }

    if (is_array($arr)) {
        return array_map(__FUNCTION__, $arr);
    } else {
        return $arr;
    }
}

function array_to_object($arr) {
 json_decode (json_encode ($arr), FALSE);
}  

function inMultiArray($needle,$heystack) {
    if (array_key_exists($needle,$heystack) or in_array($needle,$heystack)) {
             return true;
        } else {
            $return = false;
            foreach (array_values($heystack) as $value) {
                if (is_array($value) and !$return) {
                    $return = inMultiArray($needle,$value);
                }
            }
            return $return;
        }
    }

if (! function_exists("array_lower")) {
	function array_lower($arr) {
		if(is_array($arr)) {
			foreach ($arr as $k => $v) {
				if(is_array($v)) {
					$new_arr[$k] = array_lower($v);
				} else {
					$new_arr[$k] = strtolower($v);
				}
			}
		}
		return $new_arr; 
	}
}	

if(! function_exists('all_param')){
	function all_param($url = ""){
		$pq['query'] = NULL;	
		$pq = parse_url($url);
		
		if( is_array($pq) && count($pq) > 1 && isset($pq['query']) ){
			$headr_param = $pq['query'];
			
			return $headr_param;
			
		}else{
			return false;
		}
	}
}

if( ! function_exists("allowed_extensions")) {
	function allowed_extensions($filename = "", $type = "image") {
		
		$allow_ext = array("image" => array("jpg", "jpeg", "png", "gif", "bmp"));
					
		if(trim($filename) != "") {
			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			if(in_array(strtolower($ext), $allow_ext[$type])) {
				return strtolower($ext);
			}
		}
		return false;
	}
}

if (! function_exists("_random_word") ) {
	function _random_word( $length, $case=0 ) {
	   $word = ""; 
	   for ( $ix = 1; $ix <= $length; $ix++) { 
		  $word .= chr(rand(97, 122)); 
	   }
	   if($case == 1) { 
		  $word = strtoupper($word); 
	   } elseif($case == 2) { 
		  $word = strtolower($word); 	   
	   } 
	   return($word); 
	}
}


if( ! function_exists("make_img_thumb")) {
	function make_img_thumb($src, $dest, $desired_width) {
	
		$ext = pathinfo($src, PATHINFO_EXTENSION);
		
		if(strtolower($ext) == 'jpeg' || strtolower($ext) == 'jpg') {
			$source_image = imagecreatefromjpeg($src);					
		} else if(strtolower($ext) == 'png') {
			$source_image = imagecreatefrompng($src);					
		} else if(strtolower($ext) == 'gif') {
			$source_image = imagecreatefromgif($src);					
		}
		
		$width = imagesx($source_image);
		$height = imagesy($source_image);
		
		if($width > $desired_width + 100) {
		
			/* find the "desired height" of this thumbnail, relative to the desired width  */
			$desired_height = floor($height * ($desired_width / $width));
			
			/* create a new, "virtual" image */
			$virtual_image = imagecreatetruecolor($desired_width, $desired_height);
			
			if(strtolower($ext) == "gif" or strtolower($ext) == "png" ){
				$white = imagecolorallocate($virtual_image, 255, 255, 255);
				imagecolortransparent($virtual_image, $white);
				imagefill($virtual_image, 0, 0, $white);

			}
			
			/* copy source image at a resized size */
			imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
			
			/* create the physical thumbnail image to its destination */
			
			if(strtolower($ext) == 'jpeg' || strtolower($ext) == 'jpg') {
				imagejpeg($virtual_image, $dest);					
			} else if(strtolower($ext) == 'png') {
				imagepng($virtual_image, $dest);					
			} else if(strtolower($ext) == 'gif') {
				imagegif($virtual_image, $dest);					
			}
			return true;
		} else {
			if (!copy($src, $dest)) {
				return true;
			} else {
				return false;
			}
		}
	}
}


if(! function_exists ('getOS') ) {
	function getOS() { 
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		$os_platform    =   "Unknown_OS_Platform";
		$os_array       =   array(
							'/windows nt 10.0/i'    =>  'Windows_10',
							'/windows phone 8/i'    =>  'Windows_Phone_8',
                            '/windows phone os 7/i' =>  'Windows_Phone_7',
                            '/windows nt 6.3/i'     =>  'Windows_8.1',
                            '/windows nt 6.2/i'     =>  'Windows_8',
                            '/windows nt 6.1/i'     =>  'Windows_7',
                            '/windows nt 6.0/i'     =>  'Windows_Vista',
                            '/windows nt 5.2/i'     =>  'Windows_Server_2003_or_XP_x64',
                            '/windows nt 5.1/i'     =>  'Windows_XP',
                            '/windows xp/i'         =>  'Windows_XP',
                            '/windows nt 5.0/i'     =>  'Windows_2000',
                            '/windows me/i'         =>  'Windows_ME',
                            '/win98/i'              =>  'Windows_98',
                            '/win95/i'              =>  'Windows_95',
                            '/win16/i'              =>  'Windows_3.11',
                            '/macintosh|mac os x/i' =>  'Mac_OS_X',
                            '/mac_powerpc/i'        =>  'Mac_OS_9',
                            '/linux/i'              =>  'Linux',
                            '/ubuntu/i'             =>  'Ubuntu',
                            '/iphone/i'             =>  'iPhone',
                            '/ipod/i'               =>  'iPod',
                            '/ipad/i'               =>  'iPad',
                            '/android/i'            =>  'Android',
                            '/blackberry/i'         =>  'BlackBerry',
                            '/webos/i'              =>  'Mobile');
	
		foreach ($os_array as $regex => $value) { 
	
			if (preg_match($regex, $user_agent)) {
				$os_platform    =   $value;
			}
		}   
		return $os_platform;	
	}
}


if(! function_exists ('getBrowser') ){	
	function getBrowser() {
		$user_agent 	= 	$_SERVER['HTTP_USER_AGENT'];
		$browser        =   "UnknownBrowser";
		$browser_array  = array(
								'/msie/i'       			 =>  'InternetExplorer',
								'/OPR/i'    				 =>  'Opera',
								'/chrome/i'    				 =>  'Chrome',
								'/safari/i'     			 =>  'Safari',
								'/Opera/i'     				 =>  'Opera',
								'/opera.*Mini/i' 			 =>  'Opera',
								'/Opera.*Mobi/i' 			 =>  'Opera',
								'/Android.*Opera/i' 		 =>  'Opera',
								'/Mobile.*OPR.[0-9.]+/i'	 =>  'Opera',
								'/Coast.[0-9.]+/i' 			 =>  'Opera',
								'/firefox/i'    			 =>  'Firefox',
								'/netscape/i'  				 =>  'Netscape',
								'/maxthon/i'   				 =>  'Maxthon',
								'/konqueror/i'  			 =>  'Konqueror',
								'/bolt/i'           		 =>  'bolt',
								'/teaShark/i'       		 =>  'Teashark',
								'/blazer/i'         		 =>  'Blazer',
								'/Version.*Mobile.*Safari/i' =>  'Safari',
								'/Safari.*Mobile/i'          =>  'Safari',
								'/MobileSafari/i'            =>  'Safari',
								'/tizen/i'          		 =>  'Tizen',
								'/UC.*Browser/i'     		 =>  'UCBrowser' ,
								'/UCWEB/i'     		     	 =>  'UCBrowser',
								'/baiduboxapp/i'     		 =>  'baiduboxapp',
								'/baidubrowser/i'     		 =>  'baidubrowser',
								'/DiigoBrowser/i'     		 =>  'DiigoBrowser',
								'/Puffin/i'     		     =>  'Puffin',
								'/Mercury/i'     		     =>  'Mercury',
								'/Obigo/i'     		     	 =>  'ObigoBrowser',
								'/NF-Browser/i'     		 =>  'NetFront',
								'/NokiaBrowser/i'     		 =>  'GenericBrowser' ,
								'/OviBrowser/i'     		 =>  'GenericBrowser', 
								'/OneBrowser/i'     		 =>  'GenericBrowser', 
								'/TwonkyBeamBrowser/i'     	 =>  'GenericBrowser' ,
								'/FlyFlow/i'     		     =>  'GenericBrowser', 
								'/Minimo/i'     		     =>  'GenericBrowser' ,
								'/NetFront/i'     		     =>  'GenericBrowser' ,
								'/MQQBrowser/i'     		 =>  'GenericBrowser',
								'/MicroMessenger/i'     	 =>  'GenericBrowser',
								'/Novarra-Vision/i'     	 =>  'GenericBrowser',
								'/SEMC.*Browser/i'     		 =>  'GenericBrowser' ,
								'/mobile/i'     		     =>  'HandheldBrowser'
								
							);
		foreach ($browser_array as $regex => $value) { 
			if (preg_match($regex, $user_agent)&& $browser == "UnknownBrowser") {
				$browser    =   $value;
			}
		}
		return $browser;
		
	}	
}

function check_mysql_inject($input_text = ""){
	
	if($input_text != ""){
	   $mysql_vars   = array("select", "delete", "update", "insert", "groupby", "orderby", "=", ";", "*", ";");
	   $txt_valid    = strtolower($input_text);
		
		foreach($mysql_vars as $v ){ 
			if (strpos($txt_valid, $v) !== false) {
				return true; 
			}
		}
		return false;
	}
}

function sortBy_interest($a, $b) {
    	return $a['interest_rate_min'] - $b['interest_rate_min'];
}

function sortBy_procfee($a, $b) {
    	return $a['processing_fee'] - $b['processing_fee'];
}

function sortBy_minbal($a, $b) {
    	return $a['min_saving_bal'] - $b['min_saving_bal'];
}

function sortBy_activdeposit($a, $b) {
    	return $a['sav_act_deposit'] - $b['sav_act_deposit'];
}

function sortBy_year_fee($a, $b) {
    	return $a['cc_yearly_fee'] - $b['cc_yearly_fee'];
}

function sortBy_join_perk($a, $b) {
    	return $a['cc_joining_bonus'] - $b['cc_joining_bonus'];
}

function _sendsms($mobile = '', $message = '') {
		// CHECK MOBILE NO IS VALID OR NOT
		if (isset($mobile) && trim($mobile) > 0 && strlen($mobile) == 10 && isset($message) && trim($message) != '') {
			
			// IS CURL INSTALLED YET
			if (!function_exists('curl_init')){
				die('Sorry cURL is not installed!');
			}
		 
		 	$url = 'http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$mobile.'&msg='.urlencode($message).'&msg_type=TEXT&userid='.SMSGUPSUP_USERID.'&auth_scheme=plain&password='.SMSGUPSUP_PASS.'&v=1.1&format=text&mask=PAISFT';
			
			// OK cool - then let's create a new cURL resource handle
			$ch = curl_init();
		 		
			// Set URL to download
			curl_setopt($ch, CURLOPT_URL, $url);
		 
			// Should cURL return or print out the data? (true = return, false = print)
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		 
			// Timeout in seconds
			curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		 
			// Download the given URL, and return output
			$output = curl_exec($ch);
		 	return $output;
			// Close the cURL resource, and free system resources
			curl_close($ch);
		 
			if (strpos(strtolower($output), 'error') !== false) {
				// STORE SUCCESS LOG FOR THE REFERENCE
				return false;
				// SEND MAIL IF ERROR COMES FMOR SMSGUPSUP
			} else {
				// STORE SUCCESS LOG FOR THE REFERENCE
				return true;
			}
			
		}
	}
	

function ajax_loader($img, $text){
?>	
	<div class="overlay_loading hidden_dev" id="overlay_loading">
    <div class="loading_page_center">
      <img src="<?php echo $img?>" /> 
     <div class="loading_text"><?php echo $text?></div>
    </div>
 </div>
 
<?php  
}

function moneyFormatIndia($moneyformat){
	
	$moneyfmt_ex = explode(".",$moneyformat);
	$num = $moneyfmt_ex[0];
	
    $explrestunits = "" ;
    if(strlen($num)>3){
        $lastthree = substr($num, strlen($num)-3, strlen($num));
        $restunits = substr($num, 0, strlen($num)-3); // extracts the last three digits
        $restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
        $expunit = str_split($restunits, 2);
        for($i=0; $i<sizeof($expunit); $i++){
            // creates each of the 2's group and adds a comma to the end
            if($i==0)
            {
                $explrestunits .= (int)$expunit[$i].","; // if is first value , convert into integer
            }else{
                $explrestunits .= $expunit[$i].",";
            }
        }
        $thecash = $explrestunits.$lastthree;
    } else {
        $thecash = $num;
    }
	
	if(count($moneyfmt_ex) > 1){
		$thecash = $thecash.".".$moneyfmt_ex[1];		
	}
    return $thecash; // writes the final format where $currency is the currency symbol.
}

function get_year($date){
	
	$from = new DateTime($date);
	$to   = new DateTime('today');
	return $from->diff($to)->y;	

}

function check_uri_for_country($get_uri){

    $uri_arr = explode('/', $get_uri);

    if(in_array('en-in', $uri_arr)){

        $coun = "in";
        setcookie("countrytest", $coun, time() + (86400 * 2), "/"); // 86400 = 1 day

    }else if(in_array('en-us', $uri_arr)){

        $coun = "us";
        setcookie("countrytest", $coun, time() + (86400 * 2), "/"); // 86400 = 1 day

    }else if(in_array('en-gb', $uri_arr) || in_array('en-uk', $uri_arr) ){

        $coun = "gb";
        setcookie("countrytest", $coun, time() + (86400 * 2), "/"); // 86400 = 1 day

    }else{
    	$coun = 'in'; 
    }

    return $coun;  
}

function get_country_name($country_code){

	$country_name = "";

	if($country_code == "in"){
		
		$country_name = "India";
	}else if($country_code == "us"){
		$country_name = "US";
	}else if($country_code == "gb"){
		$country_name = "UK";
	}

	return $country_name;

}

function all_arrays(){

	$ipl_team_name = array( "Chennai Super Kings" => "CSK",
							"Delhi Capitals" => "DC",
							"Kolkata Knight Riders" => "KKR",
							"Mumbai Indians" => "MI",
							"Punjab Kings" => "PK",
							"Rajasthan Royals" => "RR",
							"Royal Challengers Bangalore" => "RCB",
							"Sunrisers Hyderabad" => "SH");	


		$ads_position = array('1' => "300_250_RightTop",
							  '2' => "980_160Top",
							  '3' => "980x160Bottom",
							  '4' => "300_250_center"
							);

			

	$arr['IPL_TEAM_NAME'] = $ipl_team_name;
	$arr['ads_position'] = $ads_position;

	return $arr;

}

function get_scorecard($series_id, $match_id){

	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => "https://dev132-cricket-live-scores-v1.p.rapidapi.com/scorecards.php?seriesid=".$series_id."&matchid=".$match_id,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
			"x-rapidapi-host: dev132-cricket-live-scores-v1.p.rapidapi.com",
			"x-rapidapi-key: 8f79da05cbmsh3f733d0df782af5p10e40ejsn034125a199a7"
		),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
		//echo "cURL Error #:" . $err;
		return "error";
	} else {

		return $res = json_decode($response);

		if(is_object($res) && count($res) > 0) {

			return $res;
		}else{
			return "error";
		}	
		
	}
} 


function get_series_match_list(){

	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => "https://dev132-cricket-live-scores-v1.p.rapidapi.com/matchseries.php?seriesid=2780",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
			"x-rapidapi-host: dev132-cricket-live-scores-v1.p.rapidapi.com",
			"x-rapidapi-key: 8f79da05cbmsh3f733d0df782af5p10e40ejsn034125a199a7"
		),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
		echo "cURL Error #:" . $err;
	} else {
		
		return $res = json_decode($response);

		if(is_object($res) && count($res) > 0) {

			return $res;
		}else{
			return "error";
		}
	}
}


if (! function_exists('array_column_')) {
    function array_column_(array $array, $column) {
        
        $newarr = array();
	    foreach ($array as $row) $newarr[] = $row->$column;
	    return $newarr;
    }
}

?>