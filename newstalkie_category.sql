-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: oyevi.cf7kqmgalpwf.ap-southeast-1.rds.amazonaws.com    Database: local_launde
-- ------------------------------------------------------
-- Server version	5.5.53-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_category`
--

DROP TABLE IF EXISTS `tbl_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_category` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `seourl` varchar(255) NOT NULL,
  `image` varchar(250) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `datecreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateupdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_category`
--

LOCK TABLES `tbl_category` WRITE;
/*!40000 ALTER TABLE `tbl_category` DISABLE KEYS */;
INSERT INTO `tbl_category` VALUES (1,'Inspiration','inspiration','inspiration.jpg','Inspiration','Inspiration','Inspiration',1,'2018-02-01 06:37:47','2018-05-03 16:35:12'),(2,'Lifestyle','lifestyle','lifestyle.jpg','Latest Lifestyle News, Breaking News of Lifestyle,Live Updates | Locallaunde','lifestyle websites, lifestyle living, articles on lifestyle, lifestyle online, lifestyle news, lifestyle sites','Get updated with all Lifestyle news & articles including celebrities, fashion, hot trends live updates & more! \n',1,'2018-02-01 10:44:37','2018-05-22 16:17:08'),(3,'Animals','animals','animals.jpg','Animals','Animals','Animals\n',1,'2018-02-01 10:44:46','2018-05-03 16:34:59'),(4,'Memes','memes','meme-banner.jpg','Latest, Funny, Trending and Facebook Memes | Locallaunde','memes,funny memes,facebook memes,trending memes,hilarious memes,best memes,funniest memes,funniest memes ever,relationship memes ','For the latest scoop on viral images and animated GIFs of the day, check out our Locallaunde Trending Images Gallery.\n',0,'2018-02-01 10:44:53','2020-09-10 17:26:14'),(5,'politics','politics','politics.jpg','Latest Politics News ,Top Political Headlines, Current Affairs India| Locallaunde','political news, political polls, latest political news, news politics, political articles, political parties, political issues¸ real politics, political ideology, current political news, Modi, Trump, Vladimir Putin, BJP, Congress, Arvind Kejriwal','Locallaunde provides the latest politics news India, political news, current affairs & analysis on Indian politics\n',1,'2018-02-01 10:45:10','2018-05-22 16:16:14'),(6,'Celebrity','celebrity','celebrity.jpg','Celebrity','Celebrity','Celebrity',1,'2018-02-01 11:07:11','2018-05-03 16:35:49'),(8,'Science&Technology','science-technology','science-and-technology.jpg','Science News, Latest Science and Technology News, Technology Updates | Locallaunde','Products Launch, Reviews, Specifications, Gadgets, Gizmo, iPhone, iPad, Samsung, Nokia, Micromax, Oppo, Vivo','Locallaude features daily news, feature stories, reviews and more in all disciplines of science & technology updates\n',1,'2018-02-09 12:17:03','2018-05-22 16:16:39'),(9,'Travel&Adventure','travel-adventure','travel-and-adventure.jpg','Latest Travel News, Travel Guides and Reviews | Locallaunde','cheapest places, best destinations, holiday spots to travel, adventure holidays, adventure travel, overseas adventure travel, vacation, flights','Discover exciting world events, luxury travel deals and more. View the latest travel news and information at Locallaunde\n',1,'2018-02-09 12:18:07','2018-05-22 16:17:30'),(10,'History&Culture','history-culture','history&Culture.jpg','Indian Beliefs and Superstitions, Indian culture, History of India | Locallaunde','Indian culture, black history, black history facts, black history people, Indian culture and tradition','From breaking mirrors to hanging lemon and chilies. Locallaunde gives you an insight into the vast Indian superstition.\n',1,'2018-02-09 12:20:39','2018-05-22 16:18:24'),(11,'Health&Fitness','health-fitness','Health-&-Fitness.jpg','Daily Health Tips, Healthy Diet, Healthy Food Recipes, Fitness Tips | Locallaunde','\" fitness,health,gym workout ,workout plans,health and fitness,personal training,abs workout,health websites,fitness exercises,gym fitness \"','The latest health tips and health advice for better nutrition, sleep, pain relief, and more only at Locallaunde\n',1,'2018-02-09 12:21:39','2018-05-22 16:18:53'),(12,'Sports','sports','sports.jpg','Latest Sports News ,Cricket News,Football News ,Tennis News |Locallaunde','sports news, latest sports news, sports news today, sports, news sport, ESPN sports, news sports, today\'s sports news, sports scores, sport new, sports news of today','Catch the latest news and updates from the World of Sports. Visit locallaunde for sports coverage & all sporting events.\n',1,'2018-02-13 05:14:44','2018-05-22 16:14:10'),(13,'News','news','news.jpg','Latest Breaking News, National News, World News, India News |  Locallaunde','political news, political polls, latest political news, news politics, political articles, political parties, political issues¸ real politics, political ideology, current political news, Modi, Trump, Vladimir Putin, BJP, Congress, Arvind Kejriwal','Get Latest and breaking news from India. Locallaunde brings India Top News Headlines on Indian politics and government\n',1,'2018-03-01 10:35:45','2018-05-22 16:13:25'),(14,'Entertainment','entertainment','entertainment.jpg','Upcoming Bollywood Movies 2018, Movie News and Update 2018, Gossips |Locallaunde','SRK, Salman Khan, Aishwarya Rai, Deepika Padukone, Ranvir Singh, celebrity news, celebrity gossip, celeb news, entertainment news, celeb gossip, showbiz news, latest celebrity news, hot celebrities','Take a peek at the Upcoming Bollywood Movies with their Cast, Release dates, Official trailers, and first look updates\n',1,'2018-03-16 09:59:20','2020-04-26 11:03:23'),(15,'Environment','environment','environment.jpg','Environment','Environment,nature,forest','Environment',1,'2018-04-19 13:18:07','2018-05-03 16:32:24'),(16,'Food Recipes','food recipes','food.jpg','Food','Food,recipes,juices','food',1,'2018-04-23 16:35:17','2018-05-28 11:02:47'),(17,'FIFA 2018 ','fifa-2018','fifa.jpg','FIFA 2018','FIFA 2018','FIFA 2018',1,'2018-06-21 16:52:23','2018-06-21 16:54:36'),(18,'corona virus','corona-virus','Untitled.png','Corona Virus','Corona Virus','Corona Virus',1,'2020-04-07 18:06:29','2020-10-14 11:59:21');
/*!40000 ALTER TABLE `tbl_category` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-30  8:08:28
