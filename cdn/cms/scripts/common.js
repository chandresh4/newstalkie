﻿$(document).ready(function(e) {
    $(".menu-toggle").click(function(e) {
		var togle_id =$(this).attr("data-id");
		if ( $("#togglePages" + togle_id).is( ".in" ) ) {
			$("#togglePages" + togle_id).removeClass( "in" );		 
		}else{
		    $("#togglePages" + togle_id).addClass( "in" );
		}
    });
});


$(document).ready(function(e) {
	$("#cache_clear").click(function(e) {
        var _site_url= $("#cms_url").val();
        var clear = 1;
		$.ajax({
				url: _site_url + '/cache',
				type: "post",
				data: "clear="+clear,
				beforeSend:function(){
				   $("#cache_clear").text("Cache Clearing")
				},
				success:function(f){
				   $("#cache_clear").text("Clear Cache");
				}
			}); 
    });
});



$(document).ready(function(e) {
	$("#offer_id").blur(function(e) {
        var changed_offer_id = $("#offer_id").val()
		var pre_offer_id = $( this ).attr("attr-val");
		if(changed_offer_id == pre_offer_id){
			$("#offer_id-msg").html("<font color='#FF0000'>No change</font>");
		}else{
			
			var _site_url= $("#cms_url").val();
			
			$.ajax({
					url: _site_url + '/manage_campaigns_pixel/check_offer_id',
					type: "post",
					data: "offer_id="+changed_offer_id,
					beforeSend:function(){
					   $("#offer_id-msg").html("<font color='#FF0000'>Checking.....</font>");
					},
					success:function(k){
					  if(k == "already_exist"){
					     $("#offer_id-msg").html("<font color='#FF0000'>Already Exist</font>");
					  }else if(k == "unique"){
						 $("#offer_id-msg").html("<font color='#FF0000'>Unique</font>"); 
					  }
					}
				});
		}
    });
});




$(document).ready(function(e) {
    $("#gen_email_memcache").click(function(e) {
    var memcache = 1;    
	var _site_url= document.domain;
	
	if (confirm("Are you want to generate email memcache?")) {
			
		$.ajax({
				url: 'http://'+document.domain+'/cronjob/cron_user_cache.php',
				type: "post",
				data: "memcache="+memcache,
				beforeSend:function(){
				   $("#gen_email_memcache").html("<font color='#FF0000'>Generating Email Memcache.....</font>");
				},
				success:function(k){
				if(k != ""){
					 $("#gen_email_memcache").html("<font color='#FF0000'> User Email Memcache</font>");
				  }
				}
		  }); 
			
		}else{
           
		   return false;
	}
	
	
    });
});




$(document).ready(function() {
    $("#ajax_multi_camp").change(function() {
        var e = $("#ajax_multi_camp").val();
		
        if ("undefined" != typeof e) {
            var t = $("#cms_site_url").val(),
                n = "&camp_id=" + e;
            $.ajax({
                url: t + "/ajax_campaign/multi_ans_camp",
                type: "post",
                data: n,
                beforeSend: function() {},
                success: function(e) {
                    var t = JSON.parse(e);
                    return "" != e && (
					$("#camp_url").val(t[0]), 
					$("#camp_pixel").val(t[1])), !1
                }
            })
        }
    });
});

$(document).ready(function(){
	$('#celebrity_name').on('keyup', function() {
		var cumtom_url = $("#celebrity_name").val();
		var celeb_url  = cumtom_url.replace(/\s+/g, '-').toLowerCase();
		$("#custom_url").val(celeb_url);
		
	});
	
});


function updateQuickNet() {
    var e = "&fire_type=" + $("#fire_type").val() + "&net_name=" + $("#net_name").val() + "&goal=" + $("#goal").val() + "&handle_type=" + $("#handle_type").val() + "&from_date=" + $("#from_date").val() + "&to_date=" + $("#to_date").val() + "&net_id=" + $("#net_id").val(), t = $("#cms_site_url").val();
    "undefined" != typeof t && "undefined" != typeof e && 
	
	$.ajax({
        url: t + "/manage_netwisereport/update",
        type: "post",
        data: e,
        beforeSend: function() {
            $("#quick_upd_msg").hide()
        },
        success: function(e) {
            $("#quick_upd_msg").html(e).hide().slideDown(1e3)
        }
    })
}