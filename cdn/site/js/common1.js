//for newsletter popup
function subscribeForm(){
	
   var s_u= $("#site_url").val()
	if(!$("#name1").val().match(/^[a-zA-Z\s]+$/)){
	   $("#name1").css({"border":"1px solid #ff0000"}).focus();
	     $("#subscribe_err").html("Please enter correct Name");
	       return false;	
	    }else{
		  $("#name1").css("border-color","#e6e6e6").focus();
		  $("#subscribe_err").html(""); 
	    }
	if($.trim($("#name1").val()) == ""){
		 $("#subscribe_err").html("Please enter correct Name");
		 $("#name1").css({"border":"1px solid #ff0000"}).focus();
		 return false;	
	  }else{
		$("#name1").css({"border":"1px solid #ddd"});
		$("#subscribe_err").html();	
	  }
    if(!$("#email1").val().match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
		 $("#email1").css({"border":"1px solid #ff0000"}).focus();
		 $("#subscribe_err").html("Please enter correct Email");
		 return false;
	}else{
		 $("#email1").css("border-color","#e6e6e6").focus();
		 $("#subscribe_err").html(""); 
	}
	
		if($.trim($("#location").val()) == ""){
	   $("#location").css({"border":"1px solid #ff0000"}).focus();
	     $("#subscribe_err").html("Please select  correct Location");
	       return false;	
	    }else{
		  $("#location").css("border-color","#e6e6e6").focus();
		  $("#subscribe_err").html(""); 
	    }
	  var t = "email="+$("#email1").val()+"&"+"name="+$("#name1").val()+"&"+"location="+$("#location").val();
	  
	$.ajax({
		url:s_u+"subscribe/",
		type:"post",
		data:t,
		beforeSend:function(){
		  $("#btn_subscribe").val("subscribing.....");
		},
				success:function(e){ 
				alert(e);
				var e = $.trim(e);
					
				if(e === "done"){//not sub & not reg
					$("#newsletter").hide();
                    $("#newsletter_su").html("Hello! Thank You For Subscribing."); 
					$("#newsletter_su").show();

				}else if(e === "olduser"){//not sub & not reg
				   $("#newsletter").hide();
                    $("#newsletter_su").html("You Have Already Subscribed."); 
					$("#newsletter_su").show();
				   
				}else if(e == "error"){//sub & not reg
				    $("#newsletter").hide();
                    $("#newsletter_su").html("Please Try Again Later."); 
					$("#newsletter_su").show();
				   
				}
			
		}
	});
}	

//for content submit form
function submitform(){
var t="";
   var r_ =  new Array("name","email","category","ptitle","browse","comment");
   for( var i=0; i < r_.length; i++   ){
       if($.trim($("#"+r_[i]).val()) == ""){
		 $("#e1").html($("#"+r_[i]).attr("data-attr")).css({"color":"#ff0000"});
		 $("#"+r_[i]).css({"border":"1px solid #ff0000"}).focus();
		 return false;	
	  }else{
		$("#"+r_[i]).css({"border":"1px solid #ddd"});
		$("#e1").html();	
	  }
   }
   if(!$("#name").val().match(/^[a-zA-Z\s]+$/)){
	       $("#e1").html("Please enter correct Name").css({"color":"#ff0000"});
	       $("#name").css({"border":"1px solid #ff0000"}).focus();
	       return false;	
	    }else{
		   $("#name").css({"border":"1px solid #ddd"});
		   $("#e1").html();	
	    }
  if(!$("#email").val().match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i) ){
		   $("#e1").html("Please enter correct Email").css({"color":"#ff0000"});
		   $("#email").css({"border":"1px solid #ff0000"}).focus(); 
		   return false;	
		}else{
		   $("#email").css({"border":"1px solid #ddd"});
		   $("#e1").html();	
		}
 
        var fullPath = document.getElementById('browse').value;
        if (fullPath) {
           var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
           var filename = fullPath.substring(startIndex);
           if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
               filename = filename.substring(1);
           }
           var validExtensions = ['jpg','png','jpeg'];
           var type = filename.split(".");
           if ($.inArray(type[1], validExtensions) == -1) {
            $("#e1").html("Please upload correct image").css({"color":"#C50736"});
		    $("#browse").css({"border":"1px solid #C50736"}).focus(); 
		    return false;	
           }else{
		   $("#browse").css({"border":"1px solid #ddd"});
		   $("#e1").html();	
		}
       }
}
//SEARCH
$(function () {
     $('a[href="#search"]').on('click', function(event) {
        event.preventDefault();
        $('#search').addClass('open');
        $('#search > form > input[type="search"]').focus();
    });
    
    $('#search, #search button.close').on('click keyup', function(event) {
        if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
            $(this).removeClass('open');
        }
    });
    
});

function cate_search(){
	  if(!$("#val").val().match(/^[a-zA-Z0-9\s]+$/)){
	       alert("Please Search With Correct Search Name")
	      
	       return false;	
	  }
      if($.trim($("#val").val()) == ""){
	       alert("Cannot Search With Empty Name")
	      
	       return false;	
	  }
}