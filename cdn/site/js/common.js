$(document).ready(function(){
	var s_u = $("#site_url").val();
    $("#location").keyup(function(){
	var place = $("#location").val();
	if(place.length < 4){
	  $("#suggesstion-box").hide();
	}
	if(place.length >=4){ 
	    $.ajax({
			type: "POST",
			url: s_u+"subscribe/search",
			data:'keyword='+$("#location").val(),
			beforeSend: function(){
				
			},
			success: function(data){
			 $("#suggesstion-box").show();
			 $("#suggesstion-box").html(data);
			 $("#location").css("background","#FFF");
			}
		});
	}else{
	  $("#suggesstion-box").hide();
    }
	});
	 
});
function selectcamp(val) {
$("#location").val(val);
$("#suggesstion-box").hide();
}

//for newsletter popup
function subscribeForm(u){
	
   var s_u= $("#site_url").val()
	if(!$("#name1"+u).val().match(/^[a-zA-Z\s]+$/)){
	   $("#name1"+u).css({"border":"1px solid #ff0000"}).focus();
	     $("#subscribe_err"+u).html("Please enter correct Name");
	       return false;	
	    }else{
		  $("#name1"+u).css("border-color","#e6e6e6").focus();
		  $("#subscribe_err"+u).html(""); 
	    }
	if($.trim($("#name1"+u).val()) == ""){
		 $("#subscribe_err"+u).html("Please enter correct Name");
		 $("#name1"+u).css({"border":"1px solid #ff0000"}).focus();
		 return false;	
	  }else{
		$("#name1"+u).css({"border":"1px solid #ddd"});
		$("#subscribe_err"+u).html();	
	  }
    if(!$("#email1"+u).val().match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
		 $("#email1"+u).css({"border":"1px solid #ff0000"}).focus();
		 $("#subscribe_err"+u).html("Please enter correct Email");
		 return false;
	}else{
		 $("#email1"+u).css("border-color","#e6e6e6").focus();
		 $("#subscribe_err"+u).html(""); 
	}
	
		if($.trim($("#location").val()) == ""){
			
				var locations = "";
			}else{
				locations = $("#location").val();
			}
	  var t = "email="+$("#email1"+u).val()+"&"+"name="+$("#name1"+u).val()+"&"+"location="+locations
	 
	$.ajax({
		url:s_u+"subscribe/",
		type:"post",
		data:t,
		beforeSend:function(){
		  $('#newsletter'+u).hide();
           //$("#loader").css("display:block !important;");		  
		   $('#loader'+u).show();	
		   
		},
				success:function(e){ 
				
				 $('#loader'+u).hide();
                //$("#loader").css("display:block");						 
				var e = $.trim(e);
					
				if(e === "done"){//not sub & not reg
					$("#newsletter"+u).hide();
                    $("#newsletter_su"+u).html("Thanks for your Subscribing"); 
					$("#newsletter_su"+u).show();

				}else if(e === "olduser"){//not sub & not reg
				   $("#newsletter"+u).hide();
                    $("#newsletter_su"+u).html("You Have Already Subscribed."); 
					$("#newsletter_su"+u).show();
				   
				}else if(e == "error"){//sub & not reg
				    $("#newsletter"+u).hide();
                    $("#newsletter_su"+u).html("Please Try Again Later."); 
					$("#newsletter_su"+u).show();
				   
				}
			
		}
	});
}	

//for content submit form
function submitform(){
   var t="";var s_u= $("#site_url").val();
   var r_ =  new Array("name","email","category","ptitle","browse","comment");
   for( var i=0; i < r_.length; i++   ){
       if($.trim($("#"+r_[i]).val()) == ""){
		 $("#e1").html($("#"+r_[i]).attr("data-attr")).css({"color":"#ff0000"});
		 $("#check-"+r_[i]).addClass('error_1');
         $("#check-"+r_[i]).removeClass('error');
		 $("#"+r_[i]).css({"border":"1px solid #ff0000"}).focus();
		 return false;	
	  }else{
		$("#"+r_[i]).css({"border":"1px solid #ddd"});
		$("#e1").html();
        $("#check-"+r_[i]).addClass('error');
        $("#check-"+r_[i]).removeClass('error_1');		
	  }
   }
   if(!$("#name").val().match(/^[a-zA-Z\s]+$/)){
	       $("#e1").html("Please enter correct Name").css({"color":"#ff0000"});
	       $("#name").css({"border":"1px solid #ff0000"}).focus();
		   $("#check-name").addClass('error_1');
           $("#check-name").removeClass('error');
	       return false;	
	}else{
	   $("#name").css({"border":"1px solid #ddd"});
	   $("#e1").html();
       $("#check-name").addClass('error');
       $("#check-name").removeClass('error_1');	   
	}
	if(!$("#ptitle").val().length>150){
	   $("#e1").html("please make sure the title is less than 150 characters").css({"color":"#ff0000"});
	   $("#ptitle").css({"border":"1px solid #ff0000"}).focus();
	   $("#check-ptitle").addClass('error_1');
       $("#check-ptitle").removeClass('error');	  
	   return false;	
	}else{
	   $("#ptitle").css({"border":"1px solid #ddd"});
	   $("#e1").html();	
	   $("#check-ptitle").addClass('error');
       $("#check-ptitle").removeClass('error_1');
	}
	if(!$("#comment").val().length>250){
	   $("#e1").html("please make sure the title is less than 250 characters").css({"color":"#ff0000"});
	   $("#comment").css({"border":"1px solid #ff0000"}).focus();
	   $("#check-comment").addClass('error_1');
       $("#check-comment").removeClass('error');
	}else{
	   $("#comment").css({"border":"1px solid #ddd"});
	   $("#e1").html();
       $("#check-comment").addClass('error');
       $("#check-comment").removeClass('error_1');	   
	}
    if(!$("#email").val().match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i) ){
		   $("#e1").html("Please enter correct Email").css({"color":"#ff0000"});
		   $("#email").css({"border":"1px solid #ff0000"}).focus(); 
		   $("#check-email").addClass('error_1');
           $("#check-email").removeClass('error');
		   return false;	
		}else{
		   $("#email").css({"border":"1px solid #ddd"});
		   $("#e1").html();	
		   $("#check-email").addClass('error');
           $("#check-email").removeClass('error_1');
		}
 
        var fullPath = document.getElementById('browse').value;
        if (fullPath) {
           var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
           var filename = fullPath.substring(startIndex);
           if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
               filename = filename.substring(1);
           }
           var validExtensions = ['jpg','png','jpeg'];
           var type = filename.split(".");
           if ($.inArray(type[1], validExtensions) == -1) {
            $("#e1").html("Uploaded file type is not supported").css({"color":"#C50736"});
		    $("#browse").css({"border":"1px solid #C50736"}).focus(); 
			$("#check-browse").addClass('error_1');
            $("#check-browse").removeClass('error');
		    return false;	
           }else{
		   $("#browse").css({"border":"1px solid #ddd"});
		   $("#e1").html();	
		   $("#check-browse").addClass('error');
            $("#check-browse").removeClass('error_1');
		}
       }
	    /* var form_data=new FormData();
		$.each($("#browse")[0].files,function(i,file){
		form_data.append("image",file);
		});
		var name = $("#name").val();
		form_data.append("name",name)
		var email = $("#email").val();
		form_data.append("email",email)
		var category = $("#category").val();
		form_data.append("category",category)
		var ptitle = $("#ptitle").val();
		form_data.append("email",email)
		var comment = $("#comment").val();
		form_data.append("comment",comment)
	    $.ajax({
		url:s_u+"submit_content/check_user/",
		type:'POST',
		cache:false,
		async:false,
		contentType:false,
		processData:false,
		data:form_data,
		
		success:function(e){ 
				alert(e);
			
		}
	}); */
}
//SEARCH
$(function () {
     $('a[href="#search"]').on('click', function(event) {
        event.preventDefault();
        $('#search').addClass('open');
        $('#search > form > input[type="search"]').focus();
    });
    
    $('#search, #search button.close').on('click keyup', function(event) {
        if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
            $(this).removeClass('open');
        }
    });
    
});

function cate_search(){
	  if(!$("#val").val().match(/^[a-zA-Z0-9\s]+$/)){
	       alert("Please Search With Correct Search Name")
	      
	       return false;	
	  }
      if($.trim($("#val").val()) == ""){
	       alert("Cannot Search With Empty Name")
	      
	       return false;	
	  }
}