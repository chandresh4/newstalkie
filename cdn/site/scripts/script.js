$(".goToHome").click( function(){
    location.href = "index.html";
})


function myFunction() {
    var x = document.getElementById("myLinks");
    if (x.style.display === "block") {
      x.style.display = "none";
    } else {
      x.style.display = "block";
    }
}


// $('.adItemSlider').slick({
//   arrows: true,
//   infinite: false,
//   speed: 300,
//   slidesToShow: 8,
//   slidesToScroll: 8,
//   responsive: [
//     {
//       breakpoint: 1024,
//       settings: {
//         slidesToShow: 3,
//         slidesToScroll: 3,
//         infinite: true,
//         dots: false,
//         arrows: true
//       }
//     },
//     {
//       breakpoint: 600,
//       settings: {
//         slidesToShow: 2,
//         slidesToScroll: 2
//       }
//     },
//     {
//       breakpoint: 480,
//       settings: {
//         slidesToShow: 1,
//         slidesToScroll: 1
//       }
//     }
//     // You can unslick at a given breakpoint now by adding:
//     // settings: "unslick"
//     // instead of a settings object
//   ]
// });



var $window = $(window);

$(document).ready(function () {

  $window.scroll(function(){

    if($window.scrollTop() > 230 && window.innerWidth > 600){
        $(".stickyVideo").show();
    }else{
        $(".stickyVideo").hide();
    }

  });

});



// Yes Chart Render

var optionss = {
  series: [80],
  chart: {
  height: 200,
  type: 'radialBar',
  toolbar: {
    show: true
  }
},
plotOptions: {
  radialBar: {
    startAngle: 0,
    endAngle: 360,
     hollow: {
      margin: 0,
      size: '70%',
      background: 'none',
      image: undefined,
      imageOffsetX: 0,
      imageOffsetY: 0,
      position: 'front',
      dropShadow: {
        enabled: true,
        top: 3,
        left: 0,
        blur: 4,
        opacity: 0.24
      }
    },
    track: {
      background: '#6C6FC3',
      strokeWidth: '100%',
      margin: '2px', // margin is in pixels
      dropShadow: {
        enabled: true,
        top: -3,
        left: 0,
        blur: 4,
        opacity: 0.35
      }
    },

    dataLabels: {
      show: true,
      color: '#fff',
      name: {
        offsetY: -20,
        show: false,
        color: '#fff'
      },
      value: {
        formatter: function(val) {
          return parseInt(val);
        },
        color: '#fff',
        fontSize: '16px',
        fontWeight: '700',
        show: true,
      }
    }
  }
},
fill: {
  type: 'gradient',
  gradient: {
    shade: 'light',
    type: 'horizontal',
    shadeIntensity: 0,
    gradientToColors: ['#fff'],
    inverseColors: false,
    opacityFrom: 0,
    opacityTo: 100,
    stops: [0, 0]
  }
},
stroke: {
  lineCap: 'round'
},
labels: ['Percent'],
};

var chart = new ApexCharts(document.querySelector("#chart"), optionss);






// No Chart Rendering

var options = {
  series: [70],
  chart: {
  height: 200,
  type: 'radialBar',
  toolbar: {
    show: true
  }
},
plotOptions: {
  radialBar: {
    startAngle: 0,
    endAngle: 360,
     hollow: {
      margin: 0,
      size: '68%',
      background: 'none',
      image: undefined,
      imageOffsetX: 0,
      imageOffsetY: 0,
      position: 'front',
      dropShadow: {
        enabled: true,
        top: 3,
        left: 0,
        blur: 4,
        opacity: 0.24
      }
    },
    track: {
      background: '#6C6FC3',
      strokeWidth: '100%',
      margin: '2px', // margin is in pixels
      dropShadow: {
        enabled: true,
        top: -3,
        left: 0,
        blur: 4,
        opacity: 0.35
      }
    },

    dataLabels: {
      show: true,
      color: '#fff',
      name: {
        offsetY: 0,
        show: false,
        color: '#fff'
      },
      value: {
        formatter: function(val) {
          return parseInt(val);
        },
        color: '#fff',
        fontSize: '16px',
        fontWeight: '700',
        show: true,
      }
    }
  }
},
fill: {
  type: 'gradient',
  gradient: {
    shade: 'light',
    type: 'horizontal',
    shadeIntensity: 0,
    gradientToColors: ['#fff'],
    inverseColors: false,
    opacityFrom: 0,
    opacityTo: 100,
    stops: [0, 0]
  }
},
stroke: {
  lineCap: 'round'
},
labels: ['Percent'],
};

var chartt = new ApexCharts(document.querySelector("#chartTwo"), options);


// chartt.render();


$(".voteButton").click(function(){

  $(".pollVoteSec").hide();
  $(".pollContent").hide();
  $(".pollVoteSecContainer").css("margin-top" , "-20px")

  $(".pollGraphSec").show();

 

});

setTimeout(chartt.render(), 50000);

setTimeout(chart.render(), 50000);
